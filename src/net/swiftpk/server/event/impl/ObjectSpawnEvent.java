package net.swiftpk.server.event.impl;

import net.swiftpk.server.ent.locs.GameObjectLoc;
import net.swiftpk.server.event.Event;
import net.swiftpk.server.model.entity.GameObject;
import net.swiftpk.server.model.terrain.World;

public class ObjectSpawnEvent extends Event {
    private GameObjectLoc loc;

    public ObjectSpawnEvent(GameObjectLoc loc, int respawnTime) {
        super(respawnTime);
        this.loc = loc;
    }

    @Override
    public void execute() {
        World.getInstance().registerGameObject(new GameObject(loc));
        stop();
    }
}
