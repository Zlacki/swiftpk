package net.swiftpk.server.event;

/**
 * Represents a new event.
 *
 * @author Graham
 */
public abstract class Event {
    /**
     * The delay, in milliseconds.
     */
    private long delay;

    /**
     * Whether or not this event should run as soon as it's submitted
     */
    private boolean immediate;
    /**
     * The running flag.
     */
    private boolean running = true;

    /**
     * Creates an event with the specified delay.
     *
     * @param delay The delay.
     */
    public Event(long delay) {
        this(delay, false);
    }

    public Event(long delay, boolean immediate) {
        this.delay = delay;
        this.immediate = immediate;
    }

    /**
     * The execute method is called when the event is run. The general contract
     * of the execute method is that it may take any action whatsoever.
     */
    public abstract void execute();

    /**
     * Gets the event delay.
     *
     * @return The delay, in milliseconds.
     */
    public long getDelay() {
        return delay;
    }

    /**
     * Checks if the event is running.
     *
     * @return <code>true</code> if the event is still running,
     * <code>false</code> if not.
     */
    public boolean isRunning() {
        return running;
    }

    /**
     * If this event is should be run immediately or not
     *
     * @return <code>true</code> if the event should be run immediately,
     * <code>false</code> if not.
     */
    public boolean isImmediate() {
        return immediate;
    }

    /**
     * Sets the event delay.
     *
     * @param delay The delay to set.
     * @throws IllegalArgumentException if the delay is negative.
     */
    public void setDelay(long delay) {
        if (delay < 0) {
            throw new IllegalArgumentException("Delay must be positive.");
        }
        this.delay = delay;
    }

    /**
     * Stops the event from running in the future.
     */
    public void stop() {
        running = false;
    }
}