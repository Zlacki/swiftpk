package net.swiftpk.server;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import com.almworks.sqlite4java.SQLite;
import net.swiftpk.server.core.GameService;
import net.swiftpk.server.core.LoginRequestService;
import net.swiftpk.server.core.LogoutRequestService;
import net.swiftpk.server.core.RegistrationRequestService;
import net.swiftpk.server.ent.EntityHandler;
import net.swiftpk.server.io.CollisionManager;
import net.swiftpk.server.io.DBWorldLoader;
import net.swiftpk.server.kplugin.PluginBootstrap;
import net.swiftpk.server.model.terrain.World;
import net.swiftpk.server.net.ClassicChannelInitializer;
import net.swiftpk.server.net.PacketRepository;
import net.swiftpk.server.util.AsyncExecutor;
import net.swiftpk.server.util.Config;
import net.swiftpk.server.util.Logger;
import net.swiftpk.server.util.ThreadUtil;
import net.swiftpk.server.util.Tuple;
import net.swiftpk.server.util.parser.PacketRepositoryFileParser;

import com.google.common.util.concurrent.Service;
import com.google.common.util.concurrent.ServiceManager;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.util.ResourceLeakDetector;

/**
 * The entry point for RSC server.
 */
public class Server {
    public static DBWorldLoader getDbWorld() {
        return dbWorld;
    }

    public static Server getServer() {
        return INSTANCE;
    }

    public static void main(String[] args) {
        long start = System.nanoTime();
        SQLite.setLibraryPath(Paths.get(System.getProperty("user.dir")).resolve("lib").resolve("native").toString());
        INSTANCE.start();
        Logger.outf("Started game server in {}ms", (System.nanoTime() - start) / 1e6);
    }

    private static DBWorldLoader dbWorld = new DBWorldLoader();
    private static final Server INSTANCE = new Server();
    public static long START_TIME = System.currentTimeMillis();
    private final World world = World.getInstance();
    private final Config config = new Config();
    private final LoginRequestService loginService = new LoginRequestService(world);
    private final LogoutRequestService logoutService = new LogoutRequestService(world);
    private final GameService gameService = new GameService();
    private final RegistrationRequestService registrationService = new RegistrationRequestService(world);
    private final net.swiftpk.server.kplugin.PluginManager plugins = new net.swiftpk.server.kplugin.PluginManager();
    private final PacketRepository packetRepository = new PacketRepository();

    private Server() {
    }

    public Config getConf() {
        return config;
    }

    private void start() {
        AsyncExecutor executor = new AsyncExecutor(ThreadUtil.AVAILABLE_PROCESSORS, "ServerInitThread");
        executor.execute(EntityHandler::loadEntities);
        executor.execute(CollisionManager::initialize);
        executor.execute(world::propagateGameObjects);
        executor.execute(world::propagateItems);
        executor.execute(world::propagateNPCs);
        executor.execute(world::propagateShops);
        executor.execute(new PacketRepositoryFileParser(packetRepository));
        executor.execute(this::initNetwork);
        executor.execute(this::initPlugins);

        try {
            Logger.outf("Waiting for {} initialization tasks to finish...", executor.size());
            executor.await(true);
        } catch (ExecutionException e) {
            Logger.err("Error initializing server: ", e);
        }

        List<Service> serviceList = new ArrayList<>();
        serviceList.add(gameService);
        serviceList.add(loginService);
        serviceList.add(logoutService);
        serviceList.add(registrationService);
        ServiceManager allServices = new ServiceManager(serviceList);
        allServices.startAsync().awaitHealthy();
        Logger.out("All services now running.");
    }

    public GameService getGameService() {
        return gameService;
    }

    public LoginRequestService getLoginService() {
        return loginService;
    }

    public LogoutRequestService getLogoutService() {
        return logoutService;
    }

    public RegistrationRequestService getRegistrationService() {
        return registrationService;
    }

    public World getWorld() {
        return world;
    }

    public net.swiftpk.server.kplugin.PluginManager getPlugins() {
        return plugins;
    }

    private void initPlugins() {
        Logger.outf("{} Kotlin plugins have been loaded into memory.", PluginBootstrap.init());
    }

    private void initNetwork() {
        ResourceLeakDetector.setLevel(ResourceLeakDetector.Level.PARANOID);

        ServerBootstrap bootstrap = new ServerBootstrap();

        bootstrap.group(new NioEventLoopGroup());
        bootstrap.channel(NioServerSocketChannel.class);
        bootstrap.childHandler(new ClassicChannelInitializer());
        bootstrap.bind(new InetSocketAddress(Server.getServer().getConf().getInt("port"))).syncUninterruptibly();
    }

    public PacketRepository getPacketRepository() {
        return packetRepository;
    }
}
