package net.swiftpk.server.ent;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.almworks.sqlite4java.SQLiteConnection;
import com.almworks.sqlite4java.SQLiteException;
import com.almworks.sqlite4java.SQLiteJob;

import net.swiftpk.server.Server;
import net.swiftpk.server.ent.defs.BoundaryDef;
import net.swiftpk.server.ent.defs.GameObjectDef;
import net.swiftpk.server.ent.defs.ItemDef;
import net.swiftpk.server.ent.defs.NPCDef;
import net.swiftpk.server.ent.defs.PrayerDef;
import net.swiftpk.server.ent.defs.SpellDef;
import net.swiftpk.server.ent.defs.TileDef;
import net.swiftpk.server.ent.defs.extras.ItemWieldableDef;
import net.swiftpk.server.ent.defs.extras.NPCDrop;
import net.swiftpk.server.io.DBConnection;
import net.swiftpk.server.model.terrain.tiles.Point;

/**
 * This class handles the loading of entities from the conf files, and provides
 * methods for relaying these entities to the user.
 */
public class EntityHandler {
    private static int[] buildIntArray(List<Integer> integers) {
        int[] ints = new int[integers.size()];
        int i = 0;
        for (Integer n : integers) {
            ints[i++] = n;
        }
        return ints;
    }

    /**
     * @param id the entities ID
     * @return the BoundaryDef with the given ID
     */
    public static BoundaryDef getBoundaryDef(int id) {
        if (id < 0 || id > boundaries.length) {
            return null;
        }
        return boundaries[id];
    }

    /**
     * @param id the entities ID
     * @return the GameObjectDef with the given ID
     */
    public static GameObjectDef getGameObjectDef(int id) {
        if (id < 0 || id > gameObjects.length) {
            return null;
        }
        return gameObjects[id];
    }

    /**
     * @param the items type
     * @return the types of items affected
     */
    public static int[] getItemAffectedTypes(int type) {
        return itemAffectedTypes.get(type);
    }

    /**
     * @param id the entities ID
     * @return the ItemDef with the given ID
     */
    public static ItemDef getItemDef(int id) {
        if (id < 0 || id > items.length) {
            return null;
        }
        return items[id];
    }

    public static int getItemDefID(ItemDef id) {
        for (int i = 0; i < items.length; i++)
            if (id == items[i])
                return i;
        return -1;
    }

    public static List<ItemDef> getItemDefList() {
        List<ItemDef> itemls = new ArrayList<ItemDef>();
        for (ItemDef itd : items)
            itemls.add(itd);
        return itemls;
    }

    /**
     * @param the items id
     * @return the amount eating the item should heal
     */
    public static int getItemEdibleHeals(int id) {
        Integer heals = itemEdibleHeals.get(id);
        if (heals != null) {
            return heals.intValue();
        }
        return 0;
    }

    /**
     * @param id the entities ID
     * @return the ItemWieldableDef with the given ID
     */
    public static ItemWieldableDef getItemWieldableDef(int id) {
        return itemWieldable.get(id);
    }

    /**
     * @param id the entities ID
     * @return the NPCDef with the given ID
     */
    public static NPCDef getNPCDef(int id) {
        if (id < 0 || id > npcs.length) {
            return null;
        }
        return npcs[id];
    }

    /**
     * @param the point we are currently at
     * @return the point we should be teleported to
     */
    public static Point getObjectTelePoint(Point location) {
        return objectTelePoints.get(location);
    }

    /**
     * @param id the entities ID
     * @return the PrayerDef with the given ID
     */
    public static PrayerDef getPrayerDef(int id) {
        if (id < 0 || id > prayers.length) {
            return null;
        }
        return prayers[id];
    }

    /**
     * @param the spells id
     * @return the lvl of the spell (for calculating what it hits)
     */
    public static int getSpellAggressiveLvl(int id) {
        Integer lvl = spellAggressiveLvl.get(id);
        if (lvl != null) {
            return lvl.intValue();
        }
        return 0;
    }

    /**
     * @param id the entities ID
     * @return the SpellDef with the given ID
     */
    public static SpellDef getSpellDef(int id) {
        if (id < 0 || id > spells.length) {
            return null;
        }
        return spells[id];
    }

    /**
     * @param id the entities ID
     * @return the TileDef with the given ID
     */
    public static TileDef getTileDef(int id) {
        if (id < 0 || id >= tiles.length) {
            return null;
        }
        return tiles[id];
    }

    public static void loadEntities() {
        npcs = null;
        spells = null;
        prayers = null;
        items = null;
        gameObjects = null;
        try {
            DBConnection.getWorldQueue().execute(new SQLiteJob<Boolean>() {
                @Override
                protected Boolean job(SQLiteConnection conn)
                        throws SQLiteException {
                    if (!Server.getDbWorld().checkStatements(
                            Server.getDbWorld().boundary_def,
                            Server.getDbWorld().game_object,
                            Server.getDbWorld().npc,
                            Server.getDbWorld().npc_drop,
                            Server.getDbWorld().prayers,
                            Server.getDbWorld().items,
                            Server.getDbWorld().spells,
                            Server.getDbWorld().tiles,
                            Server.getDbWorld().itemaffectedtypes,
                            Server.getDbWorld().finditemaffectedtypes,
                            Server.getDbWorld().item_wieldable,
                            Server.getDbWorld().item_wieldable_req,
                            Server.getDbWorld().item_edible,
                            Server.getDbWorld().spell_agr,
                            Server.getDbWorld().tele_point))
                        Server.getDbWorld().loadStatements(conn);
                    ArrayList<BoundaryDef> boundaryArray = new ArrayList<BoundaryDef>();
                    while (Server.getDbWorld().boundary_def.step()) {
                        BoundaryDef boundary = new BoundaryDef();
                        boundary.id = Server.getDbWorld().boundary_def.columnInt(0) - 1;
                        boundary.name = Server.getDbWorld().boundary_def
                                .columnString(1);
                        boundary.description = Server.getDbWorld().boundary_def
                                .columnString(2);
                        boundary.command1 = Server.getDbWorld().boundary_def
                                .columnString(3);
                        boundary.command2 = Server.getDbWorld().boundary_def
                                .columnString(4);
                        boundary.modelVar1 = Server.getDbWorld().boundary_def
                                .columnInt(5);
                        boundary.modelVar2 = Server.getDbWorld().boundary_def
                                .columnInt(6);
                        boundary.modelVar3 = Server.getDbWorld().boundary_def
                                .columnInt(7);
                        boundary.boundaryType = Server.getDbWorld().boundary_def
                                .columnInt(8);
                        boundary.unknown = Server.getDbWorld().boundary_def
                                .columnInt(9);
                        boundaryArray.add(boundary);
                    }
                    Server.getDbWorld().boundary_def.reset();
                    boundaries = boundaryArray.toArray(new BoundaryDef[]{});
                    ArrayList<GameObjectDef> objectsArray = new ArrayList<GameObjectDef>();
                    while (Server.getDbWorld().game_object.step()) {
                        GameObjectDef obj = new GameObjectDef();
                        obj.id = Server.getDbWorld().game_object.columnInt(0) - 1;
                        obj.name = Server.getDbWorld().game_object
                                .columnString(1);
                        obj.description = Server.getDbWorld().game_object
                                .columnString(2);
                        obj.command1 = Server.getDbWorld().game_object
                                .columnString(3);
                        obj.command2 = Server.getDbWorld().game_object
                                .columnString(4);
                        obj.type = Server.getDbWorld().game_object.columnInt(5);
                        obj.width = Server.getDbWorld().game_object
                                .columnInt(6);
                        obj.height = Server.getDbWorld().game_object
                                .columnInt(7);
                        obj.groundItemVar = Server.getDbWorld().game_object
                                .columnInt(8);
                        objectsArray.add(obj);
                    }
                    Server.getDbWorld().game_object.reset();
                    gameObjects = objectsArray.toArray(new GameObjectDef[]{});
                    ArrayList<NPCDef> npcsArray = new ArrayList<NPCDef>();
                    while (Server.getDbWorld().npc.step()) {
                        NPCDef npcs = new NPCDef();
                        npcs.id = Server.getDbWorld().npc.columnInt(0);
                        npcs.name = Server.getDbWorld().npc.columnString(1);
                        npcs.description = Server.getDbWorld().npc
                                .columnString(2);
                        npcs.command = Server.getDbWorld().npc.columnString(3);
                        npcs.hits = Server.getDbWorld().npc.columnInt(4);
                        npcs.att = Server.getDbWorld().npc.columnInt(5);
                        npcs.str = Server.getDbWorld().npc.columnInt(6);
                        npcs.def = Server.getDbWorld().npc.columnInt(7);
                        npcs.attackable = Server.getDbWorld().npc.columnInt(8) == 1;
                        float cumProb = 0;
                        ArrayList<NPCDrop> drops = new ArrayList<NPCDrop>();
                        Server.getDbWorld().npc_drop.bind(1, npcs.getId());
                        while (Server.getDbWorld().npc_drop.step()) {
                            NPCDrop drop = new NPCDrop();
                            drop.setItemId(Server.getDbWorld().npc_drop
                                    .columnInt(0));
                            drop.setAmounts(Server.getDbWorld().npc_drop
                                    .columnInt(1), Server.getDbWorld().npc_drop
                                    .columnInt(2));
                            drop.setProbability(Server.getDbWorld().npc_drop
                                    .columnDouble(3));
                            cumProb += drop.getProbability();
                            drop.setCumulativeProbability(cumProb);
                            drops.add(drop);
                        }
                        Server.getDbWorld().npc_drop.reset();
                        npcs.setDrop(drops.toArray(new NPCDrop[]{}));
                        npcsArray.add(npcs);
                    }
                    Server.getDbWorld().npc.reset();
                    npcs = npcsArray.toArray(new NPCDef[]{});
                    ArrayList<PrayerDef> prayersArray = new ArrayList<PrayerDef>();
                    while (Server.getDbWorld().prayers.step()) {
                        PrayerDef prayer = new PrayerDef();
                        prayer.id = Server.getDbWorld().prayers.columnInt(0) - 1;
                        prayer.name = Server.getDbWorld().prayers
                                .columnString(1);
                        prayer.description = Server.getDbWorld().prayers
                                .columnString(2);
                        prayer.reqLevel = Server.getDbWorld().prayers
                                .columnInt(3);
                        prayer.drainRate = Server.getDbWorld().prayers
                                .columnInt(4);
                        prayersArray.add(prayer);
                    }
                    Server.getDbWorld().prayers.reset();
                    prayers = prayersArray.toArray(new PrayerDef[]{});
                    ArrayList<ItemDef> itemsArray = new ArrayList<ItemDef>();
                    while (Server.getDbWorld().items.step()) {
                        ItemDef item = new ItemDef();
                        item.id = Server.getDbWorld().items.columnInt(0);
                        item.name = Server.getDbWorld().items.columnString(1);
                        item.description = Server.getDbWorld().items
                                .columnString(2);
                        item.command = Server.getDbWorld().items
                                .columnString(3);
                        item.basePrice = Server.getDbWorld().items.columnInt(4);
                        item.stackable = Server.getDbWorld().items.columnInt(5) == 1;
                        item.special = Server.getDbWorld().items.columnInt(6);
                        item.members = Server.getDbWorld().items.columnInt(7) == 1;
                        itemsArray.add(item);
                    }
                    Server.getDbWorld().items.reset();
                    items = itemsArray.toArray(new ItemDef[]{});
                    ArrayList<SpellDef> spellsArray = new ArrayList<SpellDef>();
                    while (Server.getDbWorld().spells.step()) {
                        SpellDef spell = new SpellDef();
                        spell.id = Server.getDbWorld().spells.columnInt(0);
                        spell.name = Server.getDbWorld().spells.columnString(1);
                        spell.description = Server.getDbWorld().spells
                                .columnString(2);
                        spell.reqLevel = Server.getDbWorld().spells
                                .columnInt(3);
                        spell.runeCount = Server.getDbWorld().spells
                                .columnInt(4);
                        spell.type = Server.getDbWorld().spells.columnInt(5);
                        spell.exp = Server.getDbWorld().spells.columnInt(6);
                        HashMap<Integer, Integer> runes = new HashMap<Integer, Integer>();
                        Server.getDbWorld().spell_runes.bind(1, spell.getId());
                        while (Server.getDbWorld().spell_runes.step())
                            runes.put(Server.getDbWorld().spell_runes
                                            .columnInt(0),
                                    Server.getDbWorld().spell_runes
                                            .columnInt(1));
                        Server.getDbWorld().spell_runes.reset();
                        spell.requiredRunes = runes;
                        spellsArray.add(spell);
                    }
                    Server.getDbWorld().spells.reset();
                    spells = spellsArray.toArray(new SpellDef[]{});
                    ArrayList<TileDef> tilesArray = new ArrayList<TileDef>();
                    while (Server.getDbWorld().tiles.step()) {
                        TileDef tile = new TileDef();
                        tile.colour = Server.getDbWorld().tiles.columnInt(0);
                        tile.unknown = Server.getDbWorld().tiles.columnInt(1);
                        tile.objectType = Server.getDbWorld().tiles
                                .columnInt(2);
                        tilesArray.add(tile);
                    }
                    Server.getDbWorld().tiles.reset();
                    tiles = tilesArray.toArray(new TileDef[]{});
                    HashMap<Integer, int[]> testAffectedTypes = new HashMap<Integer, int[]>();
                    while (Server.getDbWorld().itemaffectedtypes.step()) {
                        if (testAffectedTypes
                                .get(Server.getDbWorld().itemaffectedtypes
                                        .columnInt(0)) == null) {
                            ArrayList<Integer> items = new ArrayList<Integer>();
                            Server.getDbWorld().finditemaffectedtypes.bind(1,
                                    Server.getDbWorld().itemaffectedtypes
                                            .columnInt(0));
                            while (Server.getDbWorld().finditemaffectedtypes
                                    .step())
                                items.add(Server.getDbWorld().finditemaffectedtypes
                                        .columnInt(0));
                            Server.getDbWorld().finditemaffectedtypes.reset();
                            testAffectedTypes
                                    .put(Server.getDbWorld().itemaffectedtypes
                                            .columnInt(0), buildIntArray(items));
                        }
                    }
                    Server.getDbWorld().itemaffectedtypes.reset();
                    itemAffectedTypes = testAffectedTypes;
                    HashMap<Integer, ItemWieldableDef> itemWieldables = new HashMap<Integer, ItemWieldableDef>();
                    while (Server.getDbWorld().item_wieldable.step()) {
                        int id = Server.getDbWorld().item_wieldable
                                .columnInt(0);
                        ItemWieldableDef wieldable = new ItemWieldableDef();
                        wieldable.sprite = Server.getDbWorld().item_wieldable
                                .columnInt(1);
                        wieldable.type = Server.getDbWorld().item_wieldable
                                .columnInt(2);
                        wieldable.armourPoints = Server.getDbWorld().item_wieldable
                                .columnInt(3);
                        wieldable.magicPoints = Server.getDbWorld().item_wieldable
                                .columnInt(4);
                        wieldable.prayerPoints = Server.getDbWorld().item_wieldable
                                .columnInt(5);
                        wieldable.rangePoints = Server.getDbWorld().item_wieldable
                                .columnInt(6);
                        wieldable.weaponAimPoints = Server.getDbWorld().item_wieldable
                                .columnInt(7);
                        wieldable.weaponPowerPoints = Server.getDbWorld().item_wieldable
                                .columnInt(8);
                        wieldable.wieldPos = Server.getDbWorld().item_wieldable
                                .columnInt(9);
                        wieldable.femaleOnly = Server.getDbWorld().item_wieldable
                                .columnInt(10) == 1;
                        HashMap<Integer, Integer> itemWieldablesReq = new HashMap<Integer, Integer>();
                        Server.getDbWorld().item_wieldable_req.bind(1, id);
                        while (Server.getDbWorld().item_wieldable_req.step())
                            itemWieldablesReq.put(
                                    Server.getDbWorld().item_wieldable_req
                                            .columnInt(0),
                                    Server.getDbWorld().item_wieldable_req
                                            .columnInt(1));
                        Server.getDbWorld().item_wieldable_req.reset();
                        wieldable.requiredStats = itemWieldablesReq;
                        itemWieldables.put(id, wieldable);
                    }
                    Server.getDbWorld().item_wieldable.reset();
                    itemWieldable = itemWieldables;
                    HashMap<Integer, Integer> itemEdible = new HashMap<Integer, Integer>();
                    while (Server.getDbWorld().item_edible.step())
                        itemEdible.put(Server.getDbWorld().item_edible
                                .columnInt(0), Server.getDbWorld().item_edible
                                .columnInt(1));
                    Server.getDbWorld().item_edible.reset();
                    itemEdibleHeals = itemEdible;
                    HashMap<Integer, Integer> spellagg = new HashMap<Integer, Integer>();
                    while (Server.getDbWorld().spell_agr.step())
                        spellagg.put(
                                Server.getDbWorld().spell_agr.columnInt(0),
                                Server.getDbWorld().spell_agr.columnInt(1));
                    Server.getDbWorld().spell_agr.reset();
                    spellAggressiveLvl = spellagg;
                    HashMap<Point, Point> test = new HashMap<Point, Point>();
                    while (Server.getDbWorld().tele_point.step()) {
                        Point point = new Point(Server.getDbWorld().tele_point
                                .columnInt(0), Server.getDbWorld().tele_point
                                .columnInt(1), 0);
                        Point gotoPoint = new Point(
                                Server.getDbWorld().tele_point.columnInt(2),
                                Server.getDbWorld().tele_point.columnInt(3), 0);
                        test.put(point, gotoPoint);
                    }
                    Server.getDbWorld().tele_point.reset();
                    objectTelePoints = test;
                    return true;
                }
            }).complete();
        } catch (SQLiteException e1) {
            e1.printStackTrace();
        }
    }

    // timer
    private static BoundaryDef[] boundaries;
    public static GameObjectDef[] gameObjects;
    private static HashMap<Integer, int[]> itemAffectedTypes;
    private static HashMap<Integer, Integer> itemEdibleHeals;
    private static ItemDef[] items;
    private static HashMap<Integer, ItemWieldableDef> itemWieldable;
    private static NPCDef[] npcs;
    private static HashMap<Point, Point> objectTelePoints;
    public static PrayerDef[] prayers;
    private static HashMap<Integer, Integer> spellAggressiveLvl;
    private static SpellDef[] spells;
    private static TileDef[] tiles;
}
