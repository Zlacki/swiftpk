package net.swiftpk.server.ent.locs;

import net.swiftpk.server.model.terrain.tiles.Point;

public class NPCLoc {
    public boolean aggressive;
    public int id;
    public int npcCount;
    public int respawnTime;
    public int maxX;
    public int maxY;
    public int minX;
    public int minY;

    public NPCLoc() {

    }

    public NPCLoc(int id, int npcCount, int respawnTime, int maxX, int maxY, int minX, int minY, boolean aggressive) {
        this.id = id;
        this.npcCount = npcCount;
        this.respawnTime = respawnTime;
        this.maxX = maxX;
        this.maxY = maxY;
        this.minX = minX;
        this.minY = minY;
        this.aggressive = aggressive;
    }

    public int getId() {
        return id;
    }

    public int getNPCCount() {
        return npcCount;
    }

    public int getRespawnTime() {
        return respawnTime;
    }

    public boolean isAggressive() {
        return aggressive;
    }

    public boolean withinRoamArea(Point p) {
        return p.getX() >= minX && p.getX() <= maxY && p.getY() >= minY
                && p.getY() <= maxY;
    }
}
