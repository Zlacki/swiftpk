package net.swiftpk.server.ent.defs.extras;

public class NPCDrop implements Comparable<NPCDrop> {
    private int itemid;
    private double probability;
    private transient double cumulativeProbability;
    private int maxAmount;
    private int minAmount;

    public void setItemId(int i) {
        itemid = i;
    }

    public int getItemId() {
        return itemid;
    }

    public void setProbability(double d) {
        probability = d;
    }

    public double getProbability() {
        return probability;
    }

    public double getCumulativeProbability() {
        return cumulativeProbability;
    }

    public void setCumulativeProbability(float f) {
        cumulativeProbability = f;
    }

    public void setAmounts(int min, int max) {
        minAmount = min;
        maxAmount = max;
    }

    public int getMinAmount() {
        return minAmount;
    }

    public int getMaxAmount() {
        return maxAmount;
    }

    @SuppressWarnings("deprecation")
    public int compareTo(NPCDrop npcd) {
        return -(new Double(probability).compareTo(probability));
    }

    public String toString() {
        return "id=" + itemid + ";prob=" + probability + ";cumulative="
                + cumulativeProbability;
    }
}
