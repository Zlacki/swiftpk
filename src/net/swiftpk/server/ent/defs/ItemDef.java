package net.swiftpk.server.ent.defs;

/**
 * The definition wrapper for items
 */
public class ItemDef extends EntityDef {
    /**
     * The base price of the object
     */
    public int basePrice;
    /**
     * The command of the object
     */
    public String command;
    /**
     * Whether the item is members only
     */
    public boolean members;
    /**
     * If the item is a quest item, special = 1
     */
    public int special;
    /**
     * Whether the item is stackable or not
     */
    public boolean stackable;

    public int getBasePrice() {
        return basePrice;
    }

    public String getCommand() {
        return command;
    }

    public int getSpecial() {
        return special;
    }

    public boolean isMembers() {
        return members;
    }

    public boolean isStackable() {
        return stackable;
    }
}
