package net.swiftpk.server.ent.defs;

import net.swiftpk.server.ent.defs.extras.NPCDrop;

/**
 * The definition wrapper for npcs
 */
public class NPCDef extends EntityDef {
    /**
     * The attack lvl
     */
    public int att;
    /**
     * Whether the npc is attackable
     */
    public boolean attackable;
    /**
     * The command of the npc
     */
    public String command;
    /**
     * The def lvl
     */
    public int def;

    private NPCDrop[] drop;
    /**
     * The hit points
     */
    public int hits;
    /**
     * The strength lvl
     */
    public int str;

    public int getAtt() {
        return att;
    }

    public String getCommand() {
        return command;
    }

    public int getDef() {
        return def;
    }

    public NPCDrop[] getDrop() {
        return drop;
    }

    public int getHits() {
        return hits;
    }

    public int[] getStats() {
        return new int[]{att, def, str, hits};
    }

    public int getStr() {
        return str;
    }

    public boolean isAttackable() {
        return attackable;
    }

    public void setDrop(NPCDrop[] npcd) {
        drop = npcd;
    }
}
