package net.swiftpk.server.ent.defs;

/**
 * The definition wrapper for boundaries
 */
public class BoundaryDef extends EntityDef {
    /**
     * The first command of the boundary
     */
    public String command1;
    /**
     * The second command of the boundary
     */
    public String command2;
    /**
     * The boundaries type.
     */
    public int boundaryType;
    public int modelVar1;
    public int modelVar2;
    public int modelVar3;
    /**
     * Unknown
     */
    public int unknown;

    public String getCommand1() {
        return command1.toLowerCase();
    }

    public String getCommand2() {
        return command2.toLowerCase();
    }

    public int getBoundaryType() {
        return boundaryType;
    }

    public int getModelVar1() {
        return modelVar1;
    }

    public int getModelVar2() {
        return modelVar2;
    }

    public int getModelVar3() {
        return modelVar3;
    }

    public int getUnknown() {
        return unknown;
    }
}
