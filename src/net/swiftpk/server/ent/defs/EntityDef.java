package net.swiftpk.server.ent.defs;

/**
 * The abstract class EntityDef implements methods for return values which are
 * shared between entities.
 */
public abstract class EntityDef {
    /**
     * The description of the entity
     */
    public String description;
    /**
     * The entities ID
     */
    public int id;
    /**
     * The name of the entity
     */
    public String name;

    /**
     * Returns the description of the entity
     *
     * @return the description of the entity
     */
    public String getDescription() {
        return description;
    }

    public int getId() {
        return id;
    }

    /**
     * Returns the name of the entity
     *
     * @return the name of the entity
     */
    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return name;
    }
}
