package net.swiftpk.server.io;

import com.almworks.sqlite4java.SQLiteConnection;
import com.almworks.sqlite4java.SQLiteException;
import com.almworks.sqlite4java.SQLiteJob;
import com.almworks.sqlite4java.SQLiteStatement;

public class DBWorldLoader {
    public SQLiteStatement boundary_def;
    public SQLiteStatement finditemaffectedtypes;
    public SQLiteStatement game_object;
    public SQLiteStatement item_edible;
    public SQLiteStatement item_location;
    public SQLiteStatement item_wieldable;
    public SQLiteStatement item_wieldable_req;
    public SQLiteStatement itemaffectedtypes;
    public SQLiteStatement items;
    public SQLiteStatement npc;
    public SQLiteStatement npc_drop;
    public SQLiteStatement npc_location;
    public SQLiteStatement object_location, saveObjects, clearObjects;
    public SQLiteStatement packets;
    public SQLiteStatement player_log;
    public SQLiteStatement prayers;
    public SQLiteStatement server_settings;
    public SQLiteStatement spell_agr;
    public SQLiteStatement spell_runes;
    public SQLiteStatement spells;
    public SQLiteStatement tele_point;
    public SQLiteStatement tiles;
    public SQLiteStatement shops, shopItems;

    public DBWorldLoader() {
    }

    public boolean checkStatements(SQLiteStatement... stmnts) {
        for (SQLiteStatement s : stmnts) {
            if (s == null || s.isDisposed())
                return false;
        }
        return true;
    }

    public void loadStatements(SQLiteConnection conn) throws SQLiteException {
        tiles = conn
                .prepare("SELECT `colour`, `unknown`, `objectType` FROM `tiles`");
        npc_drop = conn
                .prepare("SELECT `itemID`, `minAmount`, `maxAmount`, `probability` FROM `npc_drops` WHERE `npcID`=?");
        boundary_def = conn
                .prepare("SELECT `id`, `name`, `description`, `command_one`, `command_two`, `model_var_one`, "
                        + "`model_var_two`, `model_var_three`, `door_type`, `unknown` FROM `doors`");
        game_object = conn
                .prepare("SELECT `id`, `name`, `description`, `command_one`, `command_two`, `type`, "
                        + "`width`, `height`, `ground_item_var` FROM `game_objects`");
        npc = conn
                .prepare("SELECT `id`, `name`, `description`, `command`, `hits`, `attack`, `strength`, `defense`, `attackable` FROM `npcs`");
        prayers = conn
                .prepare("SELECT `id`, `name`, `description`, `required_level`, `drain_rate` FROM `prayers`");
        items = conn
                .prepare("SELECT `id`, `name`, `description`, `command`, `base_price`, `stackable`, `special`, `members` FROM `items`");
        spells = conn
                .prepare("SELECT `id`, `name`, `description`, `required_level`, `rune_amount`, `type`, `experience`  FROM `spells`");
        spell_runes = conn
                .prepare("SELECT `itemID`, `amount` FROM `spell_runes` WHERE `spellID`=?");
        itemaffectedtypes = conn
                .prepare("SELECT `id` FROM `itemaffectedtypes`");
        finditemaffectedtypes = conn
                .prepare("SELECT `type` FROM `itemaffectedtypes` WHERE `id`=?");
        item_wieldable = conn
                .prepare("SELECT `id`, `sprite`, `type`, `armour_points`, `magic_points`, `prayer_points`, `range_points`, "
                        + "`weapon_aim_points`, `weapon_power_points`, `pos`, `femaleOnly` FROM `item_wieldable`");
        item_wieldable_req = conn
                .prepare("SELECT `skillIndex`, `level` FROM `item_wieldable_requirements` WHERE `id`=?");
        item_edible = conn.prepare("SELECT `id`, `heal` FROM `item_edible`");
        spell_agr = conn
                .prepare("SELECT `id`, `spell` FROM `spell_aggressive_level`");
        tele_point = conn
                .prepare("SELECT `pointX`, `pointY`, `x`, `y` FROM `object_tele_points`");
        object_location = conn
                .prepare("SELECT `id`, `direction`, `type`, `x`, `y` FROM `game_object_locations`");
        saveObjects = conn
                .prepare("INSERT INTO `game_object_locations` VALUES(?, ?, ?, ?, ?);");
        clearObjects = conn
                .prepare("DELETE FROM `game_object_locations`");
        item_location = conn
                .prepare("SELECT `id`, `x`, `y`, `amount`, `respawnTime` FROM `item_locations`");
        npc_location = conn
                .prepare("SELECT `amount`, `id`, `aggressive`, `respawn`, `minX`, `minY`, `maxX`, `maxY` FROM `npc_locations`");
        shops = conn.prepare("SELECT `id`, `name`, `general` FROM `shops`");
        shopItems = conn
                .prepare("SELECT `amount`, `itemID` FROM `shop_items` WHERE `storeID`=?");
        /*
         * server_settings = c
         * .prepareStatement("SELECT * FROM mybb_server_settings WHERE setting=?"
         * );
         */
    }

    public void updateGameObjects() throws SQLiteException {
        DBConnection.getWorldQueue().execute(new SQLiteJob<Void>() {
            @Override
            protected Void job(SQLiteConnection conn) throws SQLiteException {
                if (!checkStatements(saveObjects, clearObjects))
                    loadStatements(conn);
/*				conn.exec("BEGIN TRANSACTION");
				clearObjects.step();
				clearObjects.reset();
				int savedCount = 0;
				for(GameObject object : World.getInstance().getGameObjects()) {
					saveObjects.bind(1, object.getID());
					saveObjects.bind(2, object.getDirection());
					saveObjects.bind(3, object.getType());
					saveObjects.bind(4, object.getX());
					saveObjects.bind(5, object.getY());
					saveObjects.step();
					saveObjects.reset();
					savedCount++;
				}
				System.out.println("Successfully saved " + savedCount + " objects to database.");
				conn.exec("END TRANSACTION");*/
                return null;
            }
        }).complete();
    }
}
