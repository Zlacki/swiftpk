package net.swiftpk.server.io;

import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import net.swiftpk.server.ent.EntityHandler;
import net.swiftpk.server.model.Sector;
import net.swiftpk.server.model.terrain.World;
import net.swiftpk.server.util.DataConversions;
import net.swiftpk.server.util.Logger;

import com.jagex.cache.Archive;
import com.jagex.cache.various.Utils;

public class CollisionManager {
    static {
        try {
            ARCHIVE = new Archive(Utils.readFile(Paths
                    .get(System.getProperty("user.dir") + File.separator + "data" + File.separator + "landscape.jag")
                    .toFile()));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static Archive ARCHIVE;
    private static final List<LandscapeSector> SECTORS = new ArrayList<>();
    private static final World WORLD = World.getInstance();

    private CollisionManager() {
    }

    public static void initialize() {
        long start = System.currentTimeMillis();
        try {
            for (int height = 0; height < 4; height++) {
                int wildX = 2304;
                int wildY = 1776 - (height * 944);
                for (int sectionX = 0; sectionX < 944; sectionX += 48) {
                    for (int sectionY = 0; sectionY < 944; sectionY += 48) {
                        int x = (sectionX + wildX) / 48;
                        int y = (sectionY + (height * 944) + wildY) / 48;
                        ByteBuffer area = DataConversions.streamToBuffer(com.jagex.cache.various.Utils
                                .gzDecompress(ARCHIVE.getFile(Archive.getHash("h" + height + "x" + x + "y" + y))));
                        Sector s = Sector.unpack(area);
                        int count = 0;
                        for (int i = 0; i < 2304; i++) {
                            if ((s.getTile(i).groundTexture & 0xff) == 0) {
                                count++;
                            }
                        }
                        if (count != 2304) {
                            LandscapeSector landscapeSector = new LandscapeSector();
                            landscapeSector.x = sectionX;
                            landscapeSector.y = sectionY + (height * 944);
                            landscapeSector.sector = s;
                            SECTORS.add(landscapeSector);
                        }
                    }
                }
            }
        } catch (IOException e) {
            Logger.err("Exception thrown while loading clipping data:", e);
        }
        long elapsed = System.currentTimeMillis() - start;
        Logger.out("Loaded " + SECTORS.size() + " landscape regions in " + elapsed + "ms");
        if (SECTORS.size() > 0)
            loadSectors();
    }

    private static void loadSectors() {
        for (LandscapeSector sector : SECTORS) {
            Sector s = sector.sector;
            for (int y = 0; y < Sector.HEIGHT; y++) {
                for (int x = 0; x < Sector.WIDTH; x++) {
                    int bx = sector.x + x;
                    int by = sector.y + y;
                    if (!WORLD.withinWorld(bx, by)) {
                        continue;
                    }
                    if ((s.getTile(x, y).groundOverlay & 0xff) == 250) {
                        /**
                         * ?? Forgot what this is for?
                         */
                        s.getTile(x, y).groundOverlay = (byte) 2;
                    }
                    WORLD.getTileValue(bx, by).overlay = s.getTile(x, y).groundOverlay;
                    WORLD.getTileValue(bx, by).diagWallVal = s.getTile(x, y).diagonalWalls;
                    WORLD.getTileValue(bx, by).horizontalWallVal = s.getTile(x, y).horizontalWall;
                    WORLD.getTileValue(bx, by).verticalWallVal = s.getTile(x, y).verticalWall;
                    WORLD.getTileValue(bx, by).elevation = s.getTile(x, y).groundElevation;

                    int groundOverlay = s.getTile(x, y).groundOverlay & 0xFF;
                    if (groundOverlay > 0 && EntityHandler.getTileDef(groundOverlay - 1).getObjectType() != 0) {
                        WORLD.getTileValue(bx, by).traversalMask |= 0x40; // 64
                    }
                    int verticalWall = s.getTile(x, y).verticalWall & 0xFF;
                    if (verticalWall > 0 && EntityHandler.getBoundaryDef(verticalWall - 1).getUnknown() == 0
                            && EntityHandler.getBoundaryDef(verticalWall - 1).getBoundaryType() != 0) {
                        WORLD.getTileValue(bx, by).traversalMask |= 1; // 1
                        WORLD.getTileValue(bx, by - 1).traversalMask |= 4; // 4
                    }
                    int horizontalWall = s.getTile(x, y).horizontalWall & 0xFF;
                    if (horizontalWall > 0 && EntityHandler.getBoundaryDef(horizontalWall - 1).getUnknown() == 0
                            && EntityHandler.getBoundaryDef(horizontalWall - 1).getBoundaryType() != 0) {
                        WORLD.getTileValue(bx, by).traversalMask |= 2; // 2
                        WORLD.getTileValue(bx - 1, by).traversalMask |= 8; // 8
                    }
                    int diagonalWalls = s.getTile(x, y).diagonalWalls;
                    if (diagonalWalls > 0 && diagonalWalls < 12000
                            && EntityHandler.getBoundaryDef(diagonalWalls - 1).getUnknown() == 0
                            && EntityHandler.getBoundaryDef(diagonalWalls - 1).getBoundaryType() != 0) {
                        WORLD.getTileValue(bx, by).traversalMask |= 0x20; // 32
                    }
                    if (diagonalWalls > 12000 && diagonalWalls < 24000
                            && EntityHandler.getBoundaryDef(diagonalWalls - 12001).getUnknown() == 0
                            && EntityHandler.getBoundaryDef(diagonalWalls - 12001).getBoundaryType() != 0) {
                        WORLD.getTileValue(bx, by).traversalMask |= 0x10; // 16
                    }
                }
            }
        }
    }

    private static class LandscapeSector {
        public int x, y;
        public Sector sector;
    }
}
