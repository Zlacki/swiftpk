package net.swiftpk.server.io;

import java.io.File;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.almworks.sqlite4java.SQLiteException;
import com.almworks.sqlite4java.SQLiteQueue;

public class DBConnection {
    private static SQLiteQueue playerQueue;
    private static SQLiteQueue worldQueue;

    public static SQLiteQueue getPlayerQueue() {
        if (playerQueue == null || playerQueue.isStopped()) {
            playerQueue = new SQLiteQueue(new File(System
                    .getProperty("user.dir")
                    + File.separator + "data" + File.separator + "players.db"));
            playerQueue.start();
        }
        return playerQueue;
    }

    public static SQLiteQueue getWorldQueue() throws SQLiteException {
        if (worldQueue == null || worldQueue.isStopped()) {
            worldQueue = new SQLiteQueue(new File(System
                    .getProperty("user.dir")
                    + File.separator + "data" + File.separator + "world.db"));
            worldQueue.start();
        }
        return worldQueue;
    }

    static {
        Logger.getLogger("com.almworks.sqlite4java").setLevel(Level.OFF);
    }
}
