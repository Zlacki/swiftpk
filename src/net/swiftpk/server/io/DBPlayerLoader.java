package net.swiftpk.server.io;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Queue;

import com.almworks.sqlite4java.SQLiteConnection;
import com.almworks.sqlite4java.SQLiteException;
import com.almworks.sqlite4java.SQLiteJob;
import com.almworks.sqlite4java.SQLiteStatement;

import net.swiftpk.server.Server;
import net.swiftpk.server.model.InvItem;
import net.swiftpk.server.model.Player;
import net.swiftpk.server.model.component.Bank;
import net.swiftpk.server.model.component.Communication;
import net.swiftpk.server.model.component.Settings;
import net.swiftpk.server.model.component.SkillSet.Skill;
import net.swiftpk.server.model.entity.attr.AttributeValue;
import net.swiftpk.server.model.terrain.World;
import net.swiftpk.server.model.terrain.tiles.Point;
import net.swiftpk.server.util.DataConversions;
import net.swiftpk.server.util.Formulae;
import net.swiftpk.server.util.Logger;

public class DBPlayerLoader implements PlayerLoader {
    static class BadPassword {
        int attempt = 0;
        long nextAttempt, lastAttempt;
    }

    private static boolean checkStatements(SQLiteStatement... stmnts) {
        for (SQLiteStatement s : stmnts) {
            if (s == null || s.isDisposed())
                return false;
        }
        return true;
    }

    private Map<String, BadPassword> attempts = new HashMap<String, BadPassword>();
    private SQLiteStatement queryPlayer, queryInv, queryBank, querySettings, queryStats, queryList, updatePlayer,
            clearBank, saveBank, clearInv, saveInv, updateStats, clearList, saveList, updateAppearance, updateSettings;

    public void loadStatements(SQLiteConnection conn) throws SQLiteException {
        queryPlayer = conn.prepare(
                "SELECT `player`.`id`, `player`.`x`, `player`.`y`, `player`.`lastip`, `appearance`.`haircolour`, "
                        + "`appearance`.`topcolour`, `appearance`.`trousercolour`, `appearance`.`skincolour`, `appearance`.`head`, `appearance`.`body` "
                        + "FROM `player` INNER JOIN `appearance` ON `appearance`.`playerid` = `player`.`id` WHERE `player`.`username`=? COLLATE NOCASE AND `player`.`password`=?");
        queryInv = conn.prepare(
                "SELECT `itemid`, `amount`, `wielded` FROM `inventory` WHERE `playerid`=? ORDER BY `position`");
        queryBank = conn.prepare("SELECT `itemid`, `amount` FROM `bank` WHERE `playerid`=? ORDER BY `position`");
        querySettings = conn
                .prepare("SELECT `bool1`, `bool2`, `bool3`, `bool4` FROM `settings` WHERE `playerid`=? AND `type`=?");
        queryStats = conn.prepare("SELECT `num`, `cur`, `exp` FROM `stats` WHERE `playerid`=? ORDER BY `num`");
        queryList = conn.prepare("SELECT `playerhash`, `type` FROM `playerlist` WHERE `playerid`=?");
        updatePlayer = conn.prepare("UPDATE `player` SET `x`=?, `y`=?, `lastip`=? WHERE `id`=?");
        clearInv = conn.prepare("DELETE FROM `inventory` WHERE `playerid`=?");
        saveInv = conn.prepare(
                "INSERT INTO `inventory`(`playerid`, `itemid`, `amount`, `wielded`, `position`) VALUES(?, ?, ?, ?, ?)");
        clearBank = conn.prepare("DELETE FROM `bank` WHERE `playerid`=?");
        saveBank = conn.prepare("INSERT INTO `bank`(`playerid`, `itemid`, `amount`, `position`) VALUES(?, ?, ?, ?)");
        updateStats = conn.prepare("UPDATE `stats` SET `cur`=?, `exp`=? WHERE `playerid`=? AND `num`=?");
        clearList = conn.prepare("DELETE FROM `playerlist` WHERE `playerid`=?");
        saveList = conn.prepare("INSERT INTO `playerlist`(`playerid`, `playerhash`, `type`) VALUES(?, ?, ?)");
        updateAppearance = conn
                .prepare("UPDATE `appearance` SET `haircolour`=?, `topcolour`=?, `trousercolour`=?, `skincolour`=?, "
                        + "`head`=?, `body`=? WHERE `playerid`=?");
        updateSettings = conn.prepare(
                "UPDATE `settings` SET `bool1`=?, `bool2`=?, `bool3`=?, `bool4`=? WHERE `type`=? AND `playerid`=?");
    }

    @Override
    public byte register(String username, String password) {
        return DBConnection.getPlayerQueue().execute(new SQLiteJob<Byte>() {
            @Override
            protected Byte job(SQLiteConnection conn) throws Throwable {
                SQLiteStatement s = conn.prepare("SELECT `id` FROM `player` WHERE `username`=? COLLATE NOCASE");
                s.bind(1, username);
                if (s.step())
                    return 3;

                conn.exec("BEGIN TRANSACTION");
                s = conn.prepare(
                        "INSERT INTO `player`(`username`, `password`, `x`, `y`, `lastip`) VALUES(?, ?, 220, 445, '0.0.0.0')");
                s.bind(1, username);
                s.bind(2, DataConversions.sha1(password));
                s.step();
                s.reset();
                s = conn.prepare("SELECT `id` FROM `player` WHERE `username`=?");
                s.bind(1, username);
                s.step();
                int playerID = s.columnInt(0);
                s.reset();
                s = conn.prepare("INSERT INTO `appearance`(`playerid`) VALUES(?)");
                s.bind(1, playerID);
                s.step();
                s.reset();
                s = conn.prepare("INSERT INTO `settings`(`playerid`, `type`) VALUES(?, ?)");
                s.bind(1, playerID);
                s.bind(2, "game");
                s.step();
                s.reset();
                s.bind(1, playerID);
                s.bind(2, "privacy");
                s.step();
                s.reset();
                s = conn.prepare("INSERT INTO `bank`(`playerid`, `position`, `itemid`, `amount`) VALUES(?, ?, ?, ?)");
                s.bind(1, playerID);
                s.bind(2, 0);
                s.bind(3, 546);
                s.bind(4, 96000);
                s.step();
                s.reset();
                s.bind(1, playerID);
                s.bind(2, 1);
                s.bind(3, 373);
                s.bind(4, 96000);
                s.step();
                s.reset();
                s = conn.prepare(
                        "INSERT INTO `inventory`(`playerid`, `position`, `itemid`, `amount`, `wielded`) VALUES(?, ?, ?, ?, ?)");
                s.bind(1, playerID);
                s.bind(2, 0);
                s.bind(3, 77);
                s.bind(4, 1);
                s.bind(5, 1);
                s.step();
                s.reset();
                s.bind(1, playerID);
                s.bind(2, 1);
                s.bind(3, 316);
                s.bind(4, 1);
                s.bind(5, 1);
                s.step();
                s.reset();
                s.bind(1, playerID);
                s.bind(2, 2);
                s.bind(3, 1263);
                s.bind(4, 1);
                s.bind(5, 0);
                s.step();
                s.reset();
                s.bind(1, playerID);
                s.bind(2, 3);
                s.bind(3, 10);
                s.bind(4, 10000);
                s.bind(5, 0);
                s.step();
                s.reset();
                s = conn.prepare("INSERT INTO `stats`(`playerid`, `num`, `cur`, `exp`) VALUES(?, ?, ?, ?)");
                for (int i = 0; i < 18; i++) {
                    s.bind(1, playerID);
                    s.bind(2, i);
                    s.bind(3, i == 3 ? 10 : 1);
                    s.bind(4, i == 3 ? 1154 : 0);
                    s.step();
                    s.reset();
                }
                conn.exec("END TRANSACTION");
                return 2;
            }
        }).complete();
    }

    @Override
    public byte load(Player p) {
        try {
            return DBConnection.getPlayerQueue().execute(new SQLiteJob<Byte>() {
                @Override
                protected Byte job(SQLiteConnection conn) throws SQLiteException {
                    if (!checkStatements(queryPlayer, queryInv, queryBank, querySettings, queryStats, queryList))
                        loadStatements(conn);
                    queryPlayer.bind(1, p.getCredentials().getUsername());
                    queryPlayer.bind(2, DataConversions.sha1(p.getCredentials().getPassword()));
                    if (!queryPlayer.step()) {
                        String ip = p.getCurrentIP();
                        BadPassword bp = attempts.get(ip);
                        if (bp == null || System.currentTimeMillis() - 10 * 60 * 1000 > bp.lastAttempt) {
                            bp = new BadPassword();
                            attempts.put(ip, bp);
                        }
                        bp.attempt++;
                        bp.lastAttempt = System.currentTimeMillis();
                        if (bp.attempt >= Server.getServer().getConf().getInt("loginspamcount")) {
                            bp.nextAttempt = System.currentTimeMillis()
                                    + Server.getServer().getConf().getInt("loginspamdelay");
                        }
                        queryPlayer.reset();
                        return 0x3; // no such user/password
                    } else {
                        p.setID(queryPlayer.columnInt(0));
                        p.setLocation(Point.location(queryPlayer.columnInt(1), queryPlayer.columnInt(2), 0), true);
                        p.setLastIP(queryPlayer.columnString(3));
                        // Appearance
                        p.getAppearance().init(queryPlayer.columnInt(4), queryPlayer.columnInt(5),
                                queryPlayer.columnInt(6), queryPlayer.columnInt(7), queryPlayer.columnInt(8),
                                queryPlayer.columnInt(9));
                        // Inventory
                        queryPlayer.reset();
                        if (World.getInstance().getPlayer(p.getCredentials().getEncodedUsername()) != null)
                            return 0x4; // user already logged in
                        SQLiteStatement queryAttributes = conn
                                .prepare("SELECT `name`, `value` FROM `attributes` WHERE `playerid`=?");
                        queryAttributes.bind(1, p.getID());
                        while (queryAttributes.step()) {
                            String keyName = queryAttributes.columnString(0);
                            String value = queryAttributes.columnString(1);
                            if (value.equals("true"))
                                p.getAttributes().get(keyName).set(true);
                            else if (value.equals("false"))
                                p.getAttributes().get(keyName).set(false);
                            else if (value.startsWith("i"))
                                p.getAttributes().get(keyName).set(Integer.parseInt(value.substring(1)));
                            else if (value.startsWith("d"))
                                p.getAttributes().get(keyName).set(Double.parseDouble(value.substring(1)));
                            else if (value.startsWith("l"))
                                p.getAttributes().get(keyName).set(Long.parseLong(value.substring(1)));
                            else
                                p.getAttributes().get(keyName).set(String.valueOf(value.substring(1)));
                        }
                        if (p.getRank() == 5)
                            return 0x12;
                        queryInv.bind(1, p.getID());
                        List<InvItem> items = new ArrayList<>();
                        while (queryInv.step()) {
                            int id = queryInv.columnInt(0);
                            int amount = queryInv.columnInt(1);
                            if (id == -1) {
                                break;
                            }
                            InvItem it = new InvItem(id, amount);
                            it.setWield(queryInv.columnInt(2) == 1);
                            items.add(it);
                            // System.out.println(p.getInventory().add());
                            // p.getInventory().add(it);
                        }
                        p.getInventory().add(items);
                        // Bank
                        queryInv.reset();
                        queryBank.bind(1, p.getID());
                        items = new ArrayList<>();
                        while (queryBank.step()) {
                            int id = queryBank.columnInt(0);
                            int amount = queryBank.columnInt(1);
                            if (id == -1)
                                break;
                            InvItem it = new InvItem(id, amount);
                            items.add(it);
                        }
                        p.getComponent(Bank.class).add(items);
                        queryBank.reset();
                        querySettings.bind(1, p.getID());
                        querySettings.bind(2, "game");
                        querySettings.step();
                        p.getComponent(Settings.class).setGameSetting(0, querySettings.columnInt(0) == 1);
                        p.getComponent(Settings.class).setGameSetting(1, querySettings.columnInt(1) == 1);
                        p.getComponent(Settings.class).setGameSetting(2, querySettings.columnInt(2) == 1);
                        p.getComponent(Settings.class).setGameSetting(3, querySettings.columnInt(3) == 1);
                        querySettings.reset();
                        querySettings.bind(1, p.getID());
                        querySettings.bind(2, "privacy");
                        querySettings.step();
                        p.getComponent(Settings.class).setPrivacySetting(0, querySettings.columnInt(0) == 1);
                        p.getComponent(Settings.class).setPrivacySetting(1, querySettings.columnInt(1) == 1);
                        p.getComponent(Settings.class).setPrivacySetting(2, querySettings.columnInt(2) == 1);
                        p.getComponent(Settings.class).setPrivacySetting(3, querySettings.columnInt(3) == 1);
                        querySettings.reset();
                        queryStats.bind(1, p.getID());
                        while (queryStats.step()) {
                            int stat = queryStats.columnInt(0);
                            Skill skill = p.getSkillSet().get(stat);
                            skill.setLevel(queryStats.columnInt(1));
                            skill.setExperience(queryStats.columnLong(2));
                            skill.setMaxLevel(Formulae.experienceToLevel(skill.getExperience()));
                        }
                        queryStats.reset();
                        queryList.bind(1, p.getID());
                        Queue<Long> friendsList = new LinkedList<Long>(), ignoreList = new LinkedList<Long>();
                        while (queryList.step()) {
                            long playerHash = queryList.columnLong(0);
                            String type = queryList.columnString(1);
                            if (type.equals("ignore")) {
                                ignoreList.add(playerHash);
                            } else if (type.equals("friend")) {
                                friendsList.add(playerHash);
                            }
                        }
                        queryList.reset();
                        p.getComponent(Communication.class).friends = friendsList;
                        p.getComponent(Communication.class).ignores = ignoreList;
                        p.setWornItems(p.getAppearance().getSprites());
                        return (p.getRank() == 2 ? (byte) 25 : (p.getRank() == 1 ? (byte) 24 : 0x00));
                    }
                }
            }).complete();
        } catch (NullPointerException npe) {
            Logger.err("Exception thrown while loading player:", npe);
        }
        return -1;
    }

    @Override
    public void save(Player p) {
        DBConnection.getPlayerQueue().execute(new SQLiteJob<Byte>() {
            @Override
            protected Byte job(SQLiteConnection conn) throws SQLiteException {
                if (!checkStatements(updatePlayer, clearInv, saveInv, clearBank, saveBank, updateStats, clearList,
                        saveList, updateAppearance, updateSettings))
                    loadStatements(conn);
                conn.exec("BEGIN TRANSACTION");
                updatePlayer.bind(1, p.getLocation().getX());
                updatePlayer.bind(2, p.getLocation().getY());
                updatePlayer.bind(3, p.getCurrentIP());
                updatePlayer.bind(4, p.getID());
                updatePlayer.step();
                updatePlayer.reset();
                clearInv.bind(1, p.getID());
                clearInv.step();
                clearInv.reset();
                for (int slot = 0; slot < p.getInventory().size(); slot++) {
                    InvItem i = p.getInventory().get(slot);
                    saveInv.bind(1, p.getID());
                    saveInv.bind(2, i.getID());
                    saveInv.bind(3, i.getAmount());
                    saveInv.bind(4, i.isWielded() ? 1 : 0);
                    saveInv.bind(5, slot);
                    saveInv.step();
                    saveInv.reset();
                }
                clearBank.bind(1, p.getID());
                clearBank.step();
                clearBank.reset();
                for (int slot = 0; slot < p.getComponent(Bank.class).size(); slot++) {
                    InvItem i = p.getComponent(Bank.class).get(slot);
                    saveBank.bind(1, p.getID());
                    saveBank.bind(2, i.getID());
                    saveBank.bind(3, i.getAmount());
                    saveBank.bind(4, slot);
                    saveBank.step();
                    saveBank.reset();
                }
                for(Skill skill : p.getSkillSet()) {
                    updateStats.bind(1, skill.getLevel());
                    updateStats.bind(2, skill.getExperience());
                    updateStats.bind(3, p.getID());
                    updateStats.bind(4, skill.getIndex());
                    updateStats.step();
                    updateStats.reset();
                }
                clearList.bind(1, p.getID());
                clearList.step();
                clearList.reset();
                for (long friend : p.getComponent(Communication.class).getFriendList()) {
                    saveList.bind(1, p.getID());
                    saveList.bind(2, friend);
                    saveList.bind(3, "friend");
                    saveList.step();
                    saveList.reset();
                }
                for (long ignore : p.getComponent(Communication.class).getIgnoreList()) {
                    saveList.bind(1, p.getID());
                    saveList.bind(2, ignore);
                    saveList.bind(3, "ignore");
                    saveList.step();
                    saveList.reset();
                }
                updateAppearance.bind(1, p.getAppearance().getHairColour());
                updateAppearance.bind(2, p.getAppearance().getTopColour());
                updateAppearance.bind(3, p.getAppearance().getTrouserColour());
                updateAppearance.bind(4, p.getAppearance().getSkinColour());
                updateAppearance.bind(5, p.getAppearance().getHead());
                updateAppearance.bind(6, p.getAppearance().getBody());
                updateAppearance.bind(7, p.getID());
                updateAppearance.step();
                updateAppearance.reset();
                for (int i = 0; i < 4; i++)
                    updateSettings.bind(i + 1, p.getComponent(Settings.class).getGameSetting(i) ? 1 : 0);
                updateSettings.bind(5, "game");
                updateSettings.bind(6, p.getID());
                updateSettings.step();
                updateSettings.reset();
                for (int i = 0; i < 4; i++)
                    updateSettings.bind(i + 1, p.getComponent(Settings.class).getPrivacySetting(i) ? 1 : 0);
                updateSettings.bind(5, "privacy");
                updateSettings.bind(6, p.getID());
                updateSettings.step();
                updateSettings.reset();
                SQLiteStatement clearAttributes = conn.prepare("DELETE FROM `attributes` WHERE `playerid`=?");
                clearAttributes.bind(1, p.getID());
                clearAttributes.step();
                clearAttributes.reset();
                SQLiteStatement updateAttributes = conn
                        .prepare("INSERT INTO `attributes`(`playerid`, `name`, `value`) VALUES(?, ?, ?)");
                for (@SuppressWarnings("rawtypes")
                        Entry<String, AttributeValue> attribute : p.getAttributes()) {
                    if (!p.getAttributes().getAttributeKey(attribute.getKey()).isPersistent())
                        continue;
                    updateAttributes.bind(1, p.getID());
                    updateAttributes.bind(2, attribute.getKey());
                    String type = p.getAttributes().getAttributeKey(attribute.getKey()).getTypeName();
                    if (type.contains("Boolean"))
                        updateAttributes.bind(3, (((boolean) attribute.getValue().get()) ? "true" : "false"));
                    else if (type.contains("Integer"))
                        updateAttributes.bind(3, "i" + String.valueOf((int) attribute.getValue().get()));
                    else if (type.contains("Double"))
                        updateAttributes.bind(3, "d" + String.valueOf((double) attribute.getValue().get()));
                    else if (type.contains("Long"))
                        updateAttributes.bind(3, "l" + String.valueOf((long) attribute.getValue().get()));
                    else
                        updateAttributes.bind(3, "N/A");
                    updateAttributes.step();
                    updateAttributes.reset();
                }
                conn.exec("END TRANSACTION");
                return 0;
            }
        }).complete();
    }
}