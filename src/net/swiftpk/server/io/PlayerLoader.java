package net.swiftpk.server.io;

import net.swiftpk.server.model.Player;

public interface PlayerLoader {
    public byte load(Player p) throws UnsupportedOperationException;

    public byte register(String username, String password) throws UnsupportedOperationException;

    public void save(Player p) throws UnsupportedOperationException;
}
