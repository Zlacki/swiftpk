package net.swiftpk.server.kplugin;

import net.swiftpk.server.util.Logger;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

/**
 * A wrapper for {@link ScriptEngine} that interprets Kotlin scripts.
 */
public final class KotlinInterpreter {
    static {
        // Needed for JSR-223,
        // https://discuss.kotlinlang.org/t/kotlin-script-engine-error/5654
//		UtilKt.setIdeaIoUseFallback();
        System.getProperties().setProperty("idea.io.use.nio2", Boolean.TRUE.toString());
        System.getProperties().setProperty("idea.io.use.fallback", Boolean.TRUE.toString());
    }

    /**
     * The Kotlin interpreter.
     */
    private final ScriptEngine interpreter;

    /**
     * Creates a new {@link KotlinInterpreter}.
     */
    KotlinInterpreter() {
        ClassLoader classLoader = ClassLoader.getSystemClassLoader();
        interpreter = new ScriptEngineManager(classLoader).getEngineByExtension("kts");
    }

    /**
     * Evaluates {@code script} using the backing interpreter.
     *
     * @param script The script to evaluate.
     */
    public void eval(Script script) {
        try {
            interpreter.eval(script.getContents());
        } catch (ScriptException e) {
            Logger.err("Script '" + script.getName() + "' could not be interpreted because of a caught exception:", e);
            System.exit(1);
        }
    }
}