package net.swiftpk.server.kplugin;

import com.google.common.io.MoreFiles;
import com.moandjiezana.toml.Toml;
import net.swiftpk.server.Server;
import net.swiftpk.server.kplugin.event.EventListener;
import net.swiftpk.server.kplugin.event.EventListenerPipelineSet;
import net.swiftpk.server.kplugin.event.EventMatcherListener;
import net.swiftpk.server.util.AsyncExecutor;
import net.swiftpk.server.util.ThreadUtil;
import net.swiftpk.server.util.Logger;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.CompletionException;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;

/**
 * A bootstrapper that initializes and evaluates all {@code Kotlin} plugins.
 */
public final class PluginBootstrap {

    /**
     * A {@link RuntimeException} implementation thrown when a script file cannot be
     * loaded.
     */
    @SuppressWarnings("serial")
    private static final class LoadScriptException extends RuntimeException {

        /**
         * Creates a new {@link LoadScriptException}.
         *
         * @param name The script name.
         * @param e    The reason for failure.
         */
        LoadScriptException(String name, IOException e) {
            super("Failed to read script " + name + ", its parent plugin will not be loaded.", e);
        }
    }

    /**
     * A task that will load a single plugin directory.
     */
    private static final class PluginDirLoader implements Runnable {

        /**
         * All script dependencies for the plugin.
         */
        private final Set<ScriptDependency> dependencies = new LinkedHashSet<>();

        /**
         * All scripts for the plugin.
         */
        private final Set<Script> scripts = new LinkedHashSet<>();

        /**
         * The plugin directory.
         */
        private final Path dir;

        /**
         * The plugin metadata file directory.
         */
        private final Path pluginMetadata;

        /**
         * Creates a new {@link PluginDirLoader}.
         *
         * @param dir            The plugin directory.
         * @param pluginMetadata The plugin metadata file directory.
         */
        PluginDirLoader(Path dir, Path pluginMetadata) {
            this.dir = dir;
            this.pluginMetadata = pluginMetadata;
        }

        @Override
        public void run() {
            // noinspection ConstantConditions
            Set<String> fileList = Arrays.stream(dir.toFile().list())
                    .filter(f -> f.endsWith(".kt") || f.endsWith(".kts")).collect(Collectors.toSet());

            // Metadata TOML -> Java
            PluginMetadata metadata = new Toml().read(pluginMetadata.toFile()).getTable("metadata")
                    .to(PluginMetadata.class);

            // Load all non-metadata plugin files.
            try {
                fileList.forEach(this::loadFile);
            } catch (LoadScriptException e) {
                Logger.err("Exception caught while loading file for Kotlin plugins:", e);
                return;
            }
            plugins.put(pluginMetadata.toString(), new Plugin(metadata, computePackageName(), dependencies, scripts));
        }

        /**
         * Computes the fully qualified package name.
         *
         * @return The package name.
         */
        private String computePackageName() {
            String packageDir = dir.toString().replace(File.separator, ".").substring(2);
            int firstIndex = packageDir.indexOf('.');
            int lastIndex = packageDir.lastIndexOf('.');
            if (firstIndex == lastIndex) {
                return "";
            }
            return packageDir.substring(firstIndex + 1, lastIndex);
        }

        /**
         * Loads the file with {@code fileName} into one of the backing sets.
         *
         * @param fileName The file to load.
         */
        private void loadFile(String fileName) {
            try {
                Path path = dir.resolve(fileName);
                if (fileName.endsWith(".kts")) {
                    // Load script file.
                    String scriptContents = new String(Files.readAllBytes(path));
                    scripts.add(new Script(fileName, path, scriptContents));
                } else {
                    // Load dependency file.
                    dependencies.add(new ScriptDependency(fileName, path));
                }
            } catch (IOException e) {
                throw new LoadScriptException(fileName, e);
            }
        }
    }

    /**
     * The directory containing plugin files.
     */
    private static final Path DIR = Paths.get(System.getProperty("user.dir")).resolve("kplugins");

    /**
     * The bindings. Has to be global in order for Kotlin scripts to access it.
     */
    private static KotlinBindings bindings = new KotlinBindings(Server.getServer());

    /**
     * A map of plugin names to instances.
     */
    private static final Map<String, Plugin> plugins = new ConcurrentHashMap<>();

    /**
     * Initializes this bootstrapper, loading all of the plugins.
     *
     * @return Returns a numerator and denominator indicating how many plugins out
     * of the total amount were loaded.
     */
    public static int init() {
        AsyncExecutor executor = new AsyncExecutor(ThreadUtil.AVAILABLE_PROCESSORS, "PluginDirInitThread");

        // Traverse all paths and sub-paths.
        for (Path dir : MoreFiles.fileTraverser().depthFirstPreOrder(DIR)) {
            if (Files.isDirectory(dir)) {
                Path pluginMetadata = dir.resolve("plugin.toml");
                if (Files.exists(pluginMetadata)) {
                    executor.execute(new PluginDirLoader(dir, pluginMetadata));
                }
            }
        }

        // Await completion.
        try {
            executor.await(true);
        } catch (ExecutionException e) {
            throw new CompletionException(e);
        }

        for (Plugin other : plugins.values())
            loadPlugin(other);

        EventListenerPipelineSet oldPipelines = Server.getServer().getPlugins().getPipelines();
        EventListenerPipelineSet newPipelines = bindings.getPipelines();
        Server.getServer().getGameService().sync(() -> oldPipelines.replaceAll(newPipelines));

        return plugins.size();
    }

    /**
     * Evaluates a single plugin directory.
     *
     * @param plugin      The plugin to load.
     */
    private static void loadPlugin(Plugin plugin) {
        KotlinInterpreter interpreter = new KotlinInterpreter();
        for (Script script : plugin.getScripts()) {
            // Evaluate the script.
            interpreter.eval(script);

            // Add all of its listeners, reflectively set the listener script.
            for (EventListener<?> listener : bindings.getListeners()) {
                listener.setScript(script);
                bindings.getPipelines().add(listener);
            }
            for (EventMatcherListener<?> listener : bindings.getMatchers()) {
                listener.setScript(script);
            }
            bindings.getMatchers().clear();
            bindings.getListeners().clear();
        }
    }
}
