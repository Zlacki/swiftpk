package net.swiftpk.server.kplugin;

import net.swiftpk.server.kplugin.event.Event;
import net.swiftpk.server.kplugin.event.EventListenerPipeline;
import net.swiftpk.server.kplugin.event.EventListenerPipelineSet;

/**
 * A model that acts as a bridge between interpreted Kotlin code and compiled
 * Java code.
 */
public final class PluginManager {

    /**
     * A pipeline set containing interpreted Kotlin code.
     */
    private final EventListenerPipelineSet pipelines = new EventListenerPipelineSet();

    /**
     * Creates a new {@link PluginManager}.
     *
     * @param context The context instance.
     */
    public PluginManager() {
    }

    /**
     * Traverses the event across its designated pipeline.
     *
     * @param msg The event to post.
     * @return true if the event gets posted, false otherwise.
     */
    @SuppressWarnings({"rawtypes", "unchecked"})
    public boolean post(Event msg) {
        EventListenerPipeline pipeline = pipelines.get(msg.getClass());
        if (pipeline == null)
            return false;

        pipeline.post(msg);
        return true;
    }

    /**
     * @return A pipeline set containing interpreted Kotlin code.
     */
    public EventListenerPipelineSet getPipelines() {
        return pipelines;
    }
}
