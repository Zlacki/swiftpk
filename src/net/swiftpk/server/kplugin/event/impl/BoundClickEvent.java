package net.swiftpk.server.kplugin.event.impl;

import net.swiftpk.server.model.Player;
import net.swiftpk.server.model.entity.GameObject;
import net.swiftpk.server.model.entity.NPC;

/**
 * A bound-click based event. Not intended for interception.
 */
public class BoundClickEvent extends PlayerEvent {
    /**
     * The bound object.
     */
    private final GameObject object;

    /**
     * Creates a new {@link BoundClickEvent}.
     *
     * @param player The player.
     * @param object    The bound object.
     */
    public BoundClickEvent(Player player, GameObject object) {
        super(player);
        this.object = object;
    }

    /**
     * @return The non-player character.
     */
    public GameObject getObject() {
        return object;
    }

    public static class BoundSecondClickEvent extends BoundClickEvent {
        public BoundSecondClickEvent(Player player, GameObject object) {
            super(player, object);
        }
    }
}
