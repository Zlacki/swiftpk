package net.swiftpk.server.kplugin.event.impl;

import net.swiftpk.server.model.Player;
import net.swiftpk.server.model.entity.GameObject;

/**
 * A bound-click based event. Not intended for interception.
 */
public class ObjectClickEvent extends PlayerEvent {
    /**
     * The bound object.
     */
    private final GameObject object;

    /**
     * Creates a new {@link ObjectClickEvent}.
     *
     * @param player The player.
     * @param object    The bound object.
     */
    public ObjectClickEvent(Player player, GameObject object) {
        super(player);
        this.object = object;
    }

    /**
     * @return The non-player character.
     */
    public GameObject getObject() {
        return object;
    }

    public static class ObjectSecondClickEvent extends ObjectClickEvent {
        public ObjectSecondClickEvent(Player player, GameObject object) {
            super(player, object);
        }
    }
}
