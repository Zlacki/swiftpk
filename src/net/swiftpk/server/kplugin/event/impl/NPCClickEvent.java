package net.swiftpk.server.kplugin.event.impl;

import net.swiftpk.server.model.Player;
import net.swiftpk.server.model.entity.NPC;

/**
 * An npc-click based event. Not intended for interception.
 */
public class NPCClickEvent extends PlayerEvent {

    /**
     * An event sent when a player clicks an npc's first index.
     */
    public static final class NPCFirstClickEvent extends NPCClickEvent {

        /**
         * Creates a new {@link NPCClickEvent}.
         */
        public NPCFirstClickEvent(Player player, NPC npc) {
            super(player, npc);
        }
    }

    /**
     * An event sent when a player clicks an npc's second index.
     */
    public static final class NPCSecondClickEvent extends NPCClickEvent {

        /**
         * Creates a new {@link NPCSecondClickEvent}.
         */
        public NPCSecondClickEvent(Player player, NPC npc) {
            super(player, npc);
        }
    }

    /**
     * The non-player character.
     */
    private final NPC npc;

    /**
     * Creates a new {@link NPCClickEvent}.
     *
     * @param player The player.
     * @param npc    The non-player character.
     */
    private NPCClickEvent(Player player, NPC npc) {
        super(player);
        this.npc = npc;
    }

    /**
     * @return The non-player character.
     */
    public NPC getNPC() {
        return npc;
    }
}
