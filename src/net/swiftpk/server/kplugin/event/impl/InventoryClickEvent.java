package net.swiftpk.server.kplugin.event.impl;

import net.swiftpk.server.model.InvItem;
import net.swiftpk.server.model.Item;
import net.swiftpk.server.model.Player;
import net.swiftpk.server.model.entity.GameObject;
import net.swiftpk.server.model.entity.NPC;

/**
 * An event sent when a player clicks an inventory action
 */
public class InventoryClickEvent extends PlayerEvent {
    /**
     * The inventory item that was clicked
     */
    private final InvItem item;

    /**
     * Creates a new {@link InventoryClickEvent}.
     *
     * @param player The player.
     * @param item   The item that was clicked.
     */
    public InventoryClickEvent(Player player, InvItem item) {
        super(player);
        this.item = item;
    }

    public InvItem getItem() {
        return item;
    }
    public static class InventoryDropEvent extends InventoryClickEvent {
        public InventoryDropEvent(Player player, InvItem item) {
            super(player, item);
        }
    }
    public static class InventoryOnBoundEvent extends InventoryClickEvent {
        private final GameObject object;

        public InventoryOnBoundEvent(Player player, InvItem item, GameObject object) {
            super(player, item);
            this.object = object;
        }

        public GameObject getObject() {
            return object;
        }
    }
    public static class InventoryOnObjectEvent extends InventoryClickEvent {
        private final GameObject object;

        public InventoryOnObjectEvent(Player player, InvItem item, GameObject object) {
            super(player, item);
            this.object = object;
        }

        public GameObject getObject() {
            return object;
        }
    }
    public static class InventoryOnLootableEvent extends InventoryClickEvent {
        private final Item lootable;

        public InventoryOnLootableEvent(Player player, InvItem item, Item lootable) {
            super(player, item);
            this.lootable = lootable;
        }

        public Item getLootable() {
            return lootable;
        }
    }
    public static class InventoryOnPlayerEvent extends InventoryClickEvent {
        private final Player target;

        public InventoryOnPlayerEvent(Player player, InvItem item, Player target) {
            super(player, item);
            this.target = target;
        }

        public Player getTarget() {
            return target;
        }
    }
    public static class InventoryOnNPCEvent extends InventoryClickEvent {
        private final NPC target;

        public InventoryOnNPCEvent(Player player, InvItem item, NPC target) {
            super(player, item);
            this.target = target;
        }

        public NPC getTarget() {
            return target;
        }
    }
}
