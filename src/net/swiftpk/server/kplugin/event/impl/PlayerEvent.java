package net.swiftpk.server.kplugin.event.impl;

import net.swiftpk.server.kplugin.event.Event;
import net.swiftpk.server.model.Player;

/**
 * A player-based event. Not intended for interception.
 */
public class PlayerEvent extends Event {

    /**
     * The player.
     */
    protected final Player plr;

    /**
     * Creates a new {@link PlayerEvent}.
     *
     * @param plr The player.
     */
    public PlayerEvent(Player plr) {
        this.plr = plr;
    }

    /**
     * @return The player.
     */
    public Player getPlr() {
        return plr;
    }
}
