package net.swiftpk.server.kplugin.event.impl;

import net.swiftpk.server.model.Player;
import net.swiftpk.server.model.entity.NPC;

/**
 * An event sent when a player ranges an NPC
 */
public final class NPCRangedEvent extends PlayerEvent {
    /**
     * The NPC we are ranging at
     */
    private final NPC npc;

    /**
     * Creates a new {@link NPCRangedEvent}.
     *
     * @param player The player.
     * @param npc    The NPC we are ranging at.
     */
    public NPCRangedEvent(Player player, NPC npc) {
        super(player);
        this.npc = npc;
    }

    public NPC getNPC() {
        return npc;
    }
}
