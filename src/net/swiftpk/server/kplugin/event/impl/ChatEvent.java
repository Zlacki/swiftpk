package net.swiftpk.server.kplugin.event.impl;

import net.swiftpk.server.model.Player;

/**
 * An event sent when a player speaks in local chat
 */
public final class ChatEvent extends PlayerEvent {
    /**
     * The chat message.
     */
    private final String message;

    /**
     * Creates a new {@link ChatEvent}.
     *
     * @param player  The player.
     * @param message The message the player is trying to send.
     */
    public ChatEvent(Player player, String message) {
        super(player);
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}
