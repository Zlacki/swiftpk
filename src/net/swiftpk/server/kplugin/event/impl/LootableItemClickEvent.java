package net.swiftpk.server.kplugin.event.impl;

import net.swiftpk.server.model.Item;
import net.swiftpk.server.model.Player;

/**
 * An event sent when a player clicks a lootable item
 */
public class LootableItemClickEvent extends PlayerEvent {
    /**
     * The lootable item that was clicked
     */
    private final Item item;

    /**
     * Creates a new {@link LootableItemClickEvent}.
     *
     * @param player The player.
     * @param item   The item that was clicked.
     */
    public LootableItemClickEvent(Player player, Item item) {
        super(player);
        this.item = item;
    }

    public Item getItem() {
        return item;
    }
}
