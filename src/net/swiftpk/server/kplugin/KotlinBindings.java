package net.swiftpk.server.kplugin;

import net.swiftpk.server.Server;
import net.swiftpk.server.kplugin.event.EventListener;
import net.swiftpk.server.kplugin.event.EventListenerPipelineSet;
import net.swiftpk.server.kplugin.event.EventMatcherListener;

import java.util.ArrayList;
import java.util.List;

/**
 * A model representing values that will be reflectively injected into the
 * Kotlin scripting API.
 */
public final class KotlinBindings {

    /**
     * The context instance.
     */
    private final Server server;

    /**
     * A list of event listeners.
     */
    private final List<EventListener<?>> listeners = new ArrayList<>();

    /**
     * A list of event matcher listeners.
     */
    private final List<EventMatcherListener<?>> matchers = new ArrayList<>();

    /**
     * The pipeline set.
     */
    private final EventListenerPipelineSet pipelines = new EventListenerPipelineSet();

    /**
     * Creates a new {@link KotlinBindings}.
     *
     * @param ctx The context instance.
     */
    KotlinBindings(Server server) {
        this.server = server;
    }

    /**
     * @return The context instance.
     */
    public Server getCtx() {
        return server;
    }

    /**
     * @return A list of event listeners.
     */
    public List<EventListener<?>> getListeners() {
        return listeners;
    }

    /**
     * @return A list of event matcher listeners.
     */
    public List<EventMatcherListener<?>> getMatchers() {
        return matchers;
    }

    /**
     * @return The pipeline set.
     */
    public EventListenerPipelineSet getPipelines() {
        return pipelines;
    }
}