package net.swiftpk.server.util;

public class RangedUtils {
    public static final int[][] arrows = {{45, 40, 35, 30, 25, 20, 10, 5, 5, 1, 3, 1},
            {647, 646, 645, 644, 643, 642, 641, 640, 639, 638, 574, 11}};
    public static final int[][] bolts = {{50, 1}, {786, 190}};
    public static final int[] longBowIds = {188, 648, 650, 652, 654, 656};
    public static final int[] shortBowIds = {189, 649, 651, 653, 655, 657};
    public static final int[] xbowIds = {59, 60};
    public static final double[] longBowStats = {5, 1.55, 5};
    public static final double[] shortBowStats = {3, 1.2, 4};
    public static final double[] xbowStats = {4, 1.4, 4};
}
