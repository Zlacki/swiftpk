package net.swiftpk.server.util;

import java.lang.Thread.UncaughtExceptionHandler;
import java.util.Objects;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * A static utility class which provides ease of use functionality for
 * {@link Thread}s
 *
 * @author Ryley
 * @author Major
 */
public final class ThreadUtil {

    /**
     * Awaits termination of {@code service} without interruption.
     *
     * @param service The executor to wait for.
     */
    public static void awaitTerminationUninterruptibly(ExecutorService service) {
        awaitTerminationUninterruptibly(service, Long.MAX_VALUE, TimeUnit.DAYS);
    }

    /**
     * Awaits termination of {@code service} for the designated time without
     * interruption.
     *
     * @param service The executor to wait for.
     * @param timeout The time to wait for.
     * @param unit    The time unit.
     */
    public static void awaitTerminationUninterruptibly(ExecutorService service, long timeout, TimeUnit unit) {
        // ----------- Code snippet taken from Google Guava.
        boolean interrupted = false;

        try {
            while (true) {
                try {
                    service.awaitTermination(timeout, unit);
                    break;
                } catch (InterruptedException e) {
                    interrupted = true;
                }
            }
        } finally {
            if (interrupted) {
                Thread.currentThread().interrupt();
            }
        }
        // -----------
    }

    /**
     * Returns the amount of available processors available to the Java virtual
     * machine.
     */
    public static final int AVAILABLE_PROCESSORS = Runtime.getRuntime().availableProcessors();

    /**
     * A {@link Logger} used to debug messages to the console.
     */
    private static final Logger logger = Logger.getLogger(ThreadUtil.class.getSimpleName());

    /**
     * The default {@link UncaughtExceptionHandler} which raises an error from the
     * logger with the exception and name of the specified thread the exception
     * occurred in.
     */
    private static final UncaughtExceptionHandler DEFAULT_EXCEPTION_HANDLER = (thread, exception) -> logger
            .log(Level.SEVERE, "Exception in thread " + thread.getName(), exception);

    /**
     * Builds a {@link ThreadFactory} using the specified {@code String}
     * name-format, normal thread priority and the default
     * {@link UncaughtExceptionHandler}, which simply logs exception messages.
     *
     * @param name The name-format used when creating threads. Must not be
     *             {@code null}.
     * @return The {@link ThreadFactory}. Will never be {@code null}.
     */
    public static ThreadFactory create(String name) {
        return create(name, Thread.NORM_PRIORITY, DEFAULT_EXCEPTION_HANDLER);
    }

    /**
     * Builds a {@link ThreadFactory} using the specified {@code String}
     * name-format, priority and the {@link #DEFAULT_EXCEPTION_HANDLER}.
     *
     * @param name     The name-format used when creating threads. Must not be
     *                 {@code null}.
     * @param priority The priority used when creating threads. Must be
     *                 {@code 1 <= priority <= 10}.
     * @return The {@link ThreadFactory}. Will never be {@code null}.
     */
    public static ThreadFactory create(String name, int priority) {
        return create(name, priority, DEFAULT_EXCEPTION_HANDLER);
    }

    /**
     * Creates a {@link ThreadFactory} using the specified {@code String}
     * name-format, priority and {@link UncaughtExceptionHandler}.
     *
     * @param name     The name-format used when creating threads. Must not be
     *                 {@code null}.
     * @param priority The priority used when creating threads. Must be
     *                 {@code 1 <= priority <= 10}.
     * @param handler  The {@link UncaughtExceptionHandler} used when creating
     *                 threads. Must not be {@code null}.
     * @return The {@link ThreadFactory}. Will never be {@code null}.
     */
    public static ThreadFactory create(String name, int priority, UncaughtExceptionHandler handler) {
        Objects.requireNonNull(name, "ThreadFactory name must not be null.");
        Objects.requireNonNull(handler, "UncaughtExceptionHandler must not be null.");

        return new ThreadFactory() {
            private final AtomicInteger count = new AtomicInteger(0);

            @Override
            public Thread newThread(Runnable r) {
                Thread newThread = new Thread(r);
                newThread.setName(name + "-" + count.incrementAndGet());
                newThread.setPriority(priority);
                newThread.setUncaughtExceptionHandler(handler);
                return newThread;
            }
        };
    }

    /**
     * Sole private constructor to prevent instantiation.
     */
    private ThreadUtil() {

    }
}
