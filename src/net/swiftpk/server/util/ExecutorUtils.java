package net.swiftpk.server.util;

import com.google.common.util.concurrent.ThreadFactoryBuilder;

import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.ThreadPoolExecutor.CallerRunsPolicy;
import java.util.concurrent.TimeUnit;

/**
 * A static-utility class that contains functions for manipulating
 * {@link Executor}s.
 */
public final class ExecutorUtils {

    /**
     * Create a new cached thread pool with a capacity of {@code maxThreads}. It
     * will queue tasks once the maximum amount of threads have been allocated.
     *
     * @param maxThreads The thread pool capacity.
     * @return The new cached thread pool.
     */
    public static ExecutorService newCachedThreadPool(int maxThreads) {
        ThreadFactory threadFactory = new ThreadFactoryBuilder().setNameFormat("RSWorkerThread").build();
        ThreadPoolExecutor executor = new ThreadPoolExecutor(0, maxThreads, 60L, TimeUnit.SECONDS,
                new LinkedBlockingQueue<>());
        executor.setThreadFactory(threadFactory);
        executor.setRejectedExecutionHandler(new CallerRunsPolicy());
        return executor;
    }

    /**
     * Create a new cached thread pool with a capacity of
     * {@code ThreadUtils.cpuCount() * 2}.
     *
     * @return The new cached thread pool.
     */
    public static ExecutorService newCachedThreadPool() {
        return newCachedThreadPool(ThreadUtil.AVAILABLE_PROCESSORS * 2);
    }

    /**
     * Prevent instantiation.
     */
    private ExecutorUtils() {
    }
}