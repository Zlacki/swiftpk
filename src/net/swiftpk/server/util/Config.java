package net.swiftpk.server.util;

import java.util.HashMap;
import java.util.Map;

import com.almworks.sqlite4java.SQLiteStatement;
import net.swiftpk.server.io.DBConnection;

import com.almworks.sqlite4java.SQLiteConnection;
import com.almworks.sqlite4java.SQLiteException;
import com.almworks.sqlite4java.SQLiteJob;

/**
 * This class acts as a cache for all of the various properties involved in
 * running the server. This includes things like the delay for eating, casting
 * spells, some formula values, welcome message, server location, etc. These are
 * generally accessed a lot, and since they are usually variables subject to
 * change, we put them in SQL DB to load and cache them here.
 * <p>
 * TODO: Update when cache is out of date
 *
 * @author Zach Knight
 */
public class Config {
    public static boolean npcUpdateLock = false;
    private Map<String, Object> props = new HashMap<String, Object>();

    public String get(String s) {
        long start = System.nanoTime();
        if (!props.containsKey(s))
            try {
                props.put(s, DBConnection.getWorldQueue().execute(new SQLiteJob<String>() {
                    @Override
                    protected String job(SQLiteConnection conn) throws SQLiteException {
                        SQLiteStatement statement = conn.prepare("SELECT `value` FROM `config` WHERE `name`=?");
                        statement.bind(1, s);
                        if (statement.step())
                            return statement.columnString(0);

                        return null;
                    }
                }).complete());
            } catch (SQLiteException e) {
                Logger.warnf("Exception caught while loading config property (name='{}', value='null')", s);
                Logger.warn(e);
                return "null";
            }

        Logger.outf("Cache property loaded: (name='{}', value='{}') in {}ms", s, props.get(s), (System.nanoTime() - start) / 1e6);
        return (String) props.get(s);
    }

    public double getDouble(String s) {
        long start = System.nanoTime();
        if (!props.containsKey(s))
            try {
                props.put(s, DBConnection.getWorldQueue().execute(new SQLiteJob<Double>() {
                    @Override
                    protected Double job(SQLiteConnection conn) throws SQLiteException {
                        SQLiteStatement statement = conn.prepare("SELECT `value` FROM `config` WHERE `name`=?");
                        statement.bind(1, s);
                        if (statement.step())
                            return Double.parseDouble(statement.columnString(0));

                        return 0D;
                    }
                }).complete());
            } catch (SQLiteException e) {
                Logger.warnf("Exception caught while loading config property (name='{}', value=d0)", s);
                Logger.warn(e);
                return 0D;
            }

        Logger.outf("Cache property loaded: (name='{}', value=d{}) in {}ms", s, props.get(s), (System.nanoTime() - start) / 1e6);
        return (Double) props.get(s);
    }

    public int getInt(String s) {
        long start = System.nanoTime();
        if (!props.containsKey(s))
            try {
                props.put(s, DBConnection.getWorldQueue().execute(new SQLiteJob<Integer>() {
                    @Override
                    protected Integer job(SQLiteConnection conn) throws SQLiteException {
                        SQLiteStatement statement = conn.prepare("SELECT `value` FROM `config` WHERE `name`=?");
                        statement.bind(1, s);
                        if (statement.step())
                            return Integer.parseInt(statement.columnString(0));
                        return 0;
                    }
                }).complete());
            } catch (SQLiteException e) {
                Logger.warnf("Exception caught while loading config property (name='{}', value=d0)", s);
                Logger.warn(e);
                return 0;
            }

        Logger.outf("Cache property loaded: (name='{}', value=i{}) in {}ms", s, props.get(s), (System.nanoTime() - start) / 1e6);
        return (Integer) props.get(s);
    }
}
