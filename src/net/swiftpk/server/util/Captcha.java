package net.swiftpk.server.util;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import javax.imageio.ImageIO;

import net.swiftpk.server.model.Player;

public class Captcha {
    private static List<Color> COLOR_LIST = new ArrayList<>();
    private static List<String> WORD_LIST = new ArrayList<>();
    private static List<Font> FONT_LIST = new ArrayList<>();
    private static Path FONT_PATH = Paths
            .get(System.getProperty("user.dir") + File.separator + "data" + File.separator + "fonts");
    private static Path DICTIONARY_PATH = Paths
            .get(System.getProperty("user.dir") + File.separator + "data" + File.separator + "words.list");
    private static boolean USING_DICTIONARY = true;
    public static boolean ACTIVE = true;

    private static String generateRandomWord() {
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < RandomUtil.inclusive(5, 8); i++) {
            char nextChar = (char) RandomUtil.inclusive('a', 'z');
            if (RandomUtil.inclusive(1) == 0)
                nextChar &= '_'; // Bitwise capitalization.
            builder.append(nextChar);
        }
        return builder.toString();
    }

    public static byte[] generateCaptcha(Player p) {
        BufferedImage image = new BufferedImage(307, 49, BufferedImage.TYPE_INT_RGB);
        Graphics2D gfx = image.createGraphics();
        String captcha = WORD_LIST.get(RandomUtil.inclusive(WORD_LIST.size() - 1));
        if (USING_DICTIONARY)
            captcha = WORD_LIST.get(RandomUtil.inclusive(WORD_LIST.size() - 1));
        else
            captcha = generateRandomWord();
        gfx.setColor(Color.BLACK);
        gfx.fillRect(0, 0, 308, 52);
        int currentX = 10;
        for (int i = 0; i <= captcha.length() - 1; i++) {
            gfx.setColor(COLOR_LIST.get(RandomUtil.inclusive(COLOR_LIST.size() - 1)));
            gfx.setFont(FONT_LIST.get(RandomUtil.inclusive(FONT_LIST.size() - 1)));
            gfx.drawString(String.valueOf(captcha.charAt(i)), currentX, RandomUtil.inclusive(30, 40));
            currentX += gfx.getFontMetrics().charWidth(captcha.charAt(i));
        }
        p.setSleepWord(captcha);
        try {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            ImageIO.write(image, "PNG", baos);
            return baos.toByteArray();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            gfx.dispose();
        }
        return null;
    }

    static {
        loadFontsAndDictionary();
        COLOR_LIST.clear();
        COLOR_LIST.add(Color.GREEN);
        COLOR_LIST.add(Color.WHITE);
        COLOR_LIST.add(Color.RED);
        COLOR_LIST.add(Color.PINK);
        COLOR_LIST.add(Color.CYAN);
        COLOR_LIST.add(Color.MAGENTA);
        COLOR_LIST.add(Color.YELLOW);
        COLOR_LIST.add(Color.ORANGE);
    }

    /**
     * Loads fonts from a folder to a font array
     */
    public static void loadFontsAndDictionary() {
        if (!Files.exists(FONT_PATH) || !Files.isDirectory(FONT_PATH)) {
            Logger.warnf("[CAPTCHA] Could not find font folder: '{}'", FONT_PATH.toString());
            Logger.warnf("[CAPTCHA] To enable the CAPTCHA sleeping system, please create a directory at '{}', and populate it with TTF or OTF fonts.", FONT_PATH.toString());
            Logger.out("[CAPTCHA] Disabling CAPTCHA sleeping system. (TODO: Enabling <backup_sleeping_system>.) ");
            ACTIVE = false;
            return;
        }
        WORD_LIST.clear();
        if (Files.exists(DICTIONARY_PATH)) {
            try {
                Files.lines(DICTIONARY_PATH).forEach((line) -> WORD_LIST.add(line));
            } catch (IOException e1) {
                Logger.warn("[CAPTCHA] Exception caught while loading dictionary:", e1);
                Logger.out("[CAPTCHA] Setting to random character mode.");
                USING_DICTIONARY = false;
            }
        } else {
            Logger.warnf("[CAPTCHA] Could not find dictionary file at '{}'", DICTIONARY_PATH.toString());
            Logger.warn("[CAPTCHA] If you would like the CAPTCHA sleeping system to use a dictionary to generate specific sleep words, please place the dictionary file at '{}' and put one word per line in it.");
            Logger.out("[CAPTCHA] Setting to random character mode.");
            USING_DICTIONARY = false;
        }
        FONT_LIST.clear();
        try (Stream<Path> fontFiles = Files.list(FONT_PATH)) {
            if(fontFiles.count() == 0) {
                Logger.warnf("[CAPTCHA] Font directory ('{}') contains no entries.", FONT_PATH.toString());
                Logger.warnf("[CAPTCHA] To enable the CAPTCHA sleeping system, place some TTF or OTF fonts into the directory: '{}'", FONT_PATH.toString());
                Logger.out("[CAPTCHA] Disabling CAPTCHA sleeping system. (TODO: Enabling <backup_sleeping_system>.) ");
                return;
            }
            fontFiles.forEach((font) -> {
                try {
                    FONT_LIST.add(Font.createFont(java.awt.Font.TRUETYPE_FONT, font.toFile())
                            .deriveFont(Float.valueOf(RandomUtil.inclusive(35, 40))));
                } catch (Exception e) {
                    String fontName = font.getFileName().toString();
                    Logger.warnf("[CAPTCHA] Exception caught while loading font '{}':", fontName);
                    Logger.warn(e);
                    Logger.outf("[CAPTCHA] Attempting to delete file '{}', since it will not load.", fontName);
                    if(!font.toFile().delete())
                        Logger.warnf("[CAPTCHA] Could not delete file: '{}'.", fontName);
                }
            });
        } catch (IOException | SecurityException e) {
            Logger.warnf("[CAPTCHA] Exception caught while reading the files from directory: '{}'", FONT_PATH.toString());
            Logger.warn(e);
            Logger.out("[CAPTCHA] Disabling CAPTCHA sleeping system. (TODO: Enabling <backup_sleeping_system>) ");
            ACTIVE = false;
            return;
        }
        if (!FONT_LIST.isEmpty()) {
            Logger.outf("[CAPTCHA] Loaded {} fonts.", FONT_LIST.size());
        } else {
            Logger.warnf("[CAPTCHA] Did not load any fonts from directory: '{}'", FONT_PATH.toString());
            Logger.out("[CAPTCHA] Disabling CAPTCHA sleeping system. (TODO: Enabling <backup_sleeping_system>) ");
        }
    }
}
