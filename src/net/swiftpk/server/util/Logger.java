package net.swiftpk.server.util;

import java.io.*;
import java.nio.file.*;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import net.swiftpk.server.Server;

import static net.swiftpk.server.util.Formulae.timeSince;

/**
 * A class to handle logging events
 */
public class Logger {
    private final static Path LOG_PATH = Paths.get(System.getProperty("user.dir")).resolve("log");
    private static Map<String, LogStream> streams = new HashMap<>();

    static {
        try {
            if (Files.notExists(LOG_PATH))
                Files.createDirectory(LOG_PATH);
            else if (!Files.isDirectory(LOG_PATH)) {
                Files.delete(LOG_PATH);
                Files.createDirectory(LOG_PATH);
            }
        } catch (IOException e) {
            System.err.println("Exception thrown while trying to create the log directory.");
            e.printStackTrace();
            System.exit(1);
        }

        try {
            streams.put("err.log", new LogStream(LOG_PATH.resolve("err.log"), System.err));
        } catch (IOException e) {
            streams.put("err.log", new LogStream(System.err));
            err("Exception thrown while making LogStream for 'err.log':", e);
        }
        try {
            streams.put("out.log", new LogStream(LOG_PATH.resolve("out.log"), System.out));
        } catch (IOException e) {
            streams.put("out.log", new LogStream(System.out));
            err("Exception thrown while making LogStream for 'out.log':", e);
        }
    }

    public static synchronized void close() {
        for (PrintStream stream : streams.values()) {
            stream.println("Closing stream...");
            stream.close();
        }
    }

    public static synchronized void err(Object... args) {
        log("err.log", args);
    }

    public static synchronized void out(Object... args) {
        log("out.log", args);
    }

    public static synchronized void warn(Object... args) {
        log("warn.log", args);
    }

    public static synchronized void log(String fileName, Object... objs) {
        LogStream stream = streams.get(fileName);
        if (stream == null) {
            try {
                stream = new LogStream(LOG_PATH.resolve(fileName), System.out);
                streams.put(fileName, stream);
            } catch (IOException e) {
                err("Exception thrown while making a LogStream for: '" + fileName + "'", e);
                stream = new LogStream(System.out);
            }
        }
        if (objs.length == 0) {
            stream.println();
            return;
        }
        for (Object o : objs)
            if (o instanceof Throwable)
                stream.println((Throwable) o);
            else
                stream.println(o.toString());
    }

    public static synchronized void outf(String message, Object... args) {
        logf("out.log", message, args);
    }

    public static synchronized void errf(String message, Object... args) {
        logf("err.log", message, args);
    }

    public static synchronized void warnf(String message, Object... args) {
        logf("warn.log", message, args);
    }

    public static synchronized void logf(String fileName, String message, Object... args) {
        LogStream stream = streams.get(fileName);
        if (stream == null) {
            try {
                stream = new LogStream(LOG_PATH.resolve(fileName), System.out);
                streams.put(fileName, stream);
            } catch (IOException e) {
                err("Exception thrown while making a LogStream for: '" + fileName + "'", e);
                stream = new LogStream(System.out);
            }
        }
        int argPos = message.indexOf("{}");
        if (argPos == -1) {
            stream.println(message);
            return;
        }

        StringBuilder messageBuilder = new StringBuilder(message.substring(0, argPos));
        for (int curArg = 0; curArg < args.length; curArg++) {
            messageBuilder.append(args[curArg]);
            message = message.substring(argPos + 2);
            argPos = message.indexOf("{}");
            if(argPos != -1) {
                messageBuilder.append(message, 0, argPos);
            } else {
                messageBuilder.append(message);
                break;
            }
        }

        stream.println(messageBuilder.toString());
    }

    private static String prefix() {
        return "[" + time() + "] [" + Thread.currentThread().getName() + "] ";
    }

    private static String time() {
        return timeSince(Server.START_TIME);
    }

    private static String getDate() {
        return new SimpleDateFormat("dd-MM-yy-hh-mm-ss").format(Calendar.getInstance().getTime());
    }

    private static OutputStream newOutputStream(Path file) throws IOException {
        String fileName = file.getFileName().toString();
        if (Files.exists(file) && Files.size(file) > 0) {
            try {
                int i = 0;
                while (!Files.isWritable(file)) {
                    file = LOG_PATH.resolve(fileName.substring(0, fileName.indexOf(".")) + "-" + i++ + ".log");
                    if (!Files.exists(file))
                        return Files.newOutputStream(file);
                }

                Path rotationDirectory = LOG_PATH.resolve("old-" + getDate());
                if (!Files.exists(rotationDirectory))
                    Files.createDirectory(rotationDirectory);
                else if (!Files.isDirectory(rotationDirectory)) {
                    Files.delete(rotationDirectory);
                    Files.createDirectory(rotationDirectory);
                }
                Files.move(file, rotationDirectory.resolve(fileName));
            } catch (IOException e) {
                err("Exception thrown during log rotation:", e);
                return Files.newOutputStream(LOG_PATH.resolve(fileName.substring(0, fileName.indexOf(".")) + "-" + System.currentTimeMillis() + ".log"));
            }
        }
        return Files.newOutputStream(file);
    }

    private static class LogStream extends PrintStream {
        private final PrintStream stdOut;

        LogStream(PrintStream stdOut) {
            super(null, true);
            this.stdOut = stdOut;
        }

        LogStream(Path file, PrintStream stdOut) throws IOException {
            super(newOutputStream(file), true);
            this.stdOut = stdOut;
        }

        void println(Throwable e) {
            if (e == null)
                return;
            println("Exception thrown: " + e.getClass().getName() + ": " + e.getMessage());
            for (StackTraceElement ste : e.getStackTrace())
                println("\tat " + ste.toString());
        }

        @Override
        public void println() {
            if (super.out != null)
                super.println(prefix());
            stdOut.println(prefix());
        }

        @Override
        public void println(String x) {
            if (super.out != null) {
                super.print(prefix());
                super.println(x);
            }
            stdOut.print(prefix());
            stdOut.println(x);
        }

        @Override
        public void println(boolean x) {
            if (super.out != null) {
                super.print(prefix());
                super.println(x);
            }
            stdOut.print(prefix());
            stdOut.println(x);
        }

        @Override
        public void println(char x) {
            if (super.out != null) {
                super.print(prefix());
                super.println(x);
            }
            stdOut.print(prefix());
            stdOut.println(x);
        }

        @Override
        public void println(int x) {
            if (super.out != null) {
                super.print(prefix());
                super.println(x);
            }
            stdOut.print(prefix());
            stdOut.println(x);
        }

        @Override
        public void println(long x) {
            if (super.out != null) {
                super.print(prefix());
                super.println(x);
            }
            stdOut.print(prefix());
            stdOut.println(x);
        }

        @Override
        public void println(double x) {
            if (super.out != null) {
                super.print(prefix());
                super.println(x);
            }
            stdOut.print(prefix());
            stdOut.println(x);
        }

        @Override
        public void println(float x) {
            if (super.out != null) {
                super.print(prefix());
                super.println(x);
            }
            stdOut.print(prefix());
            stdOut.println(x);
        }

        @Override
        public void println(char[] x) {
            if (super.out != null) {
                super.print(prefix());
                super.println(x);
            }
            stdOut.print(prefix());
            stdOut.println(x);
        }

        @Override
        public void println(Object x) {
            if (super.out != null) {
                super.print(prefix());
                super.println(x);
            }
            stdOut.print(prefix());
            stdOut.println(x);
        }

        @Override
        public PrintStream printf(String format, Object... args) {
            if (super.out != null) {
                super.print(prefix());
                super.printf(format, args);
            }
            stdOut.print(prefix());
            return stdOut.printf(format, args);
        }
    }
}
