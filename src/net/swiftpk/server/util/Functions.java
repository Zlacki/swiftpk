package net.swiftpk.server.util;

public class Functions {
    private Functions() {  }
    public static void sleep() {
        try {
            if(Thread.currentThread().getName().toLowerCase().contains("game"))
                return;
            Thread.sleep(600);
        } catch(Exception e) {
            Logger.err("Exception thrown while sleeping thread:", e);
        }
    }

    public static void sleep(long ms) {
        try {
            if(Thread.currentThread().getName().toLowerCase().contains("game"))
                return;
            Thread.sleep(ms);
        } catch(Exception e) {
            Logger.err("Exception thrown while sleeping thread:", e);
        }
    }
}
