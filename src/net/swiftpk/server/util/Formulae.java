package net.swiftpk.server.util;

import java.text.DecimalFormat;
import java.util.Random;
import java.util.concurrent.atomic.AtomicIntegerArray;

import net.swiftpk.server.Server;
import net.swiftpk.server.model.Entity;
import net.swiftpk.server.model.Player;
import net.swiftpk.server.model.component.SkillSet;
import net.swiftpk.server.model.component.SkillSet.Skill;
import net.swiftpk.server.model.entity.GameObject;
import net.swiftpk.server.model.entity.Mob;
import net.swiftpk.server.model.entity.NPC;
import net.swiftpk.server.model.terrain.tiles.Point;

public class Formulae {
    public static double addPrayers(boolean first, boolean second, boolean third) {
        if (third)
            return 1.15D;
        if (second)
            return 1.1D;
        if (first)
            return 1.05D;
        return 1.0D;
    }

    public static int bitToBoundaryDir(int bit) {
        switch (bit) {
            case 1:
                return 0;
            case 2:
                return 1;
            default:
                return -1;
        }
    }

    public static int bitToObjectDir(int bit) {
        switch (bit) {
            case 1:
                return 6;
            case 2:
                return 0;
            case 4:
                return 2;
            case 8:
                return 4;
            default:
                return -1;
        }
    }

    public static int calcSpellHit(int spellStr, int magicEquip) {
        int mageRatio = (int) (45D + magicEquip);
        int max = spellStr;
        int peak = (int) ((spellStr / 100D) * mageRatio);
        int dip = (int) ((peak / 3D) * 2D);
        return DataConversions.randomWeighted(0, dip, peak, max);
    }

    /**
     * Gets a gaussian distributed randomized value between 0 and the
     * {@code maximum} value. <br>
     * The mean (average) is maximum / 2.
     *
     * @param meanModifier The modifier used to determine the mean.
     * @param r            The random instance.
     * @param maximum      The maximum value.
     * @return The randomized value.
     */
    private static double getGaussian(double meanModifier, Random r, int maximum) {
        return getGaussian(meanModifier, r, (double) maximum);
    }

    private static double getGaussian(double meanModifier, Random r, double maximum) {
        double mean = maximum * meanModifier;
        double deviation = mean * 1.79;
//          double mean = maximum * 0.5;
//          double deviation = mean * 0.5;
        double value = 0;
        do {
            value = Math.floor(r.nextGaussian() * deviation + mean);
        } while (value < 0 || value > maximum);

        return value;
    }

    /**
     * Gets the current damage to be dealt to the victim.
     *
     * @param source The attacking mob.
     * @param victim The mob being attacked.
     * @return The amount to hit.
     */
    static int getDamage(Mob source, Mob victim) {
        return getDamage(source, victim, 1.0, 1.0, 1.0);
    }

    /**
     * Gets the current melee damage.
     *
     * @param source             The attacking mob.
     * @param victim             The mob being attacked.
     * @param accuracyMultiplier The amount to increase the accuracy with.
     * @param hitMultiplier      The amount to increase the hit with.
     * @param defenceMultiplier  The amount to increase the defence with.
     * @return The amount to hit.
     */
    private static int getDamage(Mob source, Mob victim, double accuracyMultiplier, double hitMultiplier,
                                 double defenceMultiplier) {
        double acc = getMeleeAccuracy(source);
        double def = getMeleeDefence(victim);
//          int maxHit = getMeleeDamage(source, hitMultiplier);
        int maxHit = calculateMaxCombatHit(source.getModifiers()[SkillSet.STRENGTH],
                source.getSkillSet().get(SkillSet.STRENGTH).getLevel(), source.getFightMode(),
                source.getWeaponPowerPoints());

        if (acc * 10 < def) // Defense bonus is >= 10x accuracy - why would this hit?
            return 0;

        int finalAccuracy;
        if (acc > def)
            finalAccuracy = (int) ((1.0 - ((def + 2.0) / (2.0 * (acc + 1.0)))) * 10000.0);
        else
            finalAccuracy = (int) ((acc / (2.0 * (def + 1.0))) * 10000.0);

        if (finalAccuracy > RandomUtil.inclusive(10000)) {
            return (int) getGaussian(1.0, RandomUtil.getRandom(), maxHit);
        }
        return 0;
    }

    private static double getMeleeDefence(Mob defender) {
        int styleBonus = styleBonus(defender, SkillSet.DEFENSE);
        double prayerBonus = defender.getModifiers()[SkillSet.DEFENSE];

        int defenseLevel = (int) (defender.getSkillSet().get(SkillSet.DEFENSE).getLevel() * prayerBonus) + styleBonus + 8;
        double bonusMultiplier = (double) (defender.getArmourPoints() + 64);

        if (defender instanceof NPC)
            bonusMultiplier *= 0.9;

        return (defenseLevel * bonusMultiplier);
    }

    private static double getMeleeAccuracy(Mob attacker) {
        int styleBonus = styleBonus(attacker, SkillSet.ATTACK);
        double prayerBonus = attacker.getModifiers()[SkillSet.ATTACK];

        int attackLevel = (int) (attacker.getSkillSet().get(SkillSet.ATTACK).getLevel() * prayerBonus) + styleBonus + 8;
        double bonusMultiplier = (double) (attacker.getWeaponAimPoints() + 64);

        if (attacker instanceof NPC)
            bonusMultiplier *= 0.9;

        return (attackLevel * bonusMultiplier);
    }

    public static int calculateCombatHit(Mob att, Mob def) {
        int damage = getDamage(att, def);
        int hits = def.getSkillSet().get(SkillSet.HITPOINTS).getLevel();
        if (hits > 0 && damage > hits)
            damage = hits;
        return damage;
    }

    public static int calculateMaxCombatHit(double strMod, int str, int fightMode, int wepPower) {
        final int[] FIGHT_MODE_STR_BONUSES = new int[]{1, 3, 0, 0};
        double rawResult = ((strMod * str) + FIGHT_MODE_STR_BONUSES[fightMode]) * ((wepPower * 0.00175) + 0.1) + 1.05;
        return (int) Math.ceil(rawResult);
    }

    public static int calculateMaxRangeHit(double arrowMod, double bowMod, int rng) {
        double rawResult = (bowMod * arrowMod * rng) * 0.1;
        return (int) Math.ceil(rawResult);
    }

    public static int calculateRangeHit(Mob att, Mob def, double arrowMod, double bowMod) {
        final int[] FIGHT_MODE_DEF_BONUSES = new int[]{1, 0, 0, 3};
        Skill range = att.getSkillSet().get(SkillSet.RANGE);
        int max = calculateMaxRangeHit(arrowMod, bowMod, range.getLevel());
        int ourAttack = range.getLevel();
        double wepAim = att.getRangePoints() * 2.5;
        Skill defense = def.getSkillSet().get(SkillSet.DEFENSE);
        int enemyDef = ((int) (defense.getLevel() * def.getModifiers()[1])) + FIGHT_MODE_DEF_BONUSES[def.getFightMode()];
        double defPoints = def.getArmourPoints() * Server.getServer().getConf().getDouble("armourdefcoefficient");
        double probabilityOfHit = ((ourAttack + wepAim) - (enemyDef + defPoints)) / 100
                + Server.getServer().getConf().getDouble("basehitprobability");
        double rand = Math.random();
        int hit = 0;
        if (probabilityOfHit < ourAttack * 0.001)
            probabilityOfHit = ourAttack * 0.001;
        if (probabilityOfHit > rand)
            hit = (int) (Math.random() * max);
        return hit;
    }

    public static int calculateSpellHit(Player p, Mob target, int spellDmg) {
        double probabilityOfHit = ((p.getSkillSet().get(SkillSet.MAGIC).getLevel()
                + Server.getServer().getConf().getDouble("magicpointscoefficient") * p.getMagicPoints())
                - (target.getSkillSet().get(SkillSet.DEFENSE).getLevel()
                * Server.getServer().getConf().getDouble("magictargetdefcoefficient")
                + Server.getServer().getConf().getDouble("magicpointscoefficient") * target.getMagicPoints()))
                / 100;
        double rand = Math.random();
        int hit = 0;
        if (probabilityOfHit < 0.015)
            probabilityOfHit = 0.015;
        if (probabilityOfHit > rand)
            hit = (int) (Math.random() * spellDmg);
        return hit;
    }

    public static String delayToString(long delay) {
        int seconds = (int) (delay / 1000);
        int minutes = seconds / 60;
        int hours = minutes / 60;
        int days = hours / 24;
        String dayStr = "";
        if (days > 0)
            dayStr = days + " days, ";
        return dayStr + df2.format(hours % 24) + ":" + df2.format(minutes % 60) + ":" + df2.format(seconds % 60);
    }

    public static boolean boundaryAtFacing(Entity e, int x, int y, int dir) {
        if (dir >= 0 && e instanceof GameObject) {
            GameObject obj = (GameObject) e;
            return obj.getType() == 1 && obj.getDirection() == dir && obj.isOn(x, y);
        }
        return false;
    }

    public static int experienceToLevel(long exp) {
        for (int level = 0; level < MAX_EXP_ARRAY_SIZE - 1; level++)
            if (exp < experienceArray[level])
                return level + 1;

        return MAX_EXP_ARRAY_SIZE;
    }

    public static int getCombatLevel(AtomicIntegerArray stats) {
        float attack = stats.get(0) + stats.get(2);
        float defense = stats.get(1) + stats.get(3);
        float mage = (stats.get(5) + stats.get(6)) / 8;
        float range = stats.get(4);
        if (attack < (range * 1.5)) {
            return (int) ((defense / 4) + (range * 0.375) + mage);
        } else {
            return (int) ((attack / 4) + (defense / 4) + mage);
        }
    }

    public static int getCombatLevel(Mob mob) {
        float attack = mob.getSkillSet().get(SkillSet.ATTACK).getMaxLevel() + mob.getSkillSet().get(SkillSet.STRENGTH).getMaxLevel();
        float defense = mob.getSkillSet().get(SkillSet.DEFENSE).getMaxLevel()
                + mob.getSkillSet().get(SkillSet.HITPOINTS).getMaxLevel();
        float mage = (mob.getSkillSet().get(SkillSet.PRAYER).getMaxLevel() + mob.getSkillSet().get(SkillSet.MAGIC).getMaxLevel()) / 8;
        float range = mob.getSkillSet().get(SkillSet.RANGE).getMaxLevel();
        if (attack < (range * 1.5)) {
            return (int) ((defense / 4) + (range * 0.375) + mage);
        } else {
            return (int) ((attack / 4) + (defense / 4) + mage);
        }
    }

    public static int getDirection(Entity you, Entity them) {
        if (you.getX() < them.getX() && you.getY() < them.getY())
            return 3; // southwest
        else if (you.getX() < them.getX() && you.getY() > them.getY())
            return 1; // northwest
        else if (you.getX() > them.getX() && you.getY() > them.getY())
            return 7; // northeast
        else if (you.getX() > them.getX() && you.getY() < them.getY())
            return 5; // southeast
        else if (you.getX() > them.getX())
            return 6; // east
        else if (you.getX() < them.getX())
            return 2; // west
        else if (you.getY() < them.getY())
            return 4; // south
        else if (you.getY() > them.getY())
            return 0; // north
        return -1;
    }

    public static int getHeight(Point location) {
        return (int) Math.floor((location.getY() + 100) / 944);
    }

    public static long getHitsExp(long... exps) {
        return (exps[0] + exps[1] + exps[2]) / 3 + 1154;
    }

    public static long levelToExperience(int lvl) {
        int lvlArrayIndex = lvl - 2;
        if (lvlArrayIndex < 0 || lvlArrayIndex > experienceArray.length)
            return 0;
        return experienceArray[lvlArrayIndex];
    }

    public static boolean objectAtFacing(Entity e, int x, int y, int dir) {
        if (dir >= 0 && e instanceof GameObject) {
            GameObject obj = (GameObject) e;
            return obj.getType() == 0 && obj.getDirection() == dir && obj.isOn(x, y);
        }
        return false;
    }

    /*
     * public static int calculateCombatHit(Mob att, Mob def) { final int[]
     * FIGHT_MODE_ATT_BONUSES = new int[] { 1, 0, 3, 0 }; final int[]
     * FIGHT_MODE_DEF_BONUSES = new int[] { 1, 0, 0, 3 }; final double[] mods =
     * att.getModifiers(); int max = calculateMaxCombatHit(mods[2],
     * att.getComponent(SkillSet.class) .getCurStat(2), att.getFightMode(),
     * att.getWeaponPowerPoints()); int ourAttack = ((int)
     * (att.getComponent(SkillSet.class).getCurStat(0) * mods[0])) +
     * FIGHT_MODE_ATT_BONUSES[att.getFightMode()]; double wepAim =
     * att.getWeaponAimPoints() Server.getServer().getConf()
     * .getDouble("combat.wepaimcoefficient"); int enemyDef = ((int)
     * (def.getComponent(SkillSet.class).getCurStat(1) * def .getModifiers()[1])) +
     * FIGHT_MODE_DEF_BONUSES[def.getFightMode()]; double defPoints =
     * def.getArmourPoints() Server.getServer().getConf()
     * .getDouble("combat.armourdefcoefficient"); double probabilityOfHit =
     * ((ourAttack + wepAim) - (enemyDef + defPoints)) / 100 +
     * Server.getServer().getConf() .getDouble("combat.basehitprobability"); double
     * rand = Math.random(); double maxrand = Math.random(); int hit = 0; //
     * Logger.log("["+(att instanceof Player ? "Plyr" : "NPC" // )+"]\thitRate="
     * +(probabilityOfHit*100)+"%\tatt="+ourAttack+"\twepAim="+wepAim+"\t" // + //
     * "def="+enemyDef+"\tdefPoints="+defPoints); if (probabilityOfHit < ourAttack *
     * 0.001) probabilityOfHit = ourAttack * 0.001; if (probabilityOfHit > 1)
     * maxrand += Math.random() * ((probabilityOfHit - 1) / 10); if (maxrand > 1)
     * maxrand = 1; if (probabilityOfHit > rand) hit = (int) (maxrand * max); return
     * hit; }
     */
    public static int styleBonus(Mob mob, int skill) {
        int style = mob.getFightMode();
        if (style == 0) {
            return 1;
        }
        return (skill == 0 && style == 2) || (skill == 1 && style == 3) || (skill == 2 && style == 1) ? 3 : 0;
    }

    public static String timeSince(long time) {
        return delayToString(System.currentTimeMillis() - time);
    }

    public static String timeUntil(long time) {
        return delayToString(time - System.currentTimeMillis());
    }

    private static final DecimalFormat df2 = new DecimalFormat("00");
    public static long[] experienceArray;
    public static final int MAX_EXP_ARRAY_SIZE = 99;
    public static int[] MAX_STATS;
    public static final String[] statArray = {"attack", "defense", "strength", "hits", "ranged", "prayer", "magic",
            "cooking", "woodcut", "fletching", "fishing", "firemaking", "crafting", "smithing", "mining", "herblaw",
            "agility", "thieving"};
    public static final Point[] noremoveTiles = {new Point(341, 487, 0), new Point(343, 581, 0), new Point(92, 649, 0),
            new Point(434, 682, 0), new Point(660, 551, 0), new Point(196, 3266, 0), new Point(59, 573, 0),
            new Point(560, 472, 0), new Point(140, 180, 0), new Point(285, 195, 0), new Point(243, 178, 0),
            new Point(394, 851, 0), new Point(388, 851, 0), new Point(512, 550, 0)};
    public static final int[] UNDEAD_NPCS = {15, 53, 80, 178, 664, 41, 52, 68, 180, 214, 319, 40, 45, 46, 50, 179,
            195};
    public static final int[] ARMOR_NPCS = {66, 102, 189, 277, 322, 401324, 323, 632, 633};

    static {
        long i = 0;
        experienceArray = new long[MAX_EXP_ARRAY_SIZE + 5];
        for (int j = 0; j < MAX_EXP_ARRAY_SIZE + 5; j++) {
            int k = j + 1;
            long i1 = (long) ((double) k + 300D * Math.pow(2D, (double) k / 7D));
            i += i1;
            experienceArray[j] = (i & 0xfffffffc) / 4;
        }
    }
}
