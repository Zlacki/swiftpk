package net.swiftpk.server.util.parser;

import com.google.common.collect.ImmutableList;
import com.google.gson.JsonObject;

import net.swiftpk.server.kplugin.event.Event;
import net.swiftpk.server.model.Player;
import net.swiftpk.server.net.Packet;
import net.swiftpk.server.net.PacketReader;
import net.swiftpk.server.net.PacketRepository;

import java.lang.reflect.Field;

/**
 * A {@link JsonFileParser} implementation that parses incoming packet listener
 * metadata.
 */
public final class PacketRepositoryFileParser extends JsonFileParser<PacketReader> {

    /**
     * A default implementation of a {@link GamePacketReader}. It does nothing.
     */
    private static final class DefaultPacketReader extends PacketReader {

        @Override
        public Event read(Player player, Packet msg) throws Exception {
            return null;
        }
    }

    /**
     * The directory of incoming packet listeners.
     */
    private static final String DIR = "net.swiftpk.server.net.in.";

    /**
     * The packet repository.
     */
    private final PacketRepository repository;

    /**
     * Creates a new {@link PacketRepositoryFileParser}.
     *
     * @param repository The packet repository.
     */
    public PacketRepositoryFileParser(PacketRepository repository) {
        super("./data/packet_repo.json");
        this.repository = repository;
    }

    @Override
    public PacketReader convert(JsonObject token) throws Exception {
        int opcode = token.get("opcode").getAsInt();
        String className = token.has("payload") ? token.get("payload").getAsString() : null;
        return createReader(opcode, className);
    }

    @Override
    public void onCompleted(ImmutableList<PacketReader> tokenObjects) throws Exception {
        tokenObjects.forEach(repository::put);
        repository.lock();
    }

    /**
     * Creates a new {@link PacketReader} using reflection.
     *
     * @param opcode    The opcode.
     * @param size      The size.
     * @param className The simple class name.
     * @return The packet listener instance.
     * @throws ReflectiveOperationException If any errors occur while creating the
     *                                      listener instance.
     */
    private PacketReader createReader(int opcode, String className) throws ReflectiveOperationException {

        // Create class and instance from qualified name.
        Object readerInstance = className != null
                ? Class.forName(DIR + className).getDeclaredConstructor().newInstance()
                : new DefaultPacketReader();

        // Retrieve opcode and size fields.
        Class<?> readerClass = readerInstance.getClass().getSuperclass();
        Field opcodeField = readerClass.getDeclaredField("opcode");

        // Make them accessible.
        opcodeField.setAccessible(true);

        // Reflectively set the values.
        opcodeField.setInt(readerInstance, opcode);

        return (PacketReader) readerInstance;
    }
}