package net.swiftpk.server.util;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.security.MessageDigest;

import net.swiftpk.server.model.terrain.tiles.Point;

public final class DataConversions {
    public static String byteToString(byte abyte0[], int i, int j) {
        try {
            char[] aCharArray560 = new char[100];
            int k = 0;
            int l = -1;
            for (int i1 = 0; i1 < j; i1++) {
                int j1 = abyte0[i++] & 0xff;
                int k1 = j1 >> 4 & 0xf;
                if (l == -1) {
                    if (k1 < 13) {
                        aCharArray560[k++] = aCharArray561[k1];
                    } else {
                        l = k1;
                    }
                } else {
                    aCharArray560[k++] = aCharArray561[((l << 4) + k1) - 195];
                    l = -1;
                }
                k1 = j1 & 0xf;
                if (l == -1) {
                    if (k1 < 13) {
                        aCharArray560[k++] = aCharArray561[k1];
                    } else {
                        l = k1;
                    }
                } else {
                    aCharArray560[k++] = aCharArray561[((l << 4) + k1) - 195];
                    l = -1;
                }
            }
            boolean flag = true;
            for (int l1 = 0; l1 < k; l1++) {
                char c = aCharArray560[l1];
                if (l1 > 4 && c == '@') {
                    aCharArray560[l1] = ' ';
                }
                if (c == '%') {
                    aCharArray560[l1] = ' ';
                }
                if (flag && c >= 'a' && c <= 'z') {
                    aCharArray560[l1] += '\uFFE0';
                    flag = false;
                }
                if (c == '.' || c == '!' || c == ':') {
                    flag = true;
                }
            }
            return new String(aCharArray560, 0, k);
        } catch (Exception _ex) {
            Logger.err("Exception caught while trying to convert a byte array to an RSC string:", _ex);
            return "Error converting string.";
        }
    }

    private static String convertToHex(byte[] data) {
        StringBuffer buf = new StringBuffer();
        for (int i = 0; i < data.length; i++) {
            int halfbyte = (data[i] >>> 4) & 0x0F;
            int two_halfs = 0;
            do {
                if ((0 <= halfbyte) && (halfbyte <= 9)) {
                    buf.append((char) ('0' + halfbyte));
                } else {
                    buf.append((char) ('a' + (halfbyte - 10)));
                }
                halfbyte = data[i] & 0x0F;
            } while (two_halfs++ < 1);
        }
        return buf.toString();
    }

    private static int getCharCode(char c) {
        for (int x = 0; x < aCharArray561.length; x++) {
            if (c == aCharArray561[x]) {
                return x;
            }
        }
        return 0;
    }

    public static String hashToUsername(long l) {
        return NameUtil.decodeBase37(l);
    }

    public static boolean inArray(int[] haystack, int needle) {
        for (int option : haystack) {
            if (needle == option) {
                return true;
            }
        }
        return false;
    }

    public static boolean inPointArray(Point[] haystack, Point needle) {
        for (Point option : haystack) {
            if (needle.getX() == option.getX() && needle.getY() == option.getY()) {
                return true;
            }
        }
        return false;
    }

    public static long IPToLong(String ip) {
        String[] octets = ip.split("\\.");
        long result = 0L;
        for (int x = 0; x < 4; x++) {
            result += Integer.parseInt(octets[x]) * Math.pow(256, 3 - x);
        }
        return result;
    }

    public static int random(int low, int high) {
        return RandomUtil.inclusive(low, high);
    }

    /**
     * Makes a wave that starts at @param low and works its way up to @param max, with parameters to control
     * how likely certain values between these numbers are to be selected.
     * <p>
     * Probability for hitting each of the numbers starts at 100 in the wave, decreases by 3
     * for every every whole number, e.g probability for 0 is 100, 1 usually is 97, 2 is 94, until it
     * reaches @param dip, then it will begin to increase probability by 3 for every number, starting
     * with @param dip, until it reaches @param peak.  Any numbers after @param peak will begin to decrease
     * probability by 3 again, until reaching @param max and the function ending.
     * <p>
     *
     * @param low  The start of the wave.
     * @param dip  The point where the wave will begin increasing probability.
     * @param peak The point where the wave will begin decreasing probability again.
     * @param max  The end of the wave.
     * @return A random number between @param low and @param max, where you can control how likely certain numbers are.
     */
    public static int randomWeighted(int low, int dip, int peak, int max) {
        int total = 0;
        int probability = 100;
        int[] probArray = new int[max + 1];
        for (int x = 0; x < probArray.length; x++) {
            total += probArray[x] = probability;
            if (low + x < dip || low + x > peak) {
                probability -= 3;
            } else {
                probability += 3;
            }
        }
        int hit = RandomUtil.inclusive(total);
        total = 0;
        for (int x = 0; x < probArray.length; x++) {
            if (hit >= total && hit < (total + probArray[x])) {
                return low + x;
            }
            total += probArray[x];
        }
        return 0;
    }

    public static int roundUp(double val) {
        return (int) Math.round(val + 0.5D);
    }

    public static String sha1(String text) {
        byte[] sha1hash = new byte[40];
        md.update(text.getBytes(), 0, text.length());
        sha1hash = md.digest();
        md.reset();
        return convertToHex(sha1hash);
    }

    public static final ByteBuffer streamToBuffer(BufferedInputStream in) throws IOException {
        byte[] buffer = new byte[in.available()];
        in.read(buffer, 0, buffer.length);
        return ByteBuffer.wrap(buffer);
    }

    public static final ByteBuffer streamToBuffer(byte[] buffer) throws IOException {
        return ByteBuffer.wrap(buffer);
    }

    public static byte[] stringToByteArray(String message) {
        byte[] buffer = new byte[100];
        if (message.length() > 80) {
            message = message.substring(0, 80);
        }
        message = message.toLowerCase();
        int length = 0;
        int j = -1;
        for (int k = 0; k < message.length(); k++) {
            int code = getCharCode(message.charAt(k));
            if (code > 12) {
                code += 195;
            }
            if (j == -1) {
                if (code < 13)
                    j = code;
                else
                    buffer[length++] = (byte) code;
            } else if (code < 13) {
                buffer[length++] = (byte) ((j << 4) + code);
                j = -1;
            } else {
                buffer[length++] = (byte) ((j << 4) + (code >> 4));
                j = code & 0xf;
            }
        }
        if (j != -1) {
            buffer[length++] = (byte) (j << 4);
        }
        byte[] string = new byte[length];
        System.arraycopy(buffer, 0, string, 0, length);
        return string;
    }

    public static String username(String s) {
        return hashToUsername(usernameToHash(s));
    }

    public static long usernameToHash(String s) {
        return NameUtil.encodeBase37(s);
    }

    private static final char[] aCharArray561 = {' ', 'e', 't', 'a', 'o', 'i', 'h', 'n', 's', 'r', 'd', 'l', 'u', 'm',
            'w', 'c', 'y', 'f', 'g', 'p', 'b', 'v', 'k', 'x', 'j', 'q', 'z', '0', '1', '2', '3', '4', '5', '6', '7',
            '8', '9', ' ', '!', '?', '.', ',', ':', ';', '(', ')', '-', '&', '*', '\\', '\'', '@', '#', '+', '=',
            '\243', '$', '%', '"', '[', ']'};
    private static MessageDigest md;

    static {
        try {
            md = MessageDigest.getInstance("SHA1");
        } catch (Exception e) {
            Logger.err("Exception caught while trying to get the SHA1 MessageDigest instance:", e);
        }
    }

    public static boolean percentChance(int percent) {
        return RandomUtil.inclusive(1, 100) <= percent;
    }
}
