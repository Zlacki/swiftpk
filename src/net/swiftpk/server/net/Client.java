package net.swiftpk.server.net;

import java.util.Queue;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.atomic.AtomicReference;

import io.netty.channel.Channel;
import io.netty.util.AttributeKey;
import net.swiftpk.server.Server;
import net.swiftpk.server.model.MobState;
import net.swiftpk.server.model.Player;
import net.swiftpk.server.model.terrain.World;
import net.swiftpk.server.plugin.PluginManager;
import net.swiftpk.server.plugin.listeners.actors.players.PlayerLogoutListener;
import net.swiftpk.server.plugin.listeners.packets.PacketListener;
import net.swiftpk.server.util.Logger;

public final class Client {

    /**
     * An attribute key used to retrieve the client instance.
     */
    public static final AttributeKey<Client> KEY = AttributeKey.valueOf("Client.key");
    private final AtomicReference<Player> owner = new AtomicReference<>();
    private final Queue<Packet> packets = new ArrayBlockingQueue<>(15);
    private final AtomicReference<Long> serverKey = new AtomicReference<>();
    private final Channel channel;

    public Client(Channel channel) {
        this.channel = channel;
    }

    public Player getOwner() {
        return owner.get();
    }

    public void setOwner(Player p) {
        owner.set(p);
    }

    public AtomicReference<Long> getServerKey() {
        return serverKey;
    }

    public Channel getChannel() {
        return channel;
    }

    public void process() {
        for (; ; ) {
            Packet packet = packets.poll();
            if (packet == null)
                break;

            getOwner().ping();
            PacketReader reader = Server.getServer().getPacketRepository().get(packet.getID());
            reader.postEvent(getOwner(), packet);

            PacketListener ph = PluginManager.get(PacketListener.class, packet);
            if (ph != null) {
                try {
                    ph.execute(packet, channel);
                    Logger.out("Packet passed to Java plugin: (handler='" + ph.getClass().getSimpleName() + "',opcode='" + packet.getID() + "')");
                } catch (Exception e) {
                    Logger.err("Exception from Java plugin caught while reading a packet: (handler='" + ph.getClass().getSimpleName() + "',opcode='" + packet.getID() + "')", e);
                }
            }
        }
    }

    public void pushToMessageQueue(Packet packet) {
        packets.offer(packet);
    }

    public void unregister() {
        if (!getOwner().loggedIn())
            return;
        getOwner().setLoggedIn(false);
        World.getInstance().getPlayers().remove(getOwner().getIndex());
        Logger.out("[LogoutEvent] '" + getOwner().getCredentials().getUsername() + "' has logged out.");
        getOwner().save();
        for (Player p1 : World.getInstance().getPlayers())
            p1.alertOfLogout(getOwner());

        for (PlayerLogoutListener listener : PluginManager.getAll(PlayerLogoutListener.class))
            listener.logout(getOwner());
        if (getOwner().getCombatPartner() != null)
            getOwner().getCombatPartner().setNotFighting(false);
        World.getInstance().setLocation(getOwner(), getOwner().getLocation(), null);
        getOwner().setState(MobState.Dead);
        channel.close();
    }
}
