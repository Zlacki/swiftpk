package net.swiftpk.server.net;

import com.google.common.collect.ConcurrentHashMultiset;
import com.google.common.collect.Multiset;

import java.net.InetSocketAddress;

import io.netty.channel.Channel;
import io.netty.channel.ChannelHandler.Sharable;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import net.swiftpk.server.util.Logger;

/**
 * The {@link ChannelInboundHandlerAdapter} implementation that will filter out
 * unwanted connections from propagating down the pipeline.
 *
 * @author Zachariah Knight
 */
@Sharable
public class ClassicChannelFilter extends ChannelInboundHandlerAdapter {
    /**
     * The {@link Multiset} of connections currently active within the server.
     */
    private final Multiset<String> connections = ConcurrentHashMultiset.create();

    /**
     * Gets the host address of the user logging in.
     *
     * @return The host address of this connection.
     */
    public static String getHost(Channel channel) {
        return ((InetSocketAddress) channel.remoteAddress()).getAddress().getHostAddress();
    }

    @Override
    public void channelRegistered(ChannelHandlerContext ctx) throws Exception {
        String host = getHost(ctx.channel());

        // if this local then, do nothing and proceed to next handler in the pipeline.


        // add the host
        connections.add(host);

        // evaluate the amount of connections from this host.
        if (connections.count(host) > 10) {
            Logger.outf("Blocked new connection on host {}", host);
            ctx.writeAndFlush(new PacketBuilder().setBare(true).addByte((byte) 10).toPacket());
            ctx.close();
            return;
        }

        // CHECK BANS

        // Nothing went wrong, so register the channel and forward the event to next
        // handler in the
        // pipeline.

        ctx.fireChannelRegistered();
    }

    @Override
    public void channelUnregistered(ChannelHandlerContext ctx) throws Exception {
        String host = getHost(ctx.channel());


        // remove the host from the connection list
        connections.remove(host);

        // the connection is unregistered so forward the event to the next handler in
        // the pipeline.
        ctx.fireChannelUnregistered();
    }

}
