package net.swiftpk.server.net;

import com.google.common.collect.Iterators;
import com.google.common.collect.UnmodifiableIterator;

import static com.google.common.base.Preconditions.checkState;

/**
 * An {@link Iterable} implementation containing mappings of {@link PacketReader}s to their opcode
 * identifiers, and functions to retrieve said message readers.
 */
public final class PacketRepository implements Iterable<PacketReader> {

    /**
     * If this repository is locked.
     */
    private volatile boolean locked;

    /**
     * An array of message readers. This array is effectively immutable since it is never exposed
     * outside of this class.
     */
    private final PacketReader[] readers = new PacketReader[257];

    /**
     * Adds a reader to this repository.
     *
     * @param messageReader The reader to add.
     * @throws IllegalStateException If this repository is locked.
     */
    public void put(PacketReader messageReader) throws IllegalStateException {
        checkState(!locked, "This repository is locked.");

        int opcode = messageReader.getOpcode();
        checkState(get(opcode) == null, "Reader already exists for opcode [" + opcode + "].");
        readers[opcode] = messageReader;
    }

    /**
     * Retrieves a reader from this repository.
     *
     * @param opcode The opcode of the reader to retrieve.
     * @return The reader.
     */
    public PacketReader get(int opcode) {
        return readers[opcode];
    }

    /**
     * Locks this repository, making it read-only. Has no effect if already locked.
     */
    public void lock() {
        locked = true;
    }

    @Override
    public UnmodifiableIterator<PacketReader> iterator() {
        return Iterators.forArray(readers);
    }
}
