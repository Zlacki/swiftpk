package net.swiftpk.server.net;

import io.netty.channel.ChannelPipeline;

/**
 * An immutable model representing data that will be used to prepare a
 * {@link Player} for gameplay.
 */
public final class LoginRequestPacket {
    /**
     * The username for the {@link Player}.
     */
    private final String username;

    /**
     * The password for the {@link Player}.
     */
    private final String password;

    private final int clientVersion;
    private final boolean reconnecting;

    /**
     * The ISAAC cipher to decode incoming packets with.
     */
    private final ISAACCipher inCipher;

    /**
     * The ISAAC cipher to encode incoming packets with.
     */
    private final ISAACCipher outCipher;

    /**
     * The channel pipeline.
     */
    private final ChannelPipeline pipeline;

    public LoginRequestPacket(String username, String password, ISAACCipher inCipher, ISAACCipher outCipher, int clientVersion, boolean reconnecting, ChannelPipeline pipeline) {
        this.username = username;
        this.password = password;
        this.inCipher = inCipher;
        this.outCipher = outCipher;
        this.clientVersion = clientVersion;
        this.reconnecting = reconnecting;
        this.pipeline = pipeline;
    }

    public ChannelPipeline getPipeline() {
        return pipeline;
    }

    public ISAACCipher getOutCipher() {
        return outCipher;
    }

    public ISAACCipher getInCipher() {
        return inCipher;
    }

    public String getPassword() {
        return password;
    }

    public String getUsername() {
        return username;
    }

    public int getClientVersion() {
        return clientVersion;
    }

    public boolean isReconnecting() {
        return reconnecting;
    }
}