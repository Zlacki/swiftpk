package net.swiftpk.server.net.codec;

import java.util.List;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ByteToMessageDecoder;
import net.swiftpk.server.net.Packet;
import net.swiftpk.server.util.Logger;

public final class RSCProtocolDecoder extends ByteToMessageDecoder { // FrameDecoder
    /**
     * Parses the data in the provided byte buffer and writes it to
     * <code>out</code> as a <code>RSCPacket</code>.
     *
     * @param ctx     The context of the channel handler
     * @param msg     The raw data received from the Client.
     * @param out     The shared collection to send read Packet objects back up the pipeline.
     */
    @Override
    protected void decode(ChannelHandlerContext ctx, ByteBuf msg, List<Object> out) {
        try {
            msg.markReaderIndex();
            if (msg.readableBytes() >= 2) {
                int length = msg.readUnsignedByte();
                byte lastByte = 0;
                if (length >= 160)
                    length = (length - 160) * 256 + msg.readUnsignedByte();
                if (length <= msg.readableBytes()) {
                    if (length < 160 && length > 1) {
                        lastByte = msg.readByte();
                        length--;
                    }
                    byte[] payload = new byte[length];
                    int id = (msg.readUnsignedByte() & 0xFF);
                    length--;
                    if (length >= 160)
                        msg.readBytes(payload, 0, length);
                    else if (length > 0)

                        msg.readBytes(payload, 0, length);
                    if (length < 160)
                        payload[length] = lastByte;
                    out.add(new Packet(id, payload));
                } else {
                    Logger.err("Couldn't decode packet header:" + length + " > " + msg.readableBytes());
                    msg.resetReaderIndex();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
