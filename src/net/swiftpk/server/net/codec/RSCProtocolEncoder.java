package net.swiftpk.server.net.codec;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToByteEncoder;
import net.swiftpk.server.net.Packet;

/**
 * Encodes the high level <code>RSCPacket</code> class into the proper protocol
 * data required for transmission.
 */
public final class RSCProtocolEncoder extends MessageToByteEncoder<Packet> { // OneToOneEncoder
    /**
     * Converts a <code>RSCPacket</code> object into the raw data needed for
     * transmission.
     *
     * @param ctx     The context
     * @param channel The channel
     * @param msg     The message
     */
    @Override
    protected void encode(ChannelHandlerContext ctx, Packet p, ByteBuf out) {
        byte[] data = p.getData();
        int dataLength = data.length;
        int packetLength = data.length + 1;
        if (!p.isBare()) {
            if (packetLength >= 160) {
                out.writeByte((byte) (160 + (packetLength / 256)));
                out.writeByte((byte) (packetLength & 0xFF));
            } else {
                out.writeByte((byte) (packetLength & 0xFF));
                if (dataLength > 0) {
                    dataLength--;
                    out.writeByte(data[dataLength]);
                }
            }
            out.writeByte((byte) (p.getID() & 0xFF));
        }
        out.writeBytes(data, 0, dataLength);
    }
}
