package net.swiftpk.server.net;

import io.netty.channel.ChannelHandler.Sharable;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.timeout.ReadTimeoutHandler;
import net.swiftpk.server.core.ConnectionHandler;
import net.swiftpk.server.net.codec.RSCProtocolDecoder;
import net.swiftpk.server.net.codec.RSCProtocolEncoder;

/**
 * A {@link ChannelInitializer} implementation that will initialize
 * {@link SocketChannel}s before they are registered.
 */
@Sharable
public final class ClassicChannelInitializer extends ChannelInitializer<SocketChannel> {
    @Override
    protected void initChannel(SocketChannel ch) throws Exception {
        ch.attr(Client.KEY).setIfAbsent(new Client(ch));

        ch.pipeline().addLast("read-timeout", new ReadTimeoutHandler(10));
        ch.pipeline().addLast("decoder", new RSCProtocolDecoder());
        ch.pipeline().addLast("encoder", new RSCProtocolEncoder());
        ch.pipeline().addLast("handler", new ConnectionHandler());
    }
}
