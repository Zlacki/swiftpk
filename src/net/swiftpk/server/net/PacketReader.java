package net.swiftpk.server.net;

import net.swiftpk.server.Server;
import net.swiftpk.server.kplugin.event.Event;
import net.swiftpk.server.model.Player;
import net.swiftpk.server.util.Logger;

/**
 * An abstraction model listener that posts events after reading data from
 * decoded game messages.
 */
public abstract class PacketReader {

    /**
     * The opcode.
     */
    protected final int opcode;

    /**
     * Creates a new {@link PacketReader}.
     */
    public PacketReader() {
        // These values are injected using reflection.
        opcode = 0;
    }

    /**
     * Reads a decoded game message and posts an {@link Event} containing the
     * decoded data. A return value of {@code null} indicates that no event needs to
     * be posted.
     *
     * @param player The player.
     * @param msg    The decoded message.
     * @throws Exception If any errors occur.
     */
    public abstract Event read(Player player, Packet msg) throws Exception;

    /**
     * Handles a decoded game message and posts its returned {@link Event}.
     *
     * @param player The player.
     * @param msg    The decoded game message.
     */
    public final void postEvent(Player player, Packet msg) {
        try {
            read(player, msg);
        } catch (Exception e) {
            Logger.err(player.getUsername() + " failed in reading packet: (handler='" + getClass().getSimpleName() + "',opcode='" + msg.getID() + "')", e);
        }
    }

    /**
     * @return The opcode.
     */
    public final int getOpcode() {
        return opcode;
    }
}
