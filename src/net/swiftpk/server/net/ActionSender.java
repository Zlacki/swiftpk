package net.swiftpk.server.net;

import java.util.*;

import net.swiftpk.server.Server;
import net.swiftpk.server.model.InvItem;
import net.swiftpk.server.model.Player;
import net.swiftpk.server.model.QueryMenu;
import net.swiftpk.server.model.Shop;
import net.swiftpk.server.model.component.Bank;
import net.swiftpk.server.model.component.Communication;
import net.swiftpk.server.model.component.Settings;
import net.swiftpk.server.model.component.SkillSet.Skill;
import net.swiftpk.server.model.terrain.World;
import net.swiftpk.server.util.Formulae;

public class ActionSender {
    private ActionSender() {
    }

    public static void sendFightMode(Player player) {
        PacketBuilder builder = new PacketBuilder().setID(132);
        builder.addByte((byte) player.getFightMode());
        player.send(builder.toPacket());
    }

    public static void sendFatigue(Player player) {
        PacketBuilder builder = new PacketBuilder().setID(244);
        builder.addShort(player.getFatigue() / 100);
        player.send(builder.toPacket());
    }

    public static void sendSleepFatigue(Player player) {
        PacketBuilder builder = new PacketBuilder().setID(168);
        builder.addShort(player.getSleepFatigue() / 100);
        player.send(builder.toPacket());
    }

    public static void sendWakeup(Player player) {
        player.send(new PacketBuilder().setID(103).toPacket());
    }

    public static void sendIncorrectSleepWord(Player player) {
        player.send(new PacketBuilder().setID(16).toPacket());
    }

    public static void sendSleepWord(Player player, byte[] data) {
        PacketBuilder builder = new PacketBuilder().setID(219);
        builder.addBytes(data, 0, data.length);
        player.send(builder.toPacket());
    }

    public static void sendAppearanceScreen(Player player) {
        player.send(new PacketBuilder().setID(207).toPacket());
    }

    public static void sendBank(Player player) {
        PacketBuilder s = new PacketBuilder().setID(93);
        s.addByte((byte) player.getComponent(Bank.class).size());
        s.addByte((byte) 0xff);
        player.getComponent(Bank.class).getItems().forEach(bi -> {
            s.addShort(bi.getID());
            s.addInt(bi.getAmount());
        });
        player.send(s.toPacket());
    }

    public static void sendBankClose(Player player) {
        player.send(new PacketBuilder().setID(171).toPacket());
    }

    public static void sendBigMsgBox(Player player, String s1) {
        PacketBuilder builder = new PacketBuilder().setID(64);
        builder.addBytes(s1.getBytes());
        player.send(builder.toPacket());
    }

    public static void sendCantLogout(Player player) {
        player.send(new PacketBuilder().setID(136).toPacket());
    }

    public static void sendDied(Player player) {
        player.send(new PacketBuilder().setID(165).toPacket());
    }

    public static void sendDuelAccept(Player player) {
        Player with = player.getDuel().getWishTo();
        if (with == null)
            return;

        PacketBuilder builder = new PacketBuilder().setID(147);
        builder.addLong(with.getCredentials().getEncodedUsername());

        builder.addByte((byte) with.getDuel().getOffer().size());
        with.getDuel().getOffer().forEach(item -> {
            builder.addShort(item.getID());
            builder.addInt(item.getAmount());
        });

        builder.addByte((byte) player.getDuel().getOffer().size());
        player.getDuel().getOffer().forEach(item -> {
            builder.addShort(item.getID());
            builder.addInt(item.getAmount());
        });

        boolean[] rules = player.getDuel().getRules();
        for (boolean b : rules) {
            builder.addByte(b ? (byte) 1 : (byte) 0);
        }
        player.send(builder.toPacket());
    }

    public static void sendDuelAcceptUpdate(Player player) {
        Player with = player.getDuel().getWishTo();
        if (with == null)
            return;

        PacketBuilder builder = new PacketBuilder();
        builder.setID(197);
        builder.addByte((byte) (player.getDuel().isConfirmed(1) ? 1 : 0));
        player.send(builder.toPacket());

        builder = new PacketBuilder();
        builder.setID(65);
        builder.addByte((byte) (with.getDuel().isConfirmed(1) ? 1 : 0));
        player.send(builder.toPacket());
    }

    public static void sendDuelItems(Player player) {
        Player with = player.getDuel().getWishTo();
        if (with == null)
            return;
        List<InvItem> items = with.getDuel().getOffer();
        PacketBuilder builder = new PacketBuilder().setID(63);
        builder.addByte((byte) items.size());
        items.forEach(item -> {
            builder.addShort(item.getID());
            builder.addInt(item.getAmount());
        });
        player.send(builder.toPacket());
    }

    public static void sendDuelRules(Player player) {
        PacketBuilder builder = new PacketBuilder().setID(198);
        boolean[] rules = player.getDuel().getRules();
        for (boolean b : rules) {
            builder.addByte(b ? (byte) 1 : (byte) 0);
        }
        player.send(builder.toPacket());
    }

    public static void sendDuelWindowClose(Player player) {
        player.send(new PacketBuilder().setID(160).toPacket());
    }

    public static void sendDuelWindowOpen(Player player) {
        Player with = player.getDuel().getWishTo();
        if (with == null)
            return;
        PacketBuilder builder = new PacketBuilder().setID(229);
        builder.addShort(with.getIndex());
        player.send(builder.toPacket());
    }

    public static void sendEquipmentStats(Player player) {
        PacketBuilder builder = new PacketBuilder().setID(177);
        builder.addShort(player.getArmourPoints());
        builder.addShort(player.getWeaponAimPoints());
        builder.addShort(player.getWeaponPowerPoints());
        builder.addShort(player.getMagicPoints());
        builder.addShort(player.getPrayerPoints());
        builder.addShort(player.getRangePoints());
        player.send(builder.toPacket());
    }

    public static void sendFriendList(Player player) {
        PacketBuilder builder = new PacketBuilder().setID(249);
        Queue<Long> friendList = player.getComponent(Communication.class).getFriendList();
        builder.addByte((byte) friendList.size());
        for (long usernameHash : friendList) {
            boolean loggedIn = false;
            Player friend = World.getInstance().getPlayer(usernameHash);
            if (friend != null) loggedIn = friend.loggedIn();
            builder.addLong(usernameHash);
            builder.addByte((byte) (loggedIn ? 99 : 0));
        }
        player.send(builder.toPacket());
    }

    public static void sendFriendUpdate(Player player, long usernameHash, boolean loggedIn) {
        PacketBuilder builder = new PacketBuilder().setID(25);
        builder.addLong(usernameHash);
        builder.addByte((byte) (loggedIn ? 99 : 0));
        player.send(builder.toPacket());
    }

    public static void sendGameSettings(Player player) {
        boolean[] settings = player.getComponent(Settings.class).getGameSettings();
        PacketBuilder builder = new PacketBuilder().setID(152);
        builder.addByte((byte) (settings[0] ? 1 : 0));
        builder.addByte((byte) (settings[2] ? 1 : 0));
        builder.addByte((byte) (settings[3] ? 1 : 0));
        player.send(builder.toPacket());
    }

    public static void sendIgnoreList(Player player) {
        PacketBuilder builder = new PacketBuilder();
        Queue<Long> ignoreList = player.getComponent(Communication.class).getIgnoreList();
        builder.setID(2);
        builder.addByte((byte) ignoreList.size());
        ignoreList.forEach(builder::addLong);
        player.send(builder.toPacket());
    }

    public static void sendInventory(Player player) {
        PacketBuilder builder = new PacketBuilder().setID(114);
        builder.addByte((byte) player.getInventory().size());
        player.getInventory().getItems().forEach(item -> {
            builder.addShort(item.getID() + (item.isWielded() ? 32768 : 0));
            if (item.getDef().isStackable())
                builder.addInt(item.getAmount());
        });
        player.send(builder.toPacket());
    }

    public static void sendKillNotification(Player player, long killedHash, long killerHash, int type) {
        PacketBuilder builder = new PacketBuilder().setID(142);
        builder.addLong(killedHash);
        builder.addLong(killerHash);
        builder.addInt(type);
        player.send(builder.toPacket());
    }

    public static void sendLoginBox(Player player) {
        PacketBuilder builder = new PacketBuilder().setID(248);
        builder.addShort(player.getDaysSinceLastLogin());
        if (player.getLastIP() != null)
            builder.addBytes(player.getLastIP().getBytes());
        else
            builder.addBytes("0.0.0.0".getBytes());
        player.send(builder.toPacket());
    }

    public static void sendLogout(Player player) {
        player.send(new PacketBuilder().setID(222).toPacket());
    }

    public static void sendMessage(Player player, String message) {
        PacketBuilder builder = new PacketBuilder().setID(48);
        builder.addBytes(message.getBytes());
        player.send(builder.toPacket());
    }

    public static void sendPrayers(Player player) {
        PacketBuilder builder = new PacketBuilder().setID(209);
        for (int x = 0; x < 14; x++) {
            builder.addByte((byte) (player.isPrayerActivated(x) ? 1 : 0));
        }
        player.send(builder.toPacket());
    }

    public static void sendPrivacySettings(Player player) {
        boolean[] settings = player.getComponent(Settings.class).getPrivacySettings();
        PacketBuilder builder = new PacketBuilder().setID(158);
        builder.addByte((byte) (settings[0] ? 1 : 0));
        builder.addByte((byte) (settings[1] ? 1 : 0));
        builder.addByte((byte) (settings[2] ? 1 : 0));
        builder.addByte((byte) (settings[3] ? 1 : 0));
        player.send(builder.toPacket());
    }

    public static void sendPrivateMessage(Player player, long usernameHash, byte[] message) {
        PacketBuilder builder = new PacketBuilder().setID(170);
        builder.addLong(usernameHash);
        builder.addBytes(message);
        player.send(builder.toPacket());
    }

    public static void sendQueryMenu(Player player, QueryMenu qm) {
        PacketBuilder builder = new PacketBuilder().setID(223);
        builder.addByte(qm.size());
        for (String s1 : qm.getQueries()) {
            builder.addByte((byte) s1.length());
            builder.addBytes(s1.getBytes());
        }
        player.send(builder.toPacket());
    }

    public static void sendRemoveItem(Player player, int slot) {
        PacketBuilder builder = new PacketBuilder().setID(191);
        builder.addByte((byte) slot);
        player.send(builder.toPacket());
    }

    public static void sendServerInfo(Player player) {
        PacketBuilder builder = new PacketBuilder().setID(110);
        builder.addLong(Server.START_TIME);
        builder.addShort(World.getInstance().getPlayers().size());
        builder.addBytes(Server.getServer().getConf().get("location")
                .getBytes());
        player.send(builder.toPacket());
    }

    public static void sendShop(Player player, Shop shop) {
        PacketBuilder builder = new PacketBuilder().setID(253);
        builder.addByte((byte) shop.getShopSize());
        builder.addByte((byte) (shop.isGeneral() ? 1 : 0));
        builder.addByte((byte) shop.getSellModifier());
        builder.addByte((byte) shop.getBuyModifier());
        for (int i = 0; i < shop.getShopSize(); i++) {
            InvItem item = shop.getShopItem(i);
            builder.addShort(item.getID());
            builder.addShort(item.getAmount());
            builder.addInt(shop.getItemPrice(i, true));
            builder.addInt(shop.getItemPrice(i, false));
        }
        player.send(builder.toPacket());
    }

    public static void sendShopClose(Player player) {
        player.send(new PacketBuilder().setID(220).toPacket());
    }

    /**
     * Action methods etc.
     **/
    public static void sendSound(Player player, String soundName) {
        PacketBuilder builder = new PacketBuilder().setID(11);
        builder.addBytes(soundName.getBytes());
        player.send(builder.toPacket());
    }

    public static void sendStat(Player player, int skillIndex) {
        Skill skill = Objects.requireNonNull(player.getSkillSet().get(skillIndex));
        PacketBuilder builder = new PacketBuilder().setID(208);
        builder.addByte((byte) skillIndex);
        builder.addShort(skill.getLevel());
        builder.addShort(skill.getMaxLevel());
        builder.addLong(skill.getExperience());
        player.send(builder.toPacket());
    }

    public static void sendStats(Player player) {
        PacketBuilder s = new PacketBuilder().setID(180);

        for(Skill skill : player.getSkillSet())
            s.addShort(skill.getLevel());
        for(Skill skill : player.getSkillSet())
            s.addShort(skill.getMaxLevel());
        for(Skill skill : player.getSkillSet())
            s.addLong(skill.getExperience());

        player.send(s.toPacket());
    }

    public static void sendTeleBubble(Player player, int x, int y) {
        PacketBuilder builder = new PacketBuilder().setID(23);
        builder.addByte((byte) 0);
        builder.addByte((byte) (x - player.getX()));
        builder.addByte((byte) (y - player.getY()));
        player.send(builder.toPacket());
    }

    public static void sendTradeAccept(Player player) {
        Player with = player.getTrade().getWishTo();
        if (with == null)
            return;
        PacketBuilder builder = new PacketBuilder().setID(251);
        builder.addLong(with.getCredentials().getEncodedUsername());
        builder.addByte((byte) with.getTrade().getOffer().size());
        for (InvItem item : with.getTrade().getOffer()) {
            builder.addShort(item.getID());
            builder.addInt(item.getAmount());
        }

        builder.addByte((byte) player.getTrade().getOffer().size());
        for (InvItem item : player.getTrade().getOffer()) {
            builder.addShort(item.getID());
            builder.addInt(item.getAmount());
        }
        player.send(builder.toPacket());
    }

    public static void sendTradeAcceptUpdate(Player player) {
        Player with = player.getTrade().getWishTo();
        if (with == null)
            return;

        PacketBuilder builder = new PacketBuilder().setID(18);
        builder.addByte((byte) (player.getTrade().isConfirmed(1) ? 1 : 0));
        player.send(builder.toPacket());

        builder = new PacketBuilder().setID(92);
        builder.addByte((byte) (with.getTrade().isConfirmed(1) ? 1 : 0));
        player.send(builder.toPacket());
    }

    public static void sendTradeItems(Player player) {
        Player with = player.getTrade().getWishTo();
        if (with == null)
            return;

        ArrayList<InvItem> items = with.getTrade().getOffer();
        PacketBuilder builder = new PacketBuilder();
        builder.setID(250);
        builder.addByte((byte) items.size());
        items.forEach(item -> {
            builder.addShort(item.getID());
            builder.addInt(item.getAmount());
        });
        player.send(builder.toPacket());
    }

    public static void sendTradeWindowClose(Player player) {
        player.send(new PacketBuilder().setID(187).toPacket());
    }

    public static void sendTradeWindowOpen(Player player) {
        Player with = player.getTrade().getWishTo();
        if (with == null)
            return;

        PacketBuilder builder = new PacketBuilder().setID(4);
        builder.addShort(with.getIndex());
        player.send(builder.toPacket());
    }

    public static void sendSystemUpdate(Player player) {
        PacketBuilder builder = new PacketBuilder().setID(172);
        builder.addShort(Math.round((Server.getServer().getGameService().getMsUntilUpdate() / 1000L / 32) * 50));
        player.send(builder.toPacket());
    }

    public static void sendUpdateItem(Player player, int slot) {
        InvItem item = player.getInventory().get(slot);
        PacketBuilder builder = new PacketBuilder().setID(228);
        builder.addByte((byte) slot);
        builder.addShort(Objects.requireNonNull(item).getID() + (item.isWielded() ? 32768 : 0));
        if (item.getDef().isStackable()) builder.addInt(item.getAmount());
        player.send(builder.toPacket());
    }

    public static void sendHeight(Player player) {
        PacketBuilder builder = new PacketBuilder().setID(131);
        builder.addShort(player.getIndex());
        builder.addShort(2304);
        builder.addShort(1776);
        int height = Formulae.getHeight(player.getLocation());
        builder.addShort(height);
        builder.addShort(944);
        player.send(builder.toPacket());
    }

    public static void updateBankItems(Player player, int slot, int itemID, int i) {
        PacketBuilder builder = new PacketBuilder().setID(139);
        builder.addByte((byte) slot);
        builder.addShort(itemID);
        builder.addInt(i);
        player.send(builder.toPacket());
    }

    public static void sendOnlineCount(Player player) {
        PacketBuilder builder = new PacketBuilder().setID(110);
        builder.addShort(World.getInstance().getPlayers().size());
        player.send(builder.toPacket());
    }
}
