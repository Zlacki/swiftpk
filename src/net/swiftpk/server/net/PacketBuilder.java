package net.swiftpk.server.net;

import net.swiftpk.server.util.Logger;

/**
 * A mutable sequence of bytes used to construct the immutable
 * <code>Packet</code> objects.
 */
public class PacketBuilder {
    /**
     * Bitmasks for <code>addBits()</code>
     */
    private static int[] bitmasks = {0, 0x1, 0x3, 0x7, 0xf, 0x1f, 0x3f, 0x7f,
            0xff, 0x1ff, 0x3ff, 0x7ff, 0xfff, 0x1fff, 0x3fff, 0x7fff, 0xffff,
            0x1ffff, 0x3ffff, 0x7ffff, 0xfffff, 0x1fffff, 0x3fffff, 0x7fffff,
            0xffffff, 0x1ffffff, 0x3ffffff, 0x7ffffff, 0xfffffff, 0x1fffffff,
            0x3fffffff, 0x7fffffff, -1};
    /**
     * Default capacity
     */
    private static final int DEFAULT_SIZE = 32;
    /**
     * Whether this packet does not use the standard packet header
     */
    public boolean bare = false;
    /**
     * Current index into the buffer by bits
     */
    private int bitPosition = 0;
    /**
     * Current number of bytes used in the buffer
     */
    private int curLength;
    /**
     * ID of the packet
     */
    private int id;
    /**
     * The payload buffer
     */
    private byte[] payload;

    /**
     * Constructs a packet builder with no data and an initial capacity of
     * <code>DEFAULT_SIZE</code>.
     */
    public PacketBuilder() {
        this(DEFAULT_SIZE);
    }

    /**
     * Constructs a packet builder with no data and an initial capacity of
     * <code>capacity</code>.
     *
     * @param capacity The initial capacity of the buffer
     */
    public PacketBuilder(int capacity) {
        setPayload(new byte[capacity]);
    }

    /**
     * TODO needs a proper description.
     */
    public PacketBuilder addBits(int value, int numBits) {
        int bytePos = bitPosition >> 3;
        int bitOffset = 8 - (bitPosition & 7);
        bitPosition += numBits;
        setCurLength((bitPosition + 7) / 8);
        ensureCapacity(getCurLength());
        for (; numBits > bitOffset; bitOffset = 8) {
            getPayload()[bytePos] &= ~bitmasks[bitOffset]; // mask out the
            // desired
            // area
            getPayload()[bytePos++] |= (value >> (numBits - bitOffset))
                    & bitmasks[bitOffset];
            numBits -= bitOffset;
        }
        if (numBits == bitOffset) {
            getPayload()[bytePos] &= ~bitmasks[bitOffset];
            getPayload()[bytePos] |= value & bitmasks[bitOffset];
        } else {
            getPayload()[bytePos] &= ~(bitmasks[numBits] << (bitOffset - numBits));
            getPayload()[bytePos] |= (value & bitmasks[numBits]) << (bitOffset - numBits);
        }
        return this;
    }

    /**
     * Adds a <code>byte</code> to the data buffer. The size of this packet will
     * grow by one byte.
     *
     * @param val The <code>byte</code> value to add
     * @return A reference to this object
     */
    public PacketBuilder addByte(byte val) {
        return addByte(val, true);
    }

    /**
     * Adds a <code>byte</code> to the data buffer. The size of this packet will
     * grow by one byte.
     *
     * @param val           The <code>byte</code> value to add
     * @param checkCapacity Whether the buffer capacity should be checked
     * @return A reference to this object
     */
    private PacketBuilder addByte(byte val, boolean checkCapacity) {
        if (checkCapacity)
            ensureCapacity(getCurLength() + 1);
        getPayload()[curLength++] = val;
        return this;
    }

    /**
     * Adds the contents of <code>byte</code> array <code>data</code> to the
     * packet. The size of this packet will grow by the length of the provided
     * array.
     *
     * @param data The bytes to add to this packet
     * @return A reference to this object
     */
    public PacketBuilder addBytes(byte[] data) {
        return addBytes(data, 0, data.length);
    }

    /**
     * Adds the contents of <code>byte</code> array <code>data</code>, starting
     * at index <code>offset</code>. The size of this packet will grow by
     * <code>len</code> bytes.
     *
     * @param data   The bytes to add to this packet
     * @param offset The index of the first byte to append
     * @param len    The number of bytes to append
     * @return A reference to this object
     */
    public PacketBuilder addBytes(byte[] data, int offset, int len) {
        int newLength = getCurLength() + len;
        ensureCapacity(newLength);
        System.arraycopy(data, offset, getPayload(), getCurLength(), len);
        setCurLength(newLength);
        return this;
    }

    /**
     * Adds a <code>int</code> to the data stream. The size of this packet will
     * grow by four bytes.
     *
     * @param val The <code>int</code> value to add
     * @return A reference to this object
     */
    public PacketBuilder addInt(int val) {
        ensureCapacity(getCurLength() + 4);
        addByte((byte) (val >> 24), false);
        addByte((byte) (val >> 16), false);
        addByte((byte) (val >> 8), false);
        addByte((byte) val, false);
        return this;
    }

    /**
     * Adds a <code>long</code> to the data stream. The size of this packet will
     * grow by eight bytes.
     *
     * @param val The <code>long</code> value to add
     * @return A reference to this object
     */
    public PacketBuilder addLong(long val) {
        addInt((int) (val >> 32));
        addInt((int) val);
        return this;
    }

    /**
     * Adds a <code>short</code> to the data stream. The size of this packet
     * will grow by two bytes.
     *
     * @param val The <code>short</code> value to add
     * @return A reference to this object
     */
    public PacketBuilder addShort(int val) {
        ensureCapacity(getCurLength() + 2);
        addByte((byte) (val >> 8), false);
        addByte((byte) val, false);
        return this;
    }

    /**
     * Ensures that the buffer is at least <code>minimumBytes</code> bytes.
     *
     * @param minimumCapacity The size needed
     */
    private void ensureCapacity(int minimumCapacity) {
        if (minimumCapacity >= getPayload().length)
            expandCapacity(minimumCapacity);
    }

    /**
     * Expands the buffer to the specified size.
     *
     * @param minimumCapacity The minimum capacity to which to expand
     */
    private void expandCapacity(int minimumCapacity) {
        int newCapacity = (getPayload().length + 1) * 2;
        if (newCapacity < 0) {
            newCapacity = Integer.MAX_VALUE;
        } else if (minimumCapacity > newCapacity) {
            newCapacity = minimumCapacity;
        }
        byte[] newPayload = new byte[newCapacity];
        try {
            while (getCurLength() > getPayload().length)
                setCurLength(getCurLength() - 1);
            System.arraycopy(getPayload(), 0, newPayload, 0, getCurLength());
        } catch (Exception e) {
            Logger.err("payload.length=" + getPayload().length
                    + ",newPayload.length=" + newPayload.length + ",curLength="
                    + getCurLength(), e);
        }
        setPayload(newPayload);
    }

    public int getCurLength() {
        return curLength;
    }

    public byte[] getPayload() {
        return payload;
    }

    public boolean isBare() {
        return bare;
    }

    /**
     * Sets this packet as bare. A bare packet will contain only the payload
     * data, rather than having the standard packet header prepended.
     *
     * @param bare Whether this packet is to be sent bare
     */
    public PacketBuilder setBare(boolean bare) {
        this.bare = bare;
        return this;
    }

    public void setCurLength(int curLength) {
        this.curLength = curLength;
    }

    /**
     * Sets the ID for this packet.
     *
     * @param id The ID of the packet
     */
    public PacketBuilder setID(int id) {
        this.id = id;
        return this;
    }

    public void setPayload(byte[] payload) {
        this.payload = payload;
    }

    public PacketBuilder setShort(int val, int offset) {
        getPayload()[offset++] = (byte) (val >> 8);
        getPayload()[offset++] = (byte) val;
        if (getCurLength() < offset + 2) {
            setCurLength(getCurLength() + 2);
        }
        return this;
    }

    /**
     * Returns a <code>Packet</code> object for the data contained in this
     * builder.
     *
     * @return A <code>Packet</code> object
     */
    public Packet toPacket() {
        byte[] data = new byte[getCurLength()];
        System.arraycopy(getPayload(), 0, data, 0, getCurLength());
        return new Packet(id, data, isBare());
    }
}
