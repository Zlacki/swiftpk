package net.swiftpk.server.net.in;

import net.swiftpk.server.kplugin.event.Event;
import net.swiftpk.server.model.Player;
import net.swiftpk.server.model.component.Communication;
import net.swiftpk.server.model.component.Settings;
import net.swiftpk.server.model.terrain.World;
import net.swiftpk.server.net.ActionSender;
import net.swiftpk.server.net.Packet;
import net.swiftpk.server.net.PacketReader;

public class IgnoreAddPacketReader extends PacketReader {

    @Override
    public Event read(Player player, Packet msg) throws Exception {
        long usernameHash = msg.readLong();
        if(player.getComponent(Communication.class).getIgnoreList().size() >= 100) {
            ActionSender.sendMessage(player, "Your ignore list is full");
            return null;
        }
        player.getComponent(Communication.class).addIgnore(usernameHash);
        return null;
    }
}
