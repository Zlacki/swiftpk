package net.swiftpk.server.net.in;

import net.swiftpk.server.kplugin.event.Event;
import net.swiftpk.server.kplugin.event.impl.ChatEvent;
import net.swiftpk.server.model.ChatMessage;
import net.swiftpk.server.model.Player;
import net.swiftpk.server.model.component.Communication;
import net.swiftpk.server.net.Packet;
import net.swiftpk.server.net.PacketReader;

public class ChatPacketReader extends PacketReader {

    @Override
    public Event read(Player player, Packet msg) throws Exception {
        ChatMessage message = new ChatMessage(player, msg.getData());
        player.getComponent(Communication.class).pushMessage(message);
        return null;
    }
}
