package net.swiftpk.server.net.in;

import net.swiftpk.server.kplugin.event.Event;
import net.swiftpk.server.model.InvItem;
import net.swiftpk.server.model.MobState;
import net.swiftpk.server.model.Player;
import net.swiftpk.server.net.ActionSender;
import net.swiftpk.server.net.Packet;
import net.swiftpk.server.net.PacketReader;
import net.swiftpk.server.util.Logger;

import java.util.ArrayList;

public class TradeAcceptPacketReader extends PacketReader {

    @Override
    public Event read(Player player, Packet msg) throws Exception {
        Player affectedPlayer;
        if(player.isFighting() || player.isDueling())
            return null;
        affectedPlayer = player.getTrade().getWishTo();
        if(affectedPlayer == null || player.getState() != MobState.Trading
                || affectedPlayer.getState() != MobState.Trading) { // This
            // shouldn't
            // happen
            unsetOptions(player);
            unsetOptions(affectedPlayer);
            return null;
        }
        player.getTrade().setConfirmed(1, true);
        if(affectedPlayer.getTrade().isConfirmed(1)) {
            ActionSender.sendTradeAccept(player);
            ActionSender.sendTradeAccept(affectedPlayer);
        } else {
            ActionSender.sendTradeAcceptUpdate(player);
            ActionSender.sendTradeAcceptUpdate(affectedPlayer);
        }
        return null;
    }

    private void unsetOptions(Player p) {
        if(p == null) {
            return;
        }
        p.getTrade().reset();
        p.setState(MobState.Idle);
    }
}
