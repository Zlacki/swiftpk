package net.swiftpk.server.net.in;

import net.swiftpk.server.kplugin.event.Event;
import net.swiftpk.server.model.InvItem;
import net.swiftpk.server.model.MobState;
import net.swiftpk.server.model.Player;
import net.swiftpk.server.net.ActionSender;
import net.swiftpk.server.net.Packet;
import net.swiftpk.server.net.PacketReader;
import net.swiftpk.server.util.Logger;

import java.util.ArrayList;

public class TradeConfirmPacketReader extends PacketReader {

    @Override
    public Event read(Player player, Packet msg) throws Exception {
        Player affectedPlayer;
        if (player.isFighting() || player.isDueling())
            return null;
        affectedPlayer = player.getTrade().getWishTo();
        if (affectedPlayer == null || player.getState() != MobState.Trading
                || affectedPlayer.getState() != MobState.Trading
                || !player.getTrade().isConfirmed(1)
                || !affectedPlayer.getTrade().isConfirmed(1)) { // This
            // shouldn't
            // happen
            unsetOptions(player);
            unsetOptions(affectedPlayer);
            return null;
        }
        player.getTrade().setConfirmed(2, true);
        if (affectedPlayer.getTrade().isConfirmed(2)) {
            ArrayList<InvItem> myOffer = player.getTrade().getOffer();
            ArrayList<InvItem> theirOffer = affectedPlayer.getTrade()
                    .getOffer();
            int myRequiredSlots = player.getInventory().getRequiredSlots(
                    theirOffer);
            int myAvailableSlots = (30 - player.getInventory().size())
                    + player.getInventory().getRequiredSlots(myOffer);
            int theirRequiredSlots = affectedPlayer.getInventory()
                    .getRequiredSlots(myOffer);
            int theirAvailableSlots = (30 - affectedPlayer.getInventory()
                    .size())
                    + affectedPlayer.getInventory()
                    .getRequiredSlots(theirOffer);
            if (theirRequiredSlots > theirAvailableSlots) {
                ActionSender.sendMessage(player, "The other player does not have room to accept your items.");
                ActionSender.sendMessage(affectedPlayer, "You do not have room in your inventory to hold those items.");
                unsetOptions(player);
                unsetOptions(affectedPlayer);
                return null;
            }
            if (myRequiredSlots > myAvailableSlots) {
                ActionSender.sendMessage(player, "You do not have room in your inventory to hold those items.");
                ActionSender.sendMessage(affectedPlayer, "The other player does not have room to accept your items.");
                unsetOptions(player);
                unsetOptions(affectedPlayer);
                return null;
            }
            for (InvItem item : myOffer) {
                InvItem affectedItem = player.getInventory().get(item);
                if (affectedItem.isWielded()) {
                    affectedItem.setWield(false);
                    player.updateWornItems(affectedItem.getWieldableDef()
                            .getWieldPos(), player.getAppearance()
                            .getSprite(
                                    affectedItem.getWieldableDef()
                                            .getWieldPos()));
                }
                player.getInventory().remove(item);
            }
            ActionSender.sendInventory(player);
            for (InvItem item : theirOffer) {
                InvItem affectedItem = affectedPlayer.getInventory().get(item);
                if (affectedItem.isWielded()) {
                    affectedItem.setWield(false);
                    affectedPlayer.updateWornItems(affectedItem
                            .getWieldableDef().getWieldPos(), affectedPlayer
                            .getAppearance().getSprite(
                                    affectedItem.getWieldableDef()
                                            .getWieldPos()));
                }
                affectedPlayer.getInventory().remove(item);
            }
            ActionSender.sendInventory(affectedPlayer);
            if (!affectedPlayer.getTrade().getOffer().isEmpty()
                    || !player.getTrade().getOffer().isEmpty())
                Logger.log("trades.log", "{", "\t'" + player.getUsername() + "': " + myOffer.toString() + ";", "\t'" + affectedPlayer.getUsername() + "': " + theirOffer.toString() + ";", "}");
            myOffer.forEach((item) -> affectedPlayer.getInventory().add(item));
            theirOffer.forEach((item) -> player.getInventory().add(item));
            ActionSender.sendEquipmentStats(player);
            ActionSender.sendMessage(player, "Trade completed.");
            ActionSender.sendTradeWindowClose(player);
            ActionSender.sendEquipmentStats(affectedPlayer);
            ActionSender.sendMessage(affectedPlayer, "Trade completed.");
            ActionSender.sendTradeWindowClose(affectedPlayer);
            unsetOptions(player);
            unsetOptions(affectedPlayer);
        }
        return null;
    }

    private void unsetOptions(Player p) {
        if (p == null) {
            return;
        }
        p.getTrade().reset();
        p.setState(MobState.Idle);
    }
}
