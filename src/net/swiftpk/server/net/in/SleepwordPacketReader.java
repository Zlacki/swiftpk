package net.swiftpk.server.net.in;

import net.swiftpk.server.kplugin.event.Event;
import net.swiftpk.server.model.MobState;
import net.swiftpk.server.model.Player;
import net.swiftpk.server.model.terrain.World;
import net.swiftpk.server.net.ActionSender;
import net.swiftpk.server.net.Packet;
import net.swiftpk.server.net.PacketReader;
import net.swiftpk.server.tickable.Tickable;
import net.swiftpk.server.util.Captcha;

public class SleepwordPacketReader extends PacketReader {

    @Override
    public Event read(Player player, Packet msg) throws Exception {
        if(player.getState() != MobState.Sleeping)
            return null;
        String sleepWord = msg.readString();
        if(sleepWord.equalsIgnoreCase("-null-"))
            ActionSender.sendSleepWord(player, Captcha.generateCaptcha(player));
        else if(sleepWord.equalsIgnoreCase(player.getSleepWord())) {
            player.setState(MobState.Idle);
            player.setFatigue(player.getSleepFatigue());
            ActionSender.sendFatigue(player);
            ActionSender.sendWakeup(player);
            ActionSender.sendMessage(player, "You wake up - feeling refreshed");
        } else {
            ActionSender.sendIncorrectSleepWord(player);
            World.getInstance().submit(new Tickable(2) {
                public void execute() {
                    ActionSender.sendSleepWord(player, Captcha.generateCaptcha(player));
                    stop();
                }
            });
        }
        return null;
    }
}
