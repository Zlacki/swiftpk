package net.swiftpk.server.net.in;

import net.swiftpk.server.kplugin.event.Event;
import net.swiftpk.server.model.InvItem;
import net.swiftpk.server.model.MobState;
import net.swiftpk.server.model.Player;
import net.swiftpk.server.model.Shop;
import net.swiftpk.server.net.ActionSender;
import net.swiftpk.server.net.Packet;
import net.swiftpk.server.net.PacketReader;
import net.swiftpk.server.util.DataConversions;

public class ShopSellPacketReader extends PacketReader {
    // Shark, lobster
    private final static int[] FORBIDDEN_ITEMS = {546, 373};

    @Override
    public Event read(Player player, Packet msg) throws Exception {
        if (player.getState() != MobState.Shopping)
            return null;
        Shop shop = player.getShop();
        int id = msg.readShort();
        int amount = msg.readInt();
        InvItem item = new InvItem(id, amount);
        if (DataConversions.inArray(FORBIDDEN_ITEMS, id) || player.getInventory().countById(id) < amount || !shop.shouldStock(id)) {
            return null;
        }
        if (!shop.canHoldItem(item)) {
            ActionSender.sendMessage(player, "The shop is currently full!");
            return null;
        }
        if (player.getInventory().remove(item) > -1) {
            player.getInventory().add(new InvItem(10, shop.getItemCount(id) == 0 ? item.getDef().getBasePrice() / 2 * amount : shop.getItemSellPrice(id) * amount));
            shop.addShopItem(item);
            ActionSender.sendSound(player, "coins");
            ActionSender.sendInventory(player);
        }
        return null;
    }
}
