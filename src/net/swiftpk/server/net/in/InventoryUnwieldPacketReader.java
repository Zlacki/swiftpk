package net.swiftpk.server.net.in;

import net.swiftpk.server.kplugin.event.Event;
import net.swiftpk.server.model.InvItem;
import net.swiftpk.server.model.MobState;
import net.swiftpk.server.model.Player;
import net.swiftpk.server.net.ActionSender;
import net.swiftpk.server.net.Packet;
import net.swiftpk.server.net.PacketReader;

public class InventoryUnwieldPacketReader extends PacketReader {

    @Override
    public Event read(Player player, Packet msg) throws Exception {
        if (player.isDueling() && !player.canWield()) {
            ActionSender.sendMessage(player, "You cannot use items in this duel!");
            return null;
        }
        if (player.getState() != MobState.Idle && !player.isFighting()) {
            return null;
        }
        int idx = msg.readShort();
        if (idx < 0 || idx >= 30) {
            // player.suspiciousEvent("Tried to wield item index: "+idx);
            return null;
        }
        InvItem item = player.getInventory().get(idx);
        if (item == null || !item.isWieldable()) {
            return null;
        }
        player.unWieldItem(item);
        ActionSender.sendEquipmentStats(player);
        return null;
    }
}
