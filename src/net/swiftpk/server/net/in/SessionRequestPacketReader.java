package net.swiftpk.server.net.in;

import net.swiftpk.server.kplugin.event.Event;
import net.swiftpk.server.model.Player;
import net.swiftpk.server.model.component.Communication;
import net.swiftpk.server.net.Packet;
import net.swiftpk.server.net.PacketBuilder;
import net.swiftpk.server.net.PacketReader;

import java.security.SecureRandom;

public class SessionRequestPacketReader extends PacketReader {

    @Override
    public Event read(Player player, Packet msg) throws Exception {
        long sessionKey = new SecureRandom().nextLong();
        player.setServerKey(sessionKey);
        player.getClient().getServerKey().set(sessionKey);
        player.send(new PacketBuilder().setBare(true).addLong(sessionKey).toPacket());
        return null;
    }
}
