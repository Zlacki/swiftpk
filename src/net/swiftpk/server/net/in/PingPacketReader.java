package net.swiftpk.server.net.in;

import net.swiftpk.server.kplugin.event.Event;
import net.swiftpk.server.model.Player;
import net.swiftpk.server.model.component.Communication;
import net.swiftpk.server.net.Packet;
import net.swiftpk.server.net.PacketBuilder;
import net.swiftpk.server.net.PacketReader;

/**
 * TODO: Is this necessary?  Netty backend takes care of timeouts automatically.
 */
public class PingPacketReader extends PacketReader {

    @Override
    public Event read(Player player, Packet msg) throws Exception {
        player.ping();
        player.send(new PacketBuilder().setID(3).toPacket());
        return null;
    }
}
