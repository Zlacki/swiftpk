package net.swiftpk.server.net.in;

import net.swiftpk.server.kplugin.event.Event;
import net.swiftpk.server.model.MobState;
import net.swiftpk.server.model.Player;
import net.swiftpk.server.net.ActionSender;
import net.swiftpk.server.net.Packet;
import net.swiftpk.server.net.PacketReader;

public class DuelDeclinePacketReader extends PacketReader {

    @Override
    public Event read(Player player, Packet msg) throws Exception {
        if(player.isFighting())
            return null;
        Player affectedPlayer = player.getDuel().getWishTo();
        if(affectedPlayer != null) {
            unsetOptions(affectedPlayer);
            ActionSender.sendDuelWindowClose(affectedPlayer);
            ActionSender.sendMessage(affectedPlayer, player.getUsername() + " has declined the duel.");
        }
        unsetOptions(player);
        ActionSender.sendDuelWindowClose(player);
        return null;
    }

    private void unsetConfirmed(Player... players) {
        for(Player p : players) {
            for(int i : new int[] { 1, 2 })
                p.getDuel().setConfirmed(i, false);
        }
    }

    private void unsetOptions(Player p) {
        if(p == null) {
            return;
        }
        unsetConfirmed(p);
        p.getDuel().reset();
        p.setState(MobState.Idle);
    }
}
