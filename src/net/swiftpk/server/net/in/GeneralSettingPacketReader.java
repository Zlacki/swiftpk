package net.swiftpk.server.net.in;

import net.swiftpk.server.kplugin.event.Event;
import net.swiftpk.server.model.Player;
import net.swiftpk.server.model.component.Communication;
import net.swiftpk.server.model.component.Settings;
import net.swiftpk.server.model.terrain.World;
import net.swiftpk.server.net.ActionSender;
import net.swiftpk.server.net.Packet;
import net.swiftpk.server.net.PacketReader;

public class GeneralSettingPacketReader extends PacketReader {

    @Override
    public Event read(Player player, Packet msg) throws Exception {
        int idx = msg.readByte();
        if(idx < 0 || idx >= 4)
            return null;
        player.getComponent(net.swiftpk.server.model.component.Settings.class).setGameSetting(idx, msg.readByte() == 1);
        return null;
    }
}
