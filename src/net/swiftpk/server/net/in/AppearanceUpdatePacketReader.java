package net.swiftpk.server.net.in;

import net.swiftpk.server.kplugin.event.Event;
import net.swiftpk.server.model.InvItem;
import net.swiftpk.server.model.Player;
import net.swiftpk.server.model.component.Communication;
import net.swiftpk.server.model.component.Inventory;
import net.swiftpk.server.net.ActionSender;
import net.swiftpk.server.net.Packet;
import net.swiftpk.server.net.PacketReader;

public class AppearanceUpdatePacketReader extends PacketReader {
    private int[][] acceptableAppearances = new int[][] { { 1, 2 }, { 0, 3, 5, 6, 7 }, { 1, 4 },
            { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 }, { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14 },
            { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14 }, { 0, 1, 2, 3, 4 } };

    @Override
    public Event read(Player player, Packet msg) throws Exception {
        if (!player.isChangingAppearance()) {
            // player.suspiciousEvent("Player tried to change appearance whilst not changing
            // appearance");
            return null;
        }
        player.setChangingAppearance(false);
        byte headGender = msg.readByte();
        byte headType = msg.readByte();
        byte bodyGender = msg.readByte();
        msg.readByte();
        int hairColour = msg.readByte();
        int topColour = msg.readByte();
        int trouserColour = msg.readByte();
        int skinColour = msg.readByte();
        int headSprite = headType + 1;
        int bodySprite = bodyGender + 1;
        if (!inRange(headGender, 0) || !inRange(headType, 1) || !inRange(bodyGender, 2) || !inRange(hairColour, 3)
                || !inRange(topColour, 4) || !inRange(trouserColour, 5) || !inRange(skinColour, 6)) {
            return null;
        }
        if ((headGender == 2 && bodyGender == 4) || (headGender == 1 && bodyGender == 1)) {
            player.setMale(headGender == 1);
            if (player.isMale()) {
                Inventory inv = player.getInventory();
                for (int slot = 0; slot < inv.size(); slot++) {
                    InvItem i = inv.get(slot);
                    if (i.isWieldable() && i.getWieldableDef().getWieldPos() == 1 && i.isWielded()
                            && i.getWieldableDef().femaleOnly()) {
                        i.setWield(false);
                        player.updateWornItems(i.getWieldableDef().getWieldPos(),
                                player.getAppearance().getSprite(i.getWieldableDef().getWieldPos()));
                        ActionSender.sendUpdateItem(player, slot);
                        break;
                    }
                }
            }
            int[] oldWorn = player.getWornItems();
            int[] oldAppearance = player.getAppearance().getSprites();
            player.getAppearance().init(hairColour, topColour, trouserColour, skinColour, headSprite, bodySprite);
            int[] newAppearance = player.getAppearance().getSprites();
            for (int i = 0; i < 12; i++) {
                if (oldWorn[i] == oldAppearance[i]) {
                    player.updateWornItems(i, newAppearance[i]);
                }
            }
        }
        return null;
    }

    private boolean inRange(int value, int type) {
        for (int b1 : acceptableAppearances[type])
            if (b1 == value)
                return true;
        return false;
    }
}
