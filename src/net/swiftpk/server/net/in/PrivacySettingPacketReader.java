package net.swiftpk.server.net.in;

import net.swiftpk.server.kplugin.event.Event;
import net.swiftpk.server.model.Player;
import net.swiftpk.server.model.component.Communication;
import net.swiftpk.server.model.component.Settings;
import net.swiftpk.server.model.terrain.World;
import net.swiftpk.server.net.ActionSender;
import net.swiftpk.server.net.Packet;
import net.swiftpk.server.net.PacketReader;

public class PrivacySettingPacketReader extends PacketReader {

    @Override
    public Event read(Player player, Packet msg) throws Exception {
        boolean[] newSettings = { msg.readByte() == 1, msg.readByte() == 1, msg.readByte() == 1, msg.readByte() == 1 };
        if(newSettings[1]
                && !player.getComponent(Settings.class).getPrivacySetting(1)) {
            for(Player p1 : World.getInstance().getPlayers()) {
                if(!player.getComponent(Communication.class).isFriendsWith(
                        p1.getUsernameHash())
                        && p1.getComponent(Communication.class).isFriendsWith(
                        player.getUsernameHash())
                        && p1.getIndex() != player.getIndex()) {
                    ActionSender.sendFriendUpdate(p1, player.getUsernameHash(), false);
                }
            }
        }
        if(!newSettings[1]
                && player.getComponent(Settings.class).getPrivacySetting(1)) {
            for(Player p1 : World.getInstance().getPlayers()) {
                if(!player.getComponent(Communication.class).isFriendsWith(
                        p1.getUsernameHash())
                        && p1.getComponent(Communication.class).isFriendsWith(
                        player.getUsernameHash())
                        && p1.getIndex() != player.getIndex()) {
                    ActionSender.sendFriendUpdate(p1, player.getUsernameHash(), player.loggedIn());
                }
            }
        }
        player.getComponent(Settings.class).setPrivacySetting(0, newSettings[0]);
        player.getComponent(Settings.class).setPrivacySetting(1, newSettings[1]);
        player.getComponent(Settings.class).setPrivacySetting(2, newSettings[2]);
        player.getComponent(Settings.class).setPrivacySetting(3, newSettings[3]);
        return null;
    }
}
