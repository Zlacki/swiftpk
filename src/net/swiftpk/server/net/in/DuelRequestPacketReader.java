package net.swiftpk.server.net.in;

import net.swiftpk.server.kplugin.event.Event;
import net.swiftpk.server.model.MobState;
import net.swiftpk.server.model.Player;
import net.swiftpk.server.model.component.Communication;
import net.swiftpk.server.model.component.Settings;
import net.swiftpk.server.model.terrain.World;
import net.swiftpk.server.net.ActionSender;
import net.swiftpk.server.net.Packet;
import net.swiftpk.server.net.PacketReader;

public class DuelRequestPacketReader extends PacketReader {

    @Override
    public Event read(Player player, Packet msg) throws Exception {
        if(player.isFighting())
            return null;
        Player affectedPlayer = World.getInstance().getPlayer(msg.readShort());
        if(affectedPlayer == null || !player.withinRange(affectedPlayer)
                || player.getState() != MobState.Idle
                || affectedPlayer.getState() != MobState.Idle) {
            return null;
        }
        if(affectedPlayer.getComponent(Settings.class).getPrivacySetting(3) && !affectedPlayer.getComponent(Communication.class).isFriendsWith(player.getUsernameHash())) {
            ActionSender.sendMessage(player, "This player has Duel requests blocked.");
            return null;
        }
        player.getDuel().setWishTo(affectedPlayer);
        ActionSender.sendMessage(player, "Sending Duel request.");
        ActionSender.sendMessage(affectedPlayer, player.getUsername() + " wishes to Duel with you.");
        if(player.getState() == MobState.Idle
                && affectedPlayer.getDuel().getWishTo() != null
                && affectedPlayer.getDuel().getWishTo().equals(player)
                && affectedPlayer.getState() == MobState.Idle) {
            player.setState(MobState.Dueling);
            player.resetPath();
            affectedPlayer.setState(MobState.Dueling);
            affectedPlayer.resetPath();
            ActionSender.sendDuelWindowOpen(player);
            ActionSender.sendDuelWindowOpen(affectedPlayer);
        }
        return null;
    }
}
