package net.swiftpk.server.net.in;

import net.swiftpk.server.kplugin.event.Event;
import net.swiftpk.server.model.Player;
import net.swiftpk.server.model.component.Communication;
import net.swiftpk.server.net.Packet;
import net.swiftpk.server.net.PacketReader;

public class CombatStylePacketReader extends PacketReader {

    @Override
    public Event read(Player player, Packet msg) throws Exception {
        int style = msg.readByte();
        if(style < 0 || style > 3)
            return null;
        player.setFightMode(style);
        return null;
    }
}
