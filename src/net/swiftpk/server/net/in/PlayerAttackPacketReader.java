package net.swiftpk.server.net.in;

import net.swiftpk.server.kplugin.event.Event;
import net.swiftpk.server.model.MobState;
import net.swiftpk.server.model.Player;
import net.swiftpk.server.model.entity.WalkToEntityAction;
import net.swiftpk.server.model.terrain.World;
import net.swiftpk.server.net.ActionSender;
import net.swiftpk.server.net.Packet;
import net.swiftpk.server.net.PacketReader;
import net.swiftpk.server.tickable.impl.actor.CombatHitTick;
import net.swiftpk.server.tickable.impl.combat.RangedTick;

public class PlayerAttackPacketReader extends PacketReader {

    @Override
    public Event read(Player player, Packet msg) throws Exception {
        final Player target = World.getInstance().getPlayer(msg.readShort());
        if (player.isFighting()) {
            ActionSender.sendMessage(player, "You're already fighting!");
            player.resetPath();
            return null;
        }
        if (player.getState() != MobState.Idle && player.getState() != MobState.Eating || target.getRank() == 1
                || target.getRank() == 2 || player.getRank() == 1 || !target.isAttackable() || target.equals(player)) {
            player.resetPath();
            return null;
        }
        if (target.isFighting()) {
            ActionSender.sendMessage(player, "Your opponent is busy!");
            player.resetPath();
            return null;
        }
        if (player.getBowStats() != null) {
            player.setFollowing(target, 5);
            player.setWalkToAction(new WalkToEntityAction(player, target, 5) {
                @Override
                public void execute() {
                    player.resetAll();
                    World.getInstance().submit(new RangedTick(player, target));
                }
            });
        } else {
            player.setFollowing(target, 2);
            player.setWalkToAction(new WalkToEntityAction(player, target, 2) {
                @Override
                public void execute() {
                    if (target.isFighting()) {
                        ActionSender.sendMessage(player, "Your opponent is busy!");
                        return;
                    }
                    if (player.isFighting()) {
                        ActionSender.sendMessage(player, "You're already fighting!");
                        return;
                    }
                    if (target.getState() != MobState.Idle && target.getState() != MobState.Eating)
                        return;

                    if (!player.checkAttack(target) || !target.isAttackable()) {
                        return;
                    }
                    player.resetAll();
                    player.startCombat(target);
/*                    target.resetPath();
                    player.setLocation(target.getLocation(), true);
                    player.setState(MobState.LeftFighting);
                    player.setCombatPartner(target);
                    target.setState(MobState.RightFighting);
                    target.setCombatPartner(player);
                    ActionSender.sendMessage(target, "You are under attack!");
                    target.setRequiresSpriteUpdate(true);
                    player.attackedPlayer(target);
                    World.getInstance().submit(new CombatHitTick(player, target));*/
                }
            });
        }
        return null;
    }
}
