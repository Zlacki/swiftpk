package net.swiftpk.server.net.in;

import net.swiftpk.server.Server;
import net.swiftpk.server.ent.EntityHandler;
import net.swiftpk.server.kplugin.event.Event;
import net.swiftpk.server.kplugin.event.impl.BoundClickEvent;
import net.swiftpk.server.model.MobState;
import net.swiftpk.server.model.Player;
import net.swiftpk.server.model.entity.GameObject;
import net.swiftpk.server.model.entity.WalkToAction;
import net.swiftpk.server.model.terrain.World;
import net.swiftpk.server.model.terrain.tiles.Point;
import net.swiftpk.server.net.ActionSender;
import net.swiftpk.server.net.Packet;
import net.swiftpk.server.net.PacketReader;
import net.swiftpk.server.util.Formulae;

public class BoundClickPacketReader extends PacketReader {

    @Override
    public Event read(Player player, Packet msg) throws Exception {
        if(player.getState() != MobState.Idle) {
            player.resetPath();
            return null;
        }
        GameObject object = World.getInstance().getGameObject(msg.readShort(), msg.readShort());
        if(object == null) {
            return null;
        }
        player.setWalkToAction(new WalkToAction(player, object.getLocation(), 1) {
            public void execute() {
                if(player.getState() != MobState.Idle)
                    return;
                player.resetAll();
                if(Server.getServer().getPlugins().post(new BoundClickEvent(player, object)))
                    return;
                Point dest = EntityHandler.getObjectTelePoint(object.getLocation());
                if(dest != null) {
                    Point start = player.getLocation();
                    player.setLocation(dest, true);
                    if(Formulae.getHeight(start) != Formulae.getHeight(dest))
                        ActionSender.sendHeight(player);
                } else
                    ActionSender.sendMessage(player, "Nothing interesting happens");
            }
        });
        return null;
    }
}
