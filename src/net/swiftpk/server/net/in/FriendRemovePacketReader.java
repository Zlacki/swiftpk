package net.swiftpk.server.net.in;

import net.swiftpk.server.kplugin.event.Event;
import net.swiftpk.server.model.Player;
import net.swiftpk.server.model.component.Communication;
import net.swiftpk.server.model.component.Settings;
import net.swiftpk.server.model.terrain.World;
import net.swiftpk.server.net.ActionSender;
import net.swiftpk.server.net.Packet;
import net.swiftpk.server.net.PacketReader;

public class FriendRemovePacketReader extends PacketReader {

    @Override
    public Event read(Player player, Packet msg) throws Exception {
        long usernameHash = msg.readLong();
        Player affectedPlayer = World.getInstance().getPlayer(usernameHash);
        player.getComponent(Communication.class).removeFriend(usernameHash);
        if(affectedPlayer != null && affectedPlayer.loggedIn() && player.getComponent(Settings.class).getPrivacySetting(1) && affectedPlayer.getComponent(Communication.class).isFriendsWith(player.getUsernameHash())) {
            ActionSender.sendFriendUpdate(affectedPlayer, player.getUsernameHash(), false);
        }
        return null;
    }
}
