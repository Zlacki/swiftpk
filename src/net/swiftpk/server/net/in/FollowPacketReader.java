package net.swiftpk.server.net.in;

import net.swiftpk.server.kplugin.event.Event;
import net.swiftpk.server.model.MobState;
import net.swiftpk.server.model.Player;
import net.swiftpk.server.model.terrain.World;
import net.swiftpk.server.net.ActionSender;
import net.swiftpk.server.net.Packet;
import net.swiftpk.server.net.PacketReader;

public class FollowPacketReader extends PacketReader {

    @Override
    public Event read(Player player, Packet msg) throws Exception {
        Player affectedPlayer = World.getInstance().getPlayer(msg.readShort());
        if (player.getState() != MobState.Idle || affectedPlayer == null || affectedPlayer.timeSinceAttacked() <= 3000) {
            player.resetPath();
            return null;
        }
        player.resetFollowing();
        player.setFollowing(affectedPlayer, 2);
        ActionSender.sendMessage(player, "@gam@Following " + affectedPlayer.getUsername());
        return null;
    }
}
