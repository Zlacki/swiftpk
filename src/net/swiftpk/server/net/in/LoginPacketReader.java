package net.swiftpk.server.net.in;

import net.swiftpk.server.Server;
import net.swiftpk.server.kplugin.event.Event;
import net.swiftpk.server.model.Player;
import net.swiftpk.server.model.component.Communication;
import net.swiftpk.server.net.Packet;
import net.swiftpk.server.net.PacketReader;
import net.swiftpk.server.util.RSA;

public class LoginPacketReader extends PacketReader {

    @Override
    public Event read(Player player, Packet msg) throws Exception {
        if(player.getClient().getServerKey().get() == null)
            return null;
        byte[] loginPackets = RSA.decrypt(msg.getData());
        Packet loginPacket = new Packet(0, loginPackets, true);
        boolean reconnecting = (loginPacket.readByte() == 1);
        int clientVersion = loginPacket.readInt();
        int[] sessionKeys = new int[4];
        for(int key = 0; key < sessionKeys.length; key++)
            sessionKeys[key] = loginPacket.readInt();
        String username = loginPacket.readString(20).trim();
        String password = loginPacket.readString(20).trim();
        player.setCredentials(username, password, reconnecting);
        byte loginCode = (byte) -1;
        if(clientVersion != Server.getServer().getConf().getInt("version"))
            loginCode = 0x5;
        else if(!player.setSessionKeys(sessionKeys))
            loginCode = (byte) 8;
        player.setResponseCode(loginCode);
        Server.getServer().getLoginService().submit(player);
        return null;
    }
}
