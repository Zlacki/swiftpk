package net.swiftpk.server.net.in;

import net.swiftpk.server.kplugin.event.Event;
import net.swiftpk.server.model.MobState;
import net.swiftpk.server.model.Player;
import net.swiftpk.server.net.Packet;
import net.swiftpk.server.net.PacketReader;

public class LogoutPacketReader extends PacketReader {

    @Override
    public Event read(Player player, Packet msg) throws Exception {
        if(player.canLogout() && player.getState() == MobState.Idle || player.isAdmin())
            player.getClient().unregister();
        return null;
    }
}
