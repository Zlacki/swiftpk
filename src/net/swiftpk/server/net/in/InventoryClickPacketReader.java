package net.swiftpk.server.net.in;

import net.swiftpk.server.Server;
import net.swiftpk.server.kplugin.event.Event;
import net.swiftpk.server.kplugin.event.impl.InventoryClickEvent;
import net.swiftpk.server.model.InvItem;
import net.swiftpk.server.model.MobState;
import net.swiftpk.server.model.Player;
import net.swiftpk.server.net.ActionSender;
import net.swiftpk.server.net.Packet;
import net.swiftpk.server.net.PacketReader;

public class InventoryClickPacketReader extends PacketReader {

    @Override
    public Event read(Player player, Packet msg) throws Exception {
        int idx = msg.readShort();
        if (player.getState() != MobState.Idle || idx < 0 || idx >= player.getInventory().size())
            return null;
        InvItem item = player.getInventory().get(idx);
        if (item == null || item.getDef().getCommand().equals(""))
            return null;
        if(!Server.getServer().getPlugins().post(new InventoryClickEvent(player, item)))
            ActionSender.sendMessage(player, "Nothing interesting happens.");
//        return new InventoryClickEvent(player, item);
        return null;
    }
}
