package net.swiftpk.server.net.in;

import net.swiftpk.server.kplugin.event.Event;
import net.swiftpk.server.model.MobState;
import net.swiftpk.server.model.Player;
import net.swiftpk.server.model.component.Communication;
import net.swiftpk.server.model.component.Settings;
import net.swiftpk.server.model.terrain.World;
import net.swiftpk.server.net.ActionSender;
import net.swiftpk.server.net.Packet;
import net.swiftpk.server.net.PacketReader;

public class DuelRulesPacketReader extends PacketReader {

    @Override
    public Event read(Player player, Packet msg) throws Exception {
        if(player.isFighting())
            return null;
        Player affectedPlayer = player.getDuel().getWishTo();
        if(affectedPlayer == null || player.getState() != MobState.Dueling
                || affectedPlayer.getState() != MobState.Dueling) {
            unsetOptions(player);
            unsetOptions(affectedPlayer);
            return null;
        }
        if((affectedPlayer.getDuel().isConfirmed(1) || affectedPlayer.getDuel()
                .isConfirmed(2))
                && (player.getDuel().isConfirmed(1) || player.getDuel()
                .isConfirmed(2)))
            return null;
        for(int i = 0; i < 4; i++) {
            boolean ruleValue = msg.readByte() == 0x01;
            player.getDuel().setRule(i, ruleValue);
            affectedPlayer.getDuel().setRule(i, ruleValue);
        }
        unsetConfirmed(player, affectedPlayer);
        ActionSender.sendDuelRules(player);
        ActionSender.sendDuelRules(affectedPlayer);
        return null;
    }

    private void unsetConfirmed(Player... players) {
        for(Player p : players) {
            for(int i : new int[] { 1, 2 })
                p.getDuel().setConfirmed(i, false);
        }
    }

    private void unsetOptions(Player p) {
        if(p == null) {
            return;
        }
        unsetConfirmed(p);
        p.getDuel().reset();
        p.setState(MobState.Idle);
    }
}
