package net.swiftpk.server.net.in;

import net.swiftpk.server.kplugin.event.Event;
import net.swiftpk.server.model.MobState;
import net.swiftpk.server.model.Player;
import net.swiftpk.server.model.component.Communication;
import net.swiftpk.server.model.component.Settings;
import net.swiftpk.server.model.terrain.World;
import net.swiftpk.server.net.ActionSender;
import net.swiftpk.server.net.Packet;
import net.swiftpk.server.net.PacketReader;

public class TradeRequestPacketReader extends PacketReader {

    @Override
    public Event read(Player player, Packet msg) throws Exception {
        if(player.isFighting() || player.isDueling())
            return null;
        Player affectedPlayer = World.getInstance().getPlayer(msg.readShort());
        if(affectedPlayer == null || !player.withinRange(affectedPlayer)
                || player.getState() != MobState.Idle) {
            return null;
        }
        if(affectedPlayer.getComponent(Settings.class).getPrivacySetting(2) && !affectedPlayer.getComponent(Communication.class).isFriendsWith(player.getUsernameHash())) {
            ActionSender.sendMessage(player, "This player has trade requests blocked.");
            return null;
        }
        player.getTrade().setWishTo(affectedPlayer);
        ActionSender.sendMessage(player, "Sending trade request.");
        ActionSender.sendMessage(affectedPlayer, player.getUsername() + " wishes to trade with you.");
        if(player.getState() == MobState.Idle
                && affectedPlayer.getTrade().getWishTo() != null
                && affectedPlayer.getTrade().getWishTo().equals(player)
                && affectedPlayer.getState() == MobState.Idle) {
            player.setState(MobState.Trading);
            player.resetPath();
            affectedPlayer.setState(MobState.Trading);
            affectedPlayer.resetPath();
            ActionSender.sendTradeWindowOpen(player);
            ActionSender.sendTradeWindowOpen(affectedPlayer);
        }
        return null;
    }
}
