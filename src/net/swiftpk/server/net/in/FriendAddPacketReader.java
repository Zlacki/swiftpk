package net.swiftpk.server.net.in;

import net.swiftpk.server.kplugin.event.Event;
import net.swiftpk.server.model.Player;
import net.swiftpk.server.model.component.Communication;
import net.swiftpk.server.model.component.Settings;
import net.swiftpk.server.model.terrain.World;
import net.swiftpk.server.net.ActionSender;
import net.swiftpk.server.net.Packet;
import net.swiftpk.server.net.PacketReader;

public class FriendAddPacketReader extends PacketReader {

    @Override
    public Event read(Player player, Packet msg) throws Exception {
        long usernameHash = msg.readLong();
        Player affectedPlayer = World.getInstance().getPlayer(usernameHash);
        if(player.getComponent(Communication.class).getFriendList().size() >= 100) {
            ActionSender.sendMessage(player, "Your friends list is full");
            return null;
        }
        player.getComponent(Communication.class).addFriend(usernameHash);
        if(affectedPlayer != null && affectedPlayer.loggedIn()) {
            if(affectedPlayer.getComponent(Communication.class).isFriendsWith(player.getUsernameHash())) {
                ActionSender.sendFriendUpdate(affectedPlayer, player.getUsernameHash(), true);
                ActionSender.sendFriendUpdate(player, usernameHash, true);
            } else if(!affectedPlayer.getComponent(Settings.class).getPrivacySetting(1)) {
                ActionSender.sendFriendUpdate(player, usernameHash, true);
            }
        }
        return null;
    }
}
