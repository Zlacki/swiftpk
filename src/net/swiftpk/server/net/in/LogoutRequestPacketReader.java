package net.swiftpk.server.net.in;

import net.swiftpk.server.kplugin.event.Event;
import net.swiftpk.server.model.MobState;
import net.swiftpk.server.model.Player;
import net.swiftpk.server.net.ActionSender;
import net.swiftpk.server.net.Packet;
import net.swiftpk.server.net.PacketReader;

public class LogoutRequestPacketReader extends PacketReader {

    @Override
    public Event read(Player player, Packet msg) throws Exception {
        if(player.canLogout() && (player.getState() == MobState.Idle || player.getState() == MobState.Quitting) || player.isAdmin())
            ActionSender.sendLogout(player);
        else {
            if(player.getLocation().inWilderness())
                ActionSender.sendMessage(player, "You must be still for 8 seconds in order to log out.");
            ActionSender.sendCantLogout(player);
        }
        return null;
    }
}
