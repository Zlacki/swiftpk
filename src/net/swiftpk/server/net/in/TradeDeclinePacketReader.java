package net.swiftpk.server.net.in;

import net.swiftpk.server.kplugin.event.Event;
import net.swiftpk.server.model.MobState;
import net.swiftpk.server.model.Player;
import net.swiftpk.server.net.ActionSender;
import net.swiftpk.server.net.Packet;
import net.swiftpk.server.net.PacketReader;

public class TradeDeclinePacketReader extends PacketReader {

    @Override
    public Event read(Player player, Packet msg) throws Exception {
        if(player.isFighting() || player.isDueling())
            return null;
        Player affectedPlayer = player.getTrade().getWishTo();
        if(affectedPlayer != null) {
            unsetOptions(affectedPlayer);
            ActionSender.sendTradeWindowClose(affectedPlayer);
            ActionSender.sendMessage(affectedPlayer, player.getUsername() + " has declined the trade.");
        }
        unsetOptions(player);
        ActionSender.sendTradeWindowClose(player);
        return null;
    }

    private void unsetOptions(Player p) {
        if(p == null) {
            return;
        }
        p.getTrade().reset();
        p.setState(MobState.Idle);
    }
}
