package net.swiftpk.server.net.in;

import net.swiftpk.server.Server;
import net.swiftpk.server.kplugin.event.Event;
import net.swiftpk.server.kplugin.event.impl.InventoryClickEvent;
import net.swiftpk.server.kplugin.event.impl.InventoryClickEvent.InventoryOnLootableEvent;
import net.swiftpk.server.kplugin.event.impl.InventoryClickEvent.InventoryOnObjectEvent;
import net.swiftpk.server.model.*;
import net.swiftpk.server.model.entity.WalkToAction;
import net.swiftpk.server.model.terrain.World;
import net.swiftpk.server.model.terrain.tiles.Point;
import net.swiftpk.server.net.ActionSender;
import net.swiftpk.server.net.Packet;
import net.swiftpk.server.net.PacketReader;

public class InventoryOnLootablePacketReader extends PacketReader {

    @Override
    public Event read(Player player, Packet msg) throws Exception {
        Point location = Point.location(msg.readShort(), msg.readShort(), player.getLayer());
        int id = msg.readShort();
        final InvItem myItem = player.getInventory().get(msg.readShort());
        final Item item = World.getInstance().getItem(id, location);
        if (player.getState() != MobState.Idle || myItem == null || item == null || item.isRemoved() || !item.visibleTo(player)) {
            player.resetPath();
            return null;
        }
        /*
         * if (tile.hasGameObject() && myItem.getID() != 135) { // TODO: Workaround.
         * player.getActionSender().sendMessage(
         * "You cannot do that here, please move to a new area."); return; }
         */
        boolean blocked = false;
        for (int i : new int[] { 1, 2, 4, 8 })
            if (!PathValidation.checkAdjacent(player.getLocation(), location))
                blocked = true;
        player.setWalkToAction(new WalkToAction(player, item.getLocation(), blocked ? 1 : 0) {
            @Override
            public void execute() {
                player.resetAll();
                if (player.getState() != MobState.Idle || !player.getInventory().contains(myItem) || item.isRemoved() || !item.visibleTo(player))
                    return;
                if(!Server.getServer().getPlugins().post(new InventoryOnLootableEvent(player, myItem, item)))
                    ActionSender.sendMessage(player, "Nothing interesting happens.");
            }
        });
        return null;
    }
}
