package net.swiftpk.server.net.in;

import net.swiftpk.server.kplugin.event.Event;
import net.swiftpk.server.model.MobState;
import net.swiftpk.server.model.Player;
import net.swiftpk.server.model.component.Communication;
import net.swiftpk.server.net.ActionSender;
import net.swiftpk.server.net.Packet;
import net.swiftpk.server.net.PacketReader;

public class DuelAcceptPacketReader extends PacketReader {

    @Override
    public Event read(Player player, Packet msg) throws Exception {
        if(player.isFighting())
            return null;
        Player affectedPlayer = player.getDuel().getWishTo();
        if(affectedPlayer == null || player.getState() != MobState.Dueling
                || affectedPlayer.getState() != MobState.Dueling) {
            unsetOptions(player);
            unsetOptions(affectedPlayer);
            return null;
        }
        player.getDuel().setConfirmed(1, true);
        ActionSender.sendDuelAcceptUpdate(player);
        ActionSender.sendDuelAcceptUpdate(affectedPlayer);
        if(affectedPlayer.getDuel().isConfirmed(1)) {
            ActionSender.sendDuelAccept(player);
            ActionSender.sendDuelAccept(affectedPlayer);
        }
        return null;
    }

    private void unsetConfirmed(Player... players) {
        for(Player p : players) {
            for(int i : new int[] { 1, 2 })
                p.getDuel().setConfirmed(i, false);
        }
    }

    private void unsetOptions(Player p) {
        if(p == null) {
            return;
        }
        unsetConfirmed(p);
        p.getDuel().reset();
        p.setState(MobState.Idle);
    }
}
