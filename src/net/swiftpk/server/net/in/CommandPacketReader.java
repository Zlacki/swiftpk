package net.swiftpk.server.net.in;

import java.util.ArrayList;
import java.util.List;

import net.swiftpk.server.Server;
import net.swiftpk.server.kplugin.event.Event;
import net.swiftpk.server.kplugin.event.impl.CommandEvent;
import net.swiftpk.server.model.Player;
import net.swiftpk.server.net.Packet;
import net.swiftpk.server.net.PacketReader;
import net.swiftpk.server.util.Logger;

public class CommandPacketReader extends PacketReader {

    @Override
    public Event read(Player player, Packet msg) throws Exception {
        String s = new String(msg.getData()).trim().toLowerCase();
        int firstSpace = s.indexOf(" ");
        if (firstSpace == -1)
            return new CommandEvent(player, s);

        String name = s.substring(0, firstSpace);
        String[] args = modalParse(s.substring(firstSpace + 1));
        Logger.log("cmd.log", "[Command] from '" + player.getUsername() + "'; `" + s + "` (group=" + player.getRank() + ")");
        Server.getServer().getPlugins().post(new CommandEvent(player, name, args));
        return null;
    }

    private String[] modalParse(String s) {
        StringBuilder cur = new StringBuilder();
        boolean escaped = false, inQuotes = false;
        List<String> out = new ArrayList<String>();
        for (char c : s.toCharArray()) {
            if (c == '\\') {
                escaped = !escaped;
                continue;
            }
            if (c == '\'' && !escaped) {
                if (inQuotes) {
                    if (cur.length() > 0) {
                        out.add(cur.toString());
                    }
                    cur = new StringBuilder();
                }
                inQuotes = !inQuotes;
                continue;
            }
            if (c == ' ' && !escaped && !inQuotes) {
                if (cur.length() > 0) {
                    out.add(cur.toString());
                }
                cur = new StringBuilder();
                continue;
            }
            if (escaped) {
                escaped = false;
            }
            cur.append(c);
        }
        if (cur.length() > 0) {
            out.add(cur.toString());
        }
        return out.toArray(new String[out.size()]);
    }
}
