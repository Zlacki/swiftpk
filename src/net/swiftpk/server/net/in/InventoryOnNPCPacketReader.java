package net.swiftpk.server.net.in;

import net.swiftpk.server.Server;
import net.swiftpk.server.kplugin.event.Event;
import net.swiftpk.server.kplugin.event.impl.InventoryClickEvent.InventoryOnNPCEvent;
import net.swiftpk.server.model.InvItem;
import net.swiftpk.server.model.MobState;
import net.swiftpk.server.model.Player;
import net.swiftpk.server.model.entity.NPC;
import net.swiftpk.server.model.entity.WalkToEntityAction;
import net.swiftpk.server.model.terrain.World;
import net.swiftpk.server.net.ActionSender;
import net.swiftpk.server.net.Packet;
import net.swiftpk.server.net.PacketReader;

public class InventoryOnNPCPacketReader extends PacketReader {

    @Override
    public Event read(Player player, Packet msg) throws Exception {
        NPC affectedNPC = World.getInstance().getNPC(msg.readShort());
        InvItem item = player.getInventory().get(msg.readShort());
        if (player.getState() != MobState.Idle || affectedNPC == null || item == null) {
            player.resetPath();
            return null;
        }
        player.setFollowing(affectedNPC, 1);
        player.setWalkToAction(new WalkToEntityAction(player, affectedNPC, 1) {
            @Override
            public void execute() {
                player.resetAll();
                if (!player.getInventory().contains(item) || player.getState() != MobState.Idle)
                    return;
                if(!Server.getServer().getPlugins().post(new InventoryOnNPCEvent(player, item, affectedNPC)))
                    ActionSender.sendMessage(player, "Nothing interesting happens.");
            }
        });
        return null;
    }
}
