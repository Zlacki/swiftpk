package net.swiftpk.server.net.in;

import net.swiftpk.server.kplugin.event.Event;
import net.swiftpk.server.model.InvItem;
import net.swiftpk.server.model.MobState;
import net.swiftpk.server.model.Player;
import net.swiftpk.server.model.Shop;
import net.swiftpk.server.model.component.Communication;
import net.swiftpk.server.net.ActionSender;
import net.swiftpk.server.net.Packet;
import net.swiftpk.server.net.PacketReader;

public class ShopBuyPacketReader extends PacketReader {

    @Override
    public Event read(Player player, Packet msg) throws Exception {
        if(player.getState() != MobState.Shopping)
            return null;
        Shop shop = player.getShop();
        int id = msg.readShort();
        int amount = msg.readInt();
        InvItem item = new InvItem(id, amount);
        int value = shop.getItemBuyPrice(id) * amount;
        if(shop.getItemCount(id) < amount)
            return null;
        if(player.getInventory().countById(10) < value) {
            ActionSender.sendMessage(player, "You don't have enough money!");
            return null;
        }
        if((30 - player.getInventory().size())
                + player.getInventory().getFreedSlots(new InvItem(10, value)) < player
                .getInventory().getRequiredSlots(item)) {
            ActionSender.sendMessage(player, "You can't hold the objects you are trying to buy!");
            return null;
        }
        int price = item.getDef().getBasePrice();
        // TODO: Decide if needed, rewrite some code?
        if(price == 0 || value < price)
            return null;
        if(player.getInventory().remove(10, value) > -1) {
            shop.removeShopItem(item);
            player.getInventory().add(item);
            ActionSender.sendSound(player, "coins");
            ActionSender.sendInventory(player);
        }
        return null;
    }
}
