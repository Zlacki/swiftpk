package net.swiftpk.server.net.in;

import net.swiftpk.server.kplugin.event.Event;
import net.swiftpk.server.model.MobState;
import net.swiftpk.server.model.Player;
import net.swiftpk.server.model.QueryMenu;
import net.swiftpk.server.net.Packet;
import net.swiftpk.server.net.PacketBuilder;
import net.swiftpk.server.net.PacketReader;

public class MenuAnswerPacketReader extends PacketReader {

    @Override
    public Event read(Player player, Packet msg) throws Exception {
        QueryMenu qm = player.getQueryMenu();
        byte answer = msg.readByte();
        if(qm != null && player.getState() == MobState.Idle && answer >= 0 && answer <= qm.size()) {
            qm.choseQuery(player, answer);
            player.unsetQueryMenu();
        }

        return null;
    }
}
