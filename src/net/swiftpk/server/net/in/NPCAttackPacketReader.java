package net.swiftpk.server.net.in;

import net.swiftpk.server.kplugin.event.Event;
import net.swiftpk.server.model.MobState;
import net.swiftpk.server.model.Player;
import net.swiftpk.server.model.entity.NPC;
import net.swiftpk.server.model.entity.WalkToEntityAction;
import net.swiftpk.server.model.terrain.World;
import net.swiftpk.server.net.ActionSender;
import net.swiftpk.server.net.Packet;
import net.swiftpk.server.net.PacketReader;
import net.swiftpk.server.tickable.impl.actor.CombatHitTick;
import net.swiftpk.server.tickable.impl.combat.RangedTick;

public class NPCAttackPacketReader extends PacketReader {

    @Override
    public Event read(Player player, Packet msg) throws Exception {
        final NPC npc = World.getInstance().getNPC(msg.readShort());
        if (player.isFighting()) {
            ActionSender.sendMessage(player, "You're already fighting!");
            player.resetPath();
            return null;
        }
        if (player.getState() != MobState.Idle && player.getState() != MobState.Eating) {
            player.resetPath();
            return null;
        }
        if (!npc.getDef().isAttackable() && player.getRank() == 0) {
            ActionSender.sendMessage(player, "@gam@This player does not appear interested in fighting");
            player.resetPath();
            return null;
        }
        if (npc.isFighting()) {
            ActionSender.sendMessage(player, "Your opponent is busy!");
            player.resetPath();
            return null;
        }
        if (player.getBowStats() != null) {
            player.setFollowing(npc, 5);
            player.setWalkToAction(new WalkToEntityAction(player, npc, 5) {
                @Override
                public void execute() {
                    player.resetAll();
                    World.getInstance().submit(new RangedTick(player, npc));
                }
            });
        } else {
            player.setFollowing(npc, 2);
            player.setWalkToAction(new WalkToEntityAction(player, npc, 2) {
                public void execute() {
                    if (npc.isFighting()) {
                        ActionSender.sendMessage(player, "Your opponent is busy!");
                        return;
                    }
                    if (player.isFighting()) {
                        ActionSender.sendMessage(player, "You're already fighting!");
                        return;
                    }
                    if (npc.getState() != MobState.Idle || player.getState() != MobState.Idle)
                        return;
                    player.resetAll();
                    player.startCombat(npc);
/*                    npc.resetPath();
                    player.setLocation(npc.getLocation(), true);
                    player.setState(MobState.RightFighting);
                    player.setCombatPartner(npc);
                    npc.setState(MobState.LeftFighting);
                    npc.setCombatPartner(player);
                    npc.setRequiresSpriteUpdate(true);
                    World.getInstance().submit(new CombatHitTick(player, npc));*/
                }
            });
        }
        return null;
    }
}
