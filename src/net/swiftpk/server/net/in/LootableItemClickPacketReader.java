package net.swiftpk.server.net.in;

import net.swiftpk.server.Server;
import net.swiftpk.server.kplugin.event.Event;
import net.swiftpk.server.kplugin.event.impl.LootableItemClickEvent;
import net.swiftpk.server.model.Item;
import net.swiftpk.server.model.MobState;
import net.swiftpk.server.model.Player;
import net.swiftpk.server.model.terrain.World;
import net.swiftpk.server.model.terrain.tiles.Point;
import net.swiftpk.server.net.Packet;
import net.swiftpk.server.net.PacketReader;

public class LootableItemClickPacketReader extends PacketReader {

    @Override
    public Event read(Player player, Packet msg) throws Exception {
        int x = msg.readShort();
        int y = msg.readShort();
        int id = msg.readShort();
        Point dest = Point.location(x, y, player.getLayer());
        Item item = World.getInstance().getItem(id, dest);
        if (player.getState() != MobState.Idle || item == null || !item.visibleTo(player) || item.isRemoved()) {
            player.resetPath();
            return null;
        }
        Server.getServer().getPlugins().post(new LootableItemClickEvent(player, item));
        return null;
    }
}
