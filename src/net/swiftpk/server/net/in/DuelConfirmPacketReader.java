package net.swiftpk.server.net.in;

import net.swiftpk.server.kplugin.event.Event;
import net.swiftpk.server.model.InvItem;
import net.swiftpk.server.model.MobState;
import net.swiftpk.server.model.Player;
import net.swiftpk.server.model.terrain.World;
import net.swiftpk.server.net.ActionSender;
import net.swiftpk.server.net.Packet;
import net.swiftpk.server.net.PacketReader;
import net.swiftpk.server.tickable.impl.actor.CombatHitTick;

public class DuelConfirmPacketReader extends PacketReader {

    @Override
    public Event read(Player player, Packet msg) throws Exception {
        if(player.isFighting())
            return null;
        Player affectedPlayer = player.getDuel().getWishTo();
        if(affectedPlayer == null || player.getState() != MobState.Dueling
                || affectedPlayer.getState() != MobState.Dueling) {
            unsetOptions(player);
            unsetOptions(affectedPlayer);
            return null;
        }
        player.getDuel().setConfirmed(2, true);
        if(affectedPlayer.getDuel().isConfirmed(2)) {
            ActionSender.sendDuelWindowClose(player);
            ActionSender.sendDuelWindowClose(affectedPlayer);
            affectedPlayer.resetPath();
            player.resetPath();
            player.getDuel().setDueling(true);
            affectedPlayer.getDuel().setDueling(true);
            player.setLocation(affectedPlayer.getLocation(), true);
            player.setState(MobState.LeftFighting);
            affectedPlayer.setState(MobState.RightFighting);
            player.setCombatPartner(affectedPlayer);
            affectedPlayer.setCombatPartner(player);
            if(!player.canPray()) {
                for(int i = 0; i < 14; i++) {
                    if(player.isPrayerActivated(i)) {
                        player.setPrayer(i, false);
                        player.removePrayerDrain(i);
                    }
                    if(affectedPlayer.isPrayerActivated(i)) {
                        affectedPlayer.setPrayer(i, false);
                        affectedPlayer.removePrayerDrain(i);
                    }
                }
            }
            if(!player.canWield()) {
                for(InvItem item : player.getInventory().getItems()) {
                    if(item.isWielded()) {
                        item.setWield(false);
                        player.updateWornItems(item.getWieldableDef()
                                        .getWieldPos(),
                                player.getAppearance().getSprite(
                                        item.getWieldableDef().getWieldPos()));
                    }
                }
                for(InvItem item : affectedPlayer.getInventory().getItems()) {
                    if(item.isWielded()) {
                        item.setWield(false);
                        affectedPlayer.updateWornItems(item.getWieldableDef()
                                .getWieldPos(), affectedPlayer
                                .getAppearance().getSprite(
                                        item.getWieldableDef().getWieldPos()));
                    }
                }
            }
            ActionSender.sendMessage(affectedPlayer, "Duel commencing!");
            ActionSender.sendMessage(player, "Duel commencing!");
            World.getInstance().submit(new CombatHitTick(player, affectedPlayer));
        }
        return null;
    }

    private void unsetConfirmed(Player... players) {
        for(Player p : players) {
            for(int i : new int[] { 1, 2 })
                p.getDuel().setConfirmed(i, false);
        }
    }

    private void unsetOptions(Player p) {
        if(p == null) {
            return;
        }
        unsetConfirmed(p);
        p.getDuel().reset();
        p.setState(MobState.Idle);
    }
}
