package net.swiftpk.server.net.in;

import net.swiftpk.server.kplugin.event.Event;
import net.swiftpk.server.model.InvItem;
import net.swiftpk.server.model.MobState;
import net.swiftpk.server.model.Player;
import net.swiftpk.server.model.component.Inventory;
import net.swiftpk.server.net.ActionSender;
import net.swiftpk.server.net.Packet;
import net.swiftpk.server.net.PacketReader;

public class DuelOfferPacketReader extends PacketReader {

    @Override
    public Event read(Player player, Packet msg) throws Exception {
        if(player.isFighting())
            return null;
        Player affectedPlayer = player.getDuel().getWishTo();
        if(affectedPlayer == null
                || player.getState() != MobState.Dueling
                || affectedPlayer.getState() != MobState.Dueling
                || (player.getDuel().isConfirmed(1) && affectedPlayer.getDuel()
                .isConfirmed(1))) {
            unsetOptions(player);
            unsetOptions(affectedPlayer);
            return null;
        }
        if((affectedPlayer.getDuel().isConfirmed(1) || affectedPlayer.getDuel()
                .isConfirmed(2))
                && (player.getDuel().isConfirmed(1) || player.getDuel()
                .isConfirmed(2)))
            return null;
        unsetConfirmed(player, affectedPlayer);
        ActionSender.sendDuelAcceptUpdate(player);
        ActionSender.sendDuelAcceptUpdate(affectedPlayer);
        Inventory DuelOffer = new Inventory();
        DuelOffer.setOwner(player);
        player.getDuel().resetOffer();
        int count = msg.readByte();
        for(int slot = 0; slot < count; slot++) {
            DuelOffer.add(new InvItem(msg.readShort(), msg.readInt()));
        }
        for(InvItem item : DuelOffer.getItems()) {
            if(DuelOffer.countById(item.getID()) <= player.getInventory()
                    .countById(item.getID())) {
                player.getDuel().addToOffer(item);
            }
        }
        affectedPlayer.getDuel().setChanged();
        return null;
    }

    private void unsetConfirmed(Player... players) {
        for(Player p : players) {
            for(int i : new int[] { 1, 2 })
                p.getDuel().setConfirmed(i, false);
        }
    }

    private void unsetOptions(Player p) {
        if(p == null) {
            return;
        }
        unsetConfirmed(p);
        p.getDuel().reset();
        p.setState(MobState.Idle);
    }
}
