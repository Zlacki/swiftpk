package net.swiftpk.server.net.in;

import net.swiftpk.server.Server;
import net.swiftpk.server.kplugin.event.Event;
import net.swiftpk.server.model.Player;
import net.swiftpk.server.net.Client;
import net.swiftpk.server.net.Packet;
import net.swiftpk.server.net.PacketReader;
import net.swiftpk.server.util.DataConversions;
import net.swiftpk.server.util.RSA;

public class RegisterPacketReader extends PacketReader {

    @Override
    public Event read(Player player, Packet msg) throws Exception {
        Packet loginPacket = new Packet(0, RSA.decrypt(msg.getData()), true);
        int clientVersion = loginPacket.readInt();
        String usernameInput = loginPacket.readString(20).trim();
        String password = loginPacket.readString(20).trim();
        String username = DataConversions.hashToUsername(DataConversions.usernameToHash(usernameInput));

        byte loginCode = (byte) -1;
        if(clientVersion != Server.getServer().getConf().getInt("version"))
            loginCode = 0x5;

        if (usernameInput.length() < 4 || usernameInput.length() > 12 || usernameInput.toLowerCase().contains("mod")
                || usernameInput.toLowerCase().contains("admin"))
            loginCode = 0x13;

        if (password.length() < 4)
            loginCode = 0x13;

        player.setResponseCode(loginCode);
        player.setCredentials(username, password, false);
        Server.getServer().getRegistrationService().submit(player);
        return null;
    }
}
