package net.swiftpk.server.net.in;

import net.swiftpk.server.ent.EntityHandler;
import net.swiftpk.server.ent.defs.PrayerDef;
import net.swiftpk.server.kplugin.event.Event;
import net.swiftpk.server.model.Player;
import net.swiftpk.server.model.component.SkillSet;
import net.swiftpk.server.model.component.SkillSet.Skill;
import net.swiftpk.server.net.ActionSender;
import net.swiftpk.server.net.Packet;
import net.swiftpk.server.net.PacketReader;

public class PrayerActivatePacketReader extends PacketReader {

    @Override
    public Event read(Player player, Packet msg) throws Exception {
        int prayerID = msg.readByte();
        if(prayerID < 0 || prayerID >= 14) {
            // player.suspiciousEvent("Tried to use prayer out of range: "+prayerID);
            return null;
        }
        if(player.isDueling() && !player.canPray()) {
            ActionSender.sendMessage(player, "You cannot use prayer in this duel!");
            deactivatePrayer(player, prayerID);
            return null;
        }
        PrayerDef prayer = EntityHandler.getPrayerDef(prayerID);
        Skill prayerSkill = player.getSkillSet().get(SkillSet.PRAYER);
        if(prayerSkill.getMaxLevel() < prayer.getReqLevel()) {
            ActionSender.sendMessage(player, "Your prayer ability is not high enough to use this prayer");
            return null;
        }
        if(prayerSkill.getLevel() <= 0) {
            player.setPrayer(prayerID, false);
            ActionSender.sendMessage(player, "You have run out of prayer points. Return to a church to recharge");
            ActionSender.sendPrayers(player);
            return null;
        }
        activatePrayer(player, prayerID);
        ActionSender.sendPrayers(player);
        return null;
    }

    private boolean activatePrayer(Player player, int prayerID) {
        if(!player.isPrayerActivated(prayerID)) {
            if(prayerID == 11) {
                deactivatePrayer(player, 5);
                deactivatePrayer(player, 2);
            } else if(prayerID == 5) {
                deactivatePrayer(player, 2);
                deactivatePrayer(player, 11);
            } else if(prayerID == 2) {
                deactivatePrayer(player, 5);
                deactivatePrayer(player, 11);
            } else if(prayerID == 10) {
                deactivatePrayer(player, 4);
                deactivatePrayer(player, 1);
            } else if(prayerID == 4) {
                deactivatePrayer(player, 10);
                deactivatePrayer(player, 1);
            } else if(prayerID == 1) {
                deactivatePrayer(player, 10);
                deactivatePrayer(player, 4);
            } else if(prayerID == 9) {
                deactivatePrayer(player, 3);
                deactivatePrayer(player, 0);
            } else if(prayerID == 3) {
                deactivatePrayer(player, 9);
                deactivatePrayer(player, 0);
            } else if(prayerID == 0) {
                deactivatePrayer(player, 9);
                deactivatePrayer(player, 3);
            }
            player.setPrayer(prayerID, true);
            player.addPrayerDrain(prayerID);
            return true;
        }
        return false;
    }

    private boolean deactivatePrayer(Player player, int prayerID) {
        if(player.isPrayerActivated(prayerID)) {
            player.setPrayer(prayerID, false);
            player.removePrayerDrain(prayerID);
            return true;
        }
        return false;
    }
}
