package net.swiftpk.server.net.in;

import net.swiftpk.server.Server;
import net.swiftpk.server.kplugin.event.Event;
import net.swiftpk.server.kplugin.event.impl.InventoryClickEvent.InventoryDropEvent;
import net.swiftpk.server.model.InvItem;
import net.swiftpk.server.model.MobState;
import net.swiftpk.server.model.Player;
import net.swiftpk.server.net.Packet;
import net.swiftpk.server.net.PacketReader;

public class InventoryDropPacketReader extends PacketReader {

    @Override
    public Event read(Player player, Packet msg) throws Exception {
        int idx = msg.readShort();
        if (player.getState() != MobState.Idle || idx < 0 || idx >= player.getInventory().size())
            return null;
        InvItem item = player.getInventory().get(idx);
        if (item == null)
            return null;
        Server.getServer().getPlugins().post(new InventoryDropEvent(player, item));
        return null;
    }
}
