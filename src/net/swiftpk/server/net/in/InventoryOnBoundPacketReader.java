package net.swiftpk.server.net.in;

import net.swiftpk.server.Server;
import net.swiftpk.server.kplugin.event.Event;
import net.swiftpk.server.kplugin.event.impl.InventoryClickEvent.InventoryOnBoundEvent;
import net.swiftpk.server.model.InvItem;
import net.swiftpk.server.model.MobState;
import net.swiftpk.server.model.Player;
import net.swiftpk.server.model.entity.GameObject;
import net.swiftpk.server.model.entity.WalkToAction;
import net.swiftpk.server.model.terrain.World;
import net.swiftpk.server.net.ActionSender;
import net.swiftpk.server.net.Packet;
import net.swiftpk.server.net.PacketReader;

public class InventoryOnBoundPacketReader extends PacketReader {

    @Override
    public Event read(Player player, Packet msg) throws Exception {
        GameObject object = World.getInstance().getGameObject(msg.readShort(), msg.readShort());
        msg.readByte(); // object dir - does it matter...?
        InvItem item = player.getInventory().get(msg.readShort());
        if(player.getState() != MobState.Idle || item == null || object == null || object.getType() != 1) {
            player.resetPath();
            return null;
        }
        player.setWalkToAction(new WalkToAction(player, object.getLocation(), 1) {
            @Override
            public void execute() {
                player.resetAll();
                if (object.getType() != 1 || !player.getInventory().contains(item))
                    return;
                if(!Server.getServer().getPlugins().post(new InventoryOnBoundEvent(player, item, object)))
                    ActionSender.sendMessage(player, "Nothing interesting happens.");
            }
        });
        return null;
    }
}
