package net.swiftpk.server.net.in;

import net.swiftpk.server.kplugin.event.Event;
import net.swiftpk.server.model.MobState;
import net.swiftpk.server.model.Player;
import net.swiftpk.server.net.ActionSender;
import net.swiftpk.server.net.Packet;
import net.swiftpk.server.net.PacketReader;

public class ShopClosePacketReader extends PacketReader {

    @Override
    public Event read(Player player, Packet msg) throws Exception {
        if(player.getState() != MobState.Shopping)
            return null;
        player.getShop().removePlayer(player);
        player.setState(MobState.Idle);
        ActionSender.sendShopClose(player);
        return null;
    }
}
