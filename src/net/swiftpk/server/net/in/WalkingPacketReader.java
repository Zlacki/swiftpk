package net.swiftpk.server.net.in;

import net.swiftpk.server.kplugin.event.Event;
import net.swiftpk.server.model.MobState;
import net.swiftpk.server.model.Player;
import net.swiftpk.server.model.entity.Mob;
import net.swiftpk.server.model.terrain.Path;
import net.swiftpk.server.model.terrain.Path.PathType;
import net.swiftpk.server.model.terrain.World;
import net.swiftpk.server.model.terrain.tiles.Point;
import net.swiftpk.server.net.ActionSender;
import net.swiftpk.server.net.Packet;
import net.swiftpk.server.net.PacketReader;
import net.swiftpk.server.tickable.Tickable;
import net.swiftpk.server.tickable.impl.combat.RangedTick;

public class WalkingPacketReader extends PacketReader {
    @Override
    public Event read(Player player, Packet msg) throws Exception {
        if (player.getState() != MobState.Idle && !player.isFighting())
            return null;

        for (Tickable t : World.getInstance().getTickableManager().getTickables()) {
            if (t instanceof RangedTick) {
                RangedTick rangedTick = (RangedTick) t;
                if (rangedTick.getOwner().equals(player)) {
                    rangedTick.stop();
                    break;
                }
            }
        }

        int startX = msg.readShort();
        int startY = msg.readShort();
        int waypointCount = msg.remaining() / 2;
        if (player.isFighting()) {
            /**
             * Distanced actions create this to path to the target.
             */
            if (msg.getID() == 157)
                return null;
            Mob combatPartner = player.getCombatPartner();
            if (combatPartner instanceof Player) {
                if (player.isDueling()) {
                    if (!player.canRun()) {
                        ActionSender.sendMessage(player, "You cannot retreat from this duel!");
                        return null;
                    }
                    player.getDuel().getWishTo().getDuel().reset();
                    player.getDuel().reset();
                }
                ActionSender.sendMessage((Player) combatPartner, "Your opponent is retreating");
            }

            if (combatPartner.getCombatRound() < 3) {
                ActionSender.sendMessage(player, "You can't retreat during the first 3 rounds of combat");
                return null;
            }

            player.resetCombat();
            combatPartner.resetCombat();
            player.setNotFighting(startX != player.getLocation().getX() || startY != player.getLocation().getY()
                    || waypointCount != 0);
        }
        PathType pathType = msg.getID() != 157 ? PathType.WALK_TO_POINT : PathType.WALK_TO_ENTITY;
        Path path = new Path(player, pathType);
        {
            path.addStep(startX, startY);
            for (int stepCount = 0; stepCount < waypointCount; stepCount++) {
                int stepDiffX = msg.readByte();
                int stepDiffY = msg.readByte();
                path.addStep(startX + stepDiffX, startY + stepDiffY);
            }
            path.finish();
        }
        player.resetAll();
        player.setPath(path);
        /**
         * No plugin behavior needed here.
         */
        return null;
    }
}
