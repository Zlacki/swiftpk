package net.swiftpk.server.net.in;

import net.swiftpk.server.kplugin.event.Event;
import net.swiftpk.server.model.Player;
import net.swiftpk.server.model.component.Communication;
import net.swiftpk.server.net.ActionSender;
import net.swiftpk.server.net.Packet;
import net.swiftpk.server.net.PacketReader;

public class PrayerDeactivatePacketReader extends PacketReader {

    @Override
    public Event read(Player player, Packet msg) throws Exception {
        if(player.isDueling() && !player.canPray()) {
            ActionSender.sendMessage(player, "You cannot use prayer in this duel!");
            return null;
        }
        int prayerID = msg.readByte();
        if(prayerID < 0 || prayerID >= 14) {
            // player.suspiciousEvent("Tried to use prayer out of range: "+prayerID);
            return null;
        }
        deactivatePrayer(player, prayerID);
        ActionSender.sendPrayers(player);
        return null;
    }

    private boolean deactivatePrayer(Player player, int prayerID) {
        if(player.isPrayerActivated(prayerID)) {
            player.setPrayer(prayerID, false);
            player.removePrayerDrain(prayerID);
            return true;
        }
        return false;
    }
}
