package net.swiftpk.server.net.in;

import net.swiftpk.server.kplugin.event.Event;
import net.swiftpk.server.model.Player;
import net.swiftpk.server.model.component.Communication;
import net.swiftpk.server.model.component.Settings;
import net.swiftpk.server.model.terrain.World;
import net.swiftpk.server.net.ActionSender;
import net.swiftpk.server.net.Packet;
import net.swiftpk.server.net.PacketReader;

public class IgnoreRemovePacketReader extends PacketReader {

    @Override
    public Event read(Player player, Packet msg) throws Exception {
        long usernameHash = msg.readLong();
        player.getComponent(Communication.class).removeIgnore(usernameHash);
        return null;
    }
}
