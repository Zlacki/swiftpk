package net.swiftpk.server.net.in;

import net.swiftpk.server.Server;
import net.swiftpk.server.kplugin.event.Event;
import net.swiftpk.server.kplugin.event.impl.NPCClickEvent.NPCFirstClickEvent;
import net.swiftpk.server.model.Entity;
import net.swiftpk.server.model.MobState;
import net.swiftpk.server.model.Player;
import net.swiftpk.server.model.entity.Mob;
import net.swiftpk.server.model.entity.NPC;
import net.swiftpk.server.model.entity.WalkToEntityAction;
import net.swiftpk.server.model.terrain.World;
import net.swiftpk.server.net.ActionSender;
import net.swiftpk.server.net.Packet;
import net.swiftpk.server.net.PacketReader;
import net.swiftpk.server.util.Formulae;

public class NPCClickPacketReader extends PacketReader {

    @Override
    public Event read(Player player, Packet msg) throws Exception {
        NPC target = World.getInstance().getNPC(msg.readShort());
        if (player.getState() != MobState.Idle) {
            player.resetPath();
            return null;
        }
        if (player.isFighting()) {
            ActionSender.sendMessage(player, "You can't do that whilst you are fighting");
            player.resetPath();
            return null;
        }
        player.setFollowing(target, 1);
        player.setWalkToAction(new WalkToEntityAction(player, target, 1) {
            @Override
            public void execute() {
                if (target.getState() != MobState.Idle) {
                    ActionSender.sendMessage(player, target.getDef().getName() + " is busy at the moment");
                    return;
                }
                player.resetAll();
                target.resetPath();
                faceEntity(player, target);
                faceEntity(target, player);
                if(!Server.getServer().getPlugins().post(new NPCFirstClickEvent(player, target)))
                    ActionSender.sendMessage(player, "The " + target.getDef().getName() + " does not appear interested in talking");
            }
        });
        return null;
    }

    private void faceEntity(Mob mob, Entity entity) {
        int dir = Formulae.getDirection(mob, entity);
        if (dir != -1) {
            mob.setSprite(dir);
            mob.setRequiresSpriteUpdate(true);
        }
    }
}
