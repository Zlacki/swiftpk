package net.swiftpk.server.net.in;

import net.swiftpk.server.Server;
import net.swiftpk.server.kplugin.event.Event;
import net.swiftpk.server.kplugin.event.impl.InventoryClickEvent.InventoryOnObjectEvent;
import net.swiftpk.server.model.InvItem;
import net.swiftpk.server.model.MobState;
import net.swiftpk.server.model.Player;
import net.swiftpk.server.model.entity.GameObject;
import net.swiftpk.server.model.entity.WalkToAction;
import net.swiftpk.server.model.terrain.World;
import net.swiftpk.server.net.ActionSender;
import net.swiftpk.server.net.Packet;
import net.swiftpk.server.net.PacketReader;

public class InventoryOnObjectPacketReader extends PacketReader {

    @Override
    public Event read(Player player, Packet msg) throws Exception {
        GameObject object = World.getInstance().getGameObject(msg.readShort(), msg.readShort());
        InvItem item = player.getInventory().get(msg.readShort());
        if(player.getState() != MobState.Idle || object == null || item == null) {
            player.resetPath();
            return null;
        }
        player.setWalkToAction(new WalkToAction(player, object.getLocation(), 1) {
            @Override
            public void execute() {
                player.resetAll();
                if(player.getState() != MobState.Idle || !player.getInventory().contains(item))
                    return;
                if(!Server.getServer().getPlugins().post(new InventoryOnObjectEvent(player, item, object)))
                    ActionSender.sendMessage(player, "Nothing interesting happens.");
            }
        });
        return null;
    }
}
