package net.swiftpk.server.net.in;

import net.swiftpk.server.kplugin.event.Event;
import net.swiftpk.server.model.InvItem;
import net.swiftpk.server.model.MobState;
import net.swiftpk.server.model.Player;
import net.swiftpk.server.model.component.Inventory;
import net.swiftpk.server.net.ActionSender;
import net.swiftpk.server.net.Packet;
import net.swiftpk.server.net.PacketReader;

public class TradeOfferPacketReader extends PacketReader {

    @Override
    public Event read(Player player, Packet msg) throws Exception {
        Player affectedPlayer;
        if(player.isFighting() || player.isDueling())
            return null;
        affectedPlayer = player.getTrade().getWishTo();
        if(affectedPlayer == null
                || !affectedPlayer.loggedIn()
                || player.getState() != MobState.Trading
                || affectedPlayer.getState() != MobState.Trading
                || (player.getTrade().isConfirmed(1) && affectedPlayer
                .getTrade().isConfirmed(1))) {
            unsetOptions(player);
            unsetOptions(affectedPlayer);
            return null;
        }
        if((affectedPlayer.getTrade().isConfirmed(1) || affectedPlayer
                .getTrade().isConfirmed(2))
                && (player.getTrade().isConfirmed(1) || player.getTrade()
                .isConfirmed(2)))
            return null;
        player.getTrade().setConfirmed(1, false);
        affectedPlayer.getTrade().setConfirmed(1, false);
        ActionSender.sendTradeAcceptUpdate(player);
        ActionSender.sendTradeAcceptUpdate(affectedPlayer);
        Inventory tradeOffer = new Inventory();
        tradeOffer.setOwner(player);
        player.getTrade().resetOffer();
        int count = msg.readByte();
        for(int slot = 0; slot < count; slot++) {
            InvItem it = new InvItem(msg.readShort(), msg.readInt());
            tradeOffer.add(it);
            // com.rscd.util.Logger.log("added trade item: "+it.getDef().getName()+" x "+it.getAmount());
        }
        for(InvItem item : tradeOffer.getItems()) {
            if(tradeOffer.countById(item.getID()) > player.getInventory()
                    .countById(item.getID())) {
                // player.suspiciousEvent("Tried to offer more items than player has");
            } else {
                player.getTrade().addToOffer(item);
            }
        }
        affectedPlayer.getTrade().setChanged();
        return null;
    }

    private void unsetOptions(Player p) {
        if(p == null) {
            return;
        }
        p.getTrade().reset();
        p.setState(MobState.Idle);
    }
}
