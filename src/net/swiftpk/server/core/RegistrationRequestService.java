package net.swiftpk.server.core;

import net.swiftpk.server.model.Player;
import net.swiftpk.server.model.terrain.World;
import net.swiftpk.server.net.PacketBuilder;
import net.swiftpk.server.util.Logger;

import static net.swiftpk.server.util.ThreadUtil.awaitTerminationUninterruptibly;

/**
 * A {@link PersistenceRequestService} implementation that handles login requests.
 *
 * @author lare96 <http://github.com/lare96>
 */
public final class RegistrationRequestService extends PersistenceRequestService<Player> {

    /**
     * Creates a new {@link RegistrationRequestService}.
     *
     * @param world The world.
     */
    public RegistrationRequestService(World world) {
        super(world);
    }

    @Override
    void addRequest(Player player) {
        synchronized (player) {
            workers.execute(() -> {
                if (!pending.contains(player) && !Thread.interrupted()) {
                    if (player.getResponseCode() == -1) {
                        byte response = PERSISTENCE.register(player.getUsername(), player.getCredentials().getPassword());
                        player.setResponseCode(response);
                        pending.add(player);
                        player.send(new PacketBuilder().setBare(true).addByte(player.getResponseCode()).toPacket());
                        if (player.getResponseCode() == 2)
                            Logger.out("Registered Player: " + player.getUsername());
                        else
                            Logger.out("Denied Registration: " + player.getUsername());
                    } else {
                        player.send(new PacketBuilder().setBare(true).addByte(player.getResponseCode()).toPacket());
                        if (player.getResponseCode() == 2)
                            Logger.out("Registered Player: " + player.getUsername());
                        else
                            Logger.out("Denied Registration: " + player.getUsername());
                    }
                }
            });
        }
    }

    @Override
    void finishRequest(Player player) {
    }

    @Override
    protected void shutDown() throws Exception {
        workers.shutdownNow();
        awaitTerminationUninterruptibly(workers);
    }
}