package net.swiftpk.server.core;

import static java.util.Objects.requireNonNull;

import java.util.Set;

import com.google.common.collect.ImmutableSet;

import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.handler.timeout.ReadTimeoutException;
import net.swiftpk.server.Server;
import net.swiftpk.server.model.entity.DefaultEntityFactory;
import net.swiftpk.server.net.Packet;
import net.swiftpk.server.net.Client;
import net.swiftpk.server.util.Logger;

public final class ConnectionHandler extends ChannelInboundHandlerAdapter {
    /**
     * A set of ignored exceptions from Netty.
     */
    private static final Set<String> IGNORED = ImmutableSet.of(
            "An existing connection was forcibly closed by the remote host",
            "An established connection was aborted by the software in your host machine");

    @Override
    public void channelInactive(ChannelHandlerContext ctx) {
        Client client = getClient(ctx);
        if (Server.getServer().getGameService().getClients().contains(client)) {
            Server.getServer().getGameService().removeClient(client);
        }
    }

    @Override
    public void channelActive(ChannelHandlerContext ctx) {
        Client client = new Client(ctx.channel());
        client.setOwner(DefaultEntityFactory.getInstance().newPlayer(client));
        ctx.channel().attr(Client.KEY).set(client);
        Server.getServer().getGameService().addClient(client);
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable e) {
        Client client = getClient(ctx);
        boolean isReadTimeout = e instanceof ReadTimeoutException;
        boolean isIgnoredMessage = IGNORED.contains(e.getMessage());

        if (!isReadTimeout && !isIgnoredMessage) {
            Logger.err("Disconnecting " + client + ", upstream exception thrown.", e);
        }
//		client.onException(e);
        ctx.channel().close();
    }

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object packet) {
        Channel ch = ctx.channel();
        Client client = getClient(ctx);
        if (ch.isRegistered()) {
            client.pushToMessageQueue((Packet) packet);
        }
    }

    private Client getClient(ChannelHandlerContext ctx) {
        Client client = ctx.channel().attr(Client.KEY).get();
        return requireNonNull(client);
    }
}
