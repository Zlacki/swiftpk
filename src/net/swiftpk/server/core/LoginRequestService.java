package net.swiftpk.server.core;

import net.swiftpk.server.Server;
import net.swiftpk.server.model.InvItem;
import net.swiftpk.server.model.Player;
import net.swiftpk.server.model.component.SkillSet.Skill;
import net.swiftpk.server.model.terrain.World;
import net.swiftpk.server.net.ActionSender;
import net.swiftpk.server.net.PacketBuilder;
import net.swiftpk.server.plugin.PluginManager;
import net.swiftpk.server.plugin.listeners.actors.players.PlayerLoginListener;
import net.swiftpk.server.util.Formulae;
import net.swiftpk.server.util.Logger;

import static net.swiftpk.server.util.ThreadUtil.awaitTerminationUninterruptibly;

/**
 * A {@link PersistenceRequestService} implementation that handles login
 * requests.
 */
public final class LoginRequestService extends PersistenceRequestService<Player> {

    /**
     * Creates a new {@link LoginRequestService}.
     *
     * @param world The world.
     */
    public LoginRequestService(World world) {
        super(world);
    }

    @Override
    void addRequest(Player player) {
        workers.execute(() -> {
            // Load player and get login response.
            if (!pending.contains(player) && !Thread.interrupted()) {
                if (player.getResponseCode() == (byte) 5) {
                    Logger.out("Denied Player: '" + player.getUsername() + "' (response=5; ip=" + player.getCurrentIP() + ")");
                    player.getChannel().write(new PacketBuilder().setBare(true).addByte((byte) 5).toPacket());
                    return;
                }
                byte response = PERSISTENCE.load(player);
                player.setResponseCode(response);
                if (response == 0 || response == 25 || response == 24) {
                    // Load was successful, queue player for login.
                    pending.add(player);
                } else {
                    // Load wasn't successful, disconnect with login response.
                    Logger.out("Denied Player: '" + player.getUsername() + "' (response=" + response + "; ip=" + player.getCurrentIP() + ")");
                    player.getChannel().write(new PacketBuilder().setBare(true).addByte(response).toPacket());
                }
            }
        });
    }

    @Override
    void finishRequest(Player player) {
        player.getChannel().write(new PacketBuilder().setBare(true).addByte(player.getResponseCode()).toPacket());
        // Login completed normally!
        for(Skill skill : player.getSkillSet())
            skill.setMaxLevel(Formulae.experienceToLevel(skill.getExperience()));
        World.getInstance().registerPlayer(player);
        ActionSender.sendServerInfo(player);
        ActionSender.sendHeight(player);
        ActionSender.sendFightMode(player);
        ActionSender.sendInventory(player);
        ActionSender.sendEquipmentStats(player);
        ActionSender.sendStats(player);
        ActionSender.sendFatigue(player);
        ActionSender.sendPrivacySettings(player);
        ActionSender.sendGameSettings(player);
        ActionSender.sendFriendList(player);
        ActionSender.sendIgnoreList(player);
        if (Server.getServer().getGameService().isSystemUpdating())
            ActionSender.sendSystemUpdate(player);
        if (player.getLastLogin() == 0L) {
            player.setChangingAppearance(true);
            ActionSender.sendAppearanceScreen(player);
            player.setLastLogin(System.currentTimeMillis());
        }
        player.getSkillSet().setCombatLevel(Formulae.getCombatLevel(player));
        ActionSender.sendLoginBox(player);
        ActionSender.sendMessage(player, "@que@" + Server.getServer().getConf().get("welcomeMessage"));
        player.unWieldItems();
        for (InvItem it : player.getInventory().getItems()) {
            if (it.isWielded())
                player.wieldItem(it);
        }
        for (PlayerLoginListener listener : PluginManager.getAll(PlayerLoginListener.class))
            listener.login(player);
        player.setLoggedIn(true);
        Logger.out("Accepted Player: '" + player.getUsername() + "' (ip=" + player.getCurrentIP() + ")");
    }

    @Override
    protected void shutDown() {
        workers.shutdownNow();
        awaitTerminationUninterruptibly(workers);
    }
}