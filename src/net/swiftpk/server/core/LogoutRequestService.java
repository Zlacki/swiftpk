package net.swiftpk.server.core;

import net.swiftpk.server.model.Player;
import net.swiftpk.server.model.terrain.World;
import net.swiftpk.server.util.Logger;

import static net.swiftpk.server.util.ThreadUtil.awaitTerminationUninterruptibly;

/**
 * A {@link PersistenceRequestService} implementation that handles login
 * requests.
 */
public final class LogoutRequestService extends PersistenceRequestService<Player> {

    /**
     * Creates a new {@link LogoutRequestService}.
     *
     * @param world The world.
     */
    public LogoutRequestService(World world) {
        super(world);
    }

    @Override
    void addRequest(Player player) {
        synchronized (player) {
            workers.execute(() -> {
                if (!pending.contains(player) && !Thread.interrupted()) {
                    PERSISTENCE.save(player);
                    pending.add(player);
                }
            });
        }
    }

    @Override
    void finishRequest(Player player) {
        Logger.out("Saved Player: " + player.getUsername());
    }

    @Override
    protected void shutDown() throws Exception {
        workers.shutdownNow();
        awaitTerminationUninterruptibly(workers);
    }
}