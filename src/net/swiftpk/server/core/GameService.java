package net.swiftpk.server.core;

import com.google.common.util.concurrent.AbstractScheduledService;
import com.google.common.util.concurrent.MoreExecutors;
import com.google.common.util.concurrent.Service;

import net.swiftpk.server.Server;
import net.swiftpk.server.event.Event;
import net.swiftpk.server.model.terrain.World;
import net.swiftpk.server.net.ActionSender;
import net.swiftpk.server.net.Client;
import net.swiftpk.server.sync.ClientSynchronizer;
import net.swiftpk.server.sync.ParallelClientSynchronizer;
import net.swiftpk.server.sync.SequentialClientSynchronizer;
import net.swiftpk.server.util.ExecutorUtils;
import net.swiftpk.server.util.Logger;
import net.swiftpk.server.util.ThreadUtil;

import java.util.Queue;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.CopyOnWriteArraySet;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import static net.swiftpk.server.util.ThreadUtil.awaitTerminationUninterruptibly;

/**
 * An {@link AbstractScheduledService} implementation that handles the launch,
 * processing, and termination of the main game thread.
 */
public final class GameService extends AbstractScheduledService {

    /**
     * A listener that will be notified of any changes in the game thread's state.
     */
    private final class GameServiceListener extends Service.Listener {

        @Override
        public void running() {
            World.getInstance().registerTasks();
            taskExecutorService.scheduleAtFixedRate(() -> {
                clients.forEach(client -> client.process());
            }, 50, 50, TimeUnit.MILLISECONDS);
            taskExecutorService.scheduleAtFixedRate(() -> {
                System.gc();
                System.runFinalization();
            }, 10 * 60 * 1000, 10 * 60 * 1000, TimeUnit.MILLISECONDS);
            taskExecutorService.scheduleAtFixedRate(() -> {
                clients.forEach(client -> client.getOwner().save());
                Logger.out("Saved " + clients.size() + " players.");
            }, 15 * 60 * 1000, 15 * 60 * 1000, TimeUnit.MILLISECONDS);
        }

        @Override
        public void stopping(State from) {
            // A request to terminate gracefully has been made.
            Logger.out("Gracefully terminating server...");
        }

        @Override
        public void terminated(State from) {
            // The game thread was gracefully terminated.
            Logger.out("The application will now exit.");
            System.exit(0);
        }

        @Override
        public void failed(State from, Throwable failure) {
            // An exception was thrown on the game thread.
            Logger.err("Server has been terminated because of an uncaught exception!", failure);
            System.exit(0);
        }
    }

    /**
     * A queue of synchronization tasks.
     */
    private final Queue<Runnable> syncTasks = new ConcurrentLinkedQueue<>();

    /**
     * A thread pool for general purpose low-overhead tasks.
     */
    private final ExecutorService fastPool = ExecutorUtils.newCachedThreadPool();

    /**
     * Thread pool used for registering and removing players
     */
    private final ScheduledExecutorService taskExecutorService = Executors
            .newScheduledThreadPool(ThreadUtil.AVAILABLE_PROCESSORS * 2, ThreadUtil.create("TaskService"));

    /**
     * Set of clients that are connected to the server.
     */
    private final Set<Client> clients = new CopyOnWriteArraySet<Client>();

    private final ClientSynchronizer synchronizationService;

    private long nextSystemUpdate = Long.MAX_VALUE;

    /**
     * Creates a new {@link GameService}.
     */
    public GameService() {
        if (ThreadUtil.AVAILABLE_PROCESSORS > 1)
            synchronizationService = new ParallelClientSynchronizer();
        else
            synchronizationService = new SequentialClientSynchronizer();
        addListener(new GameServiceListener(), MoreExecutors.directExecutor());
    }

    @Override
    protected String serviceName() {
        return "RSCGameThread";
    }

    @Override
    protected void startUp() throws Exception {
        super.startUp();
    }

    @Override
    protected void runOneIteration() {
        try {
            // Do stuff from other threads.
            runSynchronizationTasks();

            // Login stuff
            Server.getServer().getLoginService().finishPendingRequests();

            // Run the main game loop.
            World.getInstance().pulse();
            synchronizationService.synchronize(World.getInstance().getPlayers(), World.getInstance().getNPCs());

            // Logout stuff
            Server.getServer().getLogoutService().finishPendingRequests();
        } catch (Exception e) {
            Logger.err("Exception thrown during GameService pulse:", e);
        }
    }

    @Override
    protected Scheduler scheduler() {
        return Scheduler.newFixedRateSchedule(0, 600, TimeUnit.MILLISECONDS);
    }

    @Override
    public void shutDown() {
        try {
            gracefulShutdown();
        } catch (Exception e) {
            Logger.err("Exception thrown during GameService.shutDown():", e);
        }
    }

    public void addClient(Client client) {
        if (!clients.contains(client)) {
            clients.add(client);
        }
    }

    public Set<Client> getClients() {
        return clients;
    }

    public void removeClient(Client client) {
        if (clients.contains(client)) {
            clients.remove(client);
        }
    }

    public ScheduledExecutorService getEventExecutor() {
        return taskExecutorService;
    }

    /**
     * Runs all pending synchronization tasks in the backing queue. This allows
     * other Threads to execute game logic on the main game thread.
     */
    private void runSynchronizationTasks() {
        for (; ; ) {
            Runnable runnable = syncTasks.poll();
            if (runnable == null) {
                break;
            }

            try {
                runnable.run();
            } catch (Exception e) {
                Logger.err("Exception thrown when trying to run a sync task:", e);
            }
        }
    }

    /**
     * Performs a graceful shutdown of Luna. A shutdown performed in this way allows
     * Luna to properly save resources before the application exits. This method
     * will block for as long as it needs to until all important threads have
     * completed their tasks.
     * <p>
     * This function runs on the game thread, so players can be freely manipulated
     * without synchronization.
     */
    private void gracefulShutdown() {
        LoginRequestService loginService = Server.getServer().getLoginService();
        LogoutRequestService logoutService = Server.getServer().getLogoutService();

        // Will stop any current and future logins.
        loginService.finishPendingRequests();
        loginService.stopAsync().awaitTerminated();

        // Run last minute game tasks from other threads.
        runSynchronizationTasks();

        // Synchronously disconnect all players.
        clients.forEach((client) -> {
            ActionSender.sendLogout(client.getOwner());
            client.unregister();
        });
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        // Wait for the disconnected players to be saved.
        logoutService.finishPendingRequests();
        logoutService.stopAsync().awaitTerminated();

        // Wait for general-purpose tasks to complete.
        fastPool.shutdown();
        taskExecutorService.shutdown();
        awaitTerminationUninterruptibly(fastPool);
    }

    public long getMsUntilUpdate() {
        return nextSystemUpdate - System.currentTimeMillis();
    }

    public boolean isSystemUpdating() {
        return nextSystemUpdate != Long.MAX_VALUE;
    }

    /**
     * Schedules a system update in {@code ticks} amount of time.
     *
     * @param ticks The amount of ticks to schedule for.
     */
    public void scheduleSystemUpdate(int delay) {
        nextSystemUpdate = System.currentTimeMillis() + (delay * 1000);
        taskExecutorService.schedule(() -> stopAsync(), delay * 1000, TimeUnit.MILLISECONDS);
    }

    /**
     * Queues a task to be ran on the next tick.
     *
     * @param t The task to run.
     */
    public void sync(Runnable t) {
        syncTasks.add(t);
    }

    /**
     * Runs a result-bearing and listening asynchronous task. <strong>Warning: Tasks
     * may not be ran right away, as there is a limit to how large the backing pool
     * can grow to. This is to prevent DOS type attacks.</strong> If you require a
     * faster pool for higher priority tasks, consider using a dedicated pool from
     * {@link ExecutorUtils}.
     *
     * @param t The task to run.
     * @return The result of {@code t}.
     */
    public <T> Future<T> submit(Callable<T> t) {
        return fastPool.submit(t);
    }

    /**
     * Runs a listening asynchronous task. <strong>Warning: Tasks may not be ran
     * right away, as there is a limit to how large the backing pool can grow to.
     * This is to prevent DOS type attacks.</strong> If you require a faster pool
     * for higher priority tasks, consider using a dedicated pool from
     * {@link ExecutorUtils}.
     *
     * @param t The task to run.
     * @return The result of {@code t}.
     */
    public Future<?> submit(Runnable t) {
        return fastPool.submit(t);
    }

    public void submitTask(Event event) {
        submitTask(event, event.getDelay());
    }

    public void submitTask(Event event, long delay) {
        taskExecutorService.schedule(new Runnable() {
            @Override
            public void run() {
                long start = System.currentTimeMillis();
                if (event.isRunning()) {
                    event.execute();
                } else {
                    return;
                }
                long elapsed = System.currentTimeMillis() - start;
                long remaining = event.getDelay() - elapsed;
                if (remaining <= 0) {
                    remaining = 0;
                }
                submitTask(event, remaining);
            }
        }, delay, TimeUnit.MILLISECONDS);
    }

    public void submitConstantTask(Event event) {
        taskExecutorService.scheduleAtFixedRate(new Runnable() {
            @Override
            public void run() {
                event.execute();
            }
        }, event.getDelay(), event.getDelay(), TimeUnit.MILLISECONDS);
    }
}
