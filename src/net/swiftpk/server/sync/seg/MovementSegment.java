package net.swiftpk.server.sync.seg;

import net.swiftpk.server.sync.block.SynchronizationBlockSet;

/**
 * A {@link SynchronizationSegment} where a mob is moved (or doesn't move!).
 */
public final class MovementSegment extends SynchronizationSegment {

    /**
     * The directions.
     */
//	private final Direction[] directions;

    private final int direction;

    private final boolean hasMoved;

    private final boolean requiresSpriteUpdate;

    /**
     * Creates the movement segment.
     *
     * @param blockSet   The block set.
     * @param directions The directions array.
     * @throws IllegalArgumentException If there are not 0, 1 or 2 directions.
     */
    public MovementSegment(SynchronizationBlockSet blockSet, int direction, boolean hasMoved, boolean requiresSpriteUpdate) {
        super(blockSet);
//		Preconditions.checkArgument(directions.length >= 0 && directions.length < 3, "Directions length must be between 0 and 2 inclusive.");
//		this.directions = directions;
        this.direction = direction;
        this.hasMoved = hasMoved;
        this.requiresSpriteUpdate = requiresSpriteUpdate;
    }

    /**
     * Gets the directions.
     *
     * @return The directions.
     */
/*	public Direction[] getDirections() {
		return directions;
	}
*/
    @Override
    public SegmentType getType() {
/*		switch (directions.length) {
			case 0:
				return SegmentType.NO_MOVEMENT;
			case 1:
				return SegmentType.WALK;
			case 2:
				return SegmentType.RUN;
			default:
				throw new IllegalStateException("Direction type unsupported.");
		}*/
        if (!hasMoved && !requiresSpriteUpdate)
            return SegmentType.NO_MOVEMENT;

        return SegmentType.WALK;
    }

    public int getDirection() {
        return direction;
    }

    public boolean hasMoved() {
        return hasMoved;
    }

    public boolean requiresSpriteUpdate() {
        return requiresSpriteUpdate;
    }
}
