package net.swiftpk.server.sync.seg;

import net.swiftpk.server.model.terrain.tiles.Point;
import net.swiftpk.server.sync.block.SynchronizationBlockSet;

/**
 * A {@link SynchronizationSegment} which adds a player.
 */
public final class AddPlayerSegment extends SynchronizationSegment {

    /**
     * The index.
     */
    private final int index;

    /**
     * The direction the player is facing.
     */
    private final int direction;

    /**
     * The position.
     */
    private final Point position;

    /**
     * Creates the add player segment.
     *
     * @param blockSet The block set.
     * @param index    The player's index.
     * @param position The position.
     */
    public AddPlayerSegment(SynchronizationBlockSet blockSet, int index, Point position, int direction) {
        super(blockSet);
        this.index = index;
        this.position = position;
        this.direction = direction;
    }

    /**
     * Gets the player's index.
     *
     * @return The index.
     */
    public int getIndex() {
        return index;
    }

    /**
     * Gets the position.
     *
     * @return The position.
     */
    public Point getPosition() {
        return position;
    }

    /**
     * Gets the player's direction
     *
     * @return The direction.
     */
    public int getDirection() {
        return direction;
    }

    @Override
    public SegmentType getType() {
        return SegmentType.ADD_MOB;
    }

}
