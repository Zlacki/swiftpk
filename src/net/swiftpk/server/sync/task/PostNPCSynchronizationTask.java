package net.swiftpk.server.sync.task;

import net.swiftpk.server.model.entity.NPC;

public class PostNPCSynchronizationTask extends SynchronizationTask {
    private NPC npc;

    public PostNPCSynchronizationTask(NPC npc) {
        this.npc = npc;
    }

    @Override
    public void run() {
        npc.resetBlockSet();
    }

}
