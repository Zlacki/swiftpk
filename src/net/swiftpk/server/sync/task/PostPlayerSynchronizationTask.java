package net.swiftpk.server.sync.task;

import net.swiftpk.server.model.Player;
import net.swiftpk.server.model.entity.Mob;
import net.swiftpk.server.model.entity.NPC;
import net.swiftpk.server.net.ActionSender;
import net.swiftpk.server.net.Client;

public class PostPlayerSynchronizationTask extends SynchronizationTask {
    private final Player player;

    public PostPlayerSynchronizationTask(Player player) {
        this.player = player;
    }

    @Override
    public void run() {
        player.resetBlockSet();
        if(!player.shouldDecreaseViewArea())
            player.getViewArea().increaseSize();
        else {
            player.getViewArea().decreaseSize();
            player.resetExcessiveLocalMobs();
        }
        updateMovementTimeout(player);
        updatePing(player);
    }

    private void updateMovementTimeout(Player player) {
        long curTime = System.currentTimeMillis();
        long lastMovedDelay = curTime - player.getLastMoved();
        if (player.isWarnedToMove()) { // if we've warned them
            if (lastMovedDelay >= (720 * 1000) && !player.isFighting()) {
                ActionSender.sendLogout(player);
                player.getChannel().attr(Client.KEY).get().unregister();
                // World.getInstance().sessionEvent.closeSession(player.getSession());
            }
        } else if (lastMovedDelay >= (660 * 1000) && !player.isFighting()) {
            ActionSender.sendMessage(player, "@gre@You have not moved for 10 mins, please move to a new area to avoid logout.");
            player.setWarnedToMove(true);
        }
    }

    private void updatePing(Player p) {
        long curTime = System.currentTimeMillis();
        long pingTimeout = 30 * 1000;
        if (!p.getChannel().isActive())
            pingTimeout = 10000;
        if (p.isFighting()) {
            Mob cp = p.getCombatPartner();
            if (cp instanceof Player)
                pingTimeout = 600 * 1000;
            else if (cp instanceof NPC && p.getLocation().inWilderness())
                pingTimeout = 60 * 1000;
        } else if (p.getLocation().inWilderness() && curTime - p.getLastMoved() < 12000)
            pingTimeout = Long.MAX_VALUE;
        if (curTime - p.getLastPing() >= pingTimeout && p.timeSinceAttacked() > 10000) {
            ActionSender.sendLogout(p);
            Client session = p.getChannel().attr(Client.KEY).get();
            if (session != null)
                session.unregister();
        }
    }
}
