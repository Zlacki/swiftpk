package net.swiftpk.server.sync.task;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import net.swiftpk.server.model.Player;
import net.swiftpk.server.model.component.Appearance;
import net.swiftpk.server.model.component.Communication;
import net.swiftpk.server.model.component.Settings;
import net.swiftpk.server.model.entity.GameObject;
import net.swiftpk.server.model.Item;
import net.swiftpk.server.model.terrain.tiles.Point;
import net.swiftpk.server.net.ActionSender;
import net.swiftpk.server.net.PacketBuilder;
import net.swiftpk.server.sync.block.*;
import net.swiftpk.server.sync.seg.AddPlayerSegment;
import net.swiftpk.server.sync.seg.MovementSegment;
import net.swiftpk.server.sync.seg.RemoveMobSegment;
import net.swiftpk.server.sync.seg.SegmentType;
import net.swiftpk.server.sync.seg.SynchronizationSegment;

public class PlayerSynchronizationTask extends SynchronizationTask {
    private int appearanceBlockSize = 0;
    private final List<Point> locationsToClear = new ArrayList<>();
    private final Player player;
    private final int distance;

    public PlayerSynchronizationTask(Player player) {
        this.player = player;
        this.distance = player.getViewArea().getDistance();
    }

    @Override
    public void run() {
        int[] appearanceTickets = player.getAppearanceTickets();
        SynchronizationBlockSet blockSet = player.getBlockSet();
        if (blockSet.contains(ChatBlock.class)) {
            blockSet = blockSet.clone();
            blockSet.remove(ChatBlock.class);
        }

/*        if (requiresAppearanceUpdate(appearanceTickets, player.getIndex(), player.getAppearanceTicket())) {
            blockSet = blockSet.clone();
            blockSet.add(SynchronizationBlock.createAppearanceBlock(player));
        }
*/
        SynchronizationSegment segment = new MovementSegment(blockSet, player.getSprite(), player.hasMoved(),
                player.requiresSpriteUpdate());
        List<Player> localPlayers = player.getLocalPlayerList();
        List<SynchronizationSegment> segments = new ArrayList<>();

        for (Iterator<Player> iterator = localPlayers.iterator(); iterator.hasNext(); ) {
            Player other = iterator.next();

            if (removeable(other)) {
                iterator.remove();
                segments.add(new RemoveMobSegment());
            } else {
 //               if (requiresAppearanceUpdate(appearanceTickets, other.getIndex(), other.getAppearanceTicket())) {
//                    blockSet = blockSet.clone();
//                    blockSet.add(SynchronizationBlock.createAppearanceBlock(other));
 //                   other.getBlockSet().add(SynchronizationBlock.createAppearanceBlock(other));
 //               }

                segments.add(new MovementSegment(other.getBlockSet(), other.getSprite(), other.hasMoved(),
                        other.requiresSpriteUpdate()));
            }
        }
        int added = 0;

        for (Player other : player.getViewArea().getPlayersInView()) {
            if (localPlayers.size() >= 255) {
                /* max local players */
                player.flagExcessiveLocalMobs();
                break;
            } else if (added >= 20) {
                /* This prevents packets from getting too big. */
                break;
            }

            if (!player.equals(other) && other.withinRange(player, distance) && !localPlayers.contains(other)) {
                localPlayers.add(other);
                added++;

                if (!other.getBlockSet().contains(AppearanceBlock.class) && requiresAppearanceUpdate(appearanceTickets, other.getIndex(), other.getAppearanceTicket())) {
                    other.getBlockSet().add(SynchronizationBlock.createAppearanceBlock(other));
                }

                segments.add(new AddPlayerSegment(other.getBlockSet(), other.getIndex(), other.getLocation(),
                        other.getSprite()));
            }
        }

        updatePlayers(segments, segment);
        updateGroundObjects();
        updateBoundaries();
        updateLootableItems();
        sendLocationsToClear();
        if (player.getTrade().hasChanged()) {
            ActionSender.sendTradeItems(player);
            player.getTrade().resetChanged();
        }
        if (player.getDuel().hasChanged()) {
            ActionSender.sendDuelItems(player);
            player.getDuel().resetChanged();
        }
    }

    private byte getCoordOffset(int coord1, int coord2) {
        return (byte) (coord1 - coord2);
    }

    private byte[] getPositionOffsets(Point p) {
        byte[] rv = new byte[2];
        rv[0] = getCoordOffset(p.getX(), player.getX());
        rv[1] = getCoordOffset(p.getY(), player.getY());
        return rv;
    }

    private boolean removeable(GameObject other) {
        return other.isRemoved() || other.getLocation().getLongestDelta(player.getLocation()) > distance + 5;
    }

    private boolean removeable(Item other) {
        return !other.visibleTo(player) || other.isRemoved() || other.getLocation().getLongestDelta(player.getLocation()) > distance + 5;
    }

    private boolean requiresAppearanceUpdate(int[] appearanceTickets, int index, int appearanceTicket) {
        return appearanceTickets[index] != appearanceTicket;
    }

    private boolean removeable(Player other) {
        return other.shouldResynchronize() || !other.loggedIn()
                || other.getLocation().getLongestDelta(player.getLocation()) > distance;
    }

    private void putBlocks(SynchronizationBlockSet blockSet, PacketBuilder updates) {
        if (blockSet.size() > 0) {
            if (blockSet.contains(ItemBubbleBlock.class)) {
                ItemBubbleBlock block = blockSet.get(ItemBubbleBlock.class);
                updates.addShort(block.getIndex());
                updates.addByte((byte) 0);
                updates.addShort(block.getItemID());
                appearanceBlockSize++;
            }
            if (blockSet.contains(HitUpdateBlock.class)) {
                HitUpdateBlock block = blockSet.get(HitUpdateBlock.class);
                updates.addShort(block.getIndex());
                updates.addByte((byte) 2);
                updates.addShort(block.getDamage());
                updates.addShort(block.getCurrentHealth());
                updates.addShort(block.getMaximumHealth());
                appearanceBlockSize++;
            }
            if (blockSet.contains(ProjectileBlock.class)) {
                ProjectileBlock block = blockSet.get(ProjectileBlock.class);
                updates.addShort(block.getIndex());
                updates.addByte(block.isVictimNPC() ? (byte) 3 : (byte) 4);
                updates.addShort(block.getType());
                updates.addShort(block.getVictimIndex());
                appearanceBlockSize++;
            }
            if (blockSet.contains(AppearanceBlock.class)) {
                AppearanceBlock appearanceBlock = blockSet.get(AppearanceBlock.class);
                Appearance appearance = appearanceBlock.getAppearance();
                updates.addShort(appearanceBlock.getIndex());
                updates.addByte((byte) 5);
                updates.addShort(appearanceBlock.getAppearanceTicket());
                updates.addLong(appearanceBlock.getName());
                updates.addByte((byte) appearanceBlock.getWornItems().length);
                for (int i : appearanceBlock.getWornItems())
                    updates.addByte((byte) i);

                updates.addByte(appearance.getHairColour());
                updates.addByte(appearance.getTopColour());
                updates.addByte(appearance.getTrouserColour());
                updates.addByte(appearance.getSkinColour());
                updates.addShort(appearanceBlock.getCombatLevel());
                updates.addByte((byte) (appearanceBlock.isSkulled() ? 1 : 0));
                updates.addByte((byte) appearanceBlock.getRank());
                player.getAppearanceTickets()[appearanceBlock.getIndex()] = appearanceBlock.getAppearanceTicket();
                appearanceBlockSize++;
            }
            if (blockSet.contains(ChatBlock.class)) {
                ChatBlock block = blockSet.get(ChatBlock.class);
                if (block.getSender().getComponent(Settings.class).getPrivacySetting(0)
                        && !block.getSender().getComponent(Communication.class).isFriendsWith(player.getUsernameHash()))
                    return;
                updates.addShort(block.getSender().getIndex());
                updates.addByte((byte) 1);
                updates.addByte((byte) block.length());
                updates.addBytes(block.getMessage());
                appearanceBlockSize++;
            }
        }
    }

    private byte getMobCoordOffset(int coord1, int coord2) {
        byte offset = (byte) (coord1 - coord2);
        if (offset < 0) {
            offset += 32;
        }
        return offset;
    }

    private byte[] getMobPositionOffsets(Point p1, Point p2) {
        byte[] rv = new byte[2];
        rv[0] = getMobCoordOffset(p1.getX(), p2.getX());
        rv[1] = getMobCoordOffset(p1.getY(), p2.getY());
        return rv;
    }

    private void updatePlayers(List<SynchronizationSegment> segments,
                               SynchronizationSegment segment) {
        PacketBuilder blockBuilder = new PacketBuilder();
        blockBuilder.setID(53);
        blockBuilder.addShort(0);
        PacketBuilder builder = new PacketBuilder();
        builder.setID(145);
        builder.addBits(player.getX(), 11);
        builder.addBits(player.getY(), 13);
        builder.addBits(player.getSprite(), 4);
        putBlocks(segment.getBlockSet(), blockBuilder);
        int knownCount = segments.stream().mapToInt(seg -> seg.getType() != SegmentType.ADD_MOB ? 1 : 0).sum();
        int updated = 0;
        builder.addBits(knownCount, 8);
        for (SynchronizationSegment s : segments) {
            if (s.getType() == SegmentType.NO_MOVEMENT || s.getType() == SegmentType.WALK) {
                putBlocks(s.getBlockSet(), blockBuilder);
                MovementSegment moveSeg = (MovementSegment) s;
                if (s.getType() == SegmentType.NO_MOVEMENT) {
                    builder.addBits(0, 1);
                } else {
                    builder.addBits(1, 1);
                    builder.addBits(!moveSeg.hasMoved() ? 1 : 0, 1);
                    builder.addBits(moveSeg.getDirection(), !moveSeg.hasMoved() ? 4 : 3);
                    updated++;
                }
            } else if (s.getType() == SegmentType.REMOVE_MOB) {
                builder.addBits(1, 1);
                builder.addBits(1, 1);
                builder.addBits(3, 2);
                updated++;
            } else if (s.getType() == SegmentType.ADD_MOB) {
                putBlocks(s.getBlockSet(), blockBuilder);
                AddPlayerSegment addSeg = (AddPlayerSegment) s;
                builder.addBits(addSeg.getIndex(), 11);
                byte[] offsets = getMobPositionOffsets(addSeg.getPosition(), player.getLocation());
                builder.addBits(offsets[0], 5);
                builder.addBits(offsets[1], 5);
                builder.addBits(addSeg.getDirection(), 4);
                builder.addBits((byte) (s.getBlockSet().contains(AppearanceBlock.class) ? 0 : 1), 1);
                updated++;
            }
        }

        if (updated > 0 || player.hasMoved() || player.requiresSpriteUpdate()
                || segment.getBlockSet().contains(AppearanceBlock.class)
                || player.shouldResynchronize()) {
            player.send(builder.toPacket());
        }

        if (appearanceBlockSize > 0) {
            blockBuilder.setShort(appearanceBlockSize, 0);
            player.send(blockBuilder.toPacket());
            appearanceBlockSize = 0;
        }
    }

    private void updateGroundObjects() {
        boolean updated = false;
        PacketBuilder update = new PacketBuilder();
        update.setID(27);
        List<GameObject> localObjects = player.getLocalObjectList();

        Iterator<GameObject> iterator = localObjects.iterator();
        while (iterator.hasNext()) {
            GameObject gameObject = iterator.next();
            if (removeable(gameObject)) {
                if (gameObject.isBoundary())
                    continue;
                iterator.remove();
                byte[] offsets = getPositionOffsets(gameObject.getLocation());
                if (player.getLocation().getLongestDelta(gameObject.getLocation()) < 128) {
                    update.addShort(Short.MAX_VALUE);
                    update.addByte(offsets[0]);
                    update.addByte(offsets[1]);
                    update.addByte((byte) gameObject.getDirection());
                    updated = true;
                } else
                    locationsToClear.add(gameObject.getLocation());
            }
        }

        for(GameObject gameObject : player.getViewArea().getGameObjectsInView()) {
            if (!localObjects.contains(gameObject) && !gameObject.isBoundary()) {
                localObjects.add(gameObject);
                byte[] offsets = getPositionOffsets(gameObject.getLocation());
                update.addShort(gameObject.getID());
                update.addByte(offsets[0]);
                update.addByte(offsets[1]);
                update.addByte((byte) gameObject.getDirection());
                updated = true;
            }
        }

        if (updated)
            player.send(update.toPacket());
    }

    private void updateBoundaries() {
        boolean updated = false;
        PacketBuilder update = new PacketBuilder();
        update.setID(95);
        List<GameObject> localObjects = player.getLocalObjectList();

        Iterator<GameObject> iterator = localObjects.iterator();
        while (iterator.hasNext()) {
            GameObject gameObject = iterator.next();
            if (removeable(gameObject)) {
                if (!gameObject.isBoundary())
                    continue;
                iterator.remove();
                byte[] offsets = getPositionOffsets(gameObject.getLocation());
                if (player.getLocation().getLongestDelta(gameObject.getLocation()) < 128) {
                    update.addShort(Short.MAX_VALUE);
                    update.addByte(offsets[0]);
                    update.addByte(offsets[1]);
                    update.addByte((byte) gameObject.getDirection());
                    updated = true;
                } else // Remove really far away stationary entities by region.
                    locationsToClear.add(gameObject.getLocation());
            }
        }

        for(GameObject gameObject : player.getViewArea().getGameObjectsInView()) {
            if (!localObjects.contains(gameObject) && gameObject.isBoundary()) {
                localObjects.add(gameObject);
                byte[] offsets = getPositionOffsets(gameObject.getLocation());
                update.addShort(gameObject.getID());
                update.addByte(offsets[0]);
                update.addByte(offsets[1]);
                update.addByte((byte) gameObject.getDirection());
                updated = true;
            }
        }

        if (updated)
            player.send(update.toPacket());
    }

    private void updateLootableItems() {
        boolean updated = false;
        PacketBuilder update = new PacketBuilder();
        update.setID(108);
        List<Item> localItems = player.getLocalItemList();

        Iterator<Item> iterator = localItems.iterator();
        while (iterator.hasNext()) {
            Item item = iterator.next();
            if (removeable(item)) {
                iterator.remove();
                byte[] offsets = getPositionOffsets(item.getLocation());
                if (!player.withinRange(item, distance + 5)) {
                    if (player.getLocation().getLongestDelta(item.getLocation()) < 128) {
                        // Clear close-by tile, this is for walking too far away
                        update.addByte((byte) 255);
                        update.addByte(offsets[0]);
                        update.addByte(offsets[1]);
                        updated = true;
                    } else // Remove really far away stationary entities with packet for clearing region
                        locationsToClear.add(item.getLocation());
                } else {
                    // Possibly item is in a Point with many other Items, and the tile is still in view, so need ID
                    update.addShort(item.getID() + 32768);
                    update.addByte(offsets[0]);
                    update.addByte(offsets[1]);
                    updated = true;
                }
            }
        }

        for(Item item : player.getViewArea().getItemsInView()) {
            if (!localItems.contains(item)) {
                localItems.add(item);
                byte[] offsets = getPositionOffsets(item.getLocation());
                update.addShort(item.getID());
                update.addByte(offsets[0]);
                update.addByte(offsets[1]);
                updated = true;
            }
        }

        if (updated)
            player.send(update.toPacket());
    }

    private void sendLocationsToClear() {
        if (!locationsToClear.isEmpty()) {
            /*
             Tells the client to remove all stationary entities at the specified locations.
             Used to remove stationary entities that are miles away from us.
            */
            PacketBuilder update = new PacketBuilder();
            update.setID(115);
            for (Point location : locationsToClear) {
                int offsetX = location.getX() - player.getX();
                int offsetY = location.getY() - player.getY();
                update.addShort(offsetX);
                update.addShort(offsetY);
            }
            locationsToClear.clear();
            player.send(update.toPacket());
        }
    }
}
