package net.swiftpk.server.sync.task;

import net.swiftpk.server.model.entity.NPC;

public class PreNPCSynchronizationTask extends SynchronizationTask {
    private NPC npc;

    public PreNPCSynchronizationTask(NPC npc) {
        this.npc = npc;
    }

    @Override
    public void run() {
        if(npc.getWalkToAction() != null) {
            if (npc.getWalkToAction().shouldExecute()) {
                npc.getWalkToAction().execute();
                npc.setWalkToAction(null);
            }
        }
        npc.resetMoved();
        npc.updatePosition();
    }

}
