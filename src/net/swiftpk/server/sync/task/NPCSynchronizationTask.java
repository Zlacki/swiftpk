package net.swiftpk.server.sync.task;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import net.swiftpk.server.model.MobState;
import net.swiftpk.server.model.Player;
import net.swiftpk.server.model.entity.NPC;
import net.swiftpk.server.model.terrain.tiles.Point;
import net.swiftpk.server.model.terrain.tiles.ViewArea;
import net.swiftpk.server.net.Packet;
import net.swiftpk.server.net.PacketBuilder;
import net.swiftpk.server.sync.block.HitUpdateBlock;
import net.swiftpk.server.sync.block.NPCChatBlock;
import net.swiftpk.server.sync.block.SynchronizationBlockSet;
import net.swiftpk.server.sync.seg.AddNPCSegment;
import net.swiftpk.server.sync.seg.MovementSegment;
import net.swiftpk.server.sync.seg.RemoveMobSegment;
import net.swiftpk.server.sync.seg.SegmentType;
import net.swiftpk.server.sync.seg.SynchronizationSegment;

public class NPCSynchronizationTask extends SynchronizationTask {
    private Player player;
    private final int distance;

    public NPCSynchronizationTask(Player player) {
        this.player = player;
        this.distance = player.getViewArea().getDistance();
    }

    @Override
    public void run() {
        List<NPC> localNPCs = player.getLocalNPCList();
        // int oldCount = localPlayers.size();
        List<SynchronizationSegment> segments = new ArrayList<>();

        for (Iterator<NPC> iterator = localNPCs.iterator(); iterator.hasNext(); ) {
            NPC npc = iterator.next();

            if (removeable(npc)) {
                iterator.remove();
                segments.add(new RemoveMobSegment());
            } else {
                segments.add(new MovementSegment(npc.getBlockSet(), npc.getSprite(), npc.hasMoved(),
                        npc.requiresSpriteUpdate()));
            }
        }

        int added = 0, count = localNPCs.size();

        for (NPC npc : player.getViewArea().getNpcsInView()) {
            if (count >= 255) {
                /* max local NPCs */
                player.flagExcessiveLocalMobs();
                break;
            } else if (added >= 20) {
                /* This prevents packets from getting too big. */
                break;
            }

            NPC other = npc;
            if (other.withinRange(player, distance) && !localNPCs.contains(other) && other.getState() != MobState.Dead) {
                localNPCs.add(other);
                count++;
                added++;

                segments.add(new AddNPCSegment(other.getBlockSet(), other.getIndex(), other.getLocation(),
                        other.getDef().getId(), other.getSprite()));
            }
        }

        synchronize(segments);
    }

    private boolean removeable(NPC npc) {
        Point npcPosition = npc.getLocation();
        return npc.shouldResynchronize() || npc.getState() == MobState.Dead
                || npcPosition.getLongestDelta(player.getLocation()) > distance
                || !npcPosition.withinRange(player.getLocation(), distance);
    }

    private int putBlocks(PacketBuilder builder, SynchronizationBlockSet blockSet) {
        int addedBlocks = 0;
        if (blockSet.contains(NPCChatBlock.class)) {
            NPCChatBlock block = blockSet.get(NPCChatBlock.class);
            builder.addShort(block.getSender().getIndex());
            builder.addByte((byte) 1);
            builder.addShort(block.getTarget().getIndex());
            builder.addByte((byte) block.length());
            builder.addBytes(block.getMessage());
            addedBlocks++;
        }
        if (blockSet.contains(HitUpdateBlock.class)) {
            HitUpdateBlock block = blockSet.get(HitUpdateBlock.class);
            builder.addShort(block.getIndex());
            builder.addByte((byte) 2);
            builder.addShort(block.getDamage());
            builder.addShort(block.getCurrentHealth());
            builder.addShort(block.getMaximumHealth());
            addedBlocks++;
        }
        return addedBlocks;
    }

    public static byte getCoordOffset(int coord1, int coord2) {
        byte offset = (byte) (coord1 - coord2);
        if (offset < 0) {
            offset += 32;
        }
        return offset;
    }

    private void synchronize(List<SynchronizationSegment> segments) {
        int blockUpdateSize = 0;
        PacketBuilder blockBuilder = new PacketBuilder().setID(190).addShort(0);
        PacketBuilder builder = new PacketBuilder().setID(77);
        int knownCount = segments.stream().mapToInt(seg -> seg.getType() != SegmentType.ADD_MOB ? 1 : 0).sum();
        int updated = 0;
        builder.addBits(knownCount, 8);
        for (SynchronizationSegment s : segments) {
            if (s.getType() == SegmentType.REMOVE_MOB) {
                builder.addBits(1, 1);
                builder.addBits(1, 1);
                builder.addBits(3, 2);
                updated++;
            } else if (s.getType() == SegmentType.WALK || s.getType() == SegmentType.NO_MOVEMENT) {
                blockUpdateSize += putBlocks(blockBuilder, s.getBlockSet());
                MovementSegment moveSeg = (MovementSegment) s;
                if (s.getType() == SegmentType.NO_MOVEMENT) {
                    builder.addBits(0, 1);
                } else {
                    builder.addBits(1, 1);
                    builder.addBits(!moveSeg.hasMoved() ? 1 : 0, 1);
                    builder.addBits(moveSeg.getDirection(), !moveSeg.hasMoved() ? 4 : 3);
                    updated++;
                }
            } else if (s.getType() == SegmentType.ADD_MOB) {
                blockUpdateSize += putBlocks(blockBuilder, s.getBlockSet());
                AddNPCSegment addSeg = (AddNPCSegment) s;
                builder.addBits(addSeg.getIndex(), 12);
                byte[] offsets = getPositionOffsets(addSeg.getPosition(), player.getLocation());
                builder.addBits(offsets[0], 5);
                builder.addBits(offsets[1], 5);
                builder.addBits(addSeg.getDirection(), 4);
                builder.addBits(addSeg.getID(), 10);
                updated++;
            }
        }
        if (updated > 0)
            player.send(builder.toPacket());

        if(blockUpdateSize > 0)
            player.send(blockBuilder.setShort(blockUpdateSize, 0).toPacket());

        return;
    }

    public static byte[] getPositionOffsets(Point p, Point p2) {
        byte[] rv = new byte[2];
        rv[0] = getCoordOffset(p.getX(), p2.getX());
        rv[1] = getCoordOffset(p.getY(), p2.getY());
        return rv;
    }
}
