package net.swiftpk.server.sync.task;

import net.swiftpk.server.model.Player;

public class PrePlayerSynchronizationTask extends SynchronizationTask {
    private Player player;

    public PrePlayerSynchronizationTask(Player player) {
        this.player = player;
    }

    @Override
    public void run() {
        if(player.getWalkToAction() != null) {
            if (player.getWalkToAction().shouldExecute()) {
                player.getWalkToAction().execute();
                player.setWalkToAction(null);
            }
        }
        if (player.hasMoved()) {
            player.setLastMoved();
            player.setWarnedToMove(false);
        }
        player.resetMoved();
        player.getWalkingQueue().processNextMovement();
    }

}
