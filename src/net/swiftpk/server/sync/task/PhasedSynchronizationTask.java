package net.swiftpk.server.sync.task;

import java.util.concurrent.Phaser;

public class PhasedSynchronizationTask extends SynchronizationTask {
    private Phaser phaser;
    private SynchronizationTask task;

    public PhasedSynchronizationTask(Phaser phaser, SynchronizationTask task) {
        this.phaser = phaser;
        this.task = task;
    }

    @Override
    public void run() {
        try {
            task.run();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            phaser.arriveAndDeregister();
        }
    }
}
