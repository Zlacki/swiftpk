package net.swiftpk.server.sync;

import net.swiftpk.server.model.Player;
import net.swiftpk.server.model.entity.NPC;
import net.swiftpk.server.util.EntityList;

public abstract class ClientSynchronizer {
    public abstract void synchronize(EntityList<Player> players, EntityList<NPC> npcs);
}
