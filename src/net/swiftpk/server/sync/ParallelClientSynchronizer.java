package net.swiftpk.server.sync;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Phaser;

import net.swiftpk.server.model.Player;
import net.swiftpk.server.model.entity.NPC;
import net.swiftpk.server.sync.task.NPCSynchronizationTask;
import net.swiftpk.server.sync.task.PhasedSynchronizationTask;
import net.swiftpk.server.sync.task.PlayerSynchronizationTask;
import net.swiftpk.server.sync.task.PostNPCSynchronizationTask;
import net.swiftpk.server.sync.task.PostPlayerSynchronizationTask;
import net.swiftpk.server.sync.task.PreNPCSynchronizationTask;
import net.swiftpk.server.sync.task.PrePlayerSynchronizationTask;
import net.swiftpk.server.sync.task.SynchronizationTask;
import net.swiftpk.server.util.EntityList;
import net.swiftpk.server.util.ExecutorUtils;

public class ParallelClientSynchronizer extends ClientSynchronizer {
    private ExecutorService executor;
    private Phaser phaser = new Phaser(1);

    public ParallelClientSynchronizer() {
        executor = ExecutorUtils.newCachedThreadPool();
    }

    @Override
    public void synchronize(EntityList<Player> players, EntityList<NPC> npcs) {
        int playerCount = players.size();
        int npcCount = npcs.size();

        phaser.bulkRegister(playerCount);
        for (Player p : players) {
            SynchronizationTask task = new PrePlayerSynchronizationTask(p);
            executor.submit(new PhasedSynchronizationTask(phaser, task));
        }
        phaser.arriveAndAwaitAdvance();

        phaser.bulkRegister(npcCount);
        for (NPC n : npcs) {
            SynchronizationTask task = new PreNPCSynchronizationTask(n);
            executor.submit(new PhasedSynchronizationTask(phaser, task));
        }
        phaser.arriveAndAwaitAdvance();

        phaser.bulkRegister(playerCount * 2);
        for (Player p : players) {
            SynchronizationTask task = new PlayerSynchronizationTask(p);
            executor.submit(new PhasedSynchronizationTask(phaser, task));
            task = new NPCSynchronizationTask(p);
            executor.submit(new PhasedSynchronizationTask(phaser, task));
        }
        phaser.arriveAndAwaitAdvance();

        phaser.bulkRegister(npcCount);
        for (NPC n : npcs) {
            SynchronizationTask task = new PostNPCSynchronizationTask(n);
            executor.submit(new PhasedSynchronizationTask(phaser, task));
        }
        phaser.arriveAndAwaitAdvance();

        phaser.bulkRegister(playerCount);
        for (Player p : players) {
            SynchronizationTask task = new PostPlayerSynchronizationTask(p);
            executor.submit(new PhasedSynchronizationTask(phaser, task));
        }
        phaser.arriveAndAwaitAdvance();
    }
}
