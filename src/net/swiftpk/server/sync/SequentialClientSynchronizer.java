package net.swiftpk.server.sync;

import net.swiftpk.server.model.Player;
import net.swiftpk.server.model.entity.NPC;
import net.swiftpk.server.net.ActionSender;
import net.swiftpk.server.net.Client;
import net.swiftpk.server.sync.task.NPCSynchronizationTask;
import net.swiftpk.server.sync.task.PlayerSynchronizationTask;
import net.swiftpk.server.sync.task.PostNPCSynchronizationTask;
import net.swiftpk.server.sync.task.PostPlayerSynchronizationTask;
import net.swiftpk.server.sync.task.PreNPCSynchronizationTask;
import net.swiftpk.server.sync.task.PrePlayerSynchronizationTask;
import net.swiftpk.server.util.EntityList;

public class SequentialClientSynchronizer extends ClientSynchronizer {
    @Override
    public void synchronize(EntityList<Player> players, EntityList<NPC> npcs) {
        for (Player player : players) {
            new PrePlayerSynchronizationTask(player).run();
        }

        for (NPC npc : npcs) {
            new PreNPCSynchronizationTask(npc).run();
        }

        for (Player player : players) {
            new PlayerSynchronizationTask(player).run();
            new NPCSynchronizationTask(player).run();
        }

        for (Player player : players) {
            new PostPlayerSynchronizationTask(player).run();
        }

        for (NPC npc : npcs) {
            new PostNPCSynchronizationTask(npc).run();
        }

        players.forEach(player -> {
            updateMovementTimeout(player);
            if(player.getWalkToAction() != null) {
                if (player.getWalkToAction().shouldExecute()) {
                    player.getWalkToAction().execute();
                    player.setWalkToAction(null);
                }
            }
//            executeMovementListeners(player);
        });
    }
/*
    private void executeMovementListeners(Player player) {
        MovementListener movementListener = player.getMovementListener();
        if (movementListener == null)
            return;
        if (movementListener.getTarget() != null
                && player.withinRange(movementListener.getTarget(), movementListener.getRadius())
                && player.nextTo(movementListener.getTarget())) {
            movementListener.arrived();
            player.setMovementListener(null);
        } else if (player.withinRange(movementListener.getDestination(), movementListener.getRadius())
                && movementListener.getTarget() == null) {
            movementListener.arrived();
            player.setMovementListener(null);
        } else if (player.getPathHandler().finishedPath()
                && (player.isFollowing())) {
            if (movementListener.getDestination().getY() == 0 && movementListener.getTarget() == null)
                movementListener.arrived();
            player.setMovementListener(null);
        }
    }
*/
    private void updateMovementTimeout(Player player) {
        long curTime = System.currentTimeMillis();
        long lastMovedDelay = curTime - player.getLastMoved();
        if (player.isWarnedToMove()) { // if we've warned them
            if (lastMovedDelay >= (720 * 1000) && !player.isFighting()) {
                ActionSender.sendLogout(player);
                player.getChannel().attr(Client.KEY).get().unregister();
                // World.getInstance().sessionEvent.closeSession(player.getSession());
            }
        } else if (lastMovedDelay >= (660 * 1000) && !player.isFighting()) {
            ActionSender.sendMessage(player, "@gre@You have not moved for 10 mins, please move to a new area to avoid logout.");
            player.setWarnedToMove(true);
        }
    }
}
