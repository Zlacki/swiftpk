package net.swiftpk.server.sync.block;

/**
 * The projectile {@link SynchronizationBlock}. Only players can utilise this block.
 */
public final class ProjectileBlock extends SynchronizationBlock {

    /**
     * The mob's server index.
     */
    private final int index;

    /**
     * The victim mob's server index.
     */
    private final int victimIndex;

    /**
     * The type of projectile
     */
    private final int type;

    /**
     * Whether or not the victim is an NPC.
     */
    private final boolean isVictimNPC;

    /**
     * Creates the projectile block.
     *
     * @param index       The server index of the mob.
     * @param victimIndex The server index of the victim.
     * @param type        The type of the projectile.
     * @param isVictimNPC Whether or not the victim is an NPC.
     */
    public ProjectileBlock(int index, int victimIndex, int type, boolean isVictimNPC) {
        this.index = index;
        this.victimIndex = victimIndex;
        this.type = type;
        this.isVictimNPC = isVictimNPC;
    }

    /**
     * Gets the server index of the victim.
     *
     * @return The server index of the victim
     */
    public int getVictimIndex() {
        return victimIndex;
    }

    /**
     * Gets the type of the projectile.
     *
     * @return The type of the projectile.
     */
    public int getType() {
        return type;
    }

    /**
     * Whether or not the victim is an NPC
     *
     * @return Whether or not the projectile was shot at an NPC
     */
    public boolean isVictimNPC() {
        return isVictimNPC;
    }

    /**
     * Gets the server index of the mob.
     *
     * @return The server index.
     */
    public int getIndex() {
        return index;
    }
}