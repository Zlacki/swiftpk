package net.swiftpk.server.sync.block;

import net.swiftpk.server.model.component.Appearance;

/**
 * The appearance {@link SynchronizationBlock}. Only players can utilise this block.
 *
 * @author Graham
 */
public final class AppearanceBlock extends SynchronizationBlock {

    /**
     * The player's appearance.
     */
    private final Appearance appearance;

    /**
     * The player's combat level.
     */
    private final int combat;

    /**
     * Whether or not the player is skulled.
     */
    private final boolean isSkulled;

    /**
     * The player's name.
     */
    private final long name;

    /**
     * The player's rank.
     */
    private final int rank;

    /**
     * The player's index.
     */
    private final int index;

    private final int appearanceTicket;

    private final int[] wornItems;

    /**
     * Creates the appearance block.
     *
     * @param name       The player's username, encoded to base 37.
     * @param appearance The {@link Appearance}.
     * @param combat     The player's combat.
     * @param isSkulled  Whether or not the player is skulled.
     * @param rank       The privilege level of the player.
     * @param index      The players index.
     */
    AppearanceBlock(long name, Appearance appearance, int combat, boolean isSkulled, int rank, int index, int appearanceTicket, int[] wornItems) {
        this.name = name;
        this.appearance = appearance;
        this.combat = combat;
        this.isSkulled = isSkulled;
        this.rank = rank;
        this.index = index;
        this.appearanceTicket = appearanceTicket;
        this.wornItems = wornItems;
    }

    /**
     * Gets the player's {@link Appearance}.
     *
     * @return The player's appearance.
     */
    public Appearance getAppearance() {
        return appearance;
    }

    /**
     * Gets the player's combat level.
     *
     * @return The player's combat level.
     */
    public int getCombatLevel() {
        return combat;
    }

    /**
     * Whether or not the player is skulled.
     *
     * @return {@code true} if the player is skulled, otherwise {@code false}.
     */
    public boolean isSkulled() {
        return isSkulled;
    }

    /**
     * Gets the player's name.
     *
     * @return The player's name.
     */
    public long getName() {
        return name;
    }

    /**
     * Gets the player's rank.
     *
     * @return The rank.
     */
    public int getRank() {
        return rank;
    }

    public int getIndex() {
        return index;
    }

    public int getAppearanceTicket() {
        return appearanceTicket;
    }

    public int[] getWornItems() {
        return wornItems;
    }
}
