package net.swiftpk.server.sync.block;

import net.swiftpk.server.model.ChatMessage;
import net.swiftpk.server.model.entity.Mob;

/**
 * The chat {@link SynchronizationBlock}. Only players can utilise this block.
 */
public final class ChatBlock extends SynchronizationBlock {

    /**
     * The {@link ChatMessage}.
     */
    private final ChatMessage chatMessage;

    /**
     * Creates the chat block.
     *
     * @param chatMessage The {@link ChatMessage}.
     */
    ChatBlock(ChatMessage chatMessage) {
        this.chatMessage = chatMessage;
    }

    /**
     * Gets the compressed message.
     *
     * @return The compressed message.
     */
    public byte[] getMessage() {
        return chatMessage.getContent();
    }

    public int length() {
        return chatMessage.getLength();
    }

    public Mob getRecipient() {
        return chatMessage.getRecipient();
    }

    public Mob getSender() {
        return chatMessage.getSender();
    }
}