package net.swiftpk.server.sync.block;

import net.swiftpk.server.model.ChatMessage;
import net.swiftpk.server.model.Player;
import net.swiftpk.server.model.entity.mob.NPCChatMessage;
import net.swiftpk.server.model.terrain.tiles.Point;

/**
 * A synchronization block is part of a SynchronizationSegment. A segment can have up to one block of each type.
 * <p>
 * This class also has static factory methods for creating {@link SynchronizationBlock}s.
 */
public abstract class SynchronizationBlock {

    /**
     * Creates an {@link AppearanceBlock} for the specified player.
     *
     * @param player The player.
     * @return The appearance block.
     */
    public static SynchronizationBlock createAppearanceBlock(Player player) {
        return new AppearanceBlock(player.getCredentials().getEncodedUsername(), player.getAppearance(), player.getSkillSet().getCombatLevel(), player.isSkulled(), player.getRank(), player.getIndex(), player.getAppearanceTicket(), player.getWornItems());
    }

    /**
     * Creates a {@link ChatBlock} for the specified player.
     *
     * @param player      The player.
     * @param chatMessage The chat message.
     * @return The chat block.
     */
    public static SynchronizationBlock createChatBlock(Player player, ChatMessage chatMessage) {
        return new ChatBlock(chatMessage);
    }

    /**
     * Creates a {@link NPCChatBlock}.
     *
     * @param chatMessage The chat message.
     * @return The chat block.
     */
    public static SynchronizationBlock createNPCChatBlock(NPCChatMessage chatMessage) {
        return new NPCChatBlock(chatMessage);
    }

    /**
     * Creates a {@link HitUpdateBlock}
     *
     * @param damage        The damage dealt by the hit.
     * @param currentHealth The current health of the mob.
     * @param maximumHealth The maximum health of the mob.
     * @return The hit update block.
     */
    public static SynchronizationBlock createHitUpdateBlock(int damage, int currentHealth, int maximumHealth, int index) {
        return new HitUpdateBlock(damage, currentHealth, maximumHealth, index);
    }

    /**
     * Creates a {@link ProjectileBlock}
     *
     * @return The projectile block.
     */
    public static SynchronizationBlock createProjectileBlock(int index, int victimIndex, int type, boolean isVictimNPC) {
        return new ProjectileBlock(index, victimIndex, type, isVictimNPC);
    }

    /**
     * Creates a {@link ProjectileBlock}
     *
     * @return The projectile block.
     */
    public static SynchronizationBlock createItemBubbleBlock(int index, int itemID) {
        return new ItemBubbleBlock(index, itemID);
    }

    /**
     * Creates a {@link TurnToPositionBlock} with the specified {@link Point}.
     *
     * @param position The position.
     * @return The turn to position block.
     */
    public static SynchronizationBlock createTurnToPositionBlock(Point position) {
        return new TurnToPositionBlock(position);
    }

}