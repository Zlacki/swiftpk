package net.swiftpk.server.sync.block;

import net.swiftpk.server.model.terrain.tiles.Point;

/**
 * The turn to position {@link SynchronizationBlock}. Both players and npcs can utilise this block.
 */
public final class TurnToPositionBlock extends SynchronizationBlock {

    /**
     * The position to turn to.
     */
    private final Point position;

    /**
     * Creates the turn to position block.
     *
     * @param position The position to turn to.
     */
    TurnToPositionBlock(Point position) {
        this.position = position;
    }

    /**
     * Gets the {@link Point} to turn to.
     *
     * @return The position to turn to.
     */
    public Point getPosition() {
        return position;
    }

}