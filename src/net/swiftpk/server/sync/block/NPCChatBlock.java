package net.swiftpk.server.sync.block;

import net.swiftpk.server.model.entity.mob.NPCChatMessage;
import net.swiftpk.server.model.entity.Mob;

/**
 * The chat {@link SynchronizationBlock}. Only players can utilise this block.
 */
public final class NPCChatBlock extends SynchronizationBlock {

    /**
     * The {@link NPCChatMessage}.
     */
    private final NPCChatMessage chatMessage;

    /**
     * Creates the chat block.
     *
     * @param chatMessage The {@link NPCChatMessage}.
     */
    NPCChatBlock(NPCChatMessage chatMessage) {
        this.chatMessage = chatMessage;
    }

    /**
     * Gets the compressed message.
     *
     * @return The compressed message.
     */
    public byte[] getMessage() {
        return chatMessage.getContent();
    }

    public int length() {
        return chatMessage.getLength();
    }

    public Mob getTarget() {
        return chatMessage.getTarget();
    }

    public Mob getSender() {
        return chatMessage.getSender();
    }
}