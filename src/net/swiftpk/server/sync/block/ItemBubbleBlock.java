package net.swiftpk.server.sync.block;

/**
 * The item bubble {@link SynchronizationBlock}. Only players can utilise this block.
 */
public final class ItemBubbleBlock extends SynchronizationBlock {

    /**
     * The player's server index.
     */
    private final int index;

    /**
     * The item ID of the item displayed in the bubble.
     */
    private final int itemID;

    /**
     * Creates the item bubble block.
     *
     * @param index  The server index of the mob.
     * @param itemID The item ID of the item to display in the bubble.
     */
    ItemBubbleBlock(int index, int itemID) {
        this.index = index;
        this.itemID = itemID;
    }

    /**
     * Gets the ID of the item to display in the bubble.
     *
     * @return The item ID of the item to display in the bubble.
     */
    public int getItemID() {
        return itemID;
    }

    /**
     * Gets the server index of the mob.
     *
     * @return The server index.
     */
    public int getIndex() {
        return index;
    }
}