package net.swiftpk.server.sync.block;

/**
 * The hit update {@link SynchronizationBlock}. Both npcs and players can utilise this block.
 */
public final class HitUpdateBlock extends SynchronizationBlock {

    /**
     * The mob's current health.
     */
    private final int currentHealth;

    /**
     * The amount of damage the hit will do.
     */
    private final int damage;

    /**
     * The mob's maximum health.
     */
    private final int maximumHealth;

    /**
     * The mob's server index.
     */
    private final int index;

    /**
     * Creates the hit update block.
     *
     * @param damage        The damage dealt by the hit.
     * @param currentHealth The current health of the mob.
     * @param maximumHealth The maximum health of the mob.
     * @param index         The server index of the mob.
     */
    HitUpdateBlock(int damage, int currentHealth, int maximumHealth, int index) {
        this.damage = damage;
        this.currentHealth = currentHealth;
        this.maximumHealth = maximumHealth;
        this.index = index;
    }

    /**
     * Gets the current health of the mob.
     *
     * @return The current health;
     */
    public int getCurrentHealth() {
        return currentHealth;
    }

    /**
     * Gets the damage done by the hit.
     *
     * @return The damage.
     */
    public int getDamage() {
        return damage;
    }

    /**
     * Gets the maximum health of the mob.
     *
     * @return The maximum health.
     */
    public int getMaximumHealth() {
        return maximumHealth;
    }

    /**
     * Gets the server index of the mob.
     *
     * @return The server index.
     */
    public int getIndex() {
        return index;
    }
}