package net.swiftpk.server.tickable;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Queue;

import net.swiftpk.server.Server;

import java.util.ArrayDeque;

public class TickableManager {
    /**
     * The list of tickables.
     */
    private List<Tickable> active = new ArrayList<>();

    private Queue<Tickable> pending = new ArrayDeque<>();

    public void pulse() {
        Tickable t;
        while ((t = pending.poll()) != null)
            active.add(t);

        for (Iterator<Tickable> iterator = active.iterator(); iterator.hasNext(); ) {
            Tickable tick = iterator.next();
            if (tick.isRunning())
                Server.getServer().getGameService().submit(() -> tick.cycle());
            else
                iterator.remove();
        }
    }

    /**
     * @return The tickables.
     */
    public List<Tickable> getTickables() {
        return active;
    }

    /**
     * Submits a new tickable to the <code>GameEngine</code>.
     *
     * @param tickable The tickable to submit.
     */
    public boolean submit(final Tickable tickable) {
        return pending.add(tickable);
    }
}
