package net.swiftpk.server.tickable.impl.terrian;

import net.swiftpk.server.model.entity.GameObject;
import net.swiftpk.server.model.terrain.World;
import net.swiftpk.server.tickable.Tickable;

public class ObjectRemoverTick extends Tickable {
    private GameObject object;

    public ObjectRemoverTick(GameObject object, int delay) {
        super(delay);
        this.object = object;
    }

    @Override
    public void execute() {
        int x = object.getLocation().getX(), y = object.getLocation().getY();
        if (!World.getInstance().hasGameObject(x, y) || World.getInstance().getGameObject(x, y).getID() != object.getID()) {
            stop();
            return;
        }
        World.getInstance().unregisterGameObject(object);
        stop();
    }
}
