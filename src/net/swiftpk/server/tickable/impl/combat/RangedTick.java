package net.swiftpk.server.tickable.impl.combat;

import java.util.ArrayList;
import java.util.List;

import net.swiftpk.server.Server;
import net.swiftpk.server.ent.EntityHandler;
import net.swiftpk.server.kplugin.event.impl.NPCRangedEvent;
import net.swiftpk.server.model.Damage;
import net.swiftpk.server.model.InvItem;
import net.swiftpk.server.model.Item;
import net.swiftpk.server.model.MobState;
import net.swiftpk.server.model.Player;
import net.swiftpk.server.model.Projectile;
import net.swiftpk.server.model.component.SkillSet;
import net.swiftpk.server.model.component.SkillSet.Skill;
import net.swiftpk.server.model.entity.Mob;
import net.swiftpk.server.model.entity.NPC;
import net.swiftpk.server.model.terrain.World;
import net.swiftpk.server.net.ActionSender;
import net.swiftpk.server.sync.block.HitUpdateBlock;
import net.swiftpk.server.tickable.Tickable;
import net.swiftpk.server.util.DataConversions;
import net.swiftpk.server.util.Formulae;
import net.swiftpk.server.util.RandomUtil;
import net.swiftpk.server.util.RangedUtils;

public class RangedTick extends Tickable {
    private final Mob target;
    private final Player player;

    public RangedTick(Player player, Mob mob) {
        super(0);
        this.player = player;
        this.target = mob;
        if (player == null || mob == null) {
            stop();
            return;
        }
    }

    public final Player getOwner() {
        return player;
    }

    @Override
    public void execute() {
        player.resetAll();
        double[] bowStats = player.getBowStats();
        if (bowStats == null || player.getState() != MobState.Idle || !player.checkAttack(target)) {
            stop();
            return;
        }
        if (!player.canRange()) // Timer isn't ready.
            return;
        if (!player.withinRange(target, (int) bowStats[2])) {
            // TODO: Is this correct behavior? Check RSCPlus
            stop();
            return;
        }
        player.getRangePoints();
        int arrowID = getBestAvailableArrowID();
        if (arrowID == -1) {
            ActionSender.sendMessage(player, "You don't have any usable arrows!");
            stop();
            return;
        }
        if (target instanceof Player) {
            Player p = (Player) target;
            if (p.isPrayerActivated(13)) {
                ActionSender.sendMessage(player, "This player is invulnerable to missiles!");
                stop();
                return;
            }
            player.attackedPlayer(p);
            ActionSender.sendMessage(p, "You are under attack!");
        }
        player.resetRangeTimer();
        player.getInventory().remove(arrowID, 1);
        ActionSender.sendInventory(player);
        if (RandomUtil.inclusive(6) == 0) {
            Item arrows = getGroundArrowPile(arrowID);
            if (arrows == null)
                new Item(arrowID, target.getX(), target.getY(), 1, true, player);
            else
                arrows.setAmount(arrows.getAmount() + 1);
        }
        int damage = Formulae.calculateRangeHit(player, target, getArrowMod(arrowID), bowStats[1]);
        Skill targetHits = target.getSkillSet().get(SkillSet.HITPOINTS);
        if (targetHits.getLevel() > 0 && damage > targetHits.getLevel())
            damage = targetHits.getLevel();
        if (target instanceof NPC) {
            ((NPC) target).getSyndicate().addDamage(player, damage, Damage.RANGE_DAMAGE);
            Server.getServer().getPlugins().post(new NPCRangedEvent(player, (NPC) target));
        } else
            ActionSender.sendStat((Player) target, SkillSet.HITPOINTS);
        player.setProjectile(new Projectile(player, target, 2));
        int newHits = targetHits.getLevel() - damage;
        if (newHits > 0) {
            targetHits.setLevel(newHits);
            target.getBlockSet().add(HitUpdateBlock.createHitUpdateBlock(damage, newHits, targetHits.getMaxLevel(), target.getIndex()));
        } else {
            target.killedBy(player);
            if (target instanceof Player) {
                long exp = DataConversions.roundUp(target.getSkillSet().getCombatLevel() * .55 * 1.5);
                player.incExp(4, exp);
                ActionSender.sendStat(player, 4);
            } else
                ((NPC) target).getSyndicate().distributeExp(((NPC) target));
            stop();
            return;
        }
        if (target instanceof NPC) {
            NPC npc = (NPC) target;
            if (!npc.isFighting())
                npc.setChasing(player, 0, true);
        }
        this.setTickDelay((int) bowStats[0]);
    }

    public double getArrowMod(int arrowId) {
        double poisonDmgIncrease = 0.025;
        switch (arrowId) {
            case 11:
                return 1 + (isPoisioned(arrowId) ? poisonDmgIncrease : 0);
            case 574:
                return 1 + (isPoisioned(arrowId) ? poisonDmgIncrease : 0);
            case 638:
                return 1.05 + (isPoisioned(arrowId) ? poisonDmgIncrease : 0);
            case 639:
                return 1.05 + (isPoisioned(arrowId) ? poisonDmgIncrease : 0);
            case 640:
                return 1.1 + (isPoisioned(arrowId) ? poisonDmgIncrease : 0);
            case 641:
                return 1.1 + (isPoisioned(arrowId) ? poisonDmgIncrease : 0);
            case 642:
                return 1.2 + (isPoisioned(arrowId) ? poisonDmgIncrease : 0);
            case 643:
                return 1.2 + (isPoisioned(arrowId) ? poisonDmgIncrease : 0);
            case 786:
                return 1.3 + (isPoisioned(arrowId) ? poisonDmgIncrease : 0); // pearl
            // xbow
            // bolts
            case 644:
                return 1.3 + (isPoisioned(arrowId) ? poisonDmgIncrease : 0);
            case 645:
                return 1.3 + (isPoisioned(arrowId) ? poisonDmgIncrease : 0);
            case 646:
                return 1.4 + (isPoisioned(arrowId) ? poisonDmgIncrease : 0);
            case 647:
                return 1.4 + (isPoisioned(arrowId) ? poisonDmgIncrease : 0);
            default:
                return 1 + (isPoisioned(arrowId) ? poisonDmgIncrease : 0);
        }
    }

    private Item getGroundArrowPile(int id) {
        Item arrows = World.getInstance().getItem(id, target.getLocation());
        if (arrows != null && arrows.visibleTo(player) && !arrows.isRemoved())
            return arrows;
        return null;
    }

    public Mob getTarget() {
        return target;
    }

    public boolean isPoisioned(int arrowId) {
        switch (arrowId) {
            case 11:
                return false;
            case 574:
                return true;
            case 592:
                return true;
            case 638:
                return false;
            case 639:
                return true;
            case 640:
                return false;
            case 641:
                return true;
            case 642:
                return false;
            case 643:
                return true;
            case 644:
                return false;
            case 645:
                return true;
            case 646:
                return false;
            case 647:
                return true;
            default:
                return false;
        }
    }

    public void removeArrow(int id) {
        InvItem arrow = new InvItem(id, 1);
        player.getInventory().remove(arrow);
        ActionSender.sendInventory(player);
    }

    private final int getBestAvailableArrowID() {
        List<Integer> cantUse = new ArrayList<>();
        List<InvItem> items = player.getInventory().getItems();
        int[][] wepArray = RangedUtils.arrows;
        int bowType = player.getBowType();
        if (bowType == 2)
            wepArray = RangedUtils.bolts;
        else if (bowType == -1)
            return -1;
        for (int i = 0; i < items.size(); i++) {
            if (items.get(i).getDef().isStackable())
                for (int n = 0; n < wepArray[0].length; n++) {
                    if (items.get(i).getID() == wepArray[1][n]) {
                        if (player.getSkillSet().get(SkillSet.RANGE).getMaxLevel() >= wepArray[0][n])
                            return wepArray[1][n];
                        else
                            cantUse.add(n);
                    }
                }
        }
        for (int i : cantUse) {
            ActionSender.sendMessage(player, "You need at least " + wepArray[0][i] + " range to use "
                    + EntityHandler.getItemDef(wepArray[1][i]).getName());
        }
        return -1;
    }
}
