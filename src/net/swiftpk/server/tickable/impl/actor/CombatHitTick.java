package net.swiftpk.server.tickable.impl.actor;

import net.swiftpk.server.model.Damage;
import net.swiftpk.server.model.Player;
import net.swiftpk.server.model.component.SkillSet;
import net.swiftpk.server.model.component.SkillSet.Skill;
import net.swiftpk.server.model.entity.Mob;
import net.swiftpk.server.model.entity.NPC;
import net.swiftpk.server.net.ActionSender;
import net.swiftpk.server.sync.block.SynchronizationBlock;
import net.swiftpk.server.tickable.Tickable;
import net.swiftpk.server.util.DataConversions;
import net.swiftpk.server.util.Formulae;

public class CombatHitTick extends Tickable {
    public Mob affectedMob;
    private int firstHit;
    private int hits;
    public Mob player;

    public CombatHitTick(Mob player, Mob affectedMob) {
        this(player, affectedMob, false);
    }

    public CombatHitTick(Mob player, Mob affectedMob, boolean attacked) {
        super(2);
        this.player = player;
        if (this.player.getCombatRound() == 0) {
            this.setTickDelay(0);
        }
        if (affectedMob == null) {
            stop();
            return;
        }
        this.affectedMob = affectedMob;
        firstHit = attacked ? 1 : 0;
        hits = 0;
    }

    @Override
    public void execute() {
        if ((player instanceof Player && !((Player) player).loggedIn())
                || (affectedMob instanceof Player && !((Player) affectedMob).loggedIn())) {
            player.resetCombat();
            affectedMob.resetCombat();
            this.stop();
            return;
        }
        Mob attacker, opponent;
        if (hits++ % 2 == firstHit) {
            attacker = player;
            opponent = affectedMob;
        } else {
            attacker = affectedMob;
            opponent = player;
        }
        attacker.incCombatRound();
        if (attacker instanceof NPC && (opponent instanceof Player && ((Player) opponent).isPrayerActivated(12))) {
            return;
        }
        int damage = Formulae.calculateCombatHit(attacker, opponent);
        if (attacker instanceof Player && opponent instanceof NPC) {
            NPC npc = (NPC) opponent;
            npc.getSyndicate().addDamage((Player) attacker, damage, Damage.COMBAT_DAMAGE);
        }
        Skill opponentHits = opponent.getSkillSet().get(SkillSet.HITPOINTS);
        int newHp = opponentHits.getLevel() - damage;
        if (newHp > 0) {
            opponentHits.setLevel(newHp);
            opponent.getBlockSet().add(SynchronizationBlock.createHitUpdateBlock(damage, newHp, opponentHits.getMaxLevel(), opponent.getIndex()));
        }
        String combatSound = damage > 0 ? "combat1b" : "combat1a";
        if (opponent instanceof Player) {
            if (attacker instanceof NPC) {
                NPC n = (NPC) attacker;
                if (DataConversions.inArray(Formulae.ARMOR_NPCS, n.getID()))
                    combatSound = damage > 0 ? "combat2b" : "combat2a";
                else if (DataConversions.inArray(Formulae.UNDEAD_NPCS, n.getID()))
                    combatSound = damage > 0 ? "combat3b" : "combat3a";
                else
                    combatSound = damage > 0 ? "combat1b" : "combat1a";
            }
            ActionSender.sendStat((Player) opponent, 3);
            ActionSender.sendSound((Player) opponent, combatSound);
        }
        if (attacker instanceof Player) {
            if (opponent instanceof NPC) {
                NPC n = (NPC) opponent;
                if (DataConversions.inArray(Formulae.ARMOR_NPCS, n.getID()))
                    combatSound = damage > 0 ? "combat2b" : "combat2a";
                else if (DataConversions.inArray(Formulae.UNDEAD_NPCS, n.getID()))
                    combatSound = damage > 0 ? "combat3b" : "combat3a";
                else
                    combatSound = damage > 0 ? "combat1b" : "combat1a";
            }
            ActionSender.sendSound((Player) attacker, combatSound);
        }
        if (newHp <= 0) {
            if (opponent instanceof NPC && attacker instanceof Player) {
                ((NPC) opponent).getSyndicate().distributeExp((NPC) opponent);
            } else if (attacker instanceof Player) {
                Player attackerPlayer = (Player) attacker;
                int exp = DataConversions.roundUp(combatExperience(opponent) / 4D);
                switch (attackerPlayer.getFightMode()) {
                    case 0:
                        for (int x = 0; x < 3; x++) {
                            attackerPlayer.incExp(x, exp);
                            ActionSender.sendStat(attackerPlayer, x);
                        }
                        break;
                    case 1:
                        attackerPlayer.incExp(2, exp * 3);
                        ActionSender.sendStat(attackerPlayer, 2);
                        break;
                    case 2:
                        attackerPlayer.incExp(0, exp * 3);
                        ActionSender.sendStat(attackerPlayer, 0);
                        break;
                    case 3:
                        attackerPlayer.incExp(1, exp * 3);
                        ActionSender.sendStat(attackerPlayer, 1);
                        break;
                }
                attackerPlayer.incExp(3, exp);
                ActionSender.sendStat(attackerPlayer, 3);
            }
            opponent.killedBy(attacker);
            attacker.resetCombat();
            opponent.resetCombat();
            this.stop();
        }
        if (this.getTickDelay() == 0) {
            this.setTickDelay(2);
        }
    }

    private static int combatExperience(Mob mob) {
        double exp = ((mob.getComponent(SkillSet.class).getCombatLevel() * 2) + 10) * 1.5D;
        return (int) (mob instanceof Player ? (exp / 4D) : exp);
    }
}
