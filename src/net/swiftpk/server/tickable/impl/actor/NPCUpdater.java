package net.swiftpk.server.tickable.impl.actor;

import java.util.ArrayList;
import java.util.List;

import net.swiftpk.server.ent.locs.NPCLoc;
import net.swiftpk.server.model.MobState;
import net.swiftpk.server.model.Player;
import net.swiftpk.server.model.entity.NPC;
import net.swiftpk.server.model.terrain.World;
import net.swiftpk.server.model.terrain.tiles.Point;
import net.swiftpk.server.util.EntityList;
import net.swiftpk.server.util.Logger;
import net.swiftpk.server.util.RandomUtil;

public class NPCUpdater {
    private static final Point DEAD = Point.location(0, 0, 0);

    private static void aggressive(NPC n) {
        NPCLoc nl = n.getLoc();
        if (n.getState() == MobState.Idle && nl.isAggressive() && n.canAttack()) {
            List<Player> players = new ArrayList<>();
            for (Player player : n.getViewArea().getPlayersInView(2)) {
                if (player.getState() == MobState.Idle && !player.isFighting() && !player.isAdmin()
                        && player.isAttackableByNPCs() && n.canWalkTo(player.getLocation())) {
                    players.add(player);
                }
            }
            if (players.size() >= 2) {
                n.startCombat(players.get(RandomUtil.inclusive(players.size())));
            } else if (players.size() != 0) {
                n.startCombat(players.get(0));
            }
        }
    }

    private static Point getRandomEmptyPointInRoamArea(NPC n) {
        NPCLoc nl = n.getLoc();
        for (int i = 0; i < 10; i++) {
            if (nl.minX > nl.maxX) {
                int tmpX = nl.minX;
                nl.minX = nl.maxX;
                nl.maxX = tmpX;
            }
            if (nl.minY > nl.maxY) {
                int tmpY = nl.minY;
                nl.minY = nl.maxY;
                nl.maxY = tmpY;
            }
            Point newLocation = new Point(RandomUtil.inclusive(nl.minX, nl.maxX), RandomUtil.inclusive(nl.minY, nl.maxY), n.getLayer());
            if (!n.isFighting() && World.getInstance().withinWorld(newLocation)) {
                if (newLocation.hasGameObject() || newLocation.hasNPC() || !newLocation.isWalkable())
                    continue;
                return newLocation;
            }
        }
        return null;
    }

    private static NPCLoc makeNewLoc(NPC n) {
        int[] p = new int[]{n.getLocation().getX(), n.getLocation().getY()};
        NPCLoc tmp = new NPCLoc();
        tmp.minX = p[0] - 2;
        tmp.maxX = p[0] + 3;
        tmp.minY = p[1] - 2;
        tmp.maxY = p[1] + 3;
        tmp.respawnTime = -1;
        tmp.npcCount = 0;
        tmp.id = n.getID();
        tmp.aggressive = false;
        return tmp;
    }

    private static void moveNPC(NPC n) {
        if (n.getState() == MobState.Idle) {
            if (n.getLocation().equals(DEAD)) {
                n.setState(MobState.Dead);
                return;
            }
            if (n.getMoveTime() <= System.currentTimeMillis()) {
                Point newLocation = getRandomEmptyPointInRoamArea(n);
                if (newLocation != null) {
                    n.walk(newLocation.getX(), newLocation.getY());
                    // Logger.log(n.toString()+" New path: "+n.getPathHandler().toString());
                }
            }
        }
    }

    private static void respawnNPC(NPC n) {
        if (n.getState() == MobState.Dead || n.getLocation().equals(DEAD)) {
            NPCLoc nl = n.getLoc();
            if (nl == null) {
                nl = makeNewLoc(n);
                n.setLoc(nl);
            }
            if (nl.getRespawnTime() == -1 && n.getSpawnCount() >= 1) {
                Logger.out("Removed NPC: " + n);
                World.getInstance().unregisterNPC(n);
                return;
            }
            if (n.getTimeSinceKilled() >= nl.getRespawnTime() * 1000 || nl.getRespawnTime() == -1) {
                Point p = getRandomEmptyPointInRoamArea(n);
                if (p == null)
                    return;
                n.setState(MobState.Idle);
                n.incSpawnCounter();
                n.setLocation(p, true);
            }
        }
    }

    public static void update(EntityList<NPC> npcs) {
        for (NPC n : npcs) {
            respawnNPC(n);
            if (n.getViewArea().getPlayersInView().iterator().hasNext()) {
                moveNPC(n);
                aggressive(n);
            }
        }
    }
}
