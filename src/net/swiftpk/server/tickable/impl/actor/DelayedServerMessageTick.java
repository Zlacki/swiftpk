package net.swiftpk.server.tickable.impl.actor;

import net.swiftpk.server.model.Player;
import net.swiftpk.server.net.ActionSender;
import net.swiftpk.server.tickable.Tickable;

public class DelayedServerMessageTick extends Tickable {
    private Player player;
    private int strIndex;
    private String[] toSay;

    public DelayedServerMessageTick(Player p, String[] toSay) {
        super(0);
        this.player = p;
        this.toSay = toSay;
        if (p == null) {
            stop();
            return;
        }
        this.strIndex = 0;
    }

    @Override
    public void execute() {
        ActionSender.sendMessage(player, toSay[strIndex++]);
        if (strIndex == toSay.length)
            stop();
        this.setTickDelay(3);
    }

    public int totalDelay() {
        return 3 * (toSay.length - 1);
    }
}
