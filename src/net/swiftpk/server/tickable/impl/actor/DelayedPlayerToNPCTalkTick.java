package net.swiftpk.server.tickable.impl.actor;

import net.swiftpk.server.model.ChatMessage;
import net.swiftpk.server.model.Player;
import net.swiftpk.server.model.component.Communication;
import net.swiftpk.server.model.entity.NPC;
import net.swiftpk.server.tickable.Tickable;

public class DelayedPlayerToNPCTalkTick extends Tickable {
    private NPC npc = null;
    private Player sender;
    private int strIndex;
    private String[] toSay;

    public DelayedPlayerToNPCTalkTick(Player p, NPC npc, String[] toSay) {
        super(0);
        this.npc = npc;
        this.sender = p;
        this.toSay = toSay;
        if (p == null || npc == null) {
            stop();
            return;
        }
        this.strIndex = 0;
    }

    @Override
    public void execute() {
        String message = toSay[strIndex++];
        for (Player player : sender.getLocalPlayerList()) {
            player.getComponent(Communication.class).pushMessage(new ChatMessage(sender, message, npc));
        }
        sender.getComponent(Communication.class).pushMessage(new ChatMessage(sender, message, npc));
        if (strIndex == toSay.length)
            stop();
        this.setTickDelay(3);
    }

    public int totalDelay() {
        return 3 * (toSay.length - 1);
    }
}
