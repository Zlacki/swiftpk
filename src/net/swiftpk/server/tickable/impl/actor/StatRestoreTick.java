package net.swiftpk.server.tickable.impl.actor;

import net.swiftpk.server.model.Player;
import net.swiftpk.server.model.component.SkillSet;
import net.swiftpk.server.model.component.SkillSet.Skill;
import net.swiftpk.server.model.terrain.World;
import net.swiftpk.server.net.ActionSender;
import net.swiftpk.server.tickable.Tickable;
import net.swiftpk.server.util.Formulae;

import java.util.Objects;

public class StatRestoreTick extends Tickable {
    public StatRestoreTick() {
        super(100);
    }

    @Override
    public void execute() {
        for (Player player : World.getInstance().getPlayers()) {
            if (!player.loggedIn() || !player.getChannel().isActive())
                continue;
            for (int statIndex = 0; statIndex < SkillSet.SKILL_COUNT; statIndex++) {
                Skill skill = Objects.requireNonNull(player.getSkillSet().get(statIndex));
                int curLevel = skill.getLevel();
                if (curLevel == skill.getMaxLevel())
                    continue;
                if (curLevel > skill.getMaxLevel()) curLevel--;
                else if (statIndex != SkillSet.PRAYER) curLevel++;
                if (curLevel != skill.getLevel()) {
                    skill.setLevel(curLevel);
                    ActionSender.sendStat(player, statIndex);
                }
                if (statIndex != SkillSet.HITPOINTS && curLevel == skill.getMaxLevel())
                    ActionSender.sendMessage(player, "Your " + Formulae.statArray[statIndex] + " ability has returned to normal.");
            }
        }
    }
}
