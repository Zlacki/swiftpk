package net.swiftpk.server.tickable.impl.actor;

import net.swiftpk.server.model.MobState;
import net.swiftpk.server.model.Player;
import net.swiftpk.server.model.entity.NPC;
import net.swiftpk.server.model.entity.mob.NPCChatMessage;
import net.swiftpk.server.tickable.Tickable;

public class DelayedNPCTalkTick extends Tickable {
    private NPC npc = null;
    private Player player;
    private int strIndex;
    private String[] toSay;

    public DelayedNPCTalkTick(Player p, NPC npc, String[] toSay) {
        super(0);
        this.npc = npc;
        this.player = p;
        this.toSay = toSay;
        if (p == null || npc == null) {
            stop();
            return;
        }
        this.strIndex = 0;
    }

    @Override
    public void execute() {
        player.setState(MobState.Talking);
        npc.setState(MobState.Talking);
        NPCChatMessage msg = new NPCChatMessage(npc, toSay[strIndex++], player);
        npc.setChatMessage(msg);
        if (strIndex == toSay.length) {
            stop();
        }
        this.setTickDelay(3);
    }

    public int totalDelay() {
        return 3 * (toSay.length - 1);
    }
}
