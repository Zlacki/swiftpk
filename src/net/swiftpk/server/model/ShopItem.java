package net.swiftpk.server.model;

public class ShopItem {
    private int amount; // -1 = infinite
    private int itemid;

    public ShopItem(int item, int amount) {
        this.itemid = item;
        this.amount = amount;
    }

    public void decAmount() {
        if (amount != -1)
            amount--;
    }

    @Override
    public boolean equals(Object o) {
        return o instanceof ShopItem && ((ShopItem) o).getItemId() == itemid;
    }

    public int getAmount() {
        return amount;
    }

    public int getItemId() {
        return itemid;
    }

    public boolean hasMore() {
        return amount == -1 || amount > 0;
    }

    public void incAmount() {
        amount++;
    }

    public void setAmount(int a) {
        amount = a;
    }

    public void setItemId(int i) {
        itemid = i;
    }
}
