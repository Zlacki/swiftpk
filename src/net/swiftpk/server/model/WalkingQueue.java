package net.swiftpk.server.model;

import net.swiftpk.server.model.entity.Mob;
import net.swiftpk.server.model.entity.NPC;
import net.swiftpk.server.model.terrain.Path;
import net.swiftpk.server.model.terrain.World;
import net.swiftpk.server.model.terrain.tiles.Point;
import net.swiftpk.server.model.terrain.tiles.TileValue;
import net.swiftpk.server.util.CollisionFlag;

/**
 * <p>
 * A <code>WalkingQueue</code> stores steps the client needs to walk and allows
 * this queue of steps to be modified.
 * </p>
 *
 * <p>
 * The class will also process these steps when {@link #processNextMovement()}
 * is called. This should be called once per server cycle.
 * </p>
 *
 * @author Graham Edgecombe
 */
public class WalkingQueue {

    //private static final boolean CHARACTER_FACING = true;
    private boolean DEBUG = false;

    private Mob mob;

    private Path path;

    public WalkingQueue(Mob entity) {
        this.mob = entity;
    }

    /**
     * Processes the next player's movement.
     */
    public void processNextMovement() {
        if (path == null) {
            return;
        } else if (path.isEmpty()) {
            reset();
            return;
        }
        Point walkPoint = path.poll();

        int destX = walkPoint.getX();
        int destY = walkPoint.getY();
        int startX = mob.getX();
        int startY = mob.getY();
        if (!checkAdjacent(new Point(startX, startY), new Point(destX, destY))) {
            reset();
            return;
        }
        if (mob instanceof NPC)
            mob.setLocation(Point.location(destX, destY));
        else {
            Player p = (Player) mob;
            p.setLocation(Point.location(destX, destY));
        }

    }

    private boolean checkAdjacent(Point curPoint, Point nextPoint) {
        int[] coords = {curPoint.getX(), curPoint.getY()};
        int startX = curPoint.getX();
        int startY = curPoint.getY();
        int destX = nextPoint.getX();
        int destY = nextPoint.getY();
        boolean myXBlocked = false, myYBlocked = false, newXBlocked = false, newYBlocked = false;

        if (startX > destX) {
            // Check for wall on east edge of current square,
            myXBlocked = checkBlocking(startX, startY, CollisionFlag.WALL_EAST, true);
            // Or on west edge of square we are travelling toward.
            newXBlocked = checkBlocking(startX - 1, startY, CollisionFlag.WALL_WEST, false);
            coords[0] = startX - 1;
        } else if (startX < destX) {
            // Check for wall on west edge of current square,
            myXBlocked = checkBlocking(startX, startY, CollisionFlag.WALL_WEST, true);
            // Or on east edge of square we are travelling toward.
            newXBlocked = checkBlocking(startX + 1, startY, CollisionFlag.WALL_EAST, false);
            coords[0] = startX + 1;
        }

        if (startY > destY) {
            // Check for wall on north edge of current square,
            myYBlocked = checkBlocking(startX, startY, CollisionFlag.WALL_NORTH, true);
            // Or on south edge of square we are travelling toward.
            newYBlocked = checkBlocking(startX, startY - 1, CollisionFlag.WALL_SOUTH, false);
            coords[1] = startY - 1;

        } else if (startY < destY) {
            // Check for wall on south edge of current square,
            myYBlocked = checkBlocking(startX, startY, CollisionFlag.WALL_SOUTH, true);
            // Or on north edge of square we are travelling toward.
            newYBlocked = checkBlocking(startX, startY + 1, CollisionFlag.WALL_NORTH, false);
            coords[1] = startX + 1;
        }

        if (myXBlocked && myYBlocked) return false;
        if (myXBlocked && startY == destY) return false;
        if (myYBlocked && startX == destX) return false;
        if (newXBlocked && newYBlocked) return false;
        if (newXBlocked && startY == coords[1]) return false;
        if (newYBlocked && startX == coords[0]) return false;
        if ((myXBlocked && newXBlocked) || (myYBlocked && newYBlocked)) return false;

        if (coords[0] > startX) {
            newXBlocked = checkBlocking(coords[0], coords[1], CollisionFlag.WALL_EAST, false);
        } else if (coords[0] < startX) {
            newXBlocked = checkBlocking(coords[0], coords[1], CollisionFlag.WALL_WEST, false);
        }

        if (coords[1] > startY) {
            newXBlocked = checkBlocking(coords[0], coords[1], CollisionFlag.WALL_NORTH, false);
        } else if (coords[1] < startY) {
            newXBlocked = checkBlocking(coords[0], coords[1], CollisionFlag.WALL_SOUTH, false);
        }

        if (newXBlocked && newYBlocked) return false;
        if (newXBlocked && startY == coords[1]) return false;
        if (myYBlocked && startX == coords[0]) return false;
        if (myXBlocked && newXBlocked) return false;
        if (myYBlocked && newYBlocked) return false;

        // Diagonal checks
        boolean diagonalBlocked = false;
        if (startX + 1 == destX && startY + 1 == destY)
            diagonalBlocked = checkBlocking(startX + 1, startY + 1,
                    CollisionFlag.WALL_NORTH + CollisionFlag.WALL_EAST, false);
        else if (startX + 1 == destX && startY - 1 == destY)
            diagonalBlocked = checkBlocking(startX + 1, startY - 1,
                    CollisionFlag.WALL_SOUTH + CollisionFlag.WALL_EAST, false);
        else if (startX - 1 == destX && startY + 1 == destY)
            diagonalBlocked = checkBlocking(startX - 1, startY + 1,
                    CollisionFlag.WALL_NORTH + CollisionFlag.WALL_WEST, false);
        else if (startX - 1 == destX && startY - 1 == destY)
            diagonalBlocked = checkBlocking(startX - 1, startY - 1,
                    CollisionFlag.WALL_SOUTH + CollisionFlag.WALL_WEST, false);

        if (diagonalBlocked)
            return false;
        
        // if (mob.isPlayer()) // for debugging
        return !PathValidation.checkDiagonalPassThroughCollisions(curPoint, nextPoint);
//        return true; // for debugging

    }

    private boolean checkBlocking(int x, int y, int bit, boolean isCurrentTile) {
        TileValue t = World.getInstance().getTileValue(x, y);
		/*boolean inFisherKingdom = (mob.getLocation().inBounds(415, 976, 423, 984)
			|| mob.getLocation().inBounds(511, 976, 519, 984));*/
        boolean blockedPath = PathValidation.isBlocking(t.traversalMask, (byte) bit, isCurrentTile);
        return blockedPath || isMobBlocking(x, y);
    }

    private boolean isMobBlocking(int x, int y) {
/*        Area region = World.getInstance().getArea(Point.location(x, y));
        if (mob.getX() == x && mob.getY() == y)
            return false;

        Npc npc = World.getInstance().getNpc();

        /*
         * NPC blocking config controlled
         */
/*        if (Constants.GameServer.NPC_BLOCKING == 0) { // No NPC blocks
            return false;
        } else if (Constants.GameServer.NPC_BLOCKING == 1) { // 2 * combat level + 1 blocks AND aggressive
            if (npc != null && mob.getCombatLevel() < ((npc.getNPCCombatLevel() * 2) + 1) && npc.getDef().isAggressive()) {
                return true;
            }
        } else if (Constants.GameServer.NPC_BLOCKING == 2) { // Any aggressive NPC blocks
            if (npc != null && npc.getDef().isAggressive()) {
                return true;
            }
        } else if (Constants.GameServer.NPC_BLOCKING == 3) { // Any attackable NPC blocks
            if (npc != null && npc.getDef().isAttackable()) {
                return true;
            }
        } else if (Constants.GameServer.NPC_BLOCKING == 4) { // All NPCs block
            if (npc != null) {
                return true;
            }
        }

        if (mob.isNpc()) {
            Player p = region.getPlayer(x, y);
            return p != null;
        }*/
        return false;
    }

    public void reset() {
        path = null;
    }

    public boolean finished() {
        return path == null || path.isEmpty();
    }

    public void setPath(Path path) {
        this.path = path;
    }
}
