package net.swiftpk.server.model;

import net.swiftpk.server.model.entity.GameObject;
import net.swiftpk.server.model.entity.NPC;
import net.swiftpk.server.model.terrain.World;
import net.swiftpk.server.model.terrain.tiles.Area;
import net.swiftpk.server.model.terrain.tiles.Point;
import net.swiftpk.server.util.Formulae;
import net.swiftpk.server.util.Logger;

import java.util.concurrent.atomic.AtomicReference;

public class Entity {
    /**
     * World instance
     */
    public static final World world = World.getInstance();
    protected int id;
    protected int index;
    protected int layer;
    protected AtomicReference<Area> area = new AtomicReference<>();

    public int getLayer() {
        return location.getLayer();
    }

    protected Point location;

    public final int getID() {
        return id;
    }

    public final int getIndex() {
        return index;
    }

    public final Point getLocation() {
        return location;
    }

    public final int getX() {
        return location.getX();
    }

    public final int getY() {
        return location.getY();
    }

    public final void setID(int newID) {
        id = newID;
    }

    public final void setIndex(int newIndex) {
        index = newIndex;
    }

    public void setLocation(Point p) {
        area.set(world.getArea(p));
        world.setLocation(this, location, p);
        location = p;
    }

    public Area getArea() {
        return area.get();
    }

    public boolean withinRange(Entity e, int radius) {
        if (e == null || e.getLocation() == null)
            return false;
        return withinRange(e.getLocation(), radius);
    }

    public boolean isOn(int x, int y) {
        return x == getX() && y == getY();
    }

    public boolean withinRange(Point p, int radius) {
        if(location == null)
            return false;
        return location.withinRange(p, radius);
    }

    public boolean withinRange(Point p, int rad, int layer) {
        if (p == null || location == null)
            return false;
        return location.getLongestDelta(p) <= rad && location.getLayer() == layer;
    }
}
