package net.swiftpk.server.model;


import net.swiftpk.server.ent.EntityHandler;
import net.swiftpk.server.ent.defs.ItemDef;
import net.swiftpk.server.ent.locs.ItemLoc;
import net.swiftpk.server.event.Event;
import net.swiftpk.server.model.entity.DefaultEntityFactory;
import net.swiftpk.server.model.terrain.World;
import net.swiftpk.server.model.terrain.tiles.Point;

public class Item extends Entity {
    private static int INDEX = 0;
    private Event removerTask = new Event(3 * 60 * 1000) {
        public void execute() {
            Item.this.remove();
            stop();
        }
    };
    /**
     * Amount (for stackables)
     */
    private int amount;
    /**
     * Location definition of the item
     */
    private ItemLoc loc = null;
    /**
     * Contains the player that the item belongs to, if any
     */
    private Player owner;
    /**
     * Set when the item has been destroyed to alert players
     */
    private boolean removed = false;
    /**
     * The time that the item was spawned
     */
    private long spawnedTime;

    public Item(ItemLoc loc) {
        this(loc.id, loc.x, loc.y, loc.amount, false, null, 0);
        this.loc = loc;
        removerTask.stop();
    }

    public Item(int id, int x, int y, int amount, boolean timeOut, Player owner) {
        this(id, x, y, amount, timeOut, owner, owner != null ? owner.getLocation().getLayer() : 0);
    }

    public Item(int id, int x, int y, int amount, boolean timeOut, Player owner, int layer) {
        setIndex(INDEX++);
        setID(id);
        setAmount(amount);
        this.owner = owner;
        spawnedTime = System.currentTimeMillis();
        setLocation(Point.location(x, y, layer));
        World.getInstance().submit(removerTask);
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof Item) {
            Item item = (Item) o;
            return getIndex() == item.getIndex();
        }
        return false;
    }

    public int getAmount() {
        return amount;
    }

    public ItemDef getDef() {
        return EntityHandler.getItemDef(id);
    }

    public ItemLoc getLoc() {
        return loc;
    }

    public Player getOwner() {
        return owner;
    }

    public long getSpawnedTime() {
        return spawnedTime;
    }

    public boolean isRemoved() {
        return removed;
    }

    public void remove() {
        if(removerTask.isRunning())
            removerTask.stop();
        area.get().removeItem(this);
        if (!removed && loc != null && loc.getRespawnTime() > 0) {
            World.getInstance().submit(new Event(loc.getRespawnTime()) {
                @Override
                public void execute() {
                    DefaultEntityFactory.getInstance().newItem(loc);
                    stop();
                }
            });
        }
        removed = true;
    }

    public void setAmount(int amount) {
        if (getDef().isStackable()) {
            this.amount = amount;
        } else {
            this.amount = 1;
        }
    }

    public boolean visibleTo(Player p) {
        if ((owner == null || p.equals(owner) || p.getRank() == 2) && !removed)
            return true;
        return System.currentTimeMillis() - spawnedTime > 60 * 1000;
    }
}
