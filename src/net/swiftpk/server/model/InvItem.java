package net.swiftpk.server.model;

import net.swiftpk.server.ent.EntityHandler;
import net.swiftpk.server.ent.defs.ItemDef;
import net.swiftpk.server.ent.defs.extras.ItemWieldableDef;

public class InvItem implements Comparable<InvItem> {
    private int amount;
    private int id;
    private boolean wielded = false;

    public InvItem(int id) {
        this.id = id;
        setAmount(1);
    }

    public InvItem(int id, int amount) {
        this.id = id;
        setAmount(amount);
    }

    public InvItem(InvItem ii) {
        this.id = ii.id;
        this.amount = ii.amount;
        this.wielded = ii.wielded;
    }

    @Override
    public int compareTo(InvItem item) {
        if (item.getDef().isStackable()) {
            return -1;
        }
        if (getDef().isStackable()) {
            return 1;
        }
        return item.getDef().getBasePrice() - getDef().getBasePrice();
    }

    public int eatingHeals() {
        if (!isEdible()) {
            return 0;
        }
        return EntityHandler.getItemEdibleHeals(id);
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof InvItem) {
            InvItem item = (InvItem) o;
            return item.getID() == getID();
        }
        return false;
    }

    public int getAmount() {
        return amount;
    }

    public ItemDef getDef() {
        return EntityHandler.getItemDef(id);
    }

    public int getID() {
        return id;
    }

    public ItemWieldableDef getWieldableDef() {
        return EntityHandler.getItemWieldableDef(id);
    }

    public boolean isEdible() {
        return EntityHandler.getItemEdibleHeals(id) > 0;
    }

    public boolean isWieldable() {
        return EntityHandler.getItemWieldableDef(id) != null;
    }

    public boolean isWielded() {
        return wielded;
    }

    public void setAmount(int amount) {
        if (amount < 0) {
            amount = 0;
            return;
        }
        if (getDef().isStackable()) {
            this.amount = amount;
        } else {
            this.amount = amount;
        }
    }

    public void setWield(boolean wielded) {
        this.wielded = wielded;
    }

    @Override
    public String toString() {
        return (getDef().isStackable() ? amount + "x" : "") + "'"
                + getDef().getName() + "'";
    }

    public boolean wieldingAffectsItem(InvItem i) {
        if (!i.isWieldable() || !isWieldable()) {
            return false;
        }
        for (int affected : getWieldableDef().getAffectedTypes()) {
            if (i.getWieldableDef().getType() == affected) {
                return true;
            }
        }
        return false;
    }
}
