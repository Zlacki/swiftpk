package net.swiftpk.server.model;

import net.swiftpk.server.ent.EntityHandler;
import net.swiftpk.server.model.entity.Mob;
import net.swiftpk.server.model.entity.NPC;
import net.swiftpk.server.util.DataConversions;

public class ChatMessage {
    /**
     * The message it self, in byte format
     */
    private byte[] message;
    /**
     * Who the message is for
     */
    private Mob recipient = null;
    /**
     * Who sent the message
     */
    private Mob sender;

    public ChatMessage(Mob sender, byte[] message) {
        this.sender = sender;
        this.message = message;
    }

    public ChatMessage(Mob sender, String message, Mob recipient) {
        this.sender = sender;
        this.message = DataConversions.stringToByteArray(message);
        this.recipient = recipient;
    }

    public int getLength() {
        return message.length;
    }

    public byte[] getContent() {
        return message;
    }

    public Mob getRecipient() {
        return recipient;
    }

    public Mob getSender() {
        return sender;
    }

    @Override
    public String toString() {
        StringBuilder b = new StringBuilder();
        b.append("msg:((\n");

        b.append("\tsender:((");
        if (sender == null) {
            b.append("\t\trecipientType:`NULL`;  recipientName:`NULL`;");
        } else if (sender instanceof Player) {
            Player player = (Player) sender;
            b.append("\t\tsenderType:`Player`;\n");
            b.append("\t\tsenderName:`" + player.getCredentials().getUsername() + "`;\n");
        } else {
            b.append("\t\tsenderType:`NPC`;\n");
            b.append("\t\tsenderName:`" + EntityHandler.getNPCDef(((NPC) sender).getID()).getName() + "`;\n");
        }
        b.append("\t));\n");

        b.append("\trecipient:((\n");
        if (recipient == null) {
            b.append("\t\tNULL\n");
        } else if (recipient instanceof Player) {
            b.append("\t\trecipientType:`Player`;\n");
            b.append("\t\trecipientName:`" + ((Player) recipient).getCredentials().getUsername() + "`;\n");
        } else {
            b.append("\t\trecipientType:`NPC`;\n");
            b.append("\t\trecipientName:`" + EntityHandler.getNPCDef(((NPC) recipient).getID()).getName() + "`;\n");
        }
        b.append("\t));\n");

        b.append("\tlength:" + getLength() + ";\n");
        b.append("\tcontents:`" + getContent() + "`;\n");

        b.append("));");
        return b.toString();
    }
}