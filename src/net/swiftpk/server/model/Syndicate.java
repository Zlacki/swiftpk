package net.swiftpk.server.model;

import java.util.Map;
import java.util.WeakHashMap;
import java.util.concurrent.atomic.AtomicReference;

import net.swiftpk.server.model.component.SkillSet;
import net.swiftpk.server.model.entity.Mob;
import net.swiftpk.server.model.entity.NPC;
import net.swiftpk.server.net.ActionSender;

public class Syndicate {
    private Map<Player, Damage> damages = new WeakHashMap<Player, Damage>();

    public Player getPlayerDealtMostDamage() {
        final AtomicReference<Integer> highestDamage = new AtomicReference<Integer>(0);
        final AtomicReference<Player> highestDealer = new AtomicReference<Player>();
        damages.forEach((key, value) -> {
            if (value.getTotalDamage() > highestDamage.get().intValue()) {
                highestDealer.set(key);
                highestDamage.set(value.getTotalDamage());
            }
        });
        if (highestDamage.get().intValue() == 0)
            return null;
        return highestDealer.get();
    }

    public boolean hasEntries() {
        return !damages.isEmpty();
    }

    public void clear() {
        damages.clear();
    }

    public void addDamage(Player player, int damage, int damageType) {
        Damage prev = this.damages.get(player);
        if (prev != null) {
            prev.addDamage(damage, damageType);
        } else {
            this.damages.put(player, new Damage(damage, damageType));
        }
    }

    private static final int combatExperience(Mob mob) {
        double exp = (mob.getComponent(SkillSet.class).getCombatLevel() * 2) + 20;
        return (int) (exp / 4D);
    }

    public void distributeExp(final NPC npc) {
        int exp = combatExperience(npc);
        damages.forEach((player, damage) -> {
            int total = damage.getTotalDamage();
            if (total > npc.getDef().hits)
                total = npc.getDef().hits;
            if (player != null) {
                int newXP = (exp * total) / (npc.getDef().hits > 0 ? npc.getDef().hits : 1);
                if (damage.getRangePortion() > 0) {
                    player.incExp(4, roundAndCast(newXP * 4 * damage.getRangePortion()));
                    ActionSender.sendStat(player, 4);
                }
                if (damage.getCombatPortion() > 0) {
                    switch (player.getFightMode()) {
                        case 0:
                            for (int x = 0; x < 3; x++) {
                                player.incExp(x, roundAndCast(newXP * damage.getCombatPortion()));
                                ActionSender.sendStat(player, x);
                            }
                            break;
                        case 1:
                            player.incExp(2, roundAndCast(newXP * 3 * damage.getCombatPortion()));
                            ActionSender.sendStat(player, 2);
                            break;
                        case 2:
                            player.incExp(0, roundAndCast(newXP * 3 * damage.getCombatPortion()));
                            ActionSender.sendStat(player, 0);
                            break;
                        case 3:
                            player.incExp(1, roundAndCast(newXP * 3 * damage.getCombatPortion()));
                            ActionSender.sendStat(player, 1);
                            break;
                    }
                    player.incExp(3, roundAndCast(newXP * damage.getCombatPortion()));
                    ActionSender.sendStat(player, 3);
                }
            }
        });
    }

    private static final int roundAndCast(double d) {
        return (int) Math.round(d);
    }
}
