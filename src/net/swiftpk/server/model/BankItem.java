package net.swiftpk.server.model;

import net.swiftpk.server.ent.EntityHandler;
import net.swiftpk.server.ent.defs.ItemDef;

public class BankItem {
    private int amount;
    private int id;

    public BankItem(int id) {
        this.id = id;
        setAmount(1);
    }

    public BankItem(int id, int amount) {
        this.id = id;
        setAmount(amount);
    }

    public BankItem(InvItem i) {
        this.id = i.getID();
        setAmount(i.getAmount());
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof BankItem) {
            BankItem item = (BankItem) o;
            return item.getId() == getId();
        }
        return false;
    }

    public int getAmount() {
        return amount;
    }

    public ItemDef getDef() {
        return EntityHandler.getItemDef(id);
    }

    public int getId() {
        return id;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    @Override
    public String toString() {
        return (getDef().isStackable() ? amount + "x" : "") + "'"
                + getDef().getName() + "'";
    }
}
