package net.swiftpk.server.model;

/**
 * All the things our player can be doing. Players can only do one at a time.
 */
public enum MobState {
    Idle, Busy, Banking, ViewingBank, Shopping, Trading, Dueling, LeftFighting, RightFighting, Ranging, Dead, Talking,
    Sleeping, Quitting, Mining, Smithing, Smelting, Fishing, Woodcutting, Firemaking, Cooking, Drinking, Eating,
    Burying;
}
