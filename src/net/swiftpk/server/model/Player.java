package net.swiftpk.server.model;

import java.net.InetSocketAddress;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;

import io.netty.channel.Channel;
import net.swiftpk.server.Server;
import net.swiftpk.server.ent.EntityHandler;
import net.swiftpk.server.model.component.*;
import net.swiftpk.server.model.component.SkillSet.Skill;
import net.swiftpk.server.model.entity.GameObject;
import net.swiftpk.server.model.entity.Mob;
import net.swiftpk.server.model.entity.NPC;
import net.swiftpk.server.model.entity.WalkToAction;
import net.swiftpk.server.model.entity.attr.AttributeMap;
import net.swiftpk.server.model.entity.attr.AttributeValue;
import net.swiftpk.server.model.terrain.Path;
import net.swiftpk.server.model.terrain.World;
import net.swiftpk.server.model.terrain.tiles.Area;
import net.swiftpk.server.model.terrain.tiles.Point;
import net.swiftpk.server.net.Packet;
import net.swiftpk.server.net.Client;
import net.swiftpk.server.plugin.PluginManager;
import net.swiftpk.server.plugin.listeners.actors.players.PlayerAttackableEvent;
import net.swiftpk.server.plugin.listeners.actors.players.PlayerDeathListener;
import net.swiftpk.server.plugin.listeners.actors.players.PlayerDeathPointListener;
import net.swiftpk.server.plugin.listeners.actors.players.PlayerTeleportBlockListener;
import net.swiftpk.server.sync.block.SynchronizationBlock;
import net.swiftpk.server.tickable.Tickable;
import net.swiftpk.server.util.DataConversions;
import net.swiftpk.server.util.Formulae;
import net.swiftpk.server.util.RangedUtils;

import static net.swiftpk.server.net.ActionSender.*;
import static net.swiftpk.server.util.Formulae.timeSince;

/**
 * A single player.
 *
 * @author Zachariah Knight
 */
public final class Player extends Mob {

    private AttributeMap attributes = new AttributeMap();

    /**
     * This Map contains entries for every Player that we have attacked.
     * <p>
     * The key is assigned to the attacked Player's base37-encoded username The
     * value is the UNIX timestamp of when we had attacked them.
     */
    private Map<Long, Long> attacked = new HashMap<Long, Long>();

    /**
     * This Map contains entries for every Player that has attacked us.
     * <p>
     * The key is assigned to the attacking Player's base37-encoded username The
     * value is the UNIX timestamp of when they had attacked us.
     */
    private Map<Long, Long> attackedBy = new HashMap<Long, Long>();

    /**
     * The duel state for this player. Contains information regarding duels.
     * <p>
     * TODO: Attributes?
     */
    private DuelState duel = new DuelState(this);

    /**
     * The Channel for the current connection to the game client.
     */
    private Client client;

    /**
     * Last IP address the Player had logged in with before this session.
     * <p>
     * TODO: Attribute
     */
    private String lastIP = "0.0.0.0";

    /**
     * Current IP address we are connected with.
     * <p>
     * TODO: Attribute
     */
    private String currentIP;

    /**
     * Whether or not we are logged in.
     * <p>
     * TODO: Attribute, maybe a LoginState enum type thing?
     */
    private boolean loggedIn = false;

    /**
     * Current QueryMenu the player is accessing
     * <p>
     * TODO: Rewrite the entire QueryMenu system.
     */
    private QueryMenu menu;

    /**
     * The login response code.
     * <p>
     * TODO: Redo login system.
     */
    private byte responseCode;

    /**
     * Session keys used to seed the ISAAC PRNG
     * <p>
     * TODO: Attribute
     */
    private int[] sessionKeys = new int[4];

    /**
     * The Shop we are currently accessing
     * <p>
     * TODO: Handle this differently.
     */
    private Shop shop;

    /**
     * The TradeState for this player. Handles everything to do with trading.
     */
    private TradeState trade = new TradeState(this);

    /**
     * List of local Players.
     * <p>
     * Players in this list are already known by our Client.
     */
    private final List<Player> localPlayers = new ArrayList<>();

    /**
     * List of local NPCs.
     * <p>
     * NPCs in this list are already known by our Client.
     */
    private final List<NPC> localNPCs = new ArrayList<>();

    /**
     * List of local GameObjects.
     * <p>
     * GameObjects in this list are already known by our Client.
     */
    private final List<GameObject> localObjects = new ArrayList<>();

    /**
     * List of local lootable Items.
     * <p>
     * Lootable Items in this list are already known by our Client.
     */
    private final List<Item> localItems = new ArrayList<>();

    /**
     * Items the Player currently has equipped.
     */
    private int[] wornItems = new int[12];

    /**
     * Variables and utilities for handling Prayer draining
     */
    public final PrayerDrain prayerTick = new PrayerDrain(this);

    /**
     * The current amount of appearance tickets.
     */
    private static final AtomicInteger appearanceTicketCounter = new AtomicInteger(0);

    /**
     * The appearance tickets for this Player.
     */
    private final int[] appearanceTickets = new int[2000];

    /**
     * Array of activated prayers.
     */
    private boolean[] activatedPrayers = new boolean[14];

    /**
     * Whether or not there are too many local mobs to accomodate in
     * our view area.
     */
    private boolean excessiveLocalMobs = false;

    public boolean shouldDecreaseViewArea() {
        return excessiveLocalMobs;
    }

    /**
     * Sets flag to indicate this Player is around a LOT of Mobs.
     */
    public void flagExcessiveLocalMobs() {
        excessiveLocalMobs = true;
    }

    /**
     * Sets flag to indicate this Player is no longer around a lot of Mobs.
     */
    public void resetExcessiveLocalMobs() {
        excessiveLocalMobs = false;
    }

    public List<Player> getLocalPlayerList() {
        return localPlayers;
    }

    public List<NPC> getLocalNPCList() {
        return localNPCs;
    }

    public List<GameObject> getLocalObjectList() {
        return localObjects;
    }

    public List<Item> getLocalItemList() {
        return localItems;
    }

    public void send(Packet packet) {
        Channel ioSession = client.getChannel();
        if (ioSession.isActive() && ioSession.isOpen()) {
            ioSession.writeAndFlush(packet);
        }
    }

    /**
     * Generates the next appearance ticket.
     *
     * @return The next available appearance ticket.
     */
    private static int nextAppearanceTicket() {
        if (appearanceTicketCounter.incrementAndGet() == 0) {
            appearanceTicketCounter.set(1);
        }
        return appearanceTicketCounter.get();
    }

    /**
     * This Players appearance ticket.
     */
    private int appearanceTicket = nextAppearanceTicket();

    /**
     * Gets this Players appearance ticket.
     *
     * @return This Players appearance ticket.
     */
    public int getAppearanceTicket() {
        return appearanceTicket;
    }

    /**
     * Gets all of this Players appearance tickets.
     *
     * @return All of this Players appearance tickets.
     */
    public int[] getAppearanceTickets() {
        return appearanceTickets;
    }

    public Player(Client client, Map<Class<? extends Component>, Component> components) {
        super(components);
        this.client = client;
        this.currentIP = ((InetSocketAddress) client.getChannel().remoteAddress()).getAddress().getHostAddress();
        setLastMoved();
        ping();
    }

    public Client getClient() {
        return client;
    }

    public int getFatigue() {
        AttributeValue<Integer> attr = attributes.get("fatigue");
        return attr.get();
    }

    public void setFatigue(int fatigue) {
        AttributeValue<Integer> attr = attributes.get("fatigue");
        if (attr.get() != fatigue)
            attr.set(fatigue);
    }

    public int getSleepFatigue() {
        AttributeValue<Integer> attr = attributes.get("sleep_fatigue");
        return attr.get();
    }

    public void setSleepFatigue(int sleepFatigue) {
        AttributeValue<Integer> attr = attributes.get("sleep_fatigue");
        if (attr.get() != sleepFatigue)
            attr.set(sleepFatigue);
    }

    public String getSleepWord() {
        AttributeValue<String> attr = attributes.get("sleep_word");
        return attr.get();
    }

    public void setSleepWord(String sleepWord) {
        AttributeValue<String> attr = attributes.get("sleep_word");
        if (!attr.get().equals(sleepWord))
            attr.set(sleepWord);
    }

    public void addPrayerDrain(int prayerID) {
        prayerTick.addPrayer(prayerID);
    }

    public void alertOfLogin(Player p) {
        sendOnlineCount(this);
        if (getComponent(Communication.class).isFriendsWith(p.getCredentials().getEncodedUsername())
                && (!getComponent(Settings.class).getPrivacySetting(1)
                || p.getComponent(Communication.class).isFriendsWith(getCredentials().getEncodedUsername()))) {
            sendFriendUpdate(this, p.getCredentials().getEncodedUsername(), true);
        }
    }

    public void alertOfLogout(Player p) {
        sendOnlineCount(this);
        if (getComponent(Communication.class).isFriendsWith(p.getCredentials().getEncodedUsername())) {
            sendFriendUpdate(this, p.getCredentials().getEncodedUsername(), false);
        }
    }

    private void attackedByPlayer(Player p) {
        attackedBy.put(p.getCredentials().getEncodedUsername(), System.currentTimeMillis());
    }

    public AttributeMap getAttributes() {
        return attributes;
    }

    public void attackedPlayer(Player p) {
        AttributeValue<Long> attr = attributes.get("last_attacked");
        attr.set(System.currentTimeMillis());
        attr = p.getAttributes().get("last_attacked");
        attr.set(System.currentTimeMillis());
        p.attackedByPlayer(this);
        if (attacked.containsKey(p.getCredentials().getEncodedUsername())) { // if we've attacked them before
            if (attackedBy.containsKey(p.getCredentials().getEncodedUsername())) { // if they've attacked us before
                long lastAttacked = System.currentTimeMillis() - attackedBy.get(p.getCredentials().getEncodedUsername());
                if (lastAttacked > 20 * 60 * 1000) { // they attacked us more than 20 mins ago
                    setSkulled(true);
                    p.attackedByPlayer(this);
                }
            } else { // they've never attacked us
                setSkulled(true);
                p.attackedByPlayer(this);
            }
        } else { // we've never attacked them
            if (attackedBy.containsKey(p.getCredentials().getEncodedUsername())) { // if they've attacked us before
                long lastAttacked = System.currentTimeMillis() - attackedBy.get(p.getCredentials().getEncodedUsername());
                if (lastAttacked > 20 * 60 * 1000) { // they attacked us more than 20 mins ago
                    setSkulled(true);
                    p.attackedByPlayer(this);
                }
            } else { // first time they've attacked us
                setSkulled(true);
                p.attackedByPlayer(this);
            }
        }
    }

    public boolean canCast() {
        AttributeValue<Long> attr = attributes.get("last_spell_cast");
        return (System.currentTimeMillis() - attr.get()) >= Server.getServer().getConf().getInt("spellDelay");
    }

    public int getSpellWait() {
        AttributeValue<Long> attr = attributes.get("last_spell_cast");
        return DataConversions.roundUp(
                (Server.getServer().getConf().getInt("spellDelay") - (System.currentTimeMillis() - attr.get()))
                        / 1000D);
    }

    public boolean canLogout() {
        return !isFighting();
    }

    public boolean canMage() {
        return !duel.getRule(1);
    }

    public boolean canPray() {
        return !duel.getRule(2);
    }

    public boolean canRange() {
        int rangeDelay = (getBowStats() != null ? (int) getBowStats()[0] : 1500);
        return System.currentTimeMillis() - getRangeTimer() > rangeDelay;
    }

    public boolean canReport() {
        AttributeValue<Long> attr = attributes.get("last_report");
        return System.currentTimeMillis() - attr.get() > 120000;
    }

    public boolean canRun() {
        return !duel.getRule(0);
    }

    public boolean canWield() {
        return !duel.getRule(3);
    }

    public boolean checkAttack(Mob e) {
        if (e instanceof Player) {
            Player victim = (Player) e;
            if (isAdmin())
                return true;
            PlayerAttackableEvent event = PluginManager.get(PlayerAttackableEvent.class, this, victim);
            if (event != null)
                return false;
            int myWildLvl = getLocation().wildernessLevel();
            int victimWildLvl = victim.getLocation().wildernessLevel();
            if (myWildLvl < 1 || victimWildLvl < 1) {
                sendMessage(this, "You cannot attack other players outside of the wilderness!");
                return false;
            }
            int combDiff = Math.abs(getSkillSet().getCombatLevel() - victim.getSkillSet().getCombatLevel());
            if (combDiff > myWildLvl) {
                sendMessage(this, "You must move to at least level " + combDiff + " wilderness to attack "
                        + victim.getCredentials().getUsername() + "!");
                return false;
            }
            if (combDiff > victimWildLvl) {
                sendMessage(this, victim.getCredentials().getUsername() + " is not in high enough wilderness for you to attack!");
                return false;
            }
        }
        return true;
    }

    public void closedShop() {
        setState(MobState.Idle);
        shop = null;
    }

    public Appearance getAppearance() {
        return getComponent(Appearance.class);
    }

    @Override
    public int getArmourPoints() {
        return getInventory().getItems().stream().filter(InvItem::isWielded).mapToInt(item -> item.getWieldableDef().getArmourPoints()).sum() + 1;
    }

    public Bank getBank() {
        return getComponent(Bank.class);
    }

    public double[] getBowStats() {
        Inventory i = getInventory();
        for (InvItem it : i.getItems()) {
            if (!it.isWielded())
                continue;
            for (int i1 : RangedUtils.longBowIds)
                if (it.getID() == i1)
                    return RangedUtils.longBowStats;
            for (int i1 : RangedUtils.shortBowIds)
                if (it.getID() == i1)
                    return RangedUtils.shortBowStats;
            for (int i1 : RangedUtils.xbowIds)
                if (it.getID() == i1)
                    return RangedUtils.xbowStats;
        }
        return null;
    }

    public int getBowType() {
        Inventory i = getInventory();
        for (InvItem it : i.getItems()) {
            if (!it.isWielded())
                continue;
            for (int i1 : RangedUtils.longBowIds)
                if (it.getID() == i1)
                    return 0;
            for (int i1 : RangedUtils.shortBowIds)
                if (it.getID() == i1)
                    return 1;
            for (int i1 : RangedUtils.xbowIds)
                if (it.getID() == i1)
                    return 2;
        }
        return -1;
    }

    public Area getCurrentArea() {
        return area.get();
    }

    public String getCurrentIP() {
        return currentIP;
    }

    public long getCurrentLogin() {
        AttributeValue<Long> attr = attributes.get("last_spell_cast");
        return attr.get();
    }

    public int getDaysSinceLastLogin() {
        return (int) ((Calendar.getInstance().getTimeInMillis() / 1000 - getLastLogin()) / 86400);
    }

    public DuelState getDuel() {
        return duel;
    }

    @Override
    public int getFightMode() {
        AttributeValue<Integer> attr = attributes.get("fight_mode");
        return attr.get();
    }

    public int getRank() {
        AttributeValue<Integer> attr = attributes.get("privilege_group");
        return attr.get();
    }

    public Inventory getInventory() {
        return getComponent(Inventory.class);
    }

    public String getLastIP() {
        return lastIP;
    }

    public long getLastKilled() {
        AttributeValue<Long> attr = attributes.get("last_died");
        return attr.get();
    }

    public long getLastLogin() {
        AttributeValue<Long> attr = attributes.get("last_loggedin");
        return attr.get();
    }

    public long getLastMoved() {
        AttributeValue<Long> attr = attributes.get("last_moved");
        return attr.get();
    }

    public long getLastPing() {
        AttributeValue<Long> attr = attributes.get("last_ping");
        return attr.get();
    }

    public long getLastSkulled() {
        AttributeValue<Long> attr = attributes.get("last_skulled");
        return attr.get();
    }

    @Override
    public int getMagicPoints() {
        return getInventory().getItems().stream().filter(InvItem::isWielded).mapToInt(item -> item.getWieldableDef().getMagicPoints()).sum() + 1;
    }

    @Override
    public double[] getModifiers() {
        double[] result = {1, 1, 1};
        double[][] prayerModifier = {{1, 1, 1.05, 1, 1, 1.1, 1, 1, 1, 1, 1, 1.15, 1, 1},
                {1.05, 1, 1, 1.1, 1, 1, 1, 1, 1, 1.15, 1, 1, 1, 1},
                {1, 1.05, 1, 1, 1.1, 1, 1, 1, 1, 1, 1.15, 1, 1, 1}};
        for (int i = 0; i < 3; i++) {
            for (int i1 = 0; i1 < activatedPrayers.length; i1++)
                if (activatedPrayers[i1] && prayerModifier[i][i1] > result[i])
                    result[i] = prayerModifier[i][i1];
        }
        return result;
    }

    public int getPrayerPoints() {
        return getInventory().getItems().stream().filter(InvItem::isWielded).mapToInt(item -> item.getWieldableDef().getPrayerPoints()).sum() + 1;
    }

    public QueryMenu getQueryMenu() {
        return menu;
    }

    @Override
    public int getRangePoints() {
        return getInventory().getItems().stream().filter(InvItem::isWielded).mapToInt(item -> item.getWieldableDef().getRangePoints()).sum() + 1;
    }

    public byte getResponseCode() {
        return responseCode;
    }

    public Channel getChannel() {
        return client.getChannel();
    }

    public Shop getShop() {
        return shop;
    }

    public String getTimeSinceLogin() {
        AttributeValue<Long> attr = attributes.get("time_loggedin");
        return timeSince(attr.get());
    }

    public TradeState getTrade() {
        return trade;
    }

    public String getUsername() {
        return getCredentials().getUsername();
    }

    public long getUsernameHash() {
        return getCredentials().getEncodedUsername();
    }

    @Override
    public int getWeaponAimPoints() {
        return getInventory().getItems().stream().filter(InvItem::isWielded).mapToInt(item -> item.getWieldableDef().getWeaponAimPoints()).sum() + 1;
    }

    @Override
    public int getWeaponPowerPoints() {
        return getInventory().getItems().stream().filter(InvItem::isWielded).mapToInt(item -> item.getWieldableDef().getWeaponPowerPoints()).sum() + 1;
    }

    public int[] getWornItems() {
        return wornItems;
    }

    public void incExp(int index, long exp) {
        int fatigue = getFatigue() + (int) (16 * exp);
        if (fatigue > 75000) fatigue = 75000;
        setFatigue(fatigue);
        sendFatigue(this);
        if (fatigue == 75000) {
            sendMessage(this, "@gre@You are too tired to gain experience, get some rest!");
            return;
        }

        if (fatigue >= 72000)
            sendMessage(this, "@gre@You start to feel tired, maybe you should rest soon.");

        exp *= 100;
        Skill skill = getSkillSet().get(index);
        skill.incExperience(exp);
        int level = Formulae.experienceToLevel(skill.getExperience());
        if (level != skill.getMaxLevel()) {
            int advanced = level - skill.getMaxLevel();
            skill.incLevel(advanced);
            skill.incMaxLevel(advanced);
            sendStat(this, index);
            sendMessage(this, "@gre@You just advanced " + advanced + " " + skill.getName() + " level!");
            sendSound(this, "advance");
            int comb = Formulae.getCombatLevel(this);
            if (comb != getSkillSet().getCombatLevel()) {
                getSkillSet().setCombatLevel(comb);
                updateAppearanceTicket();
            }
        }
    }

    public void setItemBubble(Bubble b) {
        blockSet.add(SynchronizationBlock.createItemBubbleBlock(index, b.getItemID()));
    }

    public void setProjectile(Projectile p) {
        blockSet.add(SynchronizationBlock.createProjectileBlock(index, p.getVictim().getIndex(), p.getType(), p.getVictim() instanceof NPC));
    }

    public boolean isAdmin() {
        return getRank() == 2;
    }

    @Override
    public boolean isAttackable() {
        return timeSinceAttacked() >= 2200;
    }

    public boolean isAttackableByNPCs() {
        return timeSinceAttacked() >= 3200;
    }

    public boolean isChangingAppearance() {
        AttributeValue<Boolean> attr = attributes.get("changing_appearance");
        return attr.get();
    }

    public boolean isCharged() {
        AttributeValue<Long> attr = attributes.get("last_charge");
        return (System.currentTimeMillis() - attr.get()) < 6 * 60 * 1000;
    }

    public boolean isDueling() {
        return duel.isDueling();
    }

    public boolean isFighting(Player p) {
        return isFighting() && p.isFighting() && (getCombatPartner() != null && getCombatPartner().equals(p));
    }

    public boolean isMale() {
        AttributeValue<Boolean> attr = attributes.get("male");
        return attr.get();
    }

    public boolean isMod() {
        return getRank() == 1 || isAdmin();
    }

    public boolean isNoclip() {
        AttributeValue<Boolean> attr = attributes.get("blink");
        return attr.get();
    }

    public boolean isPrayerActivated(int pID) {
        return activatedPrayers[pID];
    }

    public boolean isSkulled() {
        return (System.currentTimeMillis() - getLastSkulled()) < 20 * 60 * 1000;
    }

    public boolean isWarnedToMove() {
        AttributeValue<Boolean> attr = attributes.get("warned_to_move");
        return attr.get();
    }

    public boolean isWielded(int itemId) {
        for (InvItem item : getInventory().getItems()) {
            if (item.isWielded() && item.getID() == itemId) {
                return true;
            }
        }
        return false;
    }

    @Override
    public void killedBy(Mob mob) {
        Mob opponent = super.getCombatPartner();
        if (opponent != null) {
            opponent.resetCombat();
        }
        sendSound(this, "death");
        sendDied(this);
        if (mob.isPlayer()) {
            Player player = (Player) mob;
            if (isDueling()) {
                for (InvItem item : getDuel().getOffer()) {
                    InvItem affectedItem = getInventory().getById(item.getID());
                    if (affectedItem == null) {
                        continue;
                    }
                    if (affectedItem.isWielded()) {
                        affectedItem.setWield(false);
                        updateWornItems(affectedItem.getWieldableDef().getWieldPos(),
                                getAppearance().getSprite(affectedItem.getWieldableDef().getWieldPos()));
                    }
                    getInventory().remove(item);
                    new Item(item.getID(), getX(), getY(), item.getAmount(), true, player);
                }
                getDuel().getWishTo().getDuel().reset();
                getDuel().reset();
            } else
                getInventory().dropOnDeath(mob, isSkulled(), activatedPrayers[8]);
            World.getInstance().announceKill(getCredentials().getEncodedUsername(), player.getCredentials().getEncodedUsername(), 2);
            sendMessage(player, "You have defeated " + getCredentials().getUsername() + "!");
            sendSound(player, "victory");
            player.setNotFighting(false);
            PlayerDeathListener listener = PluginManager.get(PlayerDeathListener.class, player, this);
            if (listener != null)
                listener.execute(player, this, isDueling());
            new Item(20, getX(), getY(), 1, true, player);
        } else {
            NPC n = (NPC) mob;
            n.setState(MobState.Idle);
            n.setNotFighting(false);
            getInventory().dropOnDeath(mob, isSkulled(), activatedPrayers[8]);
            new Item(20, getX(), getY(), 1, true, null);
        }

        setSkulled(false);
        attributes.get("last_skulled").set(0L);
        sendEquipmentStats(this);

        for(Skill skill : getSkillSet())
            skill.setLevel(skill.getMaxLevel());
        sendStats(this);

        for (int i = 0; i < 14; i++) {
            if (isPrayerActivated(i)) {
                setPrayer(i, false);
                removePrayerDrain(i);
            }
        }
        sendPrayers(this);
        sendInventory(this);

        setNotFighting(false);
        Point deathPoint = onDeathLocation();
        setLocation(deathPoint != null ? deathPoint : Point.location(216, 452, 0), true);
        sendHeight(this);

        resetAll();
    }

    public boolean loggedIn() {
        return loggedIn;
    }

    private Point onDeathLocation() {
        PlayerDeathPointListener listener = PluginManager.get(PlayerDeathPointListener.class, this);
        if (listener != null)
            return listener.execute(this);
        return null;
    }

    public void ping() {
        attributes.get("last_ping").set(System.currentTimeMillis());
    }

    public void removePrayerDrain(int prayerID) {
        prayerTick.removePrayer(prayerID);
    }

    public void resetAll() {
        if (isFollowing()) {
            resetFollowing();
        }
        resetCombat();
        setWalkToAction(null);
        resetPath();
    }

    public void save() {
        Server.getServer().getLogoutService().submit(this);
    }

    public void setCastTimer() {
        attributes.get("last_spell_cast").set(System.currentTimeMillis());
    }

    public void setChangingAppearance(boolean b) {
        AttributeValue<Boolean> attr = attributes.get("changing_appearance");
        if (attr.get() != b)
            attr.set(b);
    }

    public void setCharged() {
        attributes.get("last_charge").set(System.currentTimeMillis());
    }

    public void setCombatFinishedTimer() {
        attributes.get("last_combat_finished").set(System.currentTimeMillis());
    }

    public Credentials getCredentials() {
        return getComponent(Credentials.class);
    }

    public void setCredentials(String username, String password, boolean r) {
        getCredentials().initialize(username, password);
    }

    public void setFightMode(int i) {
        AttributeValue<Integer> attr = attributes.get("fight_mode");
        if (attr.get() != i)
            attr.set(i);
    }

    public void setRank(int b) {
        AttributeValue<Integer> attr = attributes.get("privilege_group");
        if (attr.get() != b)
            attr.set(b);
    }

    public void setLastIP(String ip) {
        lastIP = ip;
    }

    public void setLastKilled(long lastKilled) {
        AttributeValue<Long> attr = attributes.get("last_died");
        if (attr.get() != lastKilled)
            attr.set(lastKilled);
    }

    public void setLastLogin(long l) {
        AttributeValue<Long> attr = attributes.get("last_loggedin");
        if (attr.get() != l)
            attr.set(l);
    }

    public void setLastMoved() {
        attributes.get("last_moved").set(System.currentTimeMillis());
    }

    public void setLastReport(long l) {
        AttributeValue<Long> attr = attributes.get("last_report");
        if (attr.get() != l)
            attr.set(l);
    }

    public void setLastSkulled(long l) {
        AttributeValue<Long> attr = attributes.get("last_skulled");
        if (attr.get() != l)
            attr.set(l);
    }

    @Override
    public void setLocation(Point p) {
        this.setLocation(p, false);
    }

    public void setLoggedIn(boolean b) {
        if (b) {
            attributes.get("time_loggedin").set(System.currentTimeMillis());
        }
        loggedIn = b;
    }

    public void setMale(boolean male) {
        AttributeValue<Boolean> attr = attributes.get("male");
        if (attr.get() != male)
            attr.set(male);
    }

    @Override
    public void setNotFighting(boolean b) {
        super.setNotFighting(b);
        setCombatFinishedTimer();
    }

    public void setPrayer(int pID, boolean b) {
        activatedPrayers[pID] = b;
    }

    public void setQueryMenu(QueryMenu qm) {
        menu = qm;
    }

    public void unsetQueryMenu() {
        menu = null;
    }

    public void setResponseCode(byte b) {
        responseCode = b;
    }

    public void setServerKey(long key) {
        sessionKeys[2] = (int) (key >> 32);
        sessionKeys[3] = (int) key;
    }

    public boolean setSessionKeys(int[] keys) {
        boolean valid = (sessionKeys[2] == keys[2] && sessionKeys[3] == keys[3]);
        sessionKeys = keys;
        return valid;
    }

    public void setShop(Shop s) {
        setState(MobState.Shopping);
        shop = s;
        if (shop != null) {
            shop.addPlayer(this);
        }
    }

    public void setSkulled(boolean b) {
        updateAppearanceTicket();
        if (b)
            attributes.get("last_skulled").set(System.currentTimeMillis());
    }

    public void setWarnedToMove(boolean b) {
        AttributeValue<Boolean> attr = attributes.get("warned_to_move");
        if (attr.get() != b)
            attr.set(b);
    }

    public void setWornItems(int[] worn) {
        wornItems = worn;
        updateAppearanceTicket();
    }

    public void teleport(Point newPoint, boolean isSpecial) {
        if (PluginManager.get(PlayerTeleportBlockListener.class, this) != null)
            return;
        if (getLocation().wildernessLevel() >= 20 && !isSpecial) {
            sendMessage(this, "A magical force stops you from teleporting.");
            return;
        }
        if (isSpecial) {
            return;
        }
        for (Player p : getViewArea().getPlayersInView())
            sendTeleBubble(p, getX(), getY());
        sendTeleBubble(this, getX(), getY());
        setLocation(newPoint, true);
        resetPath();
        sendHeight(this);
        for (Player p : getViewArea().getPlayersInView())
            sendTeleBubble(p, getX(), getY());
    }

    public long timeSinceAttacked() {
        AttributeValue<Long> attr = attributes.get("last_combat_finished");
        return (System.currentTimeMillis() - attr.get());
    }

    public void toggleNoclip() {
        AttributeValue<Boolean> attr = attributes.get("blink");
        attr.set(!attr.get());
    }

    @Override
    public String toString() {
        return "['" + getCredentials().getUsername() + "'," + getState() + ",idx=" + index + ",sprite=" + getSprite() + "]";
    }

    @Override
    public boolean equals(Object o) {
        if(o instanceof Player) {
            return ((Player) o).getCredentials().getEncodedUsername() == getCredentials().getEncodedUsername();
        }
        return false;
    }

    public void unWieldItem(InvItem item) {
        item.setWield(false);
        updateWornItems(item.getWieldableDef().getWieldPos(),
                getAppearance().getSprite(item.getWieldableDef().getWieldPos()));
    }

    public void unWieldItems() {
        for (InvItem item : getInventory().getItems()) {
            if (!item.isWielded()) {
                continue;
            }
            String youNeed = "";
            for (Entry<?, ?> e : item.getWieldableDef().getStatsRequired()) {
                Skill skill = getSkillSet().get(((Integer) e.getKey()));
                if (skill.getMaxLevel() < ((Integer) e.getValue())) {
                    youNeed += (e.getValue()) + " " + skill.getName() + ", ";
                }
            }
            if (!youNeed.equals("")) {
                sendMessage(this, "You must have at least " + youNeed.substring(0, youNeed.length() - 2)
                        + " to wield a " + item.getDef().getName());
                unWieldItem(item);
            }
        }
        sendEquipmentStats(this);
    }

    public void updateAppearanceTicket() {
        appearanceTicket = nextAppearanceTicket();
        blockSet.add(SynchronizationBlock.createAppearanceBlock(this));
    }

    public void updateWornItems(int index, int id) {
        wornItems[index] = id;
        updateAppearanceTicket();
        sendInventory(this);
    }

    public void wieldItem(InvItem item) {
        StringBuilder youNeed = new StringBuilder();
        for (Entry<Integer, Integer> e : item.getWieldableDef().getStatsRequired()) {
            if (getSkillSet().get(e.getKey()).getMaxLevel() < e.getValue()) {
                youNeed.append(e.getValue()).append(" ").append(Formulae.statArray[e.getKey()]).append(", ");
            }
        }
        if (!youNeed.toString().equals("")) {
            sendMessage(this, "You must have at least " + youNeed.substring(0, youNeed.length() - 2) + " to use this item.");
            return;
        }
        if (EntityHandler.getItemWieldableDef(item.getID()).femaleOnly() && isMale()) {
            sendMessage(this, "Quit with the cross-dressing.");
            return;
        }
        List<InvItem> items = getInventory().getItems();
        for (InvItem i : items) {
            if (item.wieldingAffectsItem(i) && i.isWielded()) {
                unWieldItem(i);
            }
        }
        item.setWield(true);
        updateWornItems(item.getWieldableDef().getWieldPos(), item.getWieldableDef().getSprite());
    }
}
