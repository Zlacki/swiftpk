package net.swiftpk.server.model.component;

import java.util.*;
import java.util.concurrent.atomic.AtomicReferenceArray;
import java.util.stream.IntStream;

import net.swiftpk.server.ent.EntityHandler;
import net.swiftpk.server.ent.defs.NPCDef;
import net.swiftpk.server.model.component.SkillSet.Skill;
import net.swiftpk.server.util.Formulae;

public final class SkillSet extends AbstractComponent implements Set<Skill> {
    @Override
    public int size() {
        return SKILL_COUNT;
    }

    @Override
    public boolean isEmpty() {
        return false;
    }

    @Override
    public boolean contains(Object o) {
        if(o instanceof Skill) {
            Skill skill = (Skill) o;
            if(skill.getIndex() >= SKILL_COUNT || skill.getIndex() < 0)
                return false;
            return skills.get(skill.getIndex()).equals(skill);
        }
        return false;
    }

    @Override
    public Iterator<Skill> iterator() {
        return new Iterator<Skill>() {
            private int curIndex = 0;
            @Override
            public boolean hasNext() {
                return curIndex != skills.length();
            }

            @Override
            public Skill next() {
                return skills.get(curIndex++);
            }
        };
    }

    @Override
    public Object[] toArray() {
        return IntStream.range(0, SKILL_COUNT).mapToObj(i -> skills.get(i)).toArray(Skill[]::new);
    }

    @Override
    public <T> T[] toArray(T[] a) {
        if(a instanceof Skill[])
            return (T[]) IntStream.range(0, SKILL_COUNT).mapToObj(i -> skills.get(i)).toArray(Skill[]::new);
        return null;
    }

    @Override
    public boolean add(Skill skill) {
        if(skill.getIndex() < SKILL_COUNT && skill.getIndex() >= 0) {
            skills.set(skill.getIndex(), skill);
            return true;
        }
        return false;
    }

    @Override
    public boolean remove(Object o) {
        if(o instanceof Skill) {
            Skill skill = (Skill) o;
            if(skill.getIndex() >= SKILL_COUNT || skill.getIndex() < 0)
                return false;
            if(skills.get(skill.getIndex()).equals(skill)) {
                skills.set(skill.getIndex(), new Skill(skill.getIndex(), 0, 0, 1L));
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean containsAll(Collection<?> collection) {
        for (Object o : collection) {
            if (o instanceof Skill) {
                Skill skill = (Skill) o;
                int index = skill.getIndex();
                if(index < 0 || index >= SKILL_COUNT)
                    return false;
                if (!skills.get(index).equals(skill))
                    return false;
            } else
                return false;
        }
        return true;
    }

    @Override
    public boolean addAll(Collection<? extends Skill> collection) {
        for(Skill skill : collection) {
            int index = skill.getIndex();
            if(index < 0 || index >= SKILL_COUNT)
                return false;
            skills.set(index, skill);
        }
        return true;
    }

    @Override
    public boolean removeAll(Collection<?> collection) {
        boolean changed = false;
        for(Object o : collection) {
            if(o instanceof Skill) {
                Skill skill = (Skill) o;
                int index = skill.getIndex();
                if(index < 0 || index >= SKILL_COUNT)
                    return false;
                if(skills.get(index).equals(skill)) {
                    skills.set(index, new Skill(index, 0, 0, 1L));
                    changed = true;
                }
            } else
                return false;
        }
        return changed;
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        boolean changed = false;
        for(Object o : c) {
            if(o instanceof Skill) {
                Skill skill = (Skill) o;
                int index = skill.getIndex();
                if(index < 0 || index >= SKILL_COUNT)
                    return false;
                if(!skills.get(index).equals(skill)) {
                    skills.set(index, new Skill(index, 0, 0, 1L));
                    changed = true;
                }
            } else
                return false;
        }
        return changed;
    }

    @Override
    public void clear() {
        AtomicReferenceArray<Skill> tmpSkills = new AtomicReferenceArray<>(SKILL_COUNT);
        for(int i = 0; i < SKILL_COUNT; i++)
            tmpSkills.set(i, new Skill(i, 0, 0, 1L));
        skills = tmpSkills;
    }

    public class Skill {
        private int index;
        private int curLevel;
        private int maxLevel;
        private long experience;

        Skill(int index, int curLevel, int maxLevel, long experience) {
            this.index = index;
            this.curLevel = curLevel;
            this.maxLevel = maxLevel;
            this.experience = experience;
        }

        public int getLevel() {
            return curLevel;
        }

        public int getMaxLevel() {
            return maxLevel;
        }

        public long getExperience() {
            return experience;
        }

        public void setLevel(int i) {
            this.curLevel = i;
        }

        public void setMaxLevel(int i) {
            this.maxLevel = i;
        }

        public void setExperience(long l) {
            this.experience = l;
        }

        public void incExperience(long l) {
            this.experience += l;
            if(this.experience < 0L)
                this.experience = 0L;
        }

        public void incLevel(int i) {
            this.curLevel += i;
        }

        public void incMaxLevel(int i) {
            this.maxLevel += i;
        }

        public void setIndex(int i) { this.index = i; }

        public int getIndex() { return index; }

        public String getName() {
            return SKILLS[index];
        }

        @Override
        public boolean equals(Object o) {
            if(o instanceof Skill) {
                Skill other = (Skill) o;
                return other.getIndex() == index && other.getLevel() == curLevel && other.getMaxLevel() == maxLevel && other.getExperience() == experience;
            }
            return false;
        }
    }

    public static final String[] SKILLS = {"attack", "defense", "strength", "hits", "ranged", "prayer", "magic",
            "cooking", "woodcut", "fletching", "fishing", "firemaking", "crafting", "smithing", "mining", "herblaw",
            "agility", "thieving"};

    public static final int ATTACK = 0;
    public static final int DEFENSE = 1;
    public static final int STRENGTH = 2;
    public static final int HITPOINTS = 3;
    public static final int RANGE = 4;
    public static final int PRAYER = 5;
    public static final int MAGIC = 6;
    public static final int COOKING = 7;
    public static final int WOODCUTTING = 8;
    public static final int FLETCHING = 9;
    public static final int FISHING = 10;
    public static final int FIREMAKING = 11;
    public static final int CRAFTING = 12;
    public static final int SMITHING = 13;
    public static final int MINING = 14;
    public static final int HERBLORE = 15;
    public static final int AGILITY = 16;
    public static final int THIEVING = 17;
    public static final int SKILL_COUNT = 18;
    private AtomicReferenceArray<Skill> skills = new AtomicReferenceArray<>(SKILL_COUNT);
    private int combatLevel = 3;

    public SkillSet() {
        for (int i = 0; i < SKILL_COUNT; i++) {
            skills.set(i, new Skill(i, 0, 0, 1L));
        }
    }

    public SkillSet(int npcId) {
        NPCDef def = Objects.requireNonNull(EntityHandler.getNPCDef(npcId));
        skills.set(ATTACK, new Skill(0, def.getAtt(), def.getAtt(), Formulae.levelToExperience(def.getAtt())));
        skills.set(DEFENSE, new Skill(1, def.getDef(), def.getDef(), Formulae.levelToExperience(def.getDef())));
        skills.set(STRENGTH, new Skill(2, def.getStr(), def.getStr(), Formulae.levelToExperience(def.getStr())));
        skills.set(HITPOINTS, new Skill(3, def.getHits(), def.getHits(), Formulae.levelToExperience(def.getHits())));
        for (int i = 4; i < SKILL_COUNT; i++)
            skills.set(i, new Skill(i, 0, 0, 1L));
    }

    public Skill get(int index) {
        if (index >= SKILL_COUNT || index < 0)
            return null;

        return skills.get(index);
    }

    public int getCombatLevel() {
        return combatLevel;
    }

    public void setCombatLevel(int combatLevel) {
        this.combatLevel = combatLevel;
    }

    public int getSkillTotal() {
        int total = 0;
        for (int i = 0; i < SKILL_COUNT; i++)
            total += get(i).getMaxLevel();
        return total;
    }
}
