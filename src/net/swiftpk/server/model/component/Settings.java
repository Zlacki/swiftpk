package net.swiftpk.server.model.component;

public final class Settings extends AbstractComponent {
    private boolean[] privacySettings = new boolean[4],
            gameSettings = new boolean[4];

    public Settings() {
    }

    public boolean getGameSetting(int i) {
        return gameSettings[i];
    }

    public boolean[] getGameSettings() {
        return gameSettings;
    }

    public boolean getPrivacySetting(int i) {
        return privacySettings[i];
    }

    public boolean[] getPrivacySettings() {
        return privacySettings;
    }

    public void setGameSetting(int i, boolean b) {
        gameSettings[i] = b;
    }

    public void setGameSettings(boolean[] b) {
        gameSettings = b;
    }

    public void setPrivacySetting(int i, boolean b) {
        privacySettings[i] = b;
    }

    public void setPrivacySettings(boolean[] b) {
        privacySettings = b;
    }
}
