package net.swiftpk.server.model.component;

public final class Appearance extends AbstractComponent {
    private int body;
    private byte hairColour;
    private int head;
    private byte skinColour;
    private byte topColour;
    private byte trouserColour;

    public Appearance() {
    }

    public void init(int hairColour, int topColour, int trouserColour,
                     int skinColour, int head, int body) {
        this.hairColour = (byte) hairColour;
        this.topColour = (byte) topColour;
        this.trouserColour = (byte) trouserColour;
        this.skinColour = (byte) skinColour;
        this.head = head;
        this.body = body;
    }

    public int getBody() {
        return body;
    }

    public byte getHairColour() {
        return hairColour;
    }

    public int getHead() {
        return head;
    }

    public byte getSkinColour() {
        return skinColour;
    }

    public int getSprite(int pos) {
        switch (pos) {
            case 0:
                return head;
            case 1:
                return body;
            case 2:
                return 3;
            default:
                return 0;
        }
    }

    public int[] getSprites() {
        return new int[]{head, body, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    }

    public byte getTopColour() {
        return topColour;
    }

    public byte getTrouserColour() {
        return trouserColour;
    }
}
