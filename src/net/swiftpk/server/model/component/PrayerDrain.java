package net.swiftpk.server.model.component;

import net.swiftpk.server.ent.EntityHandler;
import net.swiftpk.server.ent.defs.PrayerDef;
import net.swiftpk.server.model.Player;
import net.swiftpk.server.net.ActionSender;

public class PrayerDrain {
    private int drainRate = 0;
    private Player player;
    public long delay1 = Long.MAX_VALUE;
    private long delayTime = Long.MAX_VALUE;

    public PrayerDrain(Player player) {
        this.player = player;
    }

    public void setDelay(long delay) {
        delayTime = System.currentTimeMillis();
        delay1 = delay;
    }

    public boolean isActive() {
        return System.currentTimeMillis() - delayTime > delay1;
    }

    public void addPrayer(int prayerID) {
        drainRate = 0;
        PrayerDef prayer = EntityHandler.getPrayerDef(prayerID);
        for (int x = 0; x <= 13; x++) {
            prayer = EntityHandler.getPrayerDef(x);
            if (player.isPrayerActivated(x)) {
                drainRate += prayer.getDrainRate();
            }
        }
        if (drainRate > 0) {
            setDelay(((int) (180000.0 / drainRate * (1 + player
                    .getPrayerPoints() / 30.0))));
        } else if (drainRate <= 0) {
            drainRate = 0;
            setDelay((Integer.MAX_VALUE));
        }
    }

    public void execute() {
        int curPrayer = player.getSkillSet().get(SkillSet.PRAYER).getLevel();
        if (drainRate > 0 && curPrayer > 0) {
            player.getSkillSet().get(SkillSet.PRAYER).incLevel(-1);
            ActionSender.sendStat(player, 5);
            if (curPrayer <= 1) {
                for (int prayerID = 0; prayerID < 14; prayerID++) {
                    player.setPrayer(prayerID, false);
                }
                drainRate = 0;
                setDelay((Integer.MAX_VALUE));
                ActionSender.sendMessage(player, "You have run out of prayer points. Return to a church to recharge");
                ActionSender.sendPrayers(player);
            }
        }
        if (drainRate != 0) {
            setDelay((180000 / drainRate));
        }
    }

    public void removePrayer(int prayerID) {
        addPrayer(prayerID);
    }
}