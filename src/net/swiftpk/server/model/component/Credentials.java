package net.swiftpk.server.model.component;

import net.swiftpk.server.util.NameUtil;

public final class Credentials extends AbstractComponent {
    private String username;
    private String password;
    private long usernameHash;

    public Credentials() {
    }

    public void initialize(String username, String password) {
        this.usernameHash = NameUtil.encodeBase37(username);
        this.username = NameUtil.decodeBase37(usernameHash);
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public long getEncodedUsername() {
        return usernameHash;
    }
}
