package net.swiftpk.server.model.component;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

import net.swiftpk.server.model.InvItem;

public class Bank extends AbstractComponent {
    /**
     * The maximum size of a bank
     */
    public static final int MAX_BANK_ITEMS = 192;
    private List<InvItem> items = new ArrayList<InvItem>();

    public Bank() {
    }

    public int add(InvItem item) {
        if (item.getAmount() <= 0)
            return -1;
        synchronized (getItems()) {
            int index = getItems().indexOf(item);
            if (index >= 0) {
                InvItem existing = getItems().get(index);
                int newAmount = item.getAmount() + existing.getAmount();
                existing.setAmount(newAmount);
                return index;
            }
            getItems().add(item);
            return getItems().size() - 1;
        }
    }

    public void add(List<InvItem> items) {
        for (InvItem item : items) {
            if (item.getAmount() <= 0)
                continue;
            synchronized (getItems()) {
                int index = getItems().indexOf(item);
                if (index >= 0) {
                    InvItem existing = getItems().get(index);
                    int newAmount = item.getAmount() + existing.getAmount();
                    existing.setAmount(newAmount);
                    continue;
                }
                getItems().add(item);
            }
        }
    }

    public boolean canHold(ArrayList<InvItem> items) {
        synchronized (getItems()) {
            return (MAX_BANK_ITEMS - getItems().size()) >= getRequiredSlots(items);
        }
    }

    public boolean canHold(InvItem item) {
        synchronized (getItems()) {
            return (MAX_BANK_ITEMS - getItems().size()) >= getRequiredSlots(item);
        }
    }

    public boolean contains(InvItem i) {
        synchronized (getItems()) {
            return getItems().contains(i);
        }
    }

    public int countId(int id) {
        synchronized (getItems()) {
            for (InvItem i : getItems())
                if (i.getID() == id)
                    return i.getAmount();
        }
        return 0;
    }

    public boolean isFull() {
        synchronized (getItems()) {
            return getItems().size() >= MAX_BANK_ITEMS;
        }
    }

    public InvItem get(int index) {
        synchronized (getItems()) {
            if (index < 0 || index >= getItems().size())
                return null;
            return getItems().get(index);
        }
    }

    public InvItem get(InvItem item) {
        synchronized (getItems()) {
            for (InvItem i : getItems())
                if (item.equals(i))
                    return i;
        }
        return null;
    }

    public int getFirstIndexById(int id) {
        synchronized (getItems()) {
            for (int index = 0; index < getItems().size(); index++)
                if (getItems().get(index).getID() == id)
                    return index;
        }
        return -1;
    }

    public List<InvItem> getItems() {
        return items;
    }

    public int getRequiredSlots(InvItem item) {
        synchronized (getItems()) {
            return (getItems().contains(item) ? 0 : 1);
        }
    }

    public int getRequiredSlots(List<InvItem> items) {
        int requiredSlots = 0;
        for (InvItem item : items)
            requiredSlots += getRequiredSlots(item);
        return requiredSlots;
    }

    public boolean hasItemId(int id) {
        synchronized (getItems()) {
            for (InvItem i : getItems())
                if (i.getID() == id)
                    return true;
        }
        return false;
    }

    public ListIterator<InvItem> iterator() {
        synchronized (getItems()) {
            return getItems().listIterator();
        }
    }

    public void remove(int index) {
        InvItem item = get(index);
        if (item == null) {
            return;
        }
        remove(item.getID(), item.getAmount());
    }

    public int remove(int id, int amount) {
        synchronized (getItems()) {
            Iterator<InvItem> iterator = getItems().iterator();
            for (int index = 0; iterator.hasNext(); index++) {
                InvItem i = iterator.next();
                if (id == i.getID()) {
                    if (amount < i.getAmount()) {
                        i.setAmount(i.getAmount() - amount);
                    } else {
                        iterator.remove();
                    }
                    return index;
                }
            }
        }
        return -1;
    }

    public int remove(InvItem item) {
        return remove(item.getID(), item.getAmount());
    }

    public int size() {
        synchronized (getItems()) {
            return getItems().size();
        }
    }
}