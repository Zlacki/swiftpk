package net.swiftpk.server.model.component;

import java.util.List;
import java.util.Map;

public interface Component {
    List<Class<? extends Component>> getDependencies();

    void resolveDependencies(
            Map<Class<? extends Component>, Component> components);
}
