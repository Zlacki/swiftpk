package net.swiftpk.server.model.component;

import java.util.LinkedList;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

import net.swiftpk.server.model.ChatMessage;
import net.swiftpk.server.model.Player;
import net.swiftpk.server.model.entity.mob.NPCChatMessage;
import net.swiftpk.server.sync.block.SynchronizationBlock;
import net.swiftpk.server.util.Logger;

public final class Communication extends AbstractComponent {
    public static final int MAX_FRIENDS = 100;
    public static final int MAX_IGNORES = 50;
    /**
     * This should be fine as a standard linked list as the only time it is
     * modified is when a player explicitly adds another player to the list.
     * This means that only one thread (the thread handling the incoming packet
     * from the player) will modify this list at a time.
     */
    public Queue<Long> friends = new LinkedList<>();
    public Queue<Long> ignores = new LinkedList<>();

    public boolean addFriend(long usernameHash) {
        if (!getFriends().contains(usernameHash)
                && getFriends().size() < MAX_FRIENDS) {
            return getFriends().add(usernameHash);
        }
        return false;
    }

    public boolean addIgnore(long usernameHash) {
        if (!getIgnores().contains(usernameHash)
                && getIgnores().size() < MAX_IGNORES) {
            return getIgnores().add(usernameHash);
        }
        return false;
    }

    public Queue<Long> getFriendList() {
        return getFriends();
    }

    public Queue<Long> getFriends() {
        return friends;
    }

    public Queue<Long> getIgnoreList() {
        return getIgnores();
    }

    public Queue<Long> getIgnores() {
        return ignores;
    }

    public void pushMessage(ChatMessage message) {
        if (message.getSender() instanceof Player)
            message.getSender().getBlockSet().add(SynchronizationBlock.createChatBlock((Player) message.getSender(), message));
    }

    public boolean isFriendsWith(long usernameHash) {
        return getFriends().contains(usernameHash);
    }

    public boolean isIgnoring(long usernameHash) {
        return getIgnores().contains(usernameHash);
    }

    public void removeFriend(long usernameHash) {
        getFriends().remove(usernameHash);
    }

    public void removeIgnore(long usernameHash) {
        getIgnores().remove(usernameHash);
    }
}
