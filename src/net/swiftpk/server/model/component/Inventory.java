package net.swiftpk.server.model.component;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

import net.swiftpk.server.model.InvItem;
import net.swiftpk.server.model.Item;
import net.swiftpk.server.model.Player;
import net.swiftpk.server.model.entity.Mob;
import net.swiftpk.server.model.terrain.tiles.Point;
import net.swiftpk.server.net.ActionSender;

public final class Inventory extends AbstractComponent {
    private final int MAX_INVENTORY_ITEMS = 30;
    private final int maxItems;
    private final AtomicReference<Player> owner = new AtomicReference<Player>();
    private final List<InvItem> items = new ArrayList<>();

    public Inventory() {
        this.maxItems = MAX_INVENTORY_ITEMS;
    }

    public int add(InvItem item) {
        if (item.getAmount() < 1) {
            return -1;
        }
        if (item.getDef().isStackable()) {
            synchronized (getItems()) {
                int index = getItems().indexOf(item);
                if (index >= 0) {
                    InvItem existing = getItems().get(index);
                    int newAmount = item.getAmount() + existing.getAmount();
                    existing.setAmount(newAmount);
                    updateInventoryItem(index, existing);
                    return index;
                }
            }
        } else {
            if (item.getAmount() > 1) {
                item.setAmount(1);
            }
        }
        if (isFull()) {
            Player p = owner.get();
            Point loc = p.getLocation();
            ActionSender.sendMessage(owner.get(), "Your inventory is full, the " + item.getDef().getName() + " drops to the ground!");
            new Item(item.getID(), loc.getX(), loc.getY(), item.getAmount(), false, p);
            return -1;
        }
        synchronized (getItems()) {
            getItems().add(item);
            ActionSender.sendInventory(owner.get());
            return getItems().size() - 1;
        }
    }

    public void add(List<InvItem> items) {
        for (InvItem item : items) {
            if (item.getAmount() < 1) {
                break;
            }
            if (item.getDef().isStackable()) {
                synchronized (getItems()) {
                    int index = getItems().indexOf(item);
                    if (index >= 0) {
                        InvItem existing = getItems().get(index);
                        int newAmount = item.getAmount() + existing.getAmount();
                        existing.setAmount(newAmount);
                        updateInventoryItem(index, existing);
                    }
                }
            } else {
                if (item.getAmount() > 1) {
                    item.setAmount(1);
                }
            }
            if (isFull()) {
                Player p = owner.get();
                Point loc = p.getLocation();
                ActionSender.sendMessage(owner.get(), "Your inventory is full, the " + item.getDef().getName() + " drops to the ground!");
                new Item(item.getID(), loc.getX(), loc.getY(), item.getAmount(), false, p);
                break;
            }
            // At this stage we know that the inventory can hold the new item,
            // so let's add it
            synchronized (getItems()) {
                getItems().add(item);
                // owner.get().getActionSender().sendInventory1();
            }
        }
    }

    public final boolean canHold(InvItem item) {
        synchronized (getItems()) {
            return (maxItems - getItems().size()) >= getRequiredSlots(item);
        }
    }

    public final boolean contains(InvItem item) {
        synchronized (getItems()) {
            return getItems().contains(item);
        }
    }

    public final int countById(int id) {
        synchronized (getItems()) {
            int count = 0;
            for (InvItem item : getItems()) {
                if (item.getID() == id) {
                    count += item.getAmount();
                }
            }
            return count;
        }
    }

    public void dropOnDeath(Mob killer, boolean skulled, boolean protectItem) {
        int keep = skulled ? 0 : 3;
        // If they have protect item enabled, increase the number of items
        // to be protected by one
        if (protectItem) {
            keep++;
        }
        Player pkiller = null;
        if (killer instanceof Player) {
            pkiller = (Player) killer;
        }
        Point victimLoc = owner.get().getLocation();
        synchronized (items) {
            InvItem item = null;
            if (keep == 0) {
                // We aren't keeping any, so don't worry about sorting them
                Iterator<InvItem> it = items.iterator();
                while (it.hasNext()) {
                    item = it.next();
                    if (item.isWielded()) {
                        item.setWield(false);
                        owner.get().updateWornItems(item.getWieldableDef().getWieldPos(),
                                owner.get().getAppearance().getSprite(item.getWieldableDef().getWieldPos()));
                    }
                    new Item(item.getID(), victimLoc.getX(), victimLoc.getY(), item.getAmount(), false, pkiller);
                    it.remove();
                }
            } else {
                // Sort the list so that the most expensive are first
                Collections.sort(items);
                int saved = 0;
                Iterator<InvItem> it = items.iterator();
                while (it.hasNext()) {
                    item = it.next();
                    if (saved++ < keep)
                        continue;
                    if (item.isWielded()) {
                        item.setWield(false);
                        owner.get().updateWornItems(item.getWieldableDef().getWieldPos(),
                                owner.get().getAppearance().getSprite(item.getWieldableDef().getWieldPos()));
                    }
                    new Item(item.getID(), victimLoc.getX(), victimLoc.getY(), item.getAmount(), false, pkiller);
                    it.remove();
                }
            }
        }
    }

    public final InvItem get(int index) {
        synchronized (getItems()) {
            if (getItems().size() <= index)
                return null;
            return getItems().get(index);
        }
    }

    public final InvItem get(InvItem index) {
        synchronized (getItems()) {
            for (InvItem item : getItems()) {
                if (item.equals(index)) {
                    return item;
                }
            }
            return null;
        }
    }

    public final InvItem getById(int itemId) {
        synchronized (getItems()) {
            for (InvItem item : getItems()) {
                if (item.getID() == itemId) {
                    return item;
                }
            }
            return null;
        }
    }

    public int getFreedSlots(InvItem item) {
        synchronized (getItems()) {
            return item.getDef().isStackable() && countById(item.getID()) > item.getAmount() ? 0 : 1;
        }
    }

    public final int getFreedSlots(List<InvItem> items) {
        synchronized (items) {
            int count = 0;
            for (InvItem item : items) {
                count += getFreedSlots(item);
            }
            return count;
        }
    }

    public List<InvItem> getItems() {
        return items;
    }

    public int getRequiredSlots(InvItem item) {
        synchronized (getItems()) {
            return item.getDef().isStackable() && getItems().contains(item) ? 0 : 1;
        }
    }

    public final int getRequiredSlots(List<InvItem> items) {
        synchronized (items) {
            int requiredSlots = 0;
            for (InvItem item : items) {
                requiredSlots += getRequiredSlots(item);
            }
            return requiredSlots;
        }
    }

    public final int indexOf(int id) {
        synchronized (getItems()) {
            int count = 0;
            for (InvItem item : getItems()) {
                if (item.getID() == id) {
                    return count;
                }
                count++;
            }
            return -1;
        }
    }

    public final int indexOf(InvItem item) {
        synchronized (getItems()) {
            return getItems().indexOf(item);
        }
    }

    public final boolean isEmpty() {
        synchronized (getItems()) {
            return getItems().isEmpty();
        }
    }

    public final boolean isFull() {
        synchronized (getItems()) {
            return getItems().size() >= maxItems;
        }
    }

    public InvItem remove(int index) {
        synchronized (getItems()) {
            if (index >= getItems().size()) {
                return null;
            }
            InvItem removed = getItems().remove(index);
            ActionSender.sendRemoveItem(owner.get(), index);
            return removed;
        }
    }

    public int remove(int itemId, int amount) {
        InvItem target = null;
        int index = 0;
        synchronized (getItems()) {
            Iterator<InvItem> it = getItems().iterator();
            while (it.hasNext()) {
                target = it.next();
                if (target.getID() == itemId) {
                    if (target.getDef().isStackable() && amount < target.getAmount()) {
                        int newAmount = target.getAmount() - amount;
                        target.setAmount(newAmount);
                    } else {
                        if (target.isWielded()) {
                            target.setWield(false);
                            owner.get().updateWornItems(target.getWieldableDef().getWieldPos(),
                                    owner.get().getAppearance().getSprite(target.getWieldableDef().getWieldPos()));
                        }
                        it.remove();
                        if (countById(itemId) > 0 && (--amount) > 0)
                            continue;
                    }
                    return index;
                }
                index++;
            }
        }
        return -1;
    }

    public int remove(InvItem item) {
        return remove(item.getID(), item.getAmount());
    }

    public void setOwner(Player owner) {
        this.owner.getAndSet(owner);
    }

    public final int size() {
        synchronized (getItems()) {
            return getItems().size();
        }
    }

    public final void updateInventoryItem(int slot, InvItem item) {
        synchronized (items) {
            ActionSender.sendInventory(owner.get());
        }
    }
}
