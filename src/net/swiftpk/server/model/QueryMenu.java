package net.swiftpk.server.model;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import net.swiftpk.server.Server;
import net.swiftpk.server.net.ActionSender;
import net.swiftpk.server.plugin.listeners.actors.players.QueryMenuListener;

public class QueryMenu {
    private long init = System.currentTimeMillis();
    private Set<QueryMenuListener> listeners;
    private Player p;
    private String[] query;

    public QueryMenu(Player p, String[] q) {
        this.p = p;
        this.query = q;
        listeners = new HashSet<QueryMenuListener>();
    }

    public void addListener(QueryMenuListener qml) {
        listeners.add(qml);
    }

    public void choseQuery(Player p, int i) {
        if (isValid()) {
            Iterator<QueryMenuListener> it = listeners.iterator();
            while (it.hasNext())
                it.next().choseQuery(p, i);
        }
        if (p.getState() == MobState.Talking)
            p.setState(MobState.Idle);
    }

    public String[] getQueries() {
        return query;
    }

    /**
     * TODO: Get rid of this and make validity based on events MobState may also
     * help determine validity. EDIT: MobState will definitely be utilized for this.
     *
     * @return
     */
    private boolean isValid() {
        return System.currentTimeMillis() - init <= Server.getServer().getConf().getInt("menuTimeout");
    }

    public void removeListener(QueryMenuListener qml) {
        listeners.remove(qml);
    }

    public void send() {
        ActionSender.sendQueryMenu(p, this);
    }

    public byte size() {
        return (byte) query.length;
    }
}
