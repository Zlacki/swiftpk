package net.swiftpk.server.model;

import net.swiftpk.server.ent.locs.NPCLoc;
import net.swiftpk.server.model.entity.NPC;

public interface EntityFactory {
    NPC newNpc(int id);

    NPC newNpc(NPCLoc loc);
}
