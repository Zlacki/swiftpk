package net.swiftpk.server.model.entity;

import java.util.HashMap;
import java.util.Map;

import net.swiftpk.server.ent.locs.ItemLoc;
import net.swiftpk.server.ent.locs.NPCLoc;
import net.swiftpk.server.model.EntityFactory;
import net.swiftpk.server.model.Item;
import net.swiftpk.server.model.Player;
import net.swiftpk.server.model.component.*;
import net.swiftpk.server.model.component.SkillSet;
import net.swiftpk.server.net.Client;

public final class DefaultEntityFactory implements EntityFactory {
    public static DefaultEntityFactory getInstance() {
        return INSTANCE;
    }

    private static final DefaultEntityFactory INSTANCE = new DefaultEntityFactory();

    @Override
    public NPC newNpc(int id) {
        Map<Class<? extends Component>, Component> components = new HashMap<Class<? extends Component>, Component>(30,
                0.4f);
        components.put(SkillSet.class, new SkillSet(id));
        for (Component c : components.values()) {
            c.resolveDependencies(components);
        }
        NPC npc = new NPC(id, components);
        return npc;
    }

    @Override
    public NPC newNpc(NPCLoc loc) {
        Map<Class<? extends Component>, Component> components = new HashMap<Class<? extends Component>, Component>(30,
                0.4f);
        components.put(SkillSet.class, new SkillSet(loc.getId()));
        for (Component c : components.values()) {
            c.resolveDependencies(components);
        }
        NPC npc = new NPC(loc, components);
        return npc;
    }

    public Player newPlayer(Client session) {
        Map<Class<? extends Component>, Component> components = new HashMap<Class<? extends Component>, Component>(30,
                0.4f);
        components.put(SkillSet.class, new SkillSet());
        components.put(Communication.class, new Communication());
        components.put(Settings.class, new Settings());
        components.put(Appearance.class, new Appearance());
        components.put(Bank.class, new Bank());
        components.put(Credentials.class, new Credentials());
        Inventory inv = new Inventory();
        components.put(Inventory.class, inv);
        for (Component c : components.values())
            c.resolveDependencies(components);

        Player player = new Player(session, components);
        inv.setOwner(player);
        return player;
    }

    public Item newItem(ItemLoc loc) {
        return new Item(loc);
    }
}
