package net.swiftpk.server.model.entity;

import java.util.Comparator;

import net.swiftpk.server.model.InvItem;

public class ItemByValueComparator implements Comparator<InvItem> {
    @Override
    public int compare(InvItem one, InvItem two) {
        return one.getDef().getBasePrice() - two.getDef().getBasePrice();
    }

    @Override
    public boolean equals(Object o) {
        if (o == this) {
            return true;
        }
        return false;
    }
}