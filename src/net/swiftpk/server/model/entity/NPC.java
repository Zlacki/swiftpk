package net.swiftpk.server.model.entity;

import java.util.Map;
import java.util.Objects;

import net.swiftpk.server.ent.EntityHandler;
import net.swiftpk.server.ent.defs.ItemDef;
import net.swiftpk.server.ent.defs.NPCDef;
import net.swiftpk.server.ent.defs.extras.NPCDrop;
import net.swiftpk.server.ent.locs.NPCLoc;
import net.swiftpk.server.model.component.SkillSet.Skill;
import net.swiftpk.server.model.Item;
import net.swiftpk.server.model.MobState;
import net.swiftpk.server.model.Player;
import net.swiftpk.server.model.Syndicate;
import net.swiftpk.server.model.component.Component;
import net.swiftpk.server.model.entity.mob.NPCChatMessage;
import net.swiftpk.server.model.terrain.tiles.Point;
import net.swiftpk.server.sync.block.NPCChatBlock;
import net.swiftpk.server.util.Formulae;
import net.swiftpk.server.util.RandomUtil;

public class NPC extends Mob {
    private long moveTime = 0L; // when the npc wants to move
    private long killedTime = 0L; // when the npc was killed
    private long lastFight = 0L;
    private int respawnCount = 0; // number of times we've respawned
    private NPCLoc loc;
    private NPCDef npcDef;
    private Syndicate syndicate = new Syndicate();

    public Syndicate getSyndicate() {
        return syndicate;
    }

    public void setSyndicate(Syndicate syndicate) {
        this.syndicate = syndicate;
    }

    public NPC(int id, Map<Class<? extends Component>, Component> components) {
        super(components);
        this.loc = null;
        npcDef = EntityHandler.getNPCDef(id);
        super.id = id;
        setState(MobState.Dead);
        resetStats();
    }

    public NPC(NPCLoc loc, Map<Class<? extends Component>, Component> components) {
        super(components);
        this.loc = loc;
        npcDef = EntityHandler.getNPCDef(loc.getId());
        super.id = loc.getId();
        setState(MobState.Dead);
        resetStats();
    }

    public void setChatMessage(NPCChatMessage msg){
        blockSet.add(NPCChatBlock.createNPCChatBlock(msg));
    }

    public boolean canAttack() {
        return (System.currentTimeMillis() - lastFight) >= 3200;
    }

    @Override
    public boolean equals(Object o) {
        return o == this;
    }

    public NPCDef getDef() {
        return npcDef;
    }

    public NPCLoc getLoc() {
        return loc;
    }

    public long getMoveTime() {
        return moveTime;
    }

    public int getSpawnCount() {
        return respawnCount;
    }

    public long getTimeSinceKilled() {
        return (System.currentTimeMillis() - killedTime);
    }

    public void incSpawnCounter() {
        respawnCount++;
    }

    @Override
    public boolean isAttackable() {
        return npcDef.isAttackable();
    }

    public boolean isTalking() {
        return getState() == MobState.Talking;
    }

    @Override
    public void killedBy(Mob m) {
        if (m instanceof Player) {
            resetCombat();
            m.resetCombat();
            if (syndicate.hasEntries()) {
                Player dropPlayer = syndicate.getPlayerDealtMostDamage();
                NPCDrop[] drops = npcDef.getDrop();
                if (drops != null) {
                    int count = drops.length;
                    if (count > 0) {
                        double rand = Math.random();
                        for (NPCDrop drop : drops) {
                            double cumProb = drop.getCumulativeProbability();
                            cumProb *= 1.2D;
                            if (cumProb >= rand) {
                                int amount = 1;
                                ItemDef item = Objects.requireNonNull(EntityHandler.getItemDef(drop.getItemId()));
                                if (item.isStackable()) {
                                    amount = RandomUtil.inclusive(drop.getMinAmount(), drop.getMaxAmount());
                                }
                                new Item(item.getId(), getX(), getY(), amount, true, dropPlayer);
                                break;
                            }
                        }
                    }
                }
                new Item(20, getX(), getY(), 1, true, dropPlayer);
                syndicate.clear();
            } else
                new Item(20, getX(), getY(), 1, true, null);
        }
        setNotFighting(true);
        setState(MobState.Dead);
        killedTime = System.currentTimeMillis();
        resetStats();
        setLocation(Point.DEATH_SPOT, true);
    }

    private void resetStats() {
        for(Skill skill : getSkillSet())
            skill.setLevel(skill.getMaxLevel());
        getSkillSet().setCombatLevel(Formulae.getCombatLevel(this));
    }

    public void setChasing(Mob target, int distance, boolean attack) {
        setFollowing(target, distance);
        if(attack) {
            setWalkToAction(new WalkToEntityAction(this, target, 1) {
                @Override
                public void execute() {
                    NPC.this.startCombat(target);
                }
            });
        }
/*        final NPC npc = this;
        setMovementListener(new MovementListener(player, 1) {
            @Override
            public void arrived() {
                if (attack) {
                    if (player instanceof Player) {
                        Player p = (Player) player;
                        npc.attack(p);
                    } else if (player instanceof NPC) {
                        NPC n = (NPC) player;
                        npc.attack(n);
                    }
                }
                npc.resetPath();
                if (!npc.withinRange(player.getLocation(), distance, player.getLayer())) {
                    return;
                } else {
                    npc.resetPath();
                }
            }
        });*/
    }

    public void setLoc(NPCLoc nl) {
        loc = nl;
    }

    @Override
    public void setLocation(Point p) {
        moveTime = System.currentTimeMillis() + RandomUtil.inclusive(10000, 30000);
        super.setLocation(p);
    }

    @Override
    public void setNotFighting(boolean b) {
        lastFight = System.currentTimeMillis();
        super.setNotFighting(b);
    }

    @Override
    public String toString() {
        return "[" + npcDef.getName() + ",idx=" + index + ",sprite=" + getSprite() + "," + getState() + ",["
                + getLocation().getX() + "," + getLocation().getY() + "]]";
    }
}
