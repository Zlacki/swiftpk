package net.swiftpk.server.model.entity;

import net.swiftpk.server.model.Entity;
import net.swiftpk.server.model.Player;
import net.swiftpk.server.model.terrain.tiles.Point;

public abstract class WalkToAction {
    protected final int radius;
    protected final Point destination;
    protected final Mob owner;
    protected boolean hasExecuted = false;

    public WalkToAction(Mob owner, Point destination, int radius) {
        this.destination = destination;
        this.radius = radius;
        this.owner = owner;
    }

    public boolean shouldExecute() {
        if(destination.getX() == 0 && destination.getY() == 0)
            return owner.finishedPath() && !hasExecuted;
        return owner.getLocation().withinRange(destination, radius) && !hasExecuted;
    }

    public abstract void execute();

    public int getRadius() {
        return radius;
    }

    public Point getDestination() {
        return destination;
    }

    public Mob getOwner() {
        return owner;
    }
}
