package net.swiftpk.server.model.entity;

import net.swiftpk.server.model.Entity;
import net.swiftpk.server.model.Player;

public abstract class WalkToEntityAction extends WalkToAction {
    private final Entity target;

    public WalkToEntityAction(Mob owner, Entity target, int radius) {
        super(owner, target.getLocation(), radius);
        this.target = target;
        if(shouldExecute()) {
            execute();
            owner.setWalkToAction(null);
            hasExecuted = true;
        }
    }

    @Override
    public boolean shouldExecute() {
        return owner.canWalkTo(target.getLocation()) && owner.getLocation().withinRange(target.getLocation(), getRadius()) && !hasExecuted;
    }

    public Entity getTarget() {
        return target;
    }
}
