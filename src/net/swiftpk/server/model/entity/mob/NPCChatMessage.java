package net.swiftpk.server.model.entity.mob;

import net.swiftpk.server.ent.EntityHandler;
import net.swiftpk.server.model.Player;
import net.swiftpk.server.model.entity.Mob;
import net.swiftpk.server.model.entity.NPC;

public class NPCChatMessage {
    /**
     * The message it self, in byte format
     */
    private byte[] message;
    /**
     * Who sent the message
     */
    private Mob sender;
    /**
     * The target of the message. Remember npc chat messages need to be told who
     * they're directed to
     */
    private Mob target;

    public NPCChatMessage(Mob sender, String message, Mob target) {
        this.sender = sender;
        this.message = message.getBytes();
        this.target = target;
    }

    public int getLength() {
        return message.length;
    }

    public byte[] getContent() {
        return message;
    }

    public Mob getSender() {
        return sender;
    }

    public Mob getTarget() {
        return target;
    }

    @Override
    public String toString() {
        StringBuilder b = new StringBuilder();
        b.append("msg:((\n");

        b.append("\tsender:((");
        if (sender == null) {
            b.append("\t\ttargetType:`NULL`;  targetName:`NULL`;");
        } else if (sender instanceof Player) {
            Player player = (Player) sender;
            b.append("\t\tsenderType:`Player`;\n");
            b.append("\t\tsenderName:`" + player.getCredentials().getUsername() + "`;\n");
        } else {
            b.append("\t\tsenderType:`NPC`;\n");
            b.append("\t\tsenderName:`" + EntityHandler.getNPCDef(((NPC) sender).getID()).getName() + "`;\n");
        }
        b.append("\t));\n");

        b.append("\ttarget:((\n");
        if (target == null) {
            b.append("\t\tNULL\n");
        } else if (target instanceof Player) {
            b.append("\t\ttargetType:`Player`;\n");
            b.append("\t\ttargetName:`" + ((Player) target).getCredentials().getUsername() + "`;\n");
        } else {
            b.append("\t\ttargetType:`NPC`;\n");
            b.append("\t\ttargetName:`" + EntityHandler.getNPCDef(((NPC) target).getID()).getName() + "`;\n");
        }
        b.append("\t));\n");

        b.append("\tlength:" + getLength() + ";\n");
        b.append("\tcontents:`" + getContent() + "`;\n");

        b.append("));");
        return b.toString();
    }
}
