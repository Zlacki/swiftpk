package net.swiftpk.server.model.entity;

import java.util.*;

import net.swiftpk.server.model.*;
import net.swiftpk.server.model.component.Component;
import net.swiftpk.server.model.component.SkillSet;
import net.swiftpk.server.model.component.SkillSet.Skill;
import net.swiftpk.server.model.terrain.Path;
import net.swiftpk.server.model.terrain.World;
import net.swiftpk.server.model.terrain.tiles.Point;
import net.swiftpk.server.model.terrain.tiles.ViewArea;
import net.swiftpk.server.net.ActionSender;
import net.swiftpk.server.sync.block.SynchronizationBlockSet;
import net.swiftpk.server.tickable.Tickable;
import net.swiftpk.server.tickable.impl.actor.CombatHitTick;
import net.swiftpk.server.util.Functions;

public abstract class Mob extends Entity {
    private static final int[][] SPRITES = new int[][]{{3, 2, 1}, {4, -1, 0}, {5, 6, 7}};
    private Mob combatPartner;
    private CombatHitTick combatTask;
    private int combatRound = 0;
    private final Map<Class<? extends Component>, Component> components;
    private boolean hasMoved;
    private int mobSprite = 0;
    private final WalkingQueue walkingQueue = new WalkingQueue(this);
    private boolean requiresSpriteUpdate = false;
    private MobState state = MobState.Idle;
    private long lastRanged;
    private final ViewArea viewArea = new ViewArea(this);
    private boolean resynchronizing = false;

    private WalkToAction walkToAction;

    public WalkToAction getWalkToAction() {
        return walkToAction;
    }

    public void setWalkToAction(WalkToAction action) {
        walkToAction = action;
    }

    /**
     * The Tickable task that handles following other Mobs
     */
    private Tickable followEvent;

    public boolean isFollowing() {
        return followEvent != null;
    }

    public void resetFollowing() {
        if (followEvent != null) {
            followEvent.stop();
            followEvent = null;
        }
        resetPath();
    }

    public boolean isPlayer() {
        return this instanceof Player;
    }

    public boolean isNPC() {
        return this instanceof NPC;
    }

    public void startCombat(Mob victim) {

        synchronized (victim) {
            boolean gotUnderAttack = false;

            MobState victimState = this.isNPC() && victim.isPlayer() ? MobState.RightFighting : MobState.LeftFighting;
            MobState ourState = this.isNPC() && victim.isPlayer() ? MobState.LeftFighting : MobState.RightFighting;

            if (this.isNPC() && victim.isNPC()) {
                victimState = MobState.LeftFighting;
                ourState = MobState.RightFighting;
            }

            if (this.isPlayer())
                ((Player) this).resetAll();
            Functions.sleep(1);

            resetPath();
            victim.resetPath();

            victim.setCombatPartner(this);
            victim.setState(victimState);
            victim.setRequiresSpriteUpdate(true);

            if (victim.isPlayer()) {
                Player playerVictim = (Player) victim;
                if (this.isPlayer())
                    ((Player) this).attackedPlayer(playerVictim);
                playerVictim.resetAll();
                gotUnderAttack = true;

                if (playerVictim.getState() == MobState.Sleeping) {
                    ActionSender.sendWakeup(playerVictim);
                    ActionSender.sendFatigue(playerVictim);
                }
            }

            setLocation(victim.getLocation(), true);

            setCombatPartner(victim);

            setState(ourState);

            combatTask = new CombatHitTick(this, victim);
            victim.setCombatTask(combatTask);
            World.getInstance().submit(combatTask);
            if (gotUnderAttack) {
                ActionSender.sendSound((Player) victim, "underattack");
                ActionSender.sendMessage((Player) victim, "You are under attack!");
            }
        }
    }

    public void setCombatTask(CombatHitTick task) {
        this.combatTask = task;
    }

    public Skill getSkill(int index) {
        return getComponent(SkillSet.class).get(index);
    }

    public boolean shouldResynchronize() {
        return resynchronizing;
    }

    /**
     * This mob's set of synchronization blocks.
     */
    protected SynchronizationBlockSet blockSet = new SynchronizationBlockSet();

    /**
     * Gets this mob's {@link SynchronizationBlockSet}.
     *
     * @return The block set.
     */
    public final SynchronizationBlockSet getBlockSet() {
        return blockSet;
    }

    public final void resetBlockSet() {
        blockSet = new SynchronizationBlockSet();
        resynchronizing = false;
        requiresSpriteUpdate = false;

    }

    public Mob(Map<Class<? extends Component>, Component> components) {
        this.components = Collections.unmodifiableMap(components);
    }

    public SkillSet getSkillSet() {
        return getComponent(SkillSet.class);
    }

    public boolean canRangeMob() {
        int rangeDelay = 1500;
        return System.currentTimeMillis() - getRangeTimer() > rangeDelay;
    }

    public int distance(Point p) {
        return (int) Math.sqrt(Math.pow(getLocation().getX() - p.getX(), 2) + Math.pow(getLocation().getY() - p.getY(), 2));
    }

    public boolean finishedPath() {
        return walkingQueue.finished();
    }

    public int getArmourPoints() {
        return 1;
    }

    public Mob getCombatPartner() {
        return combatPartner;
    }

    public int getCombatRound() {
        return combatRound;
    }

    public <T extends Component> T getComponent(Class<T> type) {
        return type.cast(components.get(type));
    }

    public int getFightMode() {
        return 1;
    }

    public int getMagicPoints() {
        return 1;
    }

    public double[] getModifiers() {
        return new double[]{1, 1, 1};
    }

    public WalkingQueue getWalkingQueue() { return walkingQueue; }

    // TODO: Is this real?
    public int getRangePoints() {
        return 1;
    }

    public long getRangeTimer() {
        return lastRanged;
    }

    public int getSprite() {
        if (state == MobState.LeftFighting)
            return 8;
        if (state == MobState.RightFighting)
            return 9;
        return mobSprite;
    }

    public MobState getState() {
        return state;
    }

    public ViewArea getViewArea() {
        return viewArea;
    }

    public int getWeaponAimPoints() {
        return 1;
    }

    public int getCombatLevel() {
        return getComponent(SkillSet.class).getCombatLevel();
    }

    public int getWeaponPowerPoints() {
        return 1;
    }

    public boolean hasMoved() {
        return hasMoved;
    }

    public void incCombatRound() {
        combatRound++;
    }

    public boolean isAttackable() {
        return false;
    }

    public boolean isFighting() {
        return state == MobState.LeftFighting || state == MobState.RightFighting && combatPartner != null;
    }

    public void killedBy(Mob m) {
    }

    public boolean requiresSpriteUpdate() {
        return requiresSpriteUpdate;
    }

    public void resetCombat() {
        CombatHitTick task = combatTask;
        if(task != null) {
            task.player.setNotFighting(false);
            task.affectedMob.setNotFighting(false);
            task.stop();
        }
    }

    public boolean withinRange(Entity e) {
        return withinRange(e, 16);
    }


    public void setFollowing(Mob mob, int radius) {
        if (isFollowing()) {
            resetFollowing();
        }
        /**
         * TODO: Maybe make this execute at a different interval. Currently, every
         * ~600ms, or every game tick.
         */
        followEvent = new Tickable(1) {
            @Override
            public void execute() {
                if (!withinRange(mob) || mob == null || getState() != MobState.Idle) {
                    resetFollowing();
                } else if (!finishedPath() && withinRange(mob, radius)) {
                    resetPath();
                } else if (finishedPath() && !withinRange(mob, radius)) {
                    walkToEntity(mob.getX(), mob.getY());
                }
            }
        };
        World.getInstance().submit(followEvent);
    }

    public void resetMoved() {
        hasMoved = false;
    }

    public void resetPath() {
        walkingQueue.reset();
    }

    public void resetRangeTimer() {
        lastRanged = System.currentTimeMillis();
    }

    public void setCombatPartner(Mob m) {
        combatPartner = m;
        if (combatPartner != null)
            resynchronize();
    }

    public void setHasMoved(boolean b) {
        hasMoved = b;
    }

    @Override
    public void setLocation(Point p) {
        setLocation(p, false);
    }

    public void setLocation(Point p, boolean teleported) {
        if (!teleported) {
            updateSprite(p);
            if (!p.equals(getLocation()))
                hasMoved = true;
        }
        if (location != null && location.getLongestDelta(p) > 1)
            resynchronize();
        super.setLocation(p);
    }

    public void setNotFighting(boolean willMove) {
        setState(MobState.Idle);
        combatRound = 0;
        setSprite(0);
        requiresSpriteUpdate = !willMove; // moving players don't need sprite updates
        setCombatPartner(null);
        setCombatTask(null);
    }

    public void setPath(Path path) {
        walkingQueue.setPath(path);
    }

    public void walk(int x, int y) {
        getWalkingQueue().reset();
        Path path = new Path(this, Path.PathType.WALK_TO_POINT);
        {
            path.addStep(x, y);
            path.finish();
        }
        getWalkingQueue().setPath(path);
    }

    public void walkToEntity(int x, int y) {
        getWalkingQueue().reset();
        Path path = new Path(this, Path.PathType.WALK_TO_ENTITY);
        {
            path.addStep(x, y);
            path.finish();
        }
        getWalkingQueue().setPath(path);
    }

    public void setRequiresSpriteUpdate(boolean b) {
        requiresSpriteUpdate = b;
    }

    public void setSprite(int x) {
        mobSprite = x;
    }

    public void setState(MobState ms) {
        state = ms;
    }

    public void updatePosition() {
        walkingQueue.processNextMovement();
    }

    public boolean canWalkTo(Point dest) {
        return PathValidation.checkPath(location, dest);
    }

    /**
     * Gets called after the synchronization cycle to reset the resynchronized flag.
     */
    public void finishResynchronizing() {
        resynchronizing = false;
    }

    /**
     * Call this when you want to move a Mob more than 1 tile per tick
     * Makes sure the packet builders tell the clients to remove then re-add this
     * mob on the next synchronization cycle.
     */
    private void resynchronize() {
        resynchronizing = true;
    }

    public void updateSprite(Point newLocation) {
        int xIndex, yIndex;
        int newSprite;
        if (getLocation() == null) {
            super.setLocation(newLocation);
        }
        xIndex = getLocation().getX() - newLocation.getX() + 1;
        yIndex = getLocation().getY() - newLocation.getY() + 1;
        if (xIndex >= 0 && yIndex >= 0 && xIndex < 3 && yIndex < 3) {
            newSprite = SPRITES[xIndex][yIndex];
            setSprite(newSprite);
        } else {
            setSprite(0);
        }
    }
}
