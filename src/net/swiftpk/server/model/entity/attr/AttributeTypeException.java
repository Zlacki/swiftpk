package net.swiftpk.server.model.entity.attr;

/**
 * A {@link ClassCastException} implementation thrown when there is an attribute
 * type mismatch.
 */
@SuppressWarnings("serial")
public final class AttributeTypeException extends ClassCastException {

    /**
     * Creates a new {@link AttributeTypeException}.
     *
     * @param alias The alias.
     */
    public AttributeTypeException(AttributeKey<?> alias) {
        super("invalid attribute{" + alias.getName() + "} type! expected{" + alias.getTypeName() + "}");
    }
}
