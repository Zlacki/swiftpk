package net.swiftpk.server.model.entity;

import java.util.ArrayList;
import java.util.List;

import net.swiftpk.server.ent.EntityHandler;
import net.swiftpk.server.ent.defs.BoundaryDef;
import net.swiftpk.server.ent.defs.GameObjectDef;
import net.swiftpk.server.ent.locs.GameObjectLoc;
import net.swiftpk.server.model.Entity;
import net.swiftpk.server.model.terrain.tiles.Point;

public class GameObject extends Entity {
    /**
     * The direction the object points in
     */
    private int direction;
    /**
     * Location definition of the object
     */
    private GameObjectLoc loc = null;
    /**
     * Set when the item has been destroyed to alert players
     */
    private boolean removed = false, isBoundary = false;
    /**
     * The type of object
     */
    private int type;

    public GameObject(GameObjectLoc loc) {
        this.loc = loc;
        setID(loc.id);
        direction = loc.direction;
        type = loc.type;
        if (type == 1)
            setBoundary(true);
        Point objectCoordinates = Point.location(loc.getX(), loc.getY());
        GameObject collidingGameObject = objectCoordinates.getObject();
        GameObject collidingWallObject = objectCoordinates.getBoundary();
        if (collidingGameObject != null && !isBoundary()) {
            world.unregisterGameObject(collidingGameObject);
        }
        if (collidingWallObject != null && isBoundary()) {
            world.unregisterGameObject(collidingWallObject);
        }
        setLocation(Point.location(loc.x, loc.y));
    }

    public GameObject(Point location, int id, int direction, int type) {
        setID(id);
        this.direction = direction;
        this.type = type;
        if (type == 1)
            setBoundary(true);
        setLocation(location);
    }

    @Override
    public boolean equals(Object o) {
        return ((GameObject) o).getX() == getX() && ((GameObject) o).getY() == getY();
    }

    public GameObjectDef getDef() {
        return EntityHandler.getGameObjectDef(super.getID());
    }

    public int getDirection() {
        return direction;
    }

    public BoundaryDef getBoundaryDef() {
        if (!isBoundary())
            return null;
        return EntityHandler.getBoundaryDef(super.getID());
    }

    public GameObjectLoc getLoc() {
        return loc;
    }

    public int getType() {
        return type;
    }

    public boolean isBoundary() {
        return isBoundary;
    }

    public boolean isRemoved() {
        return removed;
    }

    public boolean isTelePoint() {
        return EntityHandler.getObjectTelePoint(super.getLocation()) != null;
    }

    public void remove() {
        removed = true;
    }

    public void setDirection(int direction) {
        this.direction = direction;
    }

    public void setBoundary(boolean isBoundary) {
        this.isBoundary = isBoundary;
    }

    public void setType(int type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return getDef().getName() + "," + getLocation() + "," + getType();
    }
}
