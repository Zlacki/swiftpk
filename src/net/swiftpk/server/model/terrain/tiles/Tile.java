package net.swiftpk.server.model.terrain.tiles;

import java.io.IOException;
import java.nio.ByteBuffer;

import net.swiftpk.server.model.terrain.World;

public class Tile {
    public static Tile unpack(ByteBuffer in) throws IOException {
        if (in.remaining() < 10) {
            throw new IOException("Provided buffer too short");
        }
        Tile tile = new Tile();
        tile.groundElevation = in.get();
        tile.groundTexture = in.get();
        tile.groundOverlay = in.get();
        tile.roofTexture = in.get();
        tile.horizontalWall = in.get();
        tile.verticalWall = in.get();
        tile.diagonalWalls = in.getInt();
        return tile;
    }

    /**
     * The ID of any diagonal walls on this tile
     */
    public int diagonalWalls = 0;
    /**
     * The texture ID of this tile
     */
    public byte groundElevation = 0;
    /**
     * The overlay texture ID
     */
    public byte groundOverlay = 0;
    public byte groundTexture = 0;
    /**
     * The texture ID of any horizontal wall on this tile
     */
    public byte horizontalWall = 0;
    private Point loc;
    /**
     * The texture ID of the roof of this tile
     */
    public byte roofTexture = 0;
    private int val;
    /**
     * The texture ID of any vertical wall on this tile
     */
    public byte verticalWall = 0;

    public Tile() {
    }

    public Tile(int x, int y) {
        this(Point.location(x, y, 0));
    }

    public Tile(Point p) {
        this.loc = p;
    }

    public Point getLocation() {
        return loc;
    }

    public int getValue() {
        return val;
    }

    public int getX() {
        return loc.getX();
    }

    public int getY() {
        return loc.getY();
    }

    // public boolean isWalkable() {
    // return (val & 64) != 64 && isObjectWalkable() && !inTrainRoom1() &&
    // !inTrainRoom2() && !inTrainRoom3() && !inTrainRoom4();
    // }
    // private TileValue t = World.getInstance().getTileValue(getX(), getY());
    public boolean isWalkable() {
        return (World.getInstance().getTileValue(getX(), getY()).traversalMask & 64) == 0;
    }

    public ByteBuffer pack() throws IOException {
        ByteBuffer out = ByteBuffer.allocate(10);
        out.put(groundElevation);
        out.put(groundTexture);
        out.put(groundOverlay);
        out.put(roofTexture);
        out.put(horizontalWall);
        out.put(verticalWall);
        out.putInt(diagonalWalls);
        out.flip();
        return out;
    }

    public void setValue(int i) {
        val = i;
    }

    @Override
    public String toString() {
        return "[" + loc.toString() + "]";
    }
}
