package net.swiftpk.server.model.terrain.tiles;

import net.swiftpk.server.model.Item;
import net.swiftpk.server.model.Player;
import net.swiftpk.server.model.entity.GameObject;
import net.swiftpk.server.model.entity.Mob;
import net.swiftpk.server.model.entity.NPC;
import net.swiftpk.server.model.terrain.World;

import java.util.LinkedList;
import java.util.List;

public class ViewArea {
    public static final int MAX_DISTANCE = 15, DEFAULT_DISTANCE = 15;
    private int distance = DEFAULT_DISTANCE;
    private Mob mob;

    public ViewArea(Mob m) {
        mob = m;
    }

    public void decreaseSize() {
        if(distance > 1)
            distance--;
    }

    public void increaseSize() {
        if(distance < MAX_DISTANCE)
            distance++;
    }

    public int getDistance() {
        return distance;
    }

    public Iterable<GameObject> getGameObjectsInView() {
        return getViewableObjects(mob, (distance + 5));
    }

    public Iterable<Item> getItemsInView() {
        return getViewableItems(mob, (distance + 5));
    }

    public Iterable<NPC> getNpcsInView() {
        return getViewableNpcs(mob, distance);
    }

    public Iterable<NPC> getNpcsInView(int dist) {
        return getViewableNpcs(mob, dist);
    }

    public Iterable<Player> getPlayersInView() {
        return getViewablePlayers(mob, distance);
    }

    public Iterable<Player> getPlayersInView(int dist) {
        return getViewablePlayers(mob, dist);
    }

    private Area[] getViewableAreas(int x, int y) {
        try {
            Area[][] areas = World.getInstance().getAreas();
            Area[] neighbours = new Area[4];
            int areaX = x / World.AREA_SIZE;
            int areaY = y / World.AREA_SIZE;
            neighbours[0] = areas[areaX][areaY];
            int relX = x % World.AREA_SIZE;
            int relY = y % World.AREA_SIZE;
            if (relX <= World.LOWER_BOUND) {
                if (relY <= World.LOWER_BOUND) {
                    neighbours[1] = areas[areaX - 1][areaY];
                    neighbours[2] = areas[areaX - 1][areaY - 1];
                    neighbours[3] = areas[areaX][areaY - 1];
                } else {
                    neighbours[1] = areas[areaX - 1][areaY];
                    neighbours[2] = areas[areaX - 1][areaY + 1];
                    neighbours[3] = areas[areaX][areaY + 1];
                }
            } else {
                if (relY <= World.LOWER_BOUND) {
                    neighbours[1] = areas[areaX + 1][areaY];
                    neighbours[2] = areas[areaX + 1][areaY - 1];
                    neighbours[3] = areas[areaX][areaY - 1];
                } else {
                    neighbours[1] = areas[areaX + 1][areaY];
                    neighbours[2] = areas[areaX + 1][areaY + 1];
                    neighbours[3] = areas[areaX][areaY + 1];
                }
            }
            return neighbours;
        } catch (ArrayIndexOutOfBoundsException e) {
            return null;
        }
    }

    public Area[] getViewableAreas(Point p) {
        return getViewableAreas(p.getX(), p.getY());
    }

    public Iterable<Item> getViewableItems(Point point) {
        Area[] areas = getViewableAreas(point);
        List<Item> items = new LinkedList<>();
        if (areas != null) {
            for (Area r : areas) {
                for (Item item : r.getItems()) {
                    if (!items.contains(item) && !item.isRemoved() && item.withinRange(point, distance + 5)) {
                        items.add(item);
                    }
                }
            }
        }
        return items;
    }

    public Iterable<Item> getViewableItems(Mob mob, int radius) {
        Area[] areas = getViewableAreas(mob.getLocation());
        List<Item> items = new LinkedList<>();
        if (areas != null) {
            for (Area r : areas) {
                for (Item i : r.getItems()) {
                    if (!items.contains(i) && !i.isRemoved() && mob.withinRange(i.getLocation(), radius)) {
                        items.add(i);
                    }
                }
            }
        }
        return items;
    }

    public Iterable<NPC> getViewableNpcs(Point point) {
        Area[] areas = getViewableAreas(point);
        List<NPC> npcs = new LinkedList<>();
        if (areas != null) {
            for (Area r : areas) {
                for (NPC npc : r.getNpcs()) {
                    if (!npcs.contains(npc) && npc.withinRange(point, distance)) {
                        npcs.add(npc);
                    }
                }
            }
        }
        return npcs;
    }

    public Iterable<NPC> getViewableNpcs(Mob mob, int radius) {
        Area[] areas = getViewableAreas(mob.getLocation());
        List<NPC> npcs = new LinkedList<>();
        if (areas != null) {
            for (Area r : areas) {
                for (NPC n : r.getNpcs()) {
                    if (!npcs.contains(n) && mob.withinRange(n.getLocation(), radius)) {
                        npcs.add(n);
                    }
                }
            }
        }
        return npcs;
    }

    public Iterable<GameObject> getViewableObjects(Point point) {
        Area[] areas = getViewableAreas(point);
        List<GameObject> objects = new LinkedList<>();
        if (areas != null) {
            for (Area r : areas) {
                for (GameObject object : r.getObjects()) {
                    if (!objects.contains(object) && !object.isRemoved() && object.withinRange(point, distance + 5)) {
                        objects.add(object);
                    }
                }
            }
        }
        return objects;
    }

    public Iterable<GameObject> getViewableObjects(Mob mob, int radius) {
        Area[] areas = getViewableAreas(mob.getLocation());
        List<GameObject> objects = new LinkedList<>();
        if (areas != null) {
            for (Area r : areas) {
                for (GameObject go : r.getObjects()) {
                    if (!objects.contains(go) && !go.isRemoved() && mob.withinRange(go.getLocation(), radius)) {
                        objects.add(go);
                    }
                }
            }
        }
        return objects;
    }

    public  Iterable<Player> getViewablePlayers(Point point) {
        Area[] areas = getViewableAreas(point);
        List<Player> players = new LinkedList<>();
        if (areas != null) {
            for (Area r : areas) {
                for (Player player : r.getPlayers()) {
                    if (!players.contains(player) && player.loggedIn() && player.withinRange(point, distance)) {
                        players.add(player);
                    }
                }
            }
        }
        return players;
    }

    public Iterable<Player> getViewablePlayers(Mob mob, int radius) {
        Area[] areas = getViewableAreas(mob.getLocation());
        List<Player> players = new LinkedList<>();
        if (areas != null) {
            for (Area r : areas) {
                for (Player player : r.getPlayers()) {
                    if (!players.contains(player) && player.loggedIn() && mob.withinRange(player.getLocation(), radius)) {
                        players.add(player);
                    }
                }
            }
        }
        return players;
    }

    public Iterable<Player> getViewablePlayers(Player player, int radius) {
        Area[] areas = getViewableAreas(player.getLocation());
        List<Player> players = new LinkedList<>();
        if (areas != null) {
            for (Area r : areas) {
                for (Player p : r.getPlayers()) {
                    if (!p.equals(player) && !players.contains(p) && p.loggedIn() && player.withinRange(p.getLocation(), radius)) {
                        players.add(p);
                    }
                }
            }
        }
        return players;
    }
}
