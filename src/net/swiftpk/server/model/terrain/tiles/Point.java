package net.swiftpk.server.model.terrain.tiles;

import net.swiftpk.server.model.Item;
import net.swiftpk.server.model.Player;
import net.swiftpk.server.model.entity.GameObject;
import net.swiftpk.server.model.entity.NPC;
import net.swiftpk.server.model.terrain.World;

import java.util.Objects;

//supports ONLY positive numbers
public final class Point {
    public static final Point DEATH_SPOT = location(0, 0);
    private static final int DEFAULT_LAYER = 0;

    public static Point location(int x, int y, int layer) {
        if (x < 0 || y < 0)
            throw new IllegalArgumentException("Point may not contain negative values. Point.location(x=" + x + ",y=" + y + ");");
        if (x > World.MAX_WIDTH || y > World.MAX_HEIGHT)
            throw new IllegalArgumentException("Point must be within world boundaries.  Point.location(x=" + x + ",y=" + y + "); World.MAX_WIDTH:" + World.MAX_WIDTH + ",World.MAX_HEIGHT:" + World.MAX_WIDTH + ";");

        return new Point(x, y, layer);
    }

    public static Point location(int x, int y) {
        if (x < 0 || y < 0)
            throw new IllegalArgumentException("Point may not contain negative values. Point.location(x=" + x + ",y=" + y + ");");
        if (x > World.MAX_WIDTH || y > World.MAX_HEIGHT)
            throw new IllegalArgumentException("Point must be within world boundaries.  Point.location(x=" + x + ",y=" + y + "); World.MAX_WIDTH:" + World.MAX_WIDTH + ",World.MAX_HEIGHT:" + World.MAX_WIDTH + ";");

        return new Point(x, y);
    }

    private int x;
    private int y;
    private int layer;

    public Point(int x, int y, int layer) {
        this.x = x;
        this.y = y;
        this.setLayer(layer);
    }

    public Point(int x, int y) {
        this.x = x;
        this.y = y;
        this.setLayer(DEFAULT_LAYER);
    }

    @Override
    public final boolean equals(Object o) {
        if (o instanceof Point) {
            return this.x == ((Point) o).x && this.y == ((Point) o).y && this.layer == ((Point) o).layer;
        }
        return false;
    }

    public int getLayer() {
        return layer;
    }

    public final int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public void setY(int y) {
        this.y = y;
    }

    public final int getY() {
        return y;
    }

    @Override
    public int hashCode() {
        return x << 16 | y;
    }

    public boolean inBounds(int x1, int y1, int x2, int y2) {
        return x >= x1 && x <= x2 && y >= y1 && y <= y2;
    }

    public boolean inWilderness() {
        return wildernessLevel() > 0;
    }

    public void setLayer(int layer) {
        this.layer = layer;
    }

    public boolean isWalkable() {
        return (Objects.requireNonNull(World.getInstance().getTileValue(getX(), getY())).traversalMask & 64) == 0;
    }

    @Override
    public String toString() {
        return "[" + x + "," + y + "]";
    }

    public int wildernessLevel() {
        int height = net.swiftpk.server.util.Formulae.getHeight(this);
        int wildY = 1776;
        wildY -= height * 944;
        int wild = 2203 - (y + wildY);
        if (x + 2304 >= 2640) {
            wild = -50;
        }
        int lvl = 0;
        if (wild > 0)
            lvl = 1 + wild / 6;
        // com.rscd.util.Logger.log("wildY = "+wildY+", level = "+lvl+",
        // height="+height+", y="+y);
        return lvl;
    }

    public boolean withinRange(Point p, int radius) {
        if (p == null)
            return false;
        return getLongestDelta(p) <= radius && p.getLayer() == getLayer();
    }

    public int getLongestDelta(Point other) {
        if (other == null)
            return -1;
        int deltaX = Math.abs(getX() - other.getX());
        int deltaY = Math.abs(getY() - other.getY());
        return Math.max(deltaX, deltaY);
    }

    public boolean hasNPC() {
        Area area = World.getInstance().getArea(this);
        for(NPC npc : area.getNpcs())
            if (npc.getLocation() != null && npc.getLocation().equals(this))
                return true;
        return false;
    }

    public boolean hasPlayer() {
        Area area = World.getInstance().getArea(this);
        for(Player player : area.getPlayers())
            if (player.getLocation() != null && player.getLocation().equals(this))
                return true;
        return false;
    }

    public boolean hasObject() {
        Area area = World.getInstance().getArea(this);
        for(GameObject object : area.getObjects()) {
            if(object.isBoundary() || object.getLocation() == null) continue;

            int width = object.getDef().getWidth();
            int height = object.getDef().getHeight();
            if (object.getDirection() != 0 && object.getDirection() != 4) {
                height = object.getDef().getWidth();
                width = object.getDef().getHeight();
            }

            for(int x = object.getX(); x < object.getX() + width; ++x) {
                for(int y = object.getY(); y < object.getY() + height; ++y) {
                    if(equals(Point.location(x, y)))
                        return true;
                }
            }
        }
        return false;
    }

    public GameObject getObject() {
        Area area = World.getInstance().getArea(this);
        for(GameObject object : area.getObjects()) {
            if(object.isBoundary() || object.getLocation() == null) continue;

            int width = object.getDef().getWidth();
            int height = object.getDef().getHeight();
            if (object.getDirection() != 0 && object.getDirection() != 4) {
                height = object.getDef().getWidth();
                width = object.getDef().getHeight();
            }

            for(int x = object.getX(); x < object.getX() + width; ++x) {
                for(int y = object.getY(); y < object.getY() + height; ++y) {
                    if(equals(Point.location(x, y)))
                        return object;
                }
            }
        }
        return null;
    }

    public GameObject getBoundary() {
        Area area = World.getInstance().getArea(this);
        for(GameObject object : area.getObjects())
            if (object.isBoundary() && object.getLocation() != null && object.getLocation().equals(this))
                return object;
        return null;
    }

    public boolean hasBoundary() {
        Area area = World.getInstance().getArea(this);
        for(GameObject object : area.getObjects())
            if (object.isBoundary() && object.getLocation() != null && object.getLocation().equals(this))
                return true;
        return false;
    }

    public boolean hasGameObject() {
        Area area = World.getInstance().getArea(this);
        for(GameObject object : area.getObjects()) {
            if(object.getLocation() == null) continue;

            if(object.isBoundary()) {
                int width = object.getDef().getWidth();
                int height = object.getDef().getHeight();
                if (object.getDirection() != 0 && object.getDirection() != 4) {
                    height = object.getDef().getWidth();
                    width = object.getDef().getHeight();
                }

                for (int x = object.getX(); x < object.getX() + width; ++x) {
                    for (int y = object.getY(); y < object.getY() + height; ++y) {
                        Point curLocation = Point.location(x, y);
                        if (equals(curLocation))
                            return true;
                    }
                }
            } else if(equals(object.getLocation()))
                return true;
        }
        return false;
    }
    public boolean hasItem() {
        Area area = World.getInstance().getArea(this);
        for(Item item : area.getItems())
            if (!item.isRemoved() && item.getLocation() != null && item.getLocation().equals(this))
                return true;
        return false;
    }
}
