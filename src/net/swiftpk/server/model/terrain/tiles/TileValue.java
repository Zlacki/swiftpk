package net.swiftpk.server.model.terrain.tiles;

public class TileValue {
    public byte overlay = 0;
    public byte verticalWallVal = 0;
    public byte elevation = 0;
    public int diagWallVal = 0;
    public byte horizontalWallVal = 0;
    public byte traversalMask = 0;
    public boolean projectileAllowed = false;
}
