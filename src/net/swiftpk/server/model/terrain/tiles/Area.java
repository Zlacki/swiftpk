package net.swiftpk.server.model.terrain.tiles;

import java.util.*;
import java.util.concurrent.ConcurrentLinkedQueue;

import net.swiftpk.server.model.Entity;
import net.swiftpk.server.model.Item;
import net.swiftpk.server.model.Player;
import net.swiftpk.server.model.entity.GameObject;
import net.swiftpk.server.model.entity.Mob;
import net.swiftpk.server.model.entity.NPC;
import net.swiftpk.server.model.terrain.World;

public final class Area {
    private final Queue<GameObject> objects = new ConcurrentLinkedQueue<GameObject>();
    private final Queue<Item> items = new ConcurrentLinkedQueue<Item>();
    private final Queue<NPC> npcs = new ConcurrentLinkedQueue<NPC>();
    private final Queue<Player> players = new ConcurrentLinkedQueue<Player>();

    public void addItem(Item item) {
        items.add(item);
    }

    public void addNpc(NPC npc) {
        npcs.add(npc);
    }

    public void addObject(GameObject go) {
        objects.add(go);
    }

    public void addPlayer(Mob mob) {
        players.add((Player) mob);
    }

    public void addPlayer(Player player) {
        players.add(player);
    }

    public void add(Entity e) {
        if (e instanceof Item)
            addItem((Item) e);
        else if (e instanceof NPC)
            addNpc((NPC) e);
        else if (e instanceof GameObject)
            addObject((GameObject) e);
        else if (e instanceof Player)
            addPlayer((Player) e);
    }

    public Iterable<Item> getItems() {
        return Collections.unmodifiableCollection(items);
    }

    public NPC getNpc() {
        throw new UnsupportedOperationException();
    }

    public boolean pointHasNPCs(int x, int y) {
        for (NPC n : npcs)
            if (n.getX() == x && n.getY() == y)
                return true;

        return false;
    }

    public boolean pointHasPlayers(int x, int y) {
        for (Player p : players)
            if (p.getX() == x && p.getY() == y)
                return true;

        return false;
    }

    public boolean pointHasMobs(int x, int y) {
        return pointHasNPCs(x, y) || pointHasPlayers(x, y);
    }

    public Iterable<NPC> getNpcs() {
        return Collections.unmodifiableCollection(npcs);
    }

    public Iterable<GameObject> getObjects() {
        return Collections.unmodifiableCollection(objects);
    }

    public Player getPlayer() {
        throw new UnsupportedOperationException();
    }

    public Iterable<Player> getPlayers() {
        return Collections.unmodifiableCollection(players);
    }

    public void removeItem(Item item) {
        items.remove(item);
    }

    public void removeNpc(NPC npc) {
        npcs.remove(npc);
    }

    public void removeObject(GameObject go) {
        objects.remove(go);
    }

    public void removePlayer(Mob mob) {
        players.remove(mob);
    }

    public void removePlayer(Player player) {
        players.remove(player);
    }

    public void remove(Entity e) {
        if (e instanceof Item)
            removeItem((Item) e);
        else if (e instanceof NPC)
            removeNpc((NPC) e);
        else if (e instanceof GameObject)
            removeObject((GameObject) e);
        else if (e instanceof Player)
            removePlayer((Player) e);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder(2000);
        sb.append("Players:\n");
        for (Player p : players) {
            sb.append("\t").append(p).append("\n");
        }
        sb.append("\nNpcs:\n");
        for (NPC n : npcs) {
            sb.append("\t").append(n).append("\n");
        }
        sb.append("\nItems:\n");
        for (Item i : items) {
            sb.append("\t").append(i).append("\n");
        }
        sb.append("\nObjects:\n");
        for (Object o : objects) {
            sb.append("\t").append(o).append("\n");
        }
        return sb.toString();
    }
}
