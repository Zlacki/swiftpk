package net.swiftpk.server.model.terrain;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;

import com.almworks.sqlite4java.SQLiteConnection;
import com.almworks.sqlite4java.SQLiteException;
import com.almworks.sqlite4java.SQLiteJob;

import net.swiftpk.server.Server;
import net.swiftpk.server.ent.defs.extras.NPCDrop;
import net.swiftpk.server.ent.locs.GameObjectLoc;
import net.swiftpk.server.ent.locs.ItemLoc;
import net.swiftpk.server.ent.locs.NPCLoc;
import net.swiftpk.server.event.Event;
import net.swiftpk.server.model.*;
import net.swiftpk.server.tickable.impl.actor.NPCUpdater;
import net.swiftpk.server.io.DBConnection;
import net.swiftpk.server.model.component.PrayerDrain;
import net.swiftpk.server.model.entity.DefaultEntityFactory;
import net.swiftpk.server.model.entity.GameObject;
import net.swiftpk.server.model.entity.NPC;
import net.swiftpk.server.model.terrain.tiles.Area;
import net.swiftpk.server.model.terrain.tiles.Point;
import net.swiftpk.server.model.terrain.tiles.TileValue;
import net.swiftpk.server.net.ActionSender;
import net.swiftpk.server.tickable.Tickable;
import net.swiftpk.server.tickable.TickableManager;
import net.swiftpk.server.tickable.impl.actor.StatRestoreTick;
import net.swiftpk.server.util.CollisionFlag;
import net.swiftpk.server.util.DataConversions;
import net.swiftpk.server.util.EntityList;
import net.swiftpk.server.util.Logger;

public final class World {
    public static synchronized World getInstance() {
        return INSTANCE;
    }

    public static final int MAX_WIDTH = 944, MAX_HEIGHT = 3776;

    public static final int AREA_SIZE = 48;
    public static final int LOWER_BOUND = (AREA_SIZE / 2);
    public static final int HORIZONTAL_PLANES = (MAX_WIDTH / AREA_SIZE) + 1;
    public static final int VERTICAL_PLANES = (MAX_HEIGHT / AREA_SIZE) + 1;
    private static final String[] OBJECTS_PROJECTILE_CLIP_ALLOWED = {"gravestone", "sign", "broken pillar", "bone",
            "animalskull", "skull", "egg", "eggs", "ladder", "torch", "rock", "treestump", "railing",
            "railings", "gate", "fence", "table", "smashed chair", "smashed table", "longtable", "fence", "chair"};
    private final Area[][] areas = new Area[HORIZONTAL_PLANES][VERTICAL_PLANES];

    /**
     * The World instance.
     */
    private static World INSTANCE = new World();
    /**
     * The list of all the NPCs in the World.
     */
    private EntityList<NPC> npcs = new EntityList<>(16384);
    /**
     * The list of all the Players in the World.
     */
    private EntityList<Player> players = new EntityList<>(2048);
    private HashMap<String, Shop> shops = new HashMap<>();
    private TickableManager tickableManager = new TickableManager();

    /**
     * Data about the tiles, are they walkable etc
     */
    private TileValue[][] tileType = new TileValue[MAX_WIDTH][MAX_HEIGHT];

    private World() {
        for (int x = 0; x < HORIZONTAL_PLANES; x++) {
            for (int y = 0; y < VERTICAL_PLANES; y++) {
                areas[x][y] = new Area();
            }
        }
    }

    public Area[][] getAreas() {
        return areas;
    }

    public NPC getNPC(int id) {
        return npcs.get(id);
    }

    public EntityList<NPC> getNPCs() {
        return npcs;
    }

    public Player getPlayer(int id) {
        return players.get(id);
    }

    public Player getPlayer(long usernameHash) {
        for (Player p : players) {
            if (p.getCredentials().getEncodedUsername() == usernameHash) {
                return p;
            }
        }
        return null;
    }

    public Player getPlayer(String username) {
        return getPlayer(DataConversions.usernameToHash(username));
    }

    public Player getPlayerById(int id) {
        for (Player p : players) {
            if (p.getID() == id)
                return p;
        }
        return null;
    }

    public Player getPlayerID(int id) {
        for (Player p : players) {
            if (p.getID() == id) {
                return p;
            }
        }
        return null;
    }

    public EntityList<Player> getPlayers() {
        return players;
    }

    public HashMap<String, Shop> getShops() {
        return shops;
    }

    public TickableManager getTickableManager() {
        return tickableManager;
    }

    public boolean hasGameObject(int x, int y) {
        return getGameObject(x, y) != null;
    }

    public Item getItem(int id, int x, int y) {
        return getItem(id, Point.location(x, y, 0));
    }

    public Item getItem(int id, Point location) {
        for(Item item : getItems(location))
            if(item.getID() == id)
                return item;
        return null;
    }

    public List<Item> getItems(int x, int y) {
        return getItems(Point.location(x, y, 0));
    }

    public List<Item> getItems(Point location) {
        List<Item> itemList = new ArrayList<>();

        Area area = areas[location.getX() / AREA_SIZE][location.getY() / AREA_SIZE];
        for(Item item : area.getItems())
            if(item.getLocation().equals(location))
                itemList.add(item);

        return itemList;
    }

    public GameObject getGameObject(int x, int y) {
        return getGameObject(Point.location(x, y, 0));
    }

    public GameObject getGameObject(Point location) {
        Area area = areas[location.getX() / AREA_SIZE][location.getY() / AREA_SIZE];
        for(GameObject object : area.getObjects()) {
            if (object.getLocation() != null && object.getLocation().equals(location))
                return object;
        }
        return null;
    }

    public Area getArea(int x, int y) {
        int areaX = x / World.AREA_SIZE;
        int areaY = y / World.AREA_SIZE;
        return areas[areaX][areaY];
    }

    public Area getArea(Point location) {
        return getArea(location.getX(), location.getY());
    }

    public TileValue getTileValue(int x, int y) {
        if (tileType[x][y] == null)
            tileType[x][y] = new TileValue();
        return tileType[x][y];
    }

    public boolean hasPlayer(Player p) {
        return players.contains(p);
    }

    public boolean isLoggedIn(long usernameHash) {
        Player friend = getPlayer(usernameHash);
        if (friend != null) {
            return friend.loggedIn();
        }
        return false;
    }

    public void registerGameObject(GameObject o) {
        int dir = o.getDirection();
        if (o.getID() == 1147) {
            return;
        }
        switch (o.getType()) {
            case 0:
                if (o.getDef().getType() != 1 && o.getDef().getType() != 2) {
                    return;
                }
                int width, height;
                if (dir == 0 || dir == 4) {
                    width = o.getDef().getWidth();
                    height = o.getDef().getHeight();
                } else {
                    height = o.getDef().getWidth();
                    width = o.getDef().getHeight();
                }
                for (int x = o.getX(); x < o.getX() + width; ++x) {
                    for (int y = o.getY(); y < o.getY() + height; ++y) {
                        if (isProjectileClipAllowed(o)) {
                            getTileValue(x, y).projectileAllowed = true;
                        }
                        if (o.getDef().getType() == 1) {
                            getTileValue(x, y).traversalMask |= CollisionFlag.FULL_BLOCK_C;
                        } else if (dir == 0) {
                            getTileValue(x, y).traversalMask |= CollisionFlag.WALL_EAST;
                            if (getTileValue(x - 1, y) != null)
                                getTileValue(x - 1, y).traversalMask |= CollisionFlag.WALL_WEST;
                        } else if (dir == 2) {
                            getTileValue(x, y).traversalMask |= CollisionFlag.WALL_SOUTH;
                            if (getTileValue(x, y + 1) != null)
                                getTileValue(x, y + 1).traversalMask |= CollisionFlag.WALL_NORTH;
                        } else if (dir == 4) {
                            getTileValue(x, y).traversalMask |= CollisionFlag.WALL_WEST;
                            if (getTileValue(x + 1, y) != null)
                                getTileValue(x + 1, y).traversalMask |= CollisionFlag.WALL_EAST;
                        } else if (dir == 6) {
                            getTileValue(x, y).traversalMask |= CollisionFlag.WALL_NORTH;
                            if (getTileValue(x, y - 1) != null)
                                getTileValue(x, y - 1).traversalMask |= CollisionFlag.WALL_SOUTH;
                        }
                    }
                }
                break;

            case 1:
                if (o.getBoundaryDef().getBoundaryType() != 1) // Closed doors?
                    return;
                int x = o.getX(), y = o.getY();
                if (isProjectileClipAllowed(o)) {
                    getTileValue(x, y).projectileAllowed = true;
                }
                if (dir == 0) {

                    getTileValue(x, y).traversalMask |= CollisionFlag.WALL_NORTH;
                    if (getTileValue(x, y - 1) != null)
                        getTileValue(x, y - 1).traversalMask |= CollisionFlag.WALL_SOUTH;
                } else if (dir == 1) {
                    getTileValue(x, y).traversalMask |= CollisionFlag.WALL_EAST;
                    if (getTileValue(x - 1, y) != null)
                        getTileValue(x - 1, y).traversalMask |= CollisionFlag.WALL_WEST;
                } else if (dir == 2) {
                    getTileValue(x, y).traversalMask |= CollisionFlag.FULL_BLOCK_A;
                } else if (dir == 3) {
                    getTileValue(x, y).traversalMask |= CollisionFlag.FULL_BLOCK_B;
                }
                break;
        }
    }


    private boolean isProjectileClipAllowed(GameObject o) {
        for (String s : OBJECTS_PROJECTILE_CLIP_ALLOWED) {
            if (o.getType() == 0) {
                // there are many of the objects that need to
                // have clip enabled.
                if (!o.getDef().getName().equalsIgnoreCase("tree")) {
                    if (o.getDef().getHeight() == 1 && o.getDef().getWidth() == 1 && !o.getDef().getName().toLowerCase().equalsIgnoreCase("chest"))
                        return true;
                }
                if (o.getDef().getName().toLowerCase().equalsIgnoreCase(s)) {
                    return true;
                }
            } else if (o.getType() == 1) {
                if (o.getBoundaryDef().getName().toLowerCase().equalsIgnoreCase(s)) {
                    return true;
                }
            }
        }
        return false;
    }

    public void registerNPC(NPC n) {
        npcs.add(n);
    }

    public NPC getNPC(int id, int minX, int maxX, int minY, int maxY) {
        for (NPC npc : npcs)
            if (npc.getID() == id && npc.getX() >= minX && npc.getX() <= maxX && npc.getY() >= minY
                    && npc.getY() <= maxY)
                return npc;
        return null;
    }

    public void registerPlayer(Player p) {
        players.add(p);
        for (Player p1 : players) {
            p1.alertOfLogin(p);
        }
    }

    public void registerShop(String name, Shop s) {
        getShops().put(name, s);
    }

    public void registerTasks() {
        Server.getServer().getGameService().getEventExecutor().scheduleAtFixedRate(() -> getShops().values().forEach(Shop::restock), 30 * 1000, 30 * 1000, TimeUnit.MILLISECONDS);

        submit(new StatRestoreTick());
    }

    public void pulse() {
        tickableManager.pulse();
        NPCUpdater.update(npcs);
        players.stream().map(p -> p.prayerTick).filter(PrayerDrain::isActive).forEach(PrayerDrain::execute);
    }

    public void reloadDrops() {
        try {
            DBConnection.getWorldQueue().execute(new SQLiteJob<Boolean>() {
                @Override
                protected Boolean job(SQLiteConnection conn) throws SQLiteException {
                    for (NPC n : npcs) {
                        ArrayList<NPCDrop> drops = new ArrayList<NPCDrop>();
                        Server.getDbWorld().npc_drop.bind(1, n.getID());
                        float cumProb = 0;
                        while (Server.getDbWorld().npc_drop.step()) {
                            NPCDrop drop = new NPCDrop();
                            drop.setItemId(Server.getDbWorld().npc_drop.columnInt(0));
                            drop.setAmounts(Server.getDbWorld().npc_drop.columnInt(1),
                                    Server.getDbWorld().npc_drop.columnInt(2));
                            drop.setProbability(Server.getDbWorld().npc_drop.columnDouble(3));
                            cumProb += drop.getProbability();
                            drop.setCumulativeProbability(cumProb);
                            drops.add(drop);
                        }
                        Server.getDbWorld().npc_drop.reset();
                        n.getDef().setDrop(drops.toArray(new NPCDrop[]{}));
                    }
                    return true;
                }
            }).complete();
        } catch (SQLiteException e) {
            e.printStackTrace();
        }
    }

    public void remove(Entity entity) {
        Area area = getArea(entity.getLocation());
        if (area != null)
            area.remove(entity);
    }

    public void announceKill(long killedHash, long killerHash, int type) {
        for (Player player : players) {
            ActionSender.sendKillNotification(player, killedHash, killerHash, type);
        }
    }

    public void setLocation(Entity entity, Point oldPoint, Point newPoint) {
        if (oldPoint != null) {
            Area oldArea = getArea(oldPoint);
            oldArea.remove(entity);
        }
        if (newPoint != null) {
            Area newArea = getArea(newPoint);
            newArea.add(entity);
        }
    }

    public void setShops(HashMap<String, Shop> shops) {
        this.shops = shops;
    }

    public void submit(Event event) {
        Server.getServer().getGameService().submitTask(event);
    }

    public void submit(Tickable tick) {
        tickableManager.submit(tick);
    }

    public void unregisterGameObject(GameObject o) {
        o.remove();
        setLocation(o, o.getLocation(), null);
//        unregisterGameObjects(o);

        int dir = o.getDirection();
        switch (o.getType()) {
            case 0:
                if (o.getDef().getType() != 1 && o.getDef().getType() != 2) {
                    return;
                }
                int width, height;
                if (dir == 0 || dir == 4) {
                    width = o.getDef().getWidth();
                    height = o.getDef().getHeight();
                } else {
                    height = o.getDef().getWidth();
                    width = o.getDef().getHeight();
                }
                for (int x = o.getX(); x < o.getX() + width; ++x) {
                    for (int y = o.getY(); y < o.getY() + height; ++y) {
                        if (o.getDef().getType() == 1) {
                            getTileValue(x, y).traversalMask &= 0xffbf;
                        } else if (dir == 0) {
                            getTileValue(x, y).traversalMask &= 0xfffd;
                            getTileValue(x - 1, y).traversalMask &= 65535 - 8;
                        } else if (dir == 2) {
                            getTileValue(x, y).traversalMask &= 0xfffb;
                            getTileValue(x, y + 1).traversalMask &= 65535 - 1;
                        } else if (dir == 4) {
                            getTileValue(x, y).traversalMask &= 0xfff7;
                            getTileValue(x + 1, y).traversalMask &= 65535 - 2;
                        } else if (dir == 6) {
                            getTileValue(x, y).traversalMask &= 0xfffe;
                            getTileValue(x, y - 1).traversalMask &= 65535 - 4;
                        }
                    }
                }
                break;
            case 1:
                if (o.getBoundaryDef().getBoundaryType() != 1) { // Closed door?
                    return;
                }
                int x = o.getX(), y = o.getY();
                if (dir == 0) {
                    getTileValue(x, y).traversalMask &= 0xfffe;
                    getTileValue(x, y - 1).traversalMask &= 65535 - 4;
                } else if (dir == 1) {
                    getTileValue(x, y).traversalMask &= 0xfffd;
                    getTileValue(x - 1, y).traversalMask &= 65535 - 8;
                } else if (dir == 2) {
                    getTileValue(x, y).traversalMask &= 0xffef;
                } else if (dir == 3) {
                    getTileValue(x, y).traversalMask &= 0xffdf;
                }
                break;
        }
    }

    public void unregisterNPC(NPC n) {
        if (npcs.contains(n))
            npcs.remove(n.getIndex());
        setLocation(n, n.getLocation(), null);
    }

    public boolean withinWorld(int x, int y) {
        return x >= 0 && x < MAX_WIDTH && y >= 0 && y < MAX_HEIGHT;
    }

    public boolean withinWorld(Point location) {
        return location.getX() >= 0 && location.getX() < MAX_WIDTH && location.getY() >= 0 && location.getY() < MAX_HEIGHT;
    }

    /**
     * loads all GameObject locations and adds each GameObject to the World
     */
    public void propagateGameObjects() {
        int count = 0;
        long start = System.nanoTime();
        try {
            count = DBConnection.getWorldQueue().execute(new SQLiteJob<Integer>() {
                @Override
                protected Integer job(SQLiteConnection conn) throws SQLiteException {
                    int count = 0;
                    if (!Server.getDbWorld().checkStatements(Server.getDbWorld().object_location))
                        Server.getDbWorld().loadStatements(conn);
                    while (Server.getDbWorld().object_location.step()) {
                        GameObjectLoc loc = new GameObjectLoc();
                        loc.id = Server.getDbWorld().object_location.columnInt(0);
                        loc.direction = Server.getDbWorld().object_location.columnInt(1);
                        loc.type = Server.getDbWorld().object_location.columnInt(2);
                        loc.x = Server.getDbWorld().object_location.columnInt(3);
                        loc.y = Server.getDbWorld().object_location.columnInt(4);
                        GameObject go = new GameObject(loc);
                        if (loc.getType() == 1)
                            go.setBoundary(true);
                        registerGameObject(go);
                        count++;
                    }
                    Server.getDbWorld().object_location.reset();
                    return count;
                }
            }).complete();
        } catch (SQLiteException e) {
            Logger.err("Exception caught while propagating GameObjects:", e);
        }
        Logger.outf("Loaded {} game objects in {}ms", count, (System.nanoTime() - start) / 1e6);
    }

    /**
     * loads all Item locations and adds each Item to the World
     */
    public void propagateItems() {
        int count = 0;
        long start = System.nanoTime();
        try {
            count = DBConnection.getWorldQueue().execute(new SQLiteJob<Integer>() {
                @Override
                protected Integer job(SQLiteConnection conn) throws SQLiteException {
                    int count = 0;
                    if (!Server.getDbWorld().checkStatements(Server.getDbWorld().item_location))
                        Server.getDbWorld().loadStatements(conn);
                    while (Server.getDbWorld().item_location.step()) {
                        DefaultEntityFactory.getInstance()
                                .newItem(new ItemLoc(Server.getDbWorld().item_location.columnInt(0),
                                        Server.getDbWorld().item_location.columnInt(1),
                                        Server.getDbWorld().item_location.columnInt(2),
                                        Server.getDbWorld().item_location.columnInt(3),
                                        Server.getDbWorld().item_location.columnInt(4)));
                        count++;
                    }
                    Server.getDbWorld().item_location.reset();
                    return count;
                }
            }).complete();
        } catch (SQLiteException e) {
            Logger.err("Exception caught while propagating Lootable Items:", e);
        }
        Logger.outf("Loaded {} ground items in {}ms", count, (System.nanoTime() - start) / 1e6);
    }

    /**
     * Load all npc locations.
     */
    public void propagateNPCs() {
        int npcCount = 0, spawnSize = 0;
        long start = System.nanoTime();
        try {
            Integer[] counts = DBConnection.getWorldQueue().execute(new SQLiteJob<Integer[]>() {
                @Override
                protected Integer[] job(SQLiteConnection conn) throws SQLiteException {
                    int npcCount = 0, spawnSize = 0;
                    if (!Server.getDbWorld().checkStatements(Server.getDbWorld().npc_location))
                        Server.getDbWorld().loadStatements(conn);
                    while (Server.getDbWorld().npc_location.step()) {
                        for (int i = 0; i < Server.getDbWorld().npc_location.columnInt(0); i++) {
                            NPCLoc loc = new NPCLoc();
                            loc.id = Server.getDbWorld().npc_location.columnInt(1);
                            loc.aggressive = Server.getDbWorld().npc_location.columnInt(2) == 1;
                            loc.respawnTime = Server.getDbWorld().npc_location.columnInt(3);
                            loc.npcCount = Server.getDbWorld().npc_location.columnInt(0);
                            loc.minX = Server.getDbWorld().npc_location.columnInt(4);
                            loc.minY = Server.getDbWorld().npc_location.columnInt(5);
                            loc.maxX = Server.getDbWorld().npc_location.columnInt(6);
                            loc.maxY = Server.getDbWorld().npc_location.columnInt(7);
                            NPC n = DefaultEntityFactory.getInstance().newNpc(loc);
                            n.setLocation(Point.location(0, 0, 0), true);
                            registerNPC(n);
                            npcCount++;
                        }
                        spawnSize++;
                    }
                    Server.getDbWorld().npc_location.reset();
                    return new Integer[]{npcCount, spawnSize};
                }
            }).complete();
            if (counts != null) {
                npcCount = counts[0];
                spawnSize = counts[1];
            }
        } catch (SQLiteException e) {
            Logger.err("Exception caught while propagating NPCs:", e);
        }
        Logger.outf("Loaded {} NPCs at {} spawns in {}ms", npcCount, spawnSize, (System.nanoTime() - start) / 1e6);
    }

    /**
     * Load all shops.
     */
    public void propagateShops() {
        int count = 0;
        long start = System.nanoTime();
        try {
            count = DBConnection.getWorldQueue().execute(new SQLiteJob<Integer>() {
                @Override
                protected Integer job(SQLiteConnection conn) throws SQLiteException {
                    int count = 0;
                    if (!Server.getDbWorld().checkStatements(Server.getDbWorld().shops, Server.getDbWorld().shopItems))
                        Server.getDbWorld().loadStatements(conn);
                    while (Server.getDbWorld().shops.step()) {
                        int id = Server.getDbWorld().shops.columnInt(0);
                        String name = Server.getDbWorld().shops.columnString(1);
                        int general = Server.getDbWorld().shops.columnInt(2);
                        List<InvItem> store_items = new ArrayList<>();
                        List<InvItem> buy_items = new ArrayList<>();
                        Server.getDbWorld().shopItems.bind(1, id);
                        while (Server.getDbWorld().shopItems.step()) {
                            int amount = Server.getDbWorld().shopItems.columnInt(0);
                            if (amount == 0)
                                buy_items.add(new InvItem(Server.getDbWorld().shopItems.columnInt(1), 0));
                            else
                                store_items.add(new InvItem(Server.getDbWorld().shopItems.columnInt(1),
                                        amount == -1 ? 1000 : amount));
                        }
                        Server.getDbWorld().shopItems.reset();
                        InvItem[] item = new InvItem[store_items.size() + buy_items.size()];
                        int index = 0;
                        for (; index < store_items.size(); index++) {
                            item[index] = store_items.get(index);
                        }
                        for (; (index - store_items.size()) < buy_items.size(); index++) {
                            item[index] = buy_items.get(index - store_items.size());
                        }
                        Shop shop = new Shop(general != 0, 3000, 100, 50, item);
                        registerShop(name, shop);
                        count++;
                    }
                    Server.getDbWorld().shops.reset();
                    return count;
                }
            }).complete();
        } catch (SQLiteException e) {
            Logger.err("Exception caught while propagating shops:", e);
        }
        Logger.outf("Loaded {} shops in {}ms", count, (System.nanoTime() - start) / 1e6);
    }
}
