package net.swiftpk.server.model;

import java.util.ArrayList;

public class TradeState {
    private boolean[] confirmed = new boolean[]{false, false};
    private boolean hasChanged = false;
    private ArrayList<InvItem> offer = new ArrayList<InvItem>();
    private Player wishTo = null;

    public TradeState(Player p) {
        reset();
    }

    public void addToOffer(InvItem item) {
        offer.add(item);
    }

    public ArrayList<InvItem> getOffer() {
        return offer;
    }

    public Player getWishTo() {
        return wishTo;
    }

    public boolean hasChanged() {
        return hasChanged;
    }

    public boolean isConfirmed(int i) {
        return confirmed[i - 1];
    }

    public void reset() {
        wishTo = null;
        confirmed = new boolean[]{false, false};
        resetOffer();
    }

    public void resetChanged() {
        hasChanged = false;
    }

    public void resetOffer() {
        offer.clear();
    }

    public void setChanged() {
        hasChanged = true;
    }

    public void setConfirmed(int i, boolean b) {
        confirmed[i - 1] = b;
    }

    public void setWishTo(Player p) {
        wishTo = p;
    }

    @Override
    public String toString() {
        return offer.toString();
    }
}
