package net.swiftpk.server.model;

import java.util.ArrayList;

public class DuelState {
    private boolean[] confirmed = new boolean[]{false, false};// 0 = no
    private boolean dueling = false;
    private boolean hasChanged = false;
    private ArrayList<InvItem> offer = new ArrayList<InvItem>();
    // retreating,
    // 1 = no
    // magic, 2
    // = no
    // prayer, 4
    // = no
    // items
    private boolean[] rules = new boolean[4];
    private Player wishTo = null;

    public DuelState(Player p) {
        reset();
    }

    public void addToOffer(InvItem item) {
        offer.add(item);
    }

    public ArrayList<InvItem> getOffer() {
        return offer;
    }

    public boolean getRule(int i) {
        return rules[i];
    }

    public boolean[] getRules() {
        return rules;
    }

    public Player getWishTo() {
        return wishTo;
    }

    public boolean hasChanged() {
        return hasChanged;
    }

    public boolean isConfirmed(int i) {
        return confirmed[i - 1];
    }

    public boolean isDueling() {
        return dueling;
    }

    public void reset() {
        rules = new boolean[4];
        java.util.Arrays.fill(rules, false);
        wishTo = null;
        dueling = false;
        confirmed = new boolean[]{false, false};
        resetOffer();
    }

    public void resetChanged() {
        hasChanged = false;
    }

    public void resetOffer() {
        offer.clear();
    }

    public void setChanged() {
        hasChanged = true;
    }

    public void setConfirmed(int i, boolean b) {
        confirmed[i - 1] = b;
    }

    public void setDueling(boolean b) {
        dueling = b;
    }

    public void setRule(int i, boolean b) {
        rules[i] = b;
    }

    public void setRules(boolean[] b) {
        rules = b;
    }

    public void setWishTo(Player p) {
        wishTo = p;
    }
}
