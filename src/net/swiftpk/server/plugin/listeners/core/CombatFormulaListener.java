package net.swiftpk.server.plugin.listeners.core;

import net.swiftpk.server.model.entity.Mob;
import net.swiftpk.server.plugin.listeners.PluginListener;

public interface CombatFormulaListener extends PluginListener {
    public int calculateCombatHit(Mob att, Mob def);
}