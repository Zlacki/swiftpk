package net.swiftpk.server.plugin.listeners;

import net.swiftpk.server.model.Bubble;
import net.swiftpk.server.model.ChatMessage;
import net.swiftpk.server.model.Entity;
import net.swiftpk.server.model.InvItem;
import net.swiftpk.server.model.MobState;
import net.swiftpk.server.model.Player;
import net.swiftpk.server.model.QueryMenu;
import net.swiftpk.server.model.Shop;
import net.swiftpk.server.model.component.Communication;
import net.swiftpk.server.model.entity.GameObject;
import net.swiftpk.server.model.entity.Mob;
import net.swiftpk.server.model.entity.NPC;
import net.swiftpk.server.model.entity.mob.NPCChatMessage;
import net.swiftpk.server.model.terrain.World;
import net.swiftpk.server.model.terrain.tiles.Point;
import net.swiftpk.server.net.ActionSender;
import net.swiftpk.server.plugin.api.QueryBuilder;
import net.swiftpk.server.plugin.listeners.actors.players.QueryMenuListener;
import net.swiftpk.server.event.Event;
import net.swiftpk.server.event.impl.ObjectSpawnEvent;
import net.swiftpk.server.util.Formulae;

public interface PluginListener {
    public default int bankCount(Player player, int id) {
        return player.getBank().countId(id);
    }

    public default void removeItemFromBank(Player player, int id, int amount) {
        player.getBank().remove(id, amount);
    }

    public default void sendMessage(Player player, String message) {
        ActionSender.sendMessage(player, message);
    }

    public default void addItem(Player player, int id, int amount) {
        player.getInventory().add(new InvItem(id, amount));
    }

    public default void interactNPC(final Player player, final NPC npc, String question,
                                    final QueryBuilder... queryBuilder) {
        player.setState(MobState.Talking);
        npc.setState(MobState.Talking);
        if (question != null && question.length() > 1)
            npc.setChatMessage(new NPCChatMessage(npc, question, player));
        String[] options = new String[queryBuilder.length];
        if (queryBuilder != null) {
            for (int index = 0; index < options.length; index++) {
                options[index] = queryBuilder[index].getMessage();
            }
            QueryMenuListener listener = new QueryMenuListener() {
                @Override
                public void choseQuery(Player p, int id) {
                    if (id <= queryBuilder.length) {
                        final QueryBuilder builder = queryBuilder[id];
                        if (builder != null) {
                            playerChatNPC(player, npc, false, builder.getMessage());
                            World.getInstance().submit(new Event(2200) {
                                public void execute() {
                                    player.setState(MobState.Idle);
                                    npc.setState(MobState.Idle);
                                    builder.getAction().onAction();
                                    stop();
                                }
                            });
                        }
                        return;
                    }
                }
            };
            query(player, listener, options);
        }
        player.setState(MobState.Idle);
        npc.setState(MobState.Idle);
    }

    public default void interactNPC(final Player player, final NPC npc, String[] question,
                                    final QueryBuilder... queryBuilder) {
        player.setState(MobState.Talking);
        npc.setState(MobState.Talking);
        for (int i = 0; i < question.length; i++) {
            final boolean finalMessage = i == question.length - 1;
            final String message = question[i];
            World.getInstance().submit(new Event(2200 * i) {
                public void execute() {
                    npc.setChatMessage(new NPCChatMessage(npc, message, player));
                    if (finalMessage) {
                        if (queryBuilder != null) {
                            String[] options = new String[queryBuilder.length];
                            for (int index = 0; index < options.length; index++) {
                                options[index] = queryBuilder[index].getMessage();
                            }
                            QueryMenuListener listener = new QueryMenuListener() {
                                @Override
                                public void choseQuery(Player p, int id) {
                                    if (id <= queryBuilder.length) {
                                        final QueryBuilder builder = queryBuilder[id];
                                        if (builder != null) {
                                            playerChatNPC(player, npc, false, builder.getMessage());
                                            World.getInstance().submit(new Event(2200) {
                                                public void execute() {
                                                    player.setState(MobState.Idle);
                                                    npc.setState(MobState.Idle);
                                                    builder.getAction().onAction();
                                                    stop();
                                                }
                                            });
                                        }
                                        return;
                                    }
                                }
                            };
                            World.getInstance().submit(new Event(2200) {
                                public void execute() {
                                    query(player, listener, options);
                                    player.setState(MobState.Idle);
                                    npc.setState(MobState.Idle);
                                    stop();
                                }
                            });
                        }
                    }
                    stop();
                }
            });
        }
    }

    public default int inventoryCount(Player player, int id) {
        return player.getInventory().countById(id);
    }

    public default void message(Player player, String message) {
        ActionSender.sendMessage(player, message);
    }

    public default void message(final Player player, final String... messages) {
        for (int i = 0; i < messages.length; i++) {
            final String message = messages[i];
            World.getInstance().submit(new Event(2200 * i) {
                public void execute() {
                    ActionSender.sendMessage(player, message);
                    stop();
                }
            });
        }
    }

    public default void move(Player player, int x, int y, int layer) {
        player.setLocation(new Point(x, y, layer), true);
        player.resetPath();
        ActionSender.sendHeight(player);
    }

    public default void npcChatPlayer(final NPC npc, final Player player, String... messages) {
        npcChatPlayer(npc, player, true, messages);
    }

    public default void npcChatPlayer(final NPC npc, final Player player, boolean resetState, String... messages) {
        player.setState(MobState.Talking);
        npc.setState(MobState.Talking);
        for (int i = 0; i < messages.length; i++) {
            final int index = i;
            World.getInstance().submit(new Event(2200 * i) {
                public void execute() {
                    npc.setChatMessage(new NPCChatMessage(npc, messages[index], player));
                    if (index == messages.length - 1 && resetState) {
                        player.setState(MobState.Idle);
                        npc.setState(MobState.Idle);
                    }
                    stop();
                }
            });
        }
    }

    public default void playerChatNPC(Player sender, NPC npc, String... messages) {
        playerChatNPC(sender, npc, true, messages);
    }

    public default void playerChatNPC(Player sender, NPC npc, boolean resetState, String... messages) {
        sender.setState(MobState.Talking);
        npc.setState(MobState.Talking);
        for (int i = 0; i < messages.length; i++) {
            final int index = i;
            World.getInstance().submit(new Event(2200 * i) {
                public void execute() {
                    for (Player player : sender.getLocalPlayerList())
                        player.getComponent(Communication.class)
                                .pushMessage(new ChatMessage(sender, messages[index], npc));
                    sender.getComponent(Communication.class).pushMessage(new ChatMessage(sender, messages[index], npc));
                    if (index == messages.length - 1 && resetState) {
                        sender.setState(MobState.Idle);
                        npc.setState(MobState.Idle);
                    }
                    stop();
                }
            });
        }
    }

    public default void query(Player player, QueryMenuListener listener, String... options) {
        QueryMenu menu = new QueryMenu(player, options);
        menu.addListener(listener);
        menu.send();
        player.setQueryMenu(menu);
    }

    public default void query(Player player, String... options) {
        QueryMenu menu = new QueryMenu(player, options);
        menu.addListener((QueryMenuListener) this);
        menu.send();
        player.setQueryMenu(menu);
    }

    public default void removeItem(Player player, int id, int amount) {
        player.getInventory().remove(id, amount);
        ActionSender.sendInventory(player);
    }

    public default void respawnObject(GameObject object, int replacementID, int delay) {
        GameObject spawned = new GameObject(object.getLocation(), replacementID, 0, 0);
        World.getInstance().registerGameObject(spawned);
        World.getInstance().submit(new ObjectSpawnEvent(object.getLoc(), 5000));
    }

    public default void store(Player player, Shop shop) {
        if (shop != null) {
            player.setState(MobState.Shopping);
            player.setShop(shop);
            ActionSender.sendShop(player, shop);
        }
    }

    public default void faceEntity(Mob mob, Entity entity) {
        int dir = Formulae.getDirection(mob, entity);
        if (dir != -1) {
            mob.setSprite(dir);
            mob.setRequiresSpriteUpdate(true);
        }
    }

    public default void showBubble(Player player, InvItem item) {
        player.setItemBubble(new Bubble(player, item.getID()));
    }

    public default void sleep(final int delay) {
        try {
            if (Thread.currentThread().getName().toLowerCase().contains("gameengine"))
                return;
            // System.out.println("Sleeping on " +
            // Thread.currentThread().getName().toLowerCase());
            Thread.sleep(delay);
        } catch (final InterruptedException e) {
        }
    }

}
