package net.swiftpk.server.plugin.listeners.actors.npcs;

import net.swiftpk.server.model.Player;
import net.swiftpk.server.model.entity.NPC;
import net.swiftpk.server.plugin.listeners.PluginListener;

public interface TalkToNPCListener extends PluginListener {
    public void execute(NPC npc, Player player);

    public boolean isExecutable(NPC npc);
}
