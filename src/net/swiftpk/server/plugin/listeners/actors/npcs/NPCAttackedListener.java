/*
 *  Copyright (C) CodexRSC LTD, Inc - All Rights Reserved
 *  * Unauthorized copying of this file, via any medium is strictly prohibited
 *  * Proprietary and confidential
 *  * Written by Mark Gore <mark@codexrsc.info>, 19/03/15 15:42
 *  * Last Modified by Mark on 19/03/15 10:43
 *
 */
package net.swiftpk.server.plugin.listeners.actors.npcs;

import net.swiftpk.server.model.Player;
import net.swiftpk.server.model.entity.NPC;
import net.swiftpk.server.plugin.listeners.PluginListener;

/**
 * @author Mark
 */
public interface NPCAttackedListener extends PluginListener {
    /**
     *
     */
    public void execute(NPC npc, Player player);

    /**
     * @return
     */
    public boolean isExecutable(NPC npc);
}
