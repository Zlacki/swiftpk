/*
 *  Copyright (C) CodexRSC LTD, Inc - All Rights Reserved
 *  * Unauthorized copying of this file, via any medium is strictly prohibited
 *  * Proprietary and confidential
 *  * Written by Mark Gore <mark@codexrsc.info>, 19/03/15 16:39
 *  * Last Modified by Mark on 19/03/15 16:38
 *
 */
package net.swiftpk.server.plugin.listeners.actors.players;

import net.swiftpk.server.model.Player;
import net.swiftpk.server.model.terrain.tiles.Point;
import net.swiftpk.server.plugin.listeners.PluginListener;

/**
 * Created by Mark on 19/03/2015.
 */
public interface PlayerDeathPointListener extends PluginListener {
    public Point execute(Player player);

    public boolean isExecutable(Player player);
}
