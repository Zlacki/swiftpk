/*
 *  Copyright (C) CodexRSC LTD, Inc - All Rights Reserved
 *  * Unauthorized copying of this file, via any medium is strictly prohibited
 *  * Proprietary and confidential
 *  * Written by Mark Gore <mark@codexrsc.info>, 19/03/15 16:31
 *  * Last Modified by Mark on 19/03/15 16:31
 *
 */
package net.swiftpk.server.plugin.listeners.actors.players;

import net.swiftpk.server.model.Player;
import net.swiftpk.server.plugin.listeners.PluginListener;

/**
 * Created by Mark on 19/03/2015.
 */
public interface PlayerDeathListener extends PluginListener {
    public void execute(Player killedBy, Player player, boolean dueling);

    public boolean isExecutable(Player killedBy, Player player);
}
