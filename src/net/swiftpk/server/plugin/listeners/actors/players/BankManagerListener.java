/*
 *  Copyright (C) CodexRSC LTD, Inc - All Rights Reserved
 *  * Unauthorized copying of this file, via any medium is strictly prohibited
 *  * Proprietary and confidential
 *  * Written by Mark Gore <mark@codexrsc.info>, 19/03/15 16:24
 *  * Last Modified by Mark on 19/03/15 16:24
 *
 */
package net.swiftpk.server.plugin.listeners.actors.players;

import net.swiftpk.server.model.Player;
import net.swiftpk.server.plugin.listeners.PluginListener;

/**
 * Created by Mark on 19/03/2015.
 */
public interface BankManagerListener extends PluginListener {
    public void close(Player player);

    public void deposit(Player player, int id, int itemID, int itemAmount);

    public void withdraw(Player player, int id, int itemID, int itemAmount);
}
