/*
 *  Copyright (C) CodexRSC LTD, Inc - All Rights Reserved
 *  * Unauthorized copying of this file, via any medium is strictly prohibited
 *  * Proprietary and confidential
 *  * Written by Mark Gore <mark@codexrsc.info>, 19/03/15 16:10
 *  * Last Modified by Mark on 19/03/15 16:10
 *
 */
package net.swiftpk.server.plugin.listeners.object;

import net.swiftpk.server.model.Player;
import net.swiftpk.server.model.entity.GameObject;
import net.swiftpk.server.plugin.listeners.PluginListener;

/**
 * Created by Mark on 19/03/2015.
 */
public interface ObjectActionListener extends PluginListener {
    public void execute(Player player, GameObject object);

    public boolean isExecutable(Player player, GameObject object);
}
