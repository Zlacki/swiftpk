package net.swiftpk.server.plugin.listeners.packets;

import io.netty.channel.Channel;
import net.swiftpk.server.net.Packet;
import net.swiftpk.server.plugin.listeners.PluginListener;

public interface PacketListener extends PluginListener {
    public void execute(Packet p, Channel session);

    public boolean isExecutable(Packet p);
}