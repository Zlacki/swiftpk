/*
 *  Copyright (C) CodexRSC LTD, Inc - All Rights Reserved
 *  * Unauthorized copying of this file, via any medium is strictly prohibited
 *  * Proprietary and confidential
 *  * Written by Mark Gore <mark@codexrsc.info>, 19/03/15 11:26
 *  * Last Modified by Mark on 19/03/15 11:26
 *
 */
package net.swiftpk.server.plugin.listeners.loggers;

import java.util.concurrent.atomic.AtomicReference;

import net.swiftpk.server.model.Player;
import net.swiftpk.server.plugin.listeners.PluginListener;
import net.swiftpk.server.plugin.listeners.packets.PacketListener;

/**
 * Created by Mark on 19/03/2015.
 */
public interface PacketLogger extends PluginListener {
    public void logPackets(AtomicReference<Player> player,
                           PacketListener listener, long time);
}
