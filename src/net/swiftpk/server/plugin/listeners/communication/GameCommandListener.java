/*
 *  Copyright (C) CodexRSC LTD, Inc - All Rights Reserved
 *  * Unauthorized copying of this file, via any medium is strictly prohibited
 *  * Proprietary and confidential
 *  * Written by Mark Gore <mark@codexrsc.info>, 19/03/15 15:45
 *  * Last Modified by Mark on 19/03/15 15:42
 *
 */
package net.swiftpk.server.plugin.listeners.communication;

import net.swiftpk.server.model.Player;
import net.swiftpk.server.plugin.listeners.PluginListener;

/**
 * @author Mark
 */
public interface GameCommandListener extends PluginListener {
    /**
     *
     */
    public void execute(Player player, String[] arguments, String raw);

    /**
     * @return
     */
    public boolean isExecutable(Player player, String command);
}
