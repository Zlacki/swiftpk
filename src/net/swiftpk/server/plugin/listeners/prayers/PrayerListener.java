/*
 *  Copyright (C) CodexRSC LTD, Inc - All Rights Reserved
 *  * Unauthorized copying of this file, via any medium is strictly prohibited
 *  * Proprietary and confidential
 *  * Written by Mark Gore <mark@codexrsc.info>, 19/03/15 11:03
 *  * Last Modified by Mark on 19/03/15 11:03
 *
 */
package net.swiftpk.server.plugin.listeners.prayers;

import net.swiftpk.server.ent.EntityHandler;
import net.swiftpk.server.ent.defs.PrayerDef;
import net.swiftpk.server.model.Player;
import net.swiftpk.server.model.component.SkillSet;
import net.swiftpk.server.net.ActionSender;
import net.swiftpk.server.plugin.listeners.PluginListener;

/**
 * Created by Mark on 19/03/2015.
 */
public interface PrayerListener extends PluginListener {
    public void activatePrayer(Player player, int prayerid);

    public void deactivatePrayer(Player player, int prayerid);

    public default boolean doDeactivate(Player player, int prayerID) {
        if (player.isPrayerActivated(prayerID)) {
            player.setPrayer(prayerID, false);
            player.removePrayerDrain(prayerID);
            return true;
        }
        return false;
    }

    /**
     * @param id - prayer id
     * @return the prayer composite
     */
    public default PrayerDef getPrayer(int id) {
        return EntityHandler.getPrayerDef(id);
    }

    public default int getPrayerCur(Player player) {
        return player.getSkillSet().get(SkillSet.PRAYER).getLevel();
    }

    public default int getPrayerMax(Player player) {
        return player.getSkillSet().get(SkillSet.PRAYER).getMaxLevel();
    }

    public default void send(Player player) {
        ActionSender.sendPrayers(player);
    }

    public default boolean isDueling(Player player) {
        return player.isDueling() && !player.canPray();
    }

    public default boolean doActivatePrayer(Player player, int prayerID) {
        if (!player.isPrayerActivated(prayerID)) {
            if (prayerID == 11) {
                doDeactivate(player, 5);
                doDeactivate(player, 2);
            } else if (prayerID == 5) {
                doDeactivate(player, 2);
                doDeactivate(player, 11);
            } else if (prayerID == 2) {
                doDeactivate(player, 5);
                doDeactivate(player, 11);
            } else if (prayerID == 10) {
                doDeactivate(player, 4);
                doDeactivate(player, 1);
            } else if (prayerID == 4) {
                doDeactivate(player, 10);
                doDeactivate(player, 1);
            } else if (prayerID == 1) {
                doDeactivate(player, 10);
                doDeactivate(player, 4);
            } else if (prayerID == 9) {
                doDeactivate(player, 3);
                doDeactivate(player, 0);
            } else if (prayerID == 3) {
                doDeactivate(player, 9);
                doDeactivate(player, 0);
            } else if (prayerID == 0) {
                doDeactivate(player, 9);
                doDeactivate(player, 3);
            }
            player.setPrayer(prayerID, true);
            player.addPrayerDrain(prayerID);
            return true;
        }
        return false;
    }
}
