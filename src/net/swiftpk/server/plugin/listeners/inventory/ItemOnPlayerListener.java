package net.swiftpk.server.plugin.listeners.inventory;

import net.swiftpk.server.model.InvItem;
import net.swiftpk.server.model.Player;
import net.swiftpk.server.plugin.listeners.PluginListener;

public interface ItemOnPlayerListener extends PluginListener {
    public void execute(Player player, InvItem item, Player affectedPlayer);

    public boolean isExecutable(Player player, InvItem item, Player affectedPlayer);
}
