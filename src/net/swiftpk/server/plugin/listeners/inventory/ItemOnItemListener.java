/*
 *  Copyright (C) CodexRSC LTD, Inc - All Rights Reserved
 *  * Unauthorized copying of this file, via any medium is strictly prohibited
 *  * Proprietary and confidential
 *  * Written by Mark Gore <mark@codexrsc.info>, 19/03/15 10:53
 *  * Last Modified by Mark on 19/03/15 10:53
 *
 */
package net.swiftpk.server.plugin.listeners.inventory;

import net.swiftpk.server.model.InvItem;
import net.swiftpk.server.model.Player;
import net.swiftpk.server.plugin.listeners.PluginListener;

/**
 * Created by Mark on 19/03/2015.
 */
public interface ItemOnItemListener extends PluginListener {
    public void execute(Player player, InvItem item, InvItem item2);

    public boolean isExecutable(Player player, InvItem item, InvItem item2);
}
