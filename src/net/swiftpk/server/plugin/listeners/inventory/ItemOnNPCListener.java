/*
 *  Copyright (C) CodexRSC LTD, Inc - All Rights Reserved
 *  * Unauthorized copying of this file, via any medium is strictly prohibited
 *  * Proprietary and confidential
 *  * Written by Mark Gore <mark@codexrsc.info>, 19/03/15 10:56
 *  * Last Modified by Mark on 19/03/15 10:55
 *
 */
package net.swiftpk.server.plugin.listeners.inventory;

import net.swiftpk.server.model.InvItem;
import net.swiftpk.server.model.Player;
import net.swiftpk.server.model.entity.NPC;
import net.swiftpk.server.plugin.listeners.PluginListener;

/**
 * Created by Mark on 19/03/2015.
 */
public interface ItemOnNPCListener extends PluginListener {
    public void execute(Player player, InvItem item, NPC npc);

    public boolean isExecutable(Player player, InvItem item, NPC npc);
}
