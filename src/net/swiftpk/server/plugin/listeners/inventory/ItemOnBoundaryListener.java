/*
 *  Copyright (C) CodexRSC LTD, Inc - All Rights Reserved
 *  * Unauthorized copying of this file, via any medium is strictly prohibited
 *  * Proprietary and confidential
 *  * Written by Mark Gore <mark@codexrsc.info>, 19/03/15 10:52
 *  * Last Modified by Mark on 19/03/15 10:52
 *
 */
package net.swiftpk.server.plugin.listeners.inventory;

import net.swiftpk.server.model.InvItem;
import net.swiftpk.server.model.Player;
import net.swiftpk.server.model.entity.GameObject;
import net.swiftpk.server.plugin.listeners.PluginListener;

/**
 * Created by Mark on 19/03/2015.
 */
public interface ItemOnBoundaryListener extends PluginListener {
    public void execute(Player player, InvItem item, GameObject object);

    public boolean isExecutable(InvItem item, GameObject object);
}
