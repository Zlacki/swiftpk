package net.swiftpk.server.plugin.api;

public class QueryBuilder {
    public interface QueryAction {
        public void onAction();
    }

    private QueryAction action;
    private String message;

    public QueryBuilder(String message, QueryAction queryAction) {
        this.setMessage(message);
        this.setAction(queryAction);
    }

    public QueryAction getAction() {
        return action;
    }

    public String getMessage() {
        return message;
    }

    public void setAction(QueryAction action) {
        this.action = action;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}