package net.swiftpk.server.plugin.model;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicReference;

import net.swiftpk.server.plugin.listeners.PluginListener;
import net.swiftpk.server.plugin.various.ActionListeners;
import net.swiftpk.server.plugin.various.PluginClassLoader;

public class PluginModule {
    /**
     * Contains all the plugin classes and listeners
     */
    private final AtomicReference<Map<Class<? extends PluginListener>, List<PluginListener>>> plugins = new AtomicReference<>(
            new HashMap<Class<? extends PluginListener>, List<PluginListener>>());
    private final long crc32;
    private String name;

    /**
     * @param pluginLoader
     */
    @SuppressWarnings("unchecked")
    public PluginModule(PluginClassLoader pluginLoader, String name, long crc32) {
        this.crc32 = crc32;
        this.name = name;
        for (Class<?> pluginClass : pluginLoader.getAllLoadedClasses()) {
            try {
                if (pluginClass.getInterfaces().length > 0 && !pluginClass.getName().contains("$")
                        && pluginClass.getConstructors().length > 0) {
                    PluginListener plugin = (PluginListener) pluginClass.getConstructors()[0].newInstance();
                    ActionListeners.classes.stream().filter(klass -> klass.isAssignableFrom(pluginClass))
                            .forEach(klass -> {
                                if (getPlugins().get((Class<? extends PluginListener>) klass) == null) {
                                    List<PluginListener> l = new ArrayList<>();
                                    l.add(plugin);
                                    getPlugins().put((Class<? extends PluginListener>) klass, l);
                                } else
                                    getPlugins().get((Class<? extends PluginListener>) klass).add(plugin);
                            });
                }
            } catch (InstantiationException | IllegalAccessException | IllegalArgumentException
                    | InvocationTargetException | SecurityException e) {
                e.printStackTrace();
            }
        }
    }

    public boolean isPlugin(Class<?> pluginClass) {
        for (Class<?> inter : pluginClass.getClass().getInterfaces()) {
            if (inter.isAssignableFrom(PluginListener.class)) {
                return true;
            }
        }
        return false;
    }

    public <T extends PluginListener> T get(Class<T> type) {
        return type.cast(getPlugins().get(type));
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        return builder.toString();
    }

    public Map<Class<? extends PluginListener>, List<PluginListener>> getPlugins() {
        return plugins.get();
    }

    public <T> List<T> getPlugins(Class<T> type) {
        List<T> list = new ArrayList<T>();
        getPlugins().forEach((key, value) -> {
            if (key.isAssignableFrom(type))
                for (PluginListener l : value)
                    list.add(type.cast(l));
        });
        return list;
    }

    public String getName() {
        return name;
    }

    public long getCrc32() {
        return crc32;
    }
}
