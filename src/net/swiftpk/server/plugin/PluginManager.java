/*
 *  Copyright (C) CodexRSC LTD, Inc - All Rights Reserved
 *  * Unauthorized copying of this file, via any medium is strictly prohibited
 *  * Proprietary and confidential
 *  * Written by Mark Gore <mark@codexrsc.info>, 19/03/15 14:21
 *  * Last Modified by Mark on 19/03/15 14:20
 *
 */
package net.swiftpk.server.plugin;

import java.io.*;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;
import java.util.jar.JarInputStream;
import java.util.zip.CRC32;

import net.swiftpk.server.plugin.listeners.PluginListener;
import net.swiftpk.server.plugin.model.PluginModule;
import net.swiftpk.server.plugin.various.PluginClassLoader;

import static net.swiftpk.server.plugin.various.ActionListeners.classesEquals;

/**
 * @author Mark
 */
public class PluginManager {
    private static final AtomicReference<List<PluginModule>> modules = new AtomicReference<>(
            new ArrayList<>());
    private static final File dir = new File("plugins");

    public PluginManager() throws IOException {
        for (File file : dir.listFiles()) {
            add(file);
        }
    }

    public static void reload() throws IOException {
        for (File file : dir.listFiles()) {
            PluginModule module = getModule(file.getName());
            if (module == null) {
                add(file);
            } else {
                if (getCache(file.toString()) != module.getCrc32()) {
                    modules.get().remove(module);
                    add(file);
                    System.out.println("Shit it's not the same! "
                            + file.getName());
                }
            }
        }
    }

    private static boolean add(File file) throws IOException {
        FileInputStream fis = new FileInputStream(file);
        PluginClassLoader loader = new PluginClassLoader(
                new JarInputStream(fis));
        return modules.get().add(
                new PluginModule(loader, file.getName(), getCache(file
                        .toString())));
    }

    private static PluginModule getModule(String name) {
        for (PluginModule module : modules.get()) {
            if (module.getName().equals(name)) {
                return module;
            }
        }
        return null;
    }

    public static <T> List<T> getAll(Class<T> type) {
        List<T> test = new ArrayList<>();
        for (PluginModule module : modules.get()) {
            for (T value : module.getPlugins(type)) {
                if (value != null) {
                    test.add(value);
                }
            }
        }
        return test;
    }

    public static <T extends PluginListener> T get(Class<T> type) {
        for (PluginModule module : modules.get()) {
            for (T value : module.getPlugins(type)) {
                if (value != null) {
                    return value;
                }
            }
        }
        return null;
    }

    public static <T extends PluginListener> T get(Class<T> type,
                                                   Object... values) {
        for (PluginModule module : modules.get()) {
            for (T castType : module.getPlugins(type)) {
                if (castType != null)
                    for (Class<?> klass : classesEquals) {
                        if (klass.isAssignableFrom(type)) {
                            for (Class<?> inter : castType.getClass()
                                    .getInterfaces()) {
                                if (type.equals(inter)) {
                                    try {
                                        int index = 0;
                                        Class<?>[] dataClasses = new Class<?>[values.length];
                                        for (Object o : values) {
                                            dataClasses[index++] = o.getClass();
                                        }
                                        AtomicReference<Method> registerMethod = new AtomicReference<>(
                                                castType.getClass()
                                                        .getDeclaredMethod(
                                                                "isExecutable",
                                                                dataClasses));
                                        registerMethod.get()
                                                .setAccessible(true);
                                        if ((boolean) registerMethod.get()
                                                .invoke(castType, values)) {
                                            return castType;
                                        }
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                            }
                        }
                    }
            }
        }
        return null;
    }

    private static long getCache(String filepath) {
        try {
            InputStream inputStream = new BufferedInputStream(
                    new FileInputStream(filepath));
            CRC32 crc = new CRC32();
            int cnt;
            while ((cnt = inputStream.read()) != -1) {
                crc.update(cnt);
            }
            inputStream.close();
            return crc.getValue();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return 0;
    }
}
