/*
 *  Copyright (C) CodexRSC LTD, Inc - All Rights Reserved
 *  * Unauthorized copying of this file, via any medium is strictly prohibited
 *  * Proprietary and confidential
 *  * Written by Mark Gore <mark@codexrsc.info>, 19/03/15 11:01
 *  * Last Modified by Mark on 19/03/15 11:00
 *
 */
package net.swiftpk.server.plugin.various;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import net.swiftpk.server.plugin.listeners.actors.npcs.NPCAttackedListener;
import net.swiftpk.server.plugin.listeners.actors.npcs.NPCDeathListener;
import net.swiftpk.server.plugin.listeners.actors.npcs.NPCRangedListener;
import net.swiftpk.server.plugin.listeners.actors.npcs.TalkToNPCListener;
import net.swiftpk.server.plugin.listeners.actors.players.*;
import net.swiftpk.server.plugin.listeners.boundary.BoundaryActionListener;
import net.swiftpk.server.plugin.listeners.boundary.BoundarySecondActionListener;
import net.swiftpk.server.plugin.listeners.communication.GameCommandListener;
import net.swiftpk.server.plugin.listeners.core.CombatFormulaListener;
import net.swiftpk.server.plugin.listeners.core.GameCoreListener;
import net.swiftpk.server.plugin.listeners.inventory.*;
import net.swiftpk.server.plugin.listeners.loggers.PacketLogger;
import net.swiftpk.server.plugin.listeners.object.ObjectActionListener;
import net.swiftpk.server.plugin.listeners.object.ObjectSecondActionListener;
import net.swiftpk.server.plugin.listeners.packets.PacketListener;
import net.swiftpk.server.plugin.listeners.prayers.PrayerListener;

/**
 * @author Mark
 */
public class ActionListeners {
    public static List<Class<?>> classes = new ArrayList<>();
    public static List<Class<?>> classesEquals = new ArrayList<>();

    static {
        classes.add(TalkToNPCListener.class);
        classes.add(NPCAttackedListener.class);
        classes.add(NPCRangedListener.class);
        classes.add(NPCDeathListener.class);
        /**
         * The item on scenes, items and etc
         */
        classes.add(ItemActionListener.class);
        classes.add(ItemOnBoundaryListener.class);
        classes.add(ItemOnItemListener.class);
        classes.add(ItemOnLootableListener.class);
        classes.add(ItemOnGameObjectListener.class);
        classes.add(ItemOnNPCListener.class);
        classes.add(ItemOnPlayerListener.class);
        classes.add(PrayerListener.class);
        classes.add(PacketLogger.class);
        classes.add(PacketListener.class);
        classes.add(CombatFormulaListener.class);
        classes.add(GameCommandListener.class);
        classes.add(BoundaryActionListener.class);
        classes.add(BoundarySecondActionListener.class);
        classes.add(ObjectActionListener.class);
        classes.add(ObjectSecondActionListener.class);
        classes.add(GameCoreListener.class);
        classes.add(QueryMenuListener.class);
        classes.add(PlayerAttackableEvent.class);
        classes.add(PlayerDeathListener.class);
        classes.add(PlayerDeathPointListener.class);
        classes.add(PlayerTeleportBlockListener.class);
        classes.add(PlayerLogoutListener.class);
        classes.add(PlayerLoginListener.class);
        classes.add(BankManagerListener.class);
        for (Class<?> klass : classes) {
            if (isExecutable(klass.getMethods())) {
                classesEquals.add(klass);
            }
        }
    }

    private static boolean isExecutable(Method[] method) {
        for (Method m : method) {
            if (m.getName().equals("isExecutable"))
                return true;
        }
        return false;
    }
}
