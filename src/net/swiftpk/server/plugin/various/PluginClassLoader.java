/*
 *  Copyright (C) CodexRSC LTD, Inc - All Rights Reserved
 *  * Unauthorized copying of this file, via any medium is strictly prohibited
 *  * Proprietary and confidential
 *  * Written by Mark Gore <mark@codexrsc.info>, 19/03/15 10:45
 *  * Last Modified by Mark on 18/03/15 12:09
 *
 */
package net.swiftpk.server.plugin.various;

import java.io.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.jar.JarEntry;
import java.util.jar.JarInputStream;

/**
 * @author Mark
 * <p>
 * Loads all the data from the plugin.jar
 */
public class PluginClassLoader extends ClassLoader {
    private HashMap<String, Object[]> classData = new HashMap<String, Object[]>();
    private HashMap<String, Class<?>> classes = new HashMap<String, Class<?>>();
    public Class<?> mainClass;

    public PluginClassLoader(JarInputStream file) {
        try {
            JarEntry entry;
            while ((entry = file.getNextJarEntry()) != null) {
                if (entry.getName().endsWith(".class")) {
                    String name = entry.getName();
                    byte[] data;
                    final int length = (int) entry.getSize();
                    if (length == -1) {
                        final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                        byte[] buffer = new byte[1024];
                        int ret;
                        do {
                            ret = file.read(buffer, 0, 1024);
                            if (ret > 0) {
                                byteArrayOutputStream.write(buffer, 0, ret);
                            }
                        }
                        while (ret > 0);
                        data = byteArrayOutputStream.toByteArray();
                    } else {
                        int offset = 0;
                        data = new byte[length];
                        int ret;
                        do {
                            ret = file.read(data, offset, length - offset);
                            offset += ret;
                        }
                        while (ret > 0);
                    }
                    String shortName = name.replaceAll("\\.class", "");
                    String validName = shortName.replaceAll("/", ".");
                    validName = validName.replaceAll("\\\\", ".");
                    classData.put(validName, new Object[]{validName, data});
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        for (Object[] data : this.classData.values()) {
            String name = (String) data[0];
            if (classes.containsKey(name)) {
                continue;
            }
            byte[] bytes = (byte[]) data[1];
            Class<?> klass = ourDefineClass(name, bytes, 0, bytes.length);
            classes.put(name, klass);
        }
    }

    @Override
    protected Class<?> findClass(String name) throws ClassNotFoundException {
        if (classes.containsKey(name)) {
            return classes.get(name);
        } else if (classData.containsKey(name)) {
            Object[] data = classData.get(name);
            byte[] bytes = (byte[]) data[1];
            Class<?> aClass = ourDefineClass(name, bytes, 0, bytes.length);
            classes.put(name, aClass);
            return aClass;
        }
        return super.findClass(name);
    }

    public List<Class<?>> getAllLoadedClasses() {
        Class<?>[] test = classes.values().toArray(new Class[classes.size()]);
        List<Class<?>> loaded = new ArrayList<>();
        for (int index = 0; index < test.length; index++) {
            loaded.add(test[index]);
        }
        return loaded;
    }

    public Class<?> loadMainClass() {
        return mainClass;
    }

    private Class<?> ourDefineClass(String name, byte[] b, int off, int len) {
        return defineClass(name, b, off, len, null);
    }
}