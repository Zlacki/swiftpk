package api.predef

import net.swiftpk.server.kplugin.event.EventListener
import net.swiftpk.server.kplugin.event.EventMatcherListener
import net.swiftpk.server.model.MobState
import net.swiftpk.server.model.Player
import net.swiftpk.server.model.component.SkillSet
import net.swiftpk.server.model.component.SkillSet.Skill
import net.swiftpk.server.kplugin.KotlinBindings
import net.swiftpk.server.kplugin.PluginBootstrap
import net.swiftpk.server.util.Rational
import net.swiftpk.server.util.ReflectionUtils

/**
 * The Kotlin bindings. Not accessible to scripts.
 */
private val bindings: KotlinBindings = ReflectionUtils.getStaticField(
	PluginBootstrap::class.java,
	"bindings",
	KotlinBindings::class.java
)

/**
 * The [Server] instance.
 */
val ctx = bindings.ctx!!

/**
 * The script event listeners.
 */
val scriptListeners: MutableList<EventListener<*>> = bindings.listeners!!

/**
 * The script event listeners.
 */
val scriptMatchers: MutableList<EventMatcherListener<*>> = bindings.matchers!!

/**
 * The [EventListenerPipelineSet] instance.
 */
val pipelines = bindings.pipelines!!

/**
 * The [PluginManger] instance.
 */
val plugins = ctx.plugins!!

/**
 * The [World] instance.
 */
val world = ctx.world!!

/**
 * The [GameService] instance.
 */
val service = ctx.gameService!!


/*******************
 *                 *
 *  Type aliases.  *
 *                 *
 ******************/

typealias TableEntry<R, C, V> = Pair<Pair<R, C>, V>


/**************************************
 *                                    *
 *  [PlayerRights] property aliases.  *
 *                                    *
 *************************************/

val RIGHTS_PLAYER = 0
val RIGHTS_MOD = 1
val RIGHTS_ADMIN = 2


/*******************************************
 *                                         *
 *  [PlayerInteraction] property aliases.  *
 *                                         *
 ******************************************/

/*val INTERACTION_TRADE = PlayerInteraction.TRADE!!
val INTERACTION_CHALLENGE = PlayerInteraction.CHALLENGE!!
val INTERACTION_ATTACK = PlayerInteraction.ATTACK!!
val INTERACTION_FOLLOW = PlayerInteraction.FOLLOW!!
*/

/************************************
 *                                  *
 *  [EntityType] property aliases.  *
 *                                  *
 ***********************************/

/* Aliases for 'EntityType'. */
/*val TYPE_PLAYER = EntityType.PLAYER
val TYPE_NPC = EntityType.NPC
val TYPE_OBJECT = EntityType.OBJECT
val TYPE_ITEM = EntityType.ITEM
*/

/******************************************
 *                                        *
 *  [Skill] identifier property aliases.  *
 *                                        *
 *****************************************/

const val SKILL_ATTACK = SkillSet.ATTACK
const val SKILL_DEFENCE = SkillSet.DEFENSE
const val SKILL_STRENGTH = SkillSet.STRENGTH
const val SKILL_HITPOINTS = SkillSet.HITPOINTS
const val SKILL_RANGED = SkillSet.RANGE
const val SKILL_PRAYER = SkillSet.PRAYER
const val SKILL_MAGIC = SkillSet.MAGIC
const val SKILL_COOKING = SkillSet.COOKING
const val SKILL_WOODCUTTING = SkillSet.WOODCUTTING
const val SKILL_FLETCHING = SkillSet.FLETCHING
const val SKILL_FISHING = SkillSet.FISHING
const val SKILL_FIREMAKING = SkillSet.FIREMAKING
const val SKILL_CRAFTING = SkillSet.CRAFTING
const val SKILL_SMITHING = SkillSet.SMITHING
const val SKILL_MINING = SkillSet.MINING
const val SKILL_HERBLORE = SkillSet.HERBLORE
const val SKILL_AGILITY = SkillSet.AGILITY
const val SKILL_THIEVING = SkillSet.THIEVING

val STATE_IDLE = MobState.Idle
val STATE_EATING = MobState.Eating
val STATE_DRINKING = MobState.Drinking
val STATE_TRADING = MobState.Trading
val STATE_BANKING = MobState.Banking
val STATE_SLEEPING = MobState.Sleeping

/***********************************
 *                                 *
 *  [Skill] extension properties.  *
 *                                 *
 **********************************/

val Player.attack: Skill
	get() = getSkill(SKILL_ATTACK)

val Player.strength: Skill
	get() = getSkill(SKILL_STRENGTH)

val Player.defence: Skill
	get() = getSkill(SKILL_DEFENCE)

val Player.hitpoints: Skill
	get() = getSkill(SKILL_HITPOINTS)

val Player.ranged: Skill
	get() = getSkill(SKILL_RANGED)

val Player.prayer: Skill
	get() = getSkill(SKILL_PRAYER)

val Player.magic: Skill
	get() = getSkill(SKILL_MAGIC)

val Player.cooking: Skill
	get() = getSkill(SKILL_COOKING)

val Player.woodcutting: Skill
	get() = getSkill(SKILL_WOODCUTTING)

val Player.fletching: Skill
	get() = getSkill(SKILL_FLETCHING)

val Player.fishing: Skill
	get() = getSkill(SKILL_FISHING)

val Player.firemaking: Skill
	get() = getSkill(SKILL_FIREMAKING)

val Player.crafting: Skill
	get() = getSkill(SKILL_CRAFTING)

val Player.smithing: Skill
	get() = getSkill(SKILL_SMITHING)

val Player.mining: Skill
	get() = getSkill(SKILL_MINING)

val Player.herblore: Skill
	get() = getSkill(SKILL_HERBLORE)

val Player.agility: Skill
	get() = getSkill(SKILL_AGILITY)

val Player.thieving: Skill
	get() = getSkill(SKILL_THIEVING)

val Player.skillSet: SkillSet
	get() = getSkillSet()

/**********************************
 *                                *
 *  [Rational] property aliases.  *
 *                                *
 *********************************/
val ALWAYS = Rational.ALWAYS
val VERY_COMMON = Rational.VERY_COMMON
val COMMON = Rational.COMMON
val UNCOMMON = Rational.UNCOMMON
val VERY_UNCOMMON = Rational.VERY_UNCOMMON
val RARE = Rational.RARE
val VERY_RARE = Rational.VERY_RARE
val NEVER = Rational(0, 1)
