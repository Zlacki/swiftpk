package api.predef

import com.google.common.collect.HashBasedTable
import com.google.common.collect.ImmutableTable
import com.google.common.collect.Table
import java.util.concurrent.ThreadLocalRandom
import java.util.concurrent.TimeUnit
import java.util.concurrent.Future
import net.swiftpk.server.ent.EntityHandler
import net.swiftpk.server.ent.defs.ItemDef
import net.swiftpk.server.ent.defs.NPCDef
import net.swiftpk.server.ent.defs.GameObjectDef
import net.swiftpk.server.model.terrain.tiles.Point
import net.swiftpk.server.util.StringUtils

/**
 * Returns the current time in [TimeUnit.MILLISECONDS], using [System.nanoTime] for better precision.
 */
fun currentTimeMs() = TimeUnit.MILLISECONDS.convert(System.nanoTime(), TimeUnit.NANOSECONDS)

fun location(x: Int, y: Int): Point = Point.location(x, y, 0)

/**
 * Executes a block of code asynchronously.
 */
inline fun async(crossinline func: () -> Unit): Future<*> =
	service.submit {
		try {
			func()
		} catch (e: Exception) {
			e.printStackTrace()
		}
	}

/**
 * A shortcut to the lazy delegate property. Initializations are **not** thread safe!
 */
fun <T> lazyVal(initializer: () -> T): Lazy<T> = lazy(LazyThreadSafetyMode.NONE, initializer)

/**
 * Computes and returns the [ItemDefinition] for [id].
 */
fun itemDef(id: Int): ItemDef = EntityHandler.getItemDef(id)

fun itemHeals(id: Int): Int = EntityHandler.getItemEdibleHeals(id)

/**
 * Computes and returns the [EquipmentDefinition] for [id].
 */
fun itemAffectedTypes(type: Int): IntArray = EntityHandler.getItemAffectedTypes(type)

/**
 * Computes and returns the [NpcDefinition] for [id].
 */
fun npcDef(id: Int): NPCDef = EntityHandler.getNPCDef(id)

/**
 * Computes and returns the [ObjectDefinition] for [id].
 */
fun objectDef(id: Int): GameObjectDef = EntityHandler.getGameObjectDef(id)

/**
 * Forwards to [StringUtils.addArticle], returns an empty string if [thing] is null.
 */
fun addArticle(thing: Any?): String =
	when (thing) {
		null -> ""
		else -> StringUtils.addArticle(thing)
	}

/**
 * Returns the current [ThreadLocalRandom].
 */
fun rand() = ThreadLocalRandom.current()!!

/**
 * Generates a random integer from [lower] to [upper] inclusive.
 */
fun rand(lower: Int, upper: Int): Int = rand().nextInt((upper - lower) + 1) + lower

/**
 * Generates a random integer from 0 to [upper] inclusive.
 */
fun rand(upper: Int): Int = rand().nextInt(upper + 1)

/**
 * Creates an empty mutable table of [entries].
 */
fun <R, C, V> emptyTable(size: Int = 16): Table<R, C, V> = HashBasedTable.create(size, size)

/**
 * Creates an immutable table of [entries]. The syntax for creating entries is (row [to] column [to] value).
 */
fun <R, C, V> immutableTableOf(vararg entries: TableEntry<R, C, V>): Table<R, C, V> =
	when (entries.size) {
		0 -> ImmutableTable.of()
		1 -> {
			val entry = entries[0]
			val key = entry.first
			ImmutableTable.of(key.first, key.second, entry.second)
		}
		else -> {
			val table = ImmutableTable.builder<R, C, V>()
			for (entry in entries) {
				val key = entry.first
				table.put(key.first, key.second, entry.second)
			}
			table.build()
		}
	}
