package api.predef

import net.swiftpk.server.ent.locs.NPCLoc
import net.swiftpk.server.model.terrain.tiles.Point
import net.swiftpk.server.model.terrain.World
import net.swiftpk.server.model.component.Inventory
import net.swiftpk.server.model.component.SkillSet.Skill
import net.swiftpk.server.tickable.Tickable
import net.swiftpk.server.event.Event
import net.swiftpk.server.model.*
import net.swiftpk.server.model.entity.*
import java.util.*
import net.swiftpk.server.net.ActionSender


/*****************************
 *                           *
 *  [Skill] ext. functions  *
 *                           *
 ****************************/

/*fun Skill.incLevel(amt: Int) {
    if (level < maxLevel) {
        level += amt
        if (level < 0) level = 0
        if (level > maxLevel) level = maxLevel
    }
}*/

fun Skill.decLevel(amt: Int) {
    level -= amt
    if (level < 0) level = 0
}

fun Skill.boostLevel(amt: Int, maxCap: Int = amt + maxLevel) {
    if (level < maxCap) {
        level += amt
        if (level < 0) level = 0
        if (level > maxCap) level = maxCap
    }
}

/*****************************
 *                           *
 *  [Player] ext. functions  *
 *                           *
 ****************************/

fun Player.message(msg: String) = ActionSender.sendMessage(this, msg)
fun Player.sendSound(snd: String) = ActionSender.sendSound(this, snd)
fun Player.sendSkill(idx: Int) = ActionSender.sendStat(this, idx)
fun Player.onFinishedPath(action: () -> Unit) {
    walkToAction = object : WalkToAction(this, location(0, 0), 0) {
        override fun execute() {
            action()
        }
    }
}
fun Player.arrived(dest: Point, radius: Int = 0, action: () -> Unit) {
    walkToAction = object : WalkToAction(this, dest, radius) {
        override fun execute() {
            action()
        }
    }
}
fun Player.canReach(dest: Point): Boolean = PathValidation.checkAdjacent(location, dest)
fun Player.sendInventory() = ActionSender.sendInventory(this)
fun Player.sendEquipmentStats() = ActionSender.sendEquipmentStats(this)
fun Player.sendSleepWord(data: ByteArray) = ActionSender.sendSleepWord(this, data)
fun Player.sendSleepFatigue() = ActionSender.sendSleepFatigue(this)
fun Player.setItemBubble(itemID: Int) = setItemBubble(Bubble(this, itemID))
fun Player.setProjectile(victim: Mob, type: Int) = setProjectile(Projectile(this, victim, type))

fun Inventory.add(id: Int, amt: Int) = add(InvItem(id, amt))

/*****************************
 *                           *
 *  [World] ext. functions   *
 *                           *
 ****************************/

/**
 * Spawns an [Npc].
 */
fun World.addNpc(npc: NPC): NPC {
    npCs.add(npc)
    return npc
}

/**
 * Spawns an [Npc].
 */
fun World.addNpc(id: Int, x: Int, y: Int): NPC {
    val npc = DefaultEntityFactory.getInstance().newNpc(NPCLoc(id, 1, 5000, x + 5, y + 5, x - 5, y - 5, false));
    return addNpc(npc)
}

/**
 * Despawns an [Npc].
 */
fun World.removeNpc(npc: NPC) = getNPCs().remove(npc.getIndex())

/**
 * Spawns a [GameObject].
 */
fun World.addObject(obj: GameObject): GameObject {
    registerGameObject(obj)
    return obj
}

/**
 * Spawns a [GameObject]
 */
fun World.addObject(id: Int, x: Int, y: Int, type: Int, direction: Int): GameObject {
    val obj = GameObject(location(x, y), id, direction, type)
    return addObject(obj)
}

/**
 * Despawns a [GameObject].
 */
fun World.removeObject(obj: GameObject) = unregisterGameObject(obj)

/**
 * Despawns all [GameObject]s on [pos]
 */
fun World.removeObject(pos: Point): Boolean {
    val obj = World.getInstance().getGameObject(pos.x, pos.y)
    if (obj != null) {
        removeObject(obj)
        return true
    }
    return false
}

/**
 * Spawns an [Item].
 */
fun World.addItem(id: Int, amount: Int = 1, x: Int, y: Int) = Item(id, x, y, amount, false, null)

/**
 * Spawns an [Item] for [plr].
 */
fun World.addItem(id: Int, amount: Int = 1, x: Int, y: Int, plr: Player? = null): Item =
        Item(id, x, y, amount, false, plr)

/**
 * Despawns a [GroundItem].
 */
fun World.removeItem(item: Item) = item.remove()

/**
 * Despawns all [GroundItem]s on [pos] that have the ID [id].
 */
fun World.removeItem(pos: Point, id: Int): Boolean {
    var hadItem = false
    for (item in World.getInstance().getArea(pos).getItems()) {
        if(item.id == id) {
            item.remove()
            hadItem = true
        }
    }
    return hadItem
}

/**
 * Schedules a recurring task.
 */
fun World.schedule(delay: Int, action: (Tickable) -> Unit) {
    submit(object : Tickable(delay + 1) {
        override fun execute() {
            action(this)
        }
    })
}

/**
 * Schedules a task that suspends after one execution.
 */
fun World.scheduleOnce(delay: Int, action: (Tickable) -> Unit) {
    submit(object : Tickable(delay + 1) {
        override fun execute() {
            action(this)
            stop()
        }
    })
}

fun World.submitOnce(delay: Long, action: () -> Unit) {
    submit(object : Event(delay) {
        override fun execute() {
            action()
            stop()
        }
    })
}

fun World.submit(delay: Long, action: (Event) -> Unit) {
    submit(object : Event(delay) {
        override fun execute() {
            action(this)
        }
    })
}

/****************************
 *                          *
 *  [Array] ext. functions  *
 *                          *
 ***************************/

/**
 * Randomizes the elements within the array. This function modifies the backing array.
 */
fun <T> Array<T>.shuffle() {
    var i = size - 1
    while (i > 0) {
        val index = rand(i)
        val obj = this[index]
        this[index] = this[i]
        this[i] = obj
        i--
    }
}

/**********************************
 *                                *
 *  [OptionalInt] ext. functions  *
 *                                *
 *********************************/

/**
 * Adds a map function to OptionalInt.
 */
fun <T> OptionalInt.map(mapper: (Int) -> T): Optional<T> {
    if (isPresent) {
        val newValue = mapper(asInt)
        return Optional.of(newValue)
    }
    return Optional.empty()
}

/**
 * Adds a mapToInt function to OptionalInt.
 */
fun OptionalInt.mapToInt(mapper: (Int) -> Int): OptionalInt {
    if (isPresent) {
        val newValue = mapper(asInt)
        return OptionalInt.of(newValue)
    }
    return OptionalInt.empty()
}
