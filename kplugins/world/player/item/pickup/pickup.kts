import api.predef.*
import net.swiftpk.server.kplugin.event.impl.LootableItemClickEvent
import net.swiftpk.server.model.InvItem

on(LootableItemClickEvent::class) {
    val distance = if(plr.canReach(item.location)) 0 else 1
    plr.arrived(item.location, distance) {
        plr.resetAll()
        if(!item.visibleTo(plr) || item.isRemoved)
            return@arrived
        item.remove()
        plr.sendSound("takeobject")
        plr.inventory.add(InvItem(item.id, item.amount))
    }
}