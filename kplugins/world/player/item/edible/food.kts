import api.predef.*
import net.swiftpk.server.kplugin.event.impl.InventoryClickEvent

/**
 * Handles all edible food
 */
on(InventoryClickEvent::class)
	.filter { item.isEdible() }
	.then {
		plr.state = STATE_EATING
		plr.inventory.remove(item)
		plr.sendInventory()
		plr.sendSound("eat")
		if(item.id == 18 || item.id == 228)
			plr.message("You eat the ${item.def.name}.  Yuck!")
		else
			plr.message("You eat the ${item.def.name}.")
		world.scheduleOnce(1) {
			if(plr.hitpoints.level < plr.hitpoints.maxLevel) {
				plr.hitpoints.incLevel(itemHeals(item.id))
				plr.message("It heals some health.")
				plr.sendSkill(SKILL_HITPOINTS)
			}
			plr.state = STATE_IDLE
		}
	}
