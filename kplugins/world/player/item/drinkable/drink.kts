import api.predef.*

import net.swiftpk.server.kplugin.event.impl.InventoryClickEvent

import world.player.item.drinkable.Potion

/**
 * Handles all potions
 */
on(InventoryClickEvent::class)
        .filter { item.def.command.toLowerCase().equals("drink") }
        .then {
            val potion = Potion.DOSE_TO_POTION[item.id] ?: return@then
            plr.state = STATE_DRINKING
            plr.setItemBubble(item.id)
            plr.message("You drink the ${item.def.name}.")
            val dosesLeft = potion.getDosesLeft(item.id)
            val nextID = potion.getNextDose(item.id)
            plr.inventory.remove(item)
            when (nextID) {
                null -> plr.inventory.add(465, 1)
                else -> plr.inventory.add(nextID, 1)
            }
            plr.sendInventory()
            world.scheduleOnce(1) {
                when (dosesLeft) {
                    0 -> plr.message("You have finished your potion.")
                    1 -> plr.message("You have 1 dose left.")
                    else -> plr.message("You have $dosesLeft doses left.")
                }
                potion.effect(plr)
                plr.state = STATE_IDLE
            }
        }
