package world.player.item.drinkable

import api.predef.*
import net.swiftpk.server.model.Player
import net.swiftpk.server.sync.block.SynchronizationBlock

/**
 * An enum representing potions that can be consumed.
 */
enum class Potion(val fourDose: Int,
                  val threeDose: Int,
                  val twoDose: Int,
                  val oneDose: Int,
                  val effect: (Player) -> Unit) {
    // TODO All potions that remove HP should inflict damage instead.
    // TODO this still needs to be tested, and some things implemented
    ZAMORAK_BREW(fourDose = -1,
            threeDose = 963,
            twoDose = 964,
            oneDose = 965,
            effect = { it.onZamorakBrew() }),
    FISHING_POTION(fourDose = -1,
            threeDose = 489,
            twoDose = 490,
            oneDose = 491,
            effect = { it.onSkillPotion(SKILL_FISHING) }),
    RANGING_POTION(fourDose = -1,
            threeDose = 498,
            twoDose = 499,
            oneDose = 500,
            effect = { it.onCombatPotion(SKILL_RANGED) }),
    DEFENCE_POTION(fourDose = -1,
            threeDose = 480,
            twoDose = 481,
            oneDose = 482,
            effect = { it.onCombatPotion(SKILL_DEFENCE) }),
    STRENGTH_POTION(fourDose = 221,
            threeDose = 222,
            twoDose = 223,
            oneDose = 224,
            effect = { it.onCombatPotion(SKILL_STRENGTH) }),
    ATTACK_POTION(fourDose = -1,
            threeDose = 474,
            twoDose = 475,
            oneDose = 476,
            effect = { it.onCombatPotion(SKILL_ATTACK) }),
    SUPER_DEFENCE(fourDose = -1,
            threeDose = 495,
            twoDose = 496,
            oneDose = 497,
            effect = { it.onCombatPotion(SKILL_DEFENCE, true) }),
    SUPER_ATTACK(fourDose = -1,
            threeDose = 486,
            twoDose = 487,
            oneDose = 488,
            effect = { it.onCombatPotion(SKILL_ATTACK, true) }),
    SUPER_STRENGTH(fourDose = -1,
            threeDose = 492,
            twoDose = 493,
            oneDose = 494,
            effect = { it.onCombatPotion(SKILL_STRENGTH, true) }),
    ANTIPOISON_POTION(fourDose = -1,
            threeDose = 569,
            twoDose = 570,
            oneDose = 571,
            effect = { it.onAntipoison() }),
    ANTIDOTE_PLUS(fourDose = -1,
            threeDose = 566,
            twoDose = 567,
            oneDose = 568,
            effect = { it.onAntipoison(1000) }),
    PRAYER_POTION(fourDose = -1,
            threeDose = 483,
            twoDose = 484,
            oneDose = 485,
            effect = { it.onPrayerPotion() }),
    STAT_RESTORE(fourDose = -1,
            threeDose = 477,
            twoDose = 478,
            oneDose = 479,
            effect = { it.onRestorePotion() });

    companion object {

        /**
         * Mappings of dose identifiers to [Potion] instances.
         */
        val DOSE_TO_POTION = HashMap<Int, Potion>().apply {
            for (potion in Potion.values()) {
                this[potion.fourDose] = potion
                this[potion.threeDose] = potion
                this[potion.twoDose] = potion
                this[potion.oneDose] = potion
            }
        }
    }

    /**
     * Computes and returns the next potion dose identifier.
     */
    fun getNextDose(current: Int) =
            when (current) {
                fourDose -> threeDose
                threeDose -> twoDose
                twoDose -> oneDose
                else -> null
            }

    /**
     * Computes and returns the remaining dose amount.
     */
    fun getDosesLeft(current: Int) =
            when (current) {
                fourDose -> 3
                threeDose -> 2
                twoDose -> 1
                oneDose -> 0
                else -> throw IllegalArgumentException("Invalid dose identifier <$current>.")
            }
}

/**
 * Invoked when a Zamorak brew is sipped.
 */
private fun Player.onZamorakBrew() {
    fun getBoost(level: Int, mod: Double, base: Int = 0) = base + (mod * level).toInt()
    attack.boostLevel(getBoost(attack.maxLevel, 0.225))
    defence.decLevel(getBoost(defence.maxLevel, 0.125))
    strength.boostLevel(getBoost(strength.maxLevel, 0.15))
    val damage = getBoost(hitpoints.maxLevel, 0.1)
    hitpoints.decLevel(damage)
    blockSet.add(SynchronizationBlock.createHitUpdateBlock(damage, hitpoints.level, hitpoints.maxLevel, index))
    sendSkill(SKILL_ATTACK)
    sendSkill(SKILL_DEFENCE)
    sendSkill(SKILL_STRENGTH)
    sendSkill(SKILL_HITPOINTS)
}

/**
 * Invoked when a potion with anti-poisoning properties is sipped.
 */
private fun Player.onAntipoison(immunityDuration: Long = 0L) {
    world.submitOnce(immunityDuration) {
        message("Not implemented.")
    }
}

/**
 * Invoked when a prayer potion is sipped.
 */
private fun Player.onPrayerPotion() {
    prayer.incLevel(21)
    sendSkill(SKILL_PRAYER)
}

/**
 * Invoked when a non-combat skill potion is sipped.
 */
private fun Player.onSkillPotion(skillID: Int) {
    skillSet.get(skillID).boostLevel(3)
    sendSkill(skillID)
}

/**
 * Invoked when a restore potion is sipped.
 */
private fun Player.onRestorePotion() {
    fun getBoost(level: Int) = (level * 0.3).toInt() + 10
    if (attack.level < attack.maxLevel) {
        attack.incLevel(getBoost(attack.maxLevel))
        sendSkill(SKILL_ATTACK)
    }
    if (defence.level < defence.maxLevel) {
        defence.incLevel(getBoost(defence.maxLevel))
        sendSkill(SKILL_DEFENCE)
    }
    if (strength.level < strength.maxLevel) {
        strength.incLevel(getBoost(strength.maxLevel))
        sendSkill(SKILL_STRENGTH)
    }
}

/**
 * Invoked when an anti-fire potion is sipped.
 */
private fun Player.onAntifirePotion() {
}

/**
 * Invoked when a combat skill potion is sipped.
 */
private fun Player.onCombatPotion(skillID: Int, superPotion: Boolean = false) {
    fun getBoost(level: Int) = if (superPotion) 5 + (0.15 * level).toInt() else 3 + (0.10 * level).toInt()

    val skill = skillSet.get(skillID)
    skill.boostLevel(getBoost(skill.maxLevel))

    sendSkill(skillID)
}
