import api.predef.*
import net.swiftpk.server.kplugin.event.impl.InventoryClickEvent.InventoryDropEvent
import net.swiftpk.server.model.Item

/**
 * Handles all edible food
 */
on(InventoryDropEvent::class) {
    plr.onFinishedPath {
        plr.resetAll()
        if (plr.state != STATE_IDLE || !plr.inventory.contains(item))
            return@onFinishedPath
        if (item.isWielded) {
            plr.run {
                sendSound("click")
                sendEquipmentStats()
                unWieldItem(item)
            }
        }
        plr.sendSound("dropobject")
        plr.inventory.remove(item)
        plr.sendInventory()
        Item(item.id, plr.x, plr.y, item.amount, true, plr)
    }
}
