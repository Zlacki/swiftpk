import api.predef.*
import net.swiftpk.server.kplugin.event.impl.InventoryClickEvent
import net.swiftpk.server.util.Captcha

/**
 * Handles all edible food
 */
on(InventoryClickEvent::class)
        .filter { item.id == 1263 }
        .then {
            plr.state = STATE_SLEEPING
            plr.sleepFatigue = plr.fatigue
            plr.setItemBubble(1263)
            world.schedule(2) {
                if (plr.state != STATE_SLEEPING) {
                    it.stop()
                    return@schedule
                }
                // TODO: Is this right?  Maybe 10000?
                var sleepFatigue = plr.sleepFatigue - 5000
                if (sleepFatigue <= 0) {
                    sleepFatigue = 0
                    it.stop()
                }
                plr.sleepFatigue = sleepFatigue
                plr.sendSleepFatigue()
            }
            plr.run {
                sendSleepWord(Captcha.generateCaptcha(plr))
                message("You rest in the sleeping bag.")
            }
        }
