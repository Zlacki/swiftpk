import api.predef.*

cmd("goto", RIGHTS_MOD) {
    if (args.isEmpty()) {
        plr.message("Invalid arguments.  Syntax: /$name <name>")
        return@cmd
    }
    var name = ""
    for (arg in args) {
        name += "$arg "
    }

    val target = world.getPlayer(name)
    if (target == null) {
        plr.message("Could not find player: $name in active World")
        return@cmd
    }
    plr.resetPath()
    plr.setLocation(target.location, true)
}

cmd("summon", RIGHTS_MOD) {
    if (args.isEmpty()) {
        plr.message("Invalid arguments.  Syntax: /$name <name>")
        return@cmd
    }
    var name = ""
    for (arg in args) {
        name += "$arg "
    }

    val target = world.getPlayer(name)
    if (target == null) {
        plr.message("Could not find player: $name in active World")
        return@cmd
    }
    target.resetPath()
    target.setLocation(plr.location, true)
}

cmd("tele", RIGHTS_MOD) {
    if (args.size < 2) {
        plr.message("Invalid arguments.  Syntax: /$name <x> <y>")
    } else {
        val x = args[0].toInt()
        val y = args[1].toInt()
        plr.resetPath()
        plr.setLocation(location(x, y), true)
    }
}
