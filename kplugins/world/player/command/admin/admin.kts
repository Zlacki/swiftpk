import api.predef.*

cmd("item", RIGHTS_ADMIN) {
    if (args.isEmpty()) {
        plr.message("Invalid arguments.  Syntax: /$name <itemID> [amount]")
        return@cmd
    }
    if (plr.inventory.isFull) {
        plr.message("You don't have room in your inventory to create that item.")
        return@cmd
    }
    val id = args[0].toInt()
    var amount = 1
    if (args.size > 1)
        amount = args[1].toInt()
    if (amount > 1 && !itemDef(id).isStackable) {
        for (i in 1..amount) {
            plr.inventory.add(id, 1)
            if (plr.inventory.isFull)
                break;
        }
    } else
        plr.inventory.add(id, amount)
}
