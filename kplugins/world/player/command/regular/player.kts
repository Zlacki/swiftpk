import api.predef.*

/**
 * A command that tell how many players are currently online.
 */
cmd("online", RIGHTS_PLAYER) {
    plr.message("Players online: @gre@${world.players.size}")
}
