#!/bin/bash
classpath="bin"
for dep in `ls lib |grep jar`;
do
  classpath="lib/$dep;$classpath"
done
echo "java -cp $classpath net.swiftpk.server.Server"
java -cp $classpath net.swiftpk.server.Server
exit
